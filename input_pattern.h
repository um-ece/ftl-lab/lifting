// input_pattern.h: interface for the input_pattern class.
//
//
//
// Author : Alberto BOSIO	
//
// Last Update : 13-11-2006
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INPUT_PATTERN_H__FFF81DCB_034B_48BC_B525_E404BEB99E32__INCLUDED_)
#define AFX_INPUT_PATTERN_H__FFF81DCB_034B_48BC_B525_E404BEB99E32__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

// constant definition

const int six_values_computation[2][2] = {0,3, 2,1};

typedef struct pi_set {

	vector<char*> pi_value;

} pi_set_t;

class input_pattern  // this class must be read from file the list of pattern, it represents one pattern, it can be composed by one or more Primary Inputs (PI) assign
		     // in case of combinational circuit one pattern correspond to one PI assign
		     // in case of seq circuit one pattern is composed by at least one  PI assign
			
{
public:

	int n_pi; // number of primary inputs
	vector<pi_set_t> input_pattern_list; // the list of PI assigned, pi_set is the PI

	input_pattern();
	virtual ~input_pattern();


	int read_from_file(char* file_name);
	void print(ostream & os);

};






class six_value_input_pattern  // 
{
public:

	input_pattern boolean_values_input;

	vector<int> six_ipl; // six value algebra input pattern list
	
	
	six_value_input_pattern() {};
	virtual ~six_value_input_pattern() {};


	int read_from_file (char* file_name);
	int compute_algebra_computation (int index); // it computes the 6 algebra computation from input vector index and index-1 

	void print ();

};


#endif // !defined(AFX_INPUT_PATTERN_H__FFF81DCB_034B_48BC_B525_E404BEB99E32__INCLUDED_)
