// input_pattern.cpp: implementation of the input_pattern class.
//
//////////////////////////////////////////////////////////////////////

#include "input_pattern.h"
#include <stdlib.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

input_pattern::input_pattern()
{



	input_pattern_list.clear ();
}

input_pattern::~input_pattern()

{

	/* To Do
	if (!input_pattern_list.empty ()) {
		for (int i=0; i < input_pattern_list.size (); i++)
			input_pattern_list[i].pi_value;

	}
	*/
}


int input_pattern::read_from_file(char* file_name) {

	int i, tp;
	ifstream infile(file_name);
	string line;
	char* my_pi = NULL;
	pi_set_t my_pi_set;

	i = 0;
	tp = 0;
	if (infile.is_open ()) {

		while(getline(infile, line)) {
			
			if (line.find ("TP") != string::npos) { // it begins a new test pattern
			
				if (my_pi != NULL) {
					input_pattern_list.push_back (my_pi_set);

					my_pi_set.pi_value.clear ();
				}

				

			}
			else if (line.find ("PI") != string::npos) { // it is a primary input value
				
				my_pi = new char[n_pi+1] ; // it creates the prumary inputs
				if (my_pi == NULL) {
					cerr << " Memory error in input_pattern::read_from_file(char* file_name)  when creating new pi " << endl;
					exit (-1);

				}
				for (i = 3; i < line.length (); i++) // copy the value
					my_pi[i-3] = line[i];

				my_pi[i-3] = '\0';

				my_pi_set.pi_value.push_back (my_pi);
			}


		}
		input_pattern_list.push_back (my_pi_set);
			

	}
	else {
		cerr << " cannot open pattern file " << file_name << endl;
		return 1; // error, file cannot be open

	}
	
	return 0; // no error

}

void input_pattern::print(ostream & os) {

	int i,k,j;


	if (!input_pattern_list.empty ()) {
		k = input_pattern_list.size ();

		os << " input pattern list " << endl << endl;
		for (i = 0; i < k; i++) {
			
			os << " pattern " << i << endl << endl;

			for (j = 0; j < input_pattern_list[i].pi_value.size (); j++)
				os << input_pattern_list[i].pi_value[j] << endl;

			os << endl;



		}


		
	}
}


/*

// six_value_input_pattern lethods implementation

int six_value_input_pattern::read_from_file (char* file_name) {

	return boolean_values_input.read_from_file (file_name);
	

}

void six_value_input_pattern::print () {


	int k,i;

	cout << "test pattern list from file : " << endl;

	boolean_values_input.print ();
	
	if (!six_ipl.empty ()) {
		k = six_ipl.size ();

		for (i = 0; i < k; i++) 
			cout << algebra_values[six_ipl[i]] << endl;

	}
}


int six_value_input_pattern::compute_algebra_computation (int index) {

	int i,k, r,c;

//	cout << "  cmp algebra " << index << endl;

	if ((!boolean_values_input.input_pattern_list.empty ()) && (index > 0 && index < boolean_values_input.input_pattern_list.size())) {
		
		k = boolean_values_input.input_pattern_list[index].size(); // dimension of the vector pattern i.e. how many inputs

		six_ipl.clear ();
		//cout << "index = " << index << endl;

		for (i = 0; i < k; i++) {
			c = boolean_values_input.input_pattern_list[index][i] - '0';
			r = boolean_values_input.input_pattern_list[index-1][i] - '0';

			//cout << " c = " << c << "  r = " << r << endl;

			//cout << " 6 value = " << six_values_computation[r][c] << endl;
			
			if (c == 1) {
				if (r == 1) {
					six_ipl.push_back (1); // 11 :   C1
				}
				else {
					six_ipl.push_back (3); // 01 : R1
				}
			}
			else { // c == 0
				if (r == 1) {
					six_ipl.push_back (2); // 10 :   F0
				}
				else {
					six_ipl.push_back (0); // 00 : C0
				}		

			}

			// six_ipl.push_back (six_values_computation[r][c]);

		}

	}
	else {
		
		if (index == 0)	{

			// cout << " Eccomi " << endl;

			k = boolean_values_input.input_pattern_list[index].size(); // dimension of the vector pattern i.e. how many inputs

			six_ipl.clear ();
			for (i = 0; i < k; i++) {  // only c0 or c1, since the first input pattern fails
				c = boolean_values_input.input_pattern_list[index][i] - '0';
				r = c;

				six_ipl.push_back (six_values_computation[r][c]);

			}
		
		}
		else
			return 1; // error
	}

	return 0; //no error
}

*/
