#include "generic_cell.h"
#include "stuckat_fault.h"
#include "simulation_event.h"
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <queue>

using namespace std;


generic_cell* top_level;

int** input_patterns;
int** output_patterns;
int** golden_output_patterns;
int input_patterns_number;

int detected_faults;

int bitflip_enable=0;
int bitflip_time;
generic_ff* bitflip_location;

char *faultlist_coverage;
int faultlist_number;
int use_external_faultlist;

stuckat_fault** stuckat_faultlist;


char param_netlist_filename[1024];
char param_input_filename[1024];
char param_output_filename[1024];

int param_fault_simulation;
//int param_force_sequential_algorithm;
int param_generate_full_fault_dictionary;
int param_write_fault_statistic;
char param_fault_statistic_filename[1024];
int param_write_truth_fault_table;
FILE *param_truth_fault_table_file;
int param_bitflip_mintime;
int param_bitflip_experiments;
int param_saf_observe_last_input_only;
char param_faultlist_filename[1024];
int param_print_fs_result;
char param_print_fs_result_dir[1024];
map<string, int> external_faultlist;
int param_library;
int param_use_errorDetection;
char constant_fault_statistic[1000];
int param_write_fault_list=0;
char param_fault_list_filename[1024];
char param_timing_filename[1000];
char param_def_filename[1000];
int timing_type;
int timing_factor;
int set_input_no;
int set_clock_no;
int clock_no;
//char set_input_name[1024];
int end_time;
int clock_flag=0;
int set_input_exist;
#ifdef __SLACK_CALCULATION__
int param_slack_time_calculation=1;
#endif
#ifdef __PROBA_CALCULATION__
int param_proba_calculation=1;
int param_proba_calculation_max_ttsize=16;
int max_logic_level_toSimulate=50000;
#ifdef __DEBUG_PROBA_1__
long double proba[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
long double proba_moins1=0;
long double proba_moins01=0;
long double proba_moins001=0;
#endif
#endif
#ifdef __LOGIC_LEVELS__
int max_levels;
#endif

int faultCombinations_number;
int max_faultCombinations;
vector<int> *faultCombinations;
vector<int> *faultBegin;
vector<int> *faultEnd;
//---------------------23/11/2011-------------------LU
/*struct fault_list_with_time
{
	vector <int> faultList;
    vector <int> faultBegin;
	vector <int> faultEnd;
}; 
vector <fault_list_with_time> faultCombinations;
//----------------------end of 23/11/2011-------------
*/

// This variable is used for double stuck-at fault simulation
string fault_to_exclude;

// AB 
int param_passfail_dictionary; //default 1, the created dictionary is passfail. if 0 then the full dictionary is created


//PB
char memory_name[30][1024];
char memory_file[30][1024];
int memory_number=0;
//_PB
//



#ifdef __TIMING_SIMULATION__
// VARIABLES FOR TIMING SIMULATION
char fault_output_filename[1024];
char waveform_output_filename[1024];
char laser_fault_filename[1024];
char w_laser_fault_filename[1024];
char hspice_libname[1024];
int flag_hspice_lib =0;
char hspice_output_filename[1024];
priority_queue<simulation_event> timing_queue;
int current_time;
//int gate_number=0;
vector <string> list_subgate;
vector <string> temp_list_subgate;
vector <string> list_input_netname;
vector <string> list_waveform_netname;
vector <string> netlist_cap_name;
vector <float> netlist_cap_value;
vector <string> netlist_cap_netname;
int subgate_number;
int fault_start;
int fault_end;
int Hspice_wholetime_sim_flag = 0;
int flag_force_sim_time =0;
FILE* output_hspice_file;
FILE* output_laser_list;
char tcl_command[1024];
int flag_hspice=0;
int flag_wf=0;
int flag_wf_all=0;
int flag_laser_fault=0,flag_hspice_output=0;
int simulation_start=0, simulation_end =0;
int no_nett=0;
int hspice_level=1;
int flag_read_def=0;
int flag_write_laser_fault=0;
int laser_center[2];
int laser_radius;
int laser_start;
int laser_end;
float laser_power;
#endif


