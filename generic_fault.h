//////////////////////////////////////////////////////////////////////
// generic_fault.h: interface for the generic_fault class.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 03-11-2009
//////////////////////////////////////////////////////////////////////
#ifndef __generic_fault
#define __generic_fault

#include <iostream>
#include <vector>
#include "generic_gate.h"
#include "generic_ff.h"
#include "techlib/common/pi.h"
#include "techlib/common/po.h"
#include <stdio.h>

using namespace std;

/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = -1
 * Z = -2
 * ...
*/

class generic_fault {

public:

	// INFORMATION
	char name[255]; // fault name
	char name_forHTML_File[1024];
	char name_forReport_File[1024];
	char faultType[255]; // fault type (e.g., SA)
	char portName[1024];

	generic_gate* g;
	int port;
	int direction;
	int detected;

	generic_fault (generic_gate *g, int port, int direction);
	virtual ~generic_fault () {};

	// *******
	//
	// methods
	
	virtual void injectFault()=0;
	virtual void releaseFault()=0;

        void print (ostream &os) ;
};


#endif

