





#ifndef __COMB_H
#define __COMB_H

//////////////////////////////////////////////////////////////////////
// comb.h: interface for the generic combinational logic.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-07-2007
//////////////////////////////////////////////////////////////////////

#include "../../generic_gate.h"

using namespace std;

#define _AND_GATE	1
#define _OR_GATE	2
#define _NOT_GATE	3
#define _XOR_GATE	4
#define _NAND_GATE	5
#define _NOR_GATE	6
#define _XNOR_GATE	7
#define _BUFFER_GATE	8
#define _MUX_GATE	9  // added the 11-07-2007


const int AND_TRUTH_TABLE[4][4] = { -1,-1,0,-1,    -1,-1,0,-1,     0,0,0,0,    -1,-1,0,1 };
const int OR_TRUTH_TABLE[4][4] = { -1,-1,-1,1,    -1,-1,-1,1,     -1,-1,0,1,    1,1,1,1 };
const int XOR_TRUTH_TABLE[4][4] = { -1,-1,-1,-1,    -1,-1,-1,-1,     -1,-1,0,1,    -1,-1,1,0 };
const int NOT_TRUTH_TABLE[4] = { -1,-1,1,0 };
/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = -1
 * Z = -2
 * ...
*/

class andN : public generic_gate {
public:
	andN (const  char* name, int n);
	void calculate_output() ;
};

class orN : public generic_gate {
public:
	orN (const char* name, int n); 

	void calculate_output() ;
};

class xorN : public generic_gate {
public:
	xorN (const char* name, int n) ;

	void calculate_output() ;
};

class buffer : public generic_gate {
public:
	buffer (const  char* name) ;

	void calculate_output() ;
};

class inverter : public generic_gate {
public:
	inverter (const char* name) ;

	void calculate_output() ;
};

class nandN : public generic_gate {
public:
	nandN (const char* name, int n) ;

	void calculate_output() ;
};

class norN : public generic_gate {
public:
	norN (const char* name, int n) ;
	void calculate_output() ;
};

class xnorN : public generic_gate {
public:
	xnorN (const char* name, int n) ;

	void calculate_output() ;
};

// added the 11-07-2007
//
class Mux : public generic_gate {
public:
	Mux (const char* name) ;

	void calculate_output() ;
};


#endif

