#ifndef __SEQ_H
#define __SEQ_H


//////////////////////////////////////////////////////////////////////
// comb.h: interface for the generic combinational logic.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-06-2007
//////////////////////////////////////////////////////////////////////

#include "../../generic_gate.h"
#include "../../generic_ff.h"

using namespace std;

#define _DFF_GATE	100

const int DFF_TRUTH_TABLE[4] = { -1,-1,0,1 };
const int DFF_BITFLIP_TRUTH_TABLE[4] = { -1,-1,0,1 };
/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = -1
 * Z = -2
 * ...
*/

class dff : public generic_ff {
public:
	int isScan;

	dff (const char* name, int isScan=0) : generic_ff(name) {
		inputs_number=3; // 0=D, 1=Reset, 2=Set
		type=_DFF_GATE;
		this->isScan=isScan;
	};

	void calculate_output() {
		if (input_value[1] == 1) {
			temp_output_value = 0;
		} else if (input_value[2] == 1) {
			temp_output_value = 1;
		} else {
			//printf ("--Input changed on FF %s: %d (not propagated)\n", name, input_value[0]);
		}
	};

	void pulse_clock() {
		// Clock pulse allowed only if set and reset are 0
		if (input_value[1]==0 && input_value[2]==0 && stuck_at_output==0) {
			output_value = DFF_TRUTH_TABLE[input_value[0]+2];
			temp_output_value=output_value;
			//printf ("--Clock cycle at FF %s. New value=%d (not propagates)\n", name, output_value);
		}
	};

	void load_FF(int value) {
		if (stuck_at_output==0) {
			temp_output_value=value;
			output_value=value;
			input_value[0]=value;
		}
	};

	void bit_flip_FF() {
		temp_output_value=DFF_BITFLIP_TRUTH_TABLE[temp_output_value+2];
		output_value=temp_output_value;
		propagate_output();
	};
};



#endif
