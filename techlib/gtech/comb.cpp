
//////////////////////////////////////////////////////////////////////
// comb.cpp: implementation of class comb.h 
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-07-2007
//////////////////////////////////////////////////////////////////////

#include "comb.h"

using namespace std;

	andN::andN (const  char* name, int n) : generic_gate(name) {
		inputs_number=n;
		type=_AND_GATE;
	};

	void andN::calculate_output() {
		int i;
		temp_output_value=1;
		for (i=0; i<inputs_number; i++) {
			temp_output_value = AND_TRUTH_TABLE[temp_output_value+2][input_value[i]+2];
		}
	};



	orN::orN (const  char* name, int n) : generic_gate(name) {
		inputs_number=n;
		type=_OR_GATE;
	};

	void orN::calculate_output() {
		int i;
		temp_output_value=0;
		for (i=0; i<inputs_number; i++) {
			temp_output_value = OR_TRUTH_TABLE[temp_output_value+2][input_value[i]+2];
		}
	};



	xorN::xorN (const char* name, int n) : generic_gate(name) {
		inputs_number=n;
		type=_XOR_GATE;
	};

	void xorN::calculate_output() {
		int i;
		temp_output_value=0;
		for (i=0; i<inputs_number; i++) {
			temp_output_value = XOR_TRUTH_TABLE[temp_output_value+2][input_value[i]+2];
		}
	};

	buffer::buffer (const  char* name) : generic_gate(name) {
		inputs_number=1;
		type=_BUFFER_GATE;
	};

	void buffer::calculate_output() {
		temp_output_value = input_value[0];
	};

	inverter::inverter (const char* name) : generic_gate(name) {
		inputs_number=1;
		type=_NOT_GATE;
	};

	void inverter::calculate_output() {
		temp_output_value = NOT_TRUTH_TABLE[input_value[0]+2];
	};

	nandN::nandN (const  char* name, int n) : generic_gate(name) {
		inputs_number=n;
		type=_NAND_GATE;
	};

	void nandN::calculate_output() {
		int i;
		temp_output_value=1;
		for (i=0; i<inputs_number; i++) {
			temp_output_value = AND_TRUTH_TABLE[temp_output_value+2][input_value[i]+2];
		}
		temp_output_value = NOT_TRUTH_TABLE[temp_output_value+2];
	};

	norN::norN (const char* name, int n) : generic_gate(name) {
		inputs_number=n;
		type=_NOR_GATE;
	};

	void norN::calculate_output() {
		int i;
		temp_output_value=0;
		for (i=0; i<inputs_number; i++) {
			temp_output_value = OR_TRUTH_TABLE[temp_output_value+2][input_value[i]+2];
		}
		temp_output_value = NOT_TRUTH_TABLE[temp_output_value+2];
	};

	xnorN::xnorN (const char* name, int n) : generic_gate(name) {
		inputs_number=n;
		type=_XNOR_GATE;
	};

	void xnorN::calculate_output() {
		int i;
		temp_output_value=0;
		for (i=0; i<inputs_number; i++) {
			temp_output_value = XOR_TRUTH_TABLE[temp_output_value+2][input_value[i]+2];
		}
		temp_output_value = NOT_TRUTH_TABLE[temp_output_value+2];
	};


// added the 11-07-2007
//
	Mux::Mux (const char* name) : generic_gate (name) {

		inputs_number=3;
		type=_MUX_GATE;
		
	}

/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = -1
 * Z = -2
 * ...
*/


	void Mux::calculate_output() {
		// iunputs : S ; IO ; I1  
		// 	   input_value[0] = S
		//	   input_value[1] = I0
		//	   input_value[2] = I1
	
		// tetramax _MUX standard library
		if ((input_value[0] == -2) || (input_value[0] == -1))  {
			
			if (( input_value[1] ==  input_value[2]))
				temp_output_value = input_value[1] ;
			else
				temp_output_value  = -1;
		}
		else {
			if (input_value[0] == 0)
				temp_output_value  =  input_value[1] ;
			else
				temp_output_value  =  input_value[2] ;
		}

	}



