#ifndef __C35_COMB_H
#define __C35_COMB_H
#include "../../generic_gate.h"
#include "../common/truthtables.h"
#include "c35_truthtables.h"
using namespace std;
class c35_ADD21 : public generic_gate {
public:
	c35_ADD21 (const char* name);
	void calculate_output(int o) ;
};

class c35_ADD22 : public generic_gate {
public:
	c35_ADD22 (const char* name);
	void calculate_output(int o) ;
};

class c35_ADD31 : public generic_gate {
public:
	c35_ADD31 (const char* name);
	void calculate_output(int o) ;
};

class c35_ADD32 : public generic_gate {
public:
	c35_ADD32 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI210 : public generic_gate {
public:
	c35_AOI210 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI211 : public generic_gate {
public:
	c35_AOI211 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI2110 : public generic_gate {
public:
	c35_AOI2110 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI2111 : public generic_gate {
public:
	c35_AOI2111 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI2112 : public generic_gate {
public:
	c35_AOI2112 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI212 : public generic_gate {
public:
	c35_AOI212 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI220 : public generic_gate {
public:
	c35_AOI220 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI221 : public generic_gate {
public:
	c35_AOI221 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI222 : public generic_gate {
public:
	c35_AOI222 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI310 : public generic_gate {
public:
	c35_AOI310 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI311 : public generic_gate {
public:
	c35_AOI311 (const char* name);
	void calculate_output(int o) ;
};

class c35_AOI312 : public generic_gate {
public:
	c35_AOI312 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUF12 : public generic_gate {
public:
	c35_BUF12 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUF15 : public generic_gate {
public:
	c35_BUF15 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUF2 : public generic_gate {
public:
	c35_BUF2 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUF4 : public generic_gate {
public:
	c35_BUF4 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUF6 : public generic_gate {
public:
	c35_BUF6 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUF8 : public generic_gate {
public:
	c35_BUF8 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFE10 : public generic_gate {
public:
	c35_BUFE10 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFE12 : public generic_gate {
public:
	c35_BUFE12 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFE15 : public generic_gate {
public:
	c35_BUFE15 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFE2 : public generic_gate {
public:
	c35_BUFE2 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFE4 : public generic_gate {
public:
	c35_BUFE4 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFE6 : public generic_gate {
public:
	c35_BUFE6 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFE8 : public generic_gate {
public:
	c35_BUFE8 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFT10 : public generic_gate {
public:
	c35_BUFT10 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFT12 : public generic_gate {
public:
	c35_BUFT12 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFT15 : public generic_gate {
public:
	c35_BUFT15 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFT2 : public generic_gate {
public:
	c35_BUFT2 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFT4 : public generic_gate {
public:
	c35_BUFT4 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFT6 : public generic_gate {
public:
	c35_BUFT6 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUFT8 : public generic_gate {
public:
	c35_BUFT8 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKBU12 : public generic_gate {
public:
	c35_CLKBU12 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKBU15 : public generic_gate {
public:
	c35_CLKBU15 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKBU2 : public generic_gate {
public:
	c35_CLKBU2 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKBU4 : public generic_gate {
public:
	c35_CLKBU4 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKBU6 : public generic_gate {
public:
	c35_CLKBU6 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKBU8 : public generic_gate {
public:
	c35_CLKBU8 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN0 : public generic_gate {
public:
	c35_CLKIN0 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN1 : public generic_gate {
public:
	c35_CLKIN1 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN10 : public generic_gate {
public:
	c35_CLKIN10 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN12 : public generic_gate {
public:
	c35_CLKIN12 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN15 : public generic_gate {
public:
	c35_CLKIN15 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN2 : public generic_gate {
public:
	c35_CLKIN2 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN3 : public generic_gate {
public:
	c35_CLKIN3 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN4 : public generic_gate {
public:
	c35_CLKIN4 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN6 : public generic_gate {
public:
	c35_CLKIN6 (const char* name);
	void calculate_output(int o) ;
};

class c35_CLKIN8 : public generic_gate {
public:
	c35_CLKIN8 (const char* name);
	void calculate_output(int o) ;
};

class c35_DF1 : public generic_gate {
public:
	c35_DF1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DF3 : public generic_gate {
public:
	c35_DF3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFC1 : public generic_gate {
public:
	c35_DFC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFC3 : public generic_gate {
public:
	c35_DFC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFCP1 : public generic_gate {
public:
	c35_DFCP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFCP3 : public generic_gate {
public:
	c35_DFCP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFE1 : public generic_gate {
public:
	c35_DFE1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFE3 : public generic_gate {
public:
	c35_DFE3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFEC1 : public generic_gate {
public:
	c35_DFEC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFEC3 : public generic_gate {
public:
	c35_DFEC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFECP1 : public generic_gate {
public:
	c35_DFECP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFECP3 : public generic_gate {
public:
	c35_DFECP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFEP1 : public generic_gate {
public:
	c35_DFEP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFEP3 : public generic_gate {
public:
	c35_DFEP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFP1 : public generic_gate {
public:
	c35_DFP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFP3 : public generic_gate {
public:
	c35_DFP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFS1 : public generic_gate {
public:
	c35_DFS1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFS3 : public generic_gate {
public:
	c35_DFS3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSC1 : public generic_gate {
public:
	c35_DFSC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSC3 : public generic_gate {
public:
	c35_DFSC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSCP1 : public generic_gate {
public:
	c35_DFSCP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSCP3 : public generic_gate {
public:
	c35_DFSCP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSE1 : public generic_gate {
public:
	c35_DFSE1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSE3 : public generic_gate {
public:
	c35_DFSE3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSEC1 : public generic_gate {
public:
	c35_DFSEC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSEC3 : public generic_gate {
public:
	c35_DFSEC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSECP1 : public generic_gate {
public:
	c35_DFSECP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSECP3 : public generic_gate {
public:
	c35_DFSECP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSEP1 : public generic_gate {
public:
	c35_DFSEP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSEP3 : public generic_gate {
public:
	c35_DFSEP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSP1 : public generic_gate {
public:
	c35_DFSP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DFSP3 : public generic_gate {
public:
	c35_DFSP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DL1 : public generic_gate {
public:
	c35_DL1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DL3 : public generic_gate {
public:
	c35_DL3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLC1 : public generic_gate {
public:
	c35_DLC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLC3 : public generic_gate {
public:
	c35_DLC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLCP1 : public generic_gate {
public:
	c35_DLCP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLCP3 : public generic_gate {
public:
	c35_DLCP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLCPQ1 : public generic_gate {
public:
	c35_DLCPQ1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLCPQ3 : public generic_gate {
public:
	c35_DLCPQ3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLCQ1 : public generic_gate {
public:
	c35_DLCQ1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLCQ3 : public generic_gate {
public:
	c35_DLCQ3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLP1 : public generic_gate {
public:
	c35_DLP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLP3 : public generic_gate {
public:
	c35_DLP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLPQ1 : public generic_gate {
public:
	c35_DLPQ1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLPQ3 : public generic_gate {
public:
	c35_DLPQ3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLQ1 : public generic_gate {
public:
	c35_DLQ1 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLQ3 : public generic_gate {
public:
	c35_DLQ3 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLY12 : public generic_gate {
public:
	c35_DLY12 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLY22 : public generic_gate {
public:
	c35_DLY22 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLY32 : public generic_gate {
public:
	c35_DLY32 (const char* name);
	void calculate_output(int o) ;
};

class c35_DLY42 : public generic_gate {
public:
	c35_DLY42 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMAJ30 : public generic_gate {
public:
	c35_IMAJ30 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMAJ31 : public generic_gate {
public:
	c35_IMAJ31 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX20 : public generic_gate {
public:
	c35_IMUX20 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX21 : public generic_gate {
public:
	c35_IMUX21 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX22 : public generic_gate {
public:
	c35_IMUX22 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX23 : public generic_gate {
public:
	c35_IMUX23 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX24 : public generic_gate {
public:
	c35_IMUX24 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX30 : public generic_gate {
public:
	c35_IMUX30 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX31 : public generic_gate {
public:
	c35_IMUX31 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX32 : public generic_gate {
public:
	c35_IMUX32 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX33 : public generic_gate {
public:
	c35_IMUX33 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX40 : public generic_gate {
public:
	c35_IMUX40 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX41 : public generic_gate {
public:
	c35_IMUX41 (const char* name);
	void calculate_output(int o) ;
};

class c35_IMUX42 : public generic_gate {
public:
	c35_IMUX42 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV0 : public generic_gate {
public:
	c35_INV0 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV1 : public generic_gate {
public:
	c35_INV1 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV10 : public generic_gate {
public:
	c35_INV10 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV12 : public generic_gate {
public:
	c35_INV12 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV15 : public generic_gate {
public:
	c35_INV15 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV2 : public generic_gate {
public:
	c35_INV2 (const char* name);
	void calculate_output(int o) ;
};

class c35_INVP : public generic_gate {
public:
	c35_INVP (const char* name);
	void calculate_output(int o) ;
};

class c35_INVN : public generic_gate {
public:
	c35_INVN (const char* name);
	void calculate_output(int o) ;
};

class c35_INV3 : public generic_gate {
public:
	c35_INV3 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV4 : public generic_gate {
public:
	c35_INV4 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV6 : public generic_gate {
public:
	c35_INV6 (const char* name);
	void calculate_output(int o) ;
};

class c35_INV8 : public generic_gate {
public:
	c35_INV8 (const char* name);
	void calculate_output(int o) ;
};

class c35_JK1 : public generic_gate {
public:
	c35_JK1 (const char* name);
	void calculate_output(int o) ;
};

class c35_JK3 : public generic_gate {
public:
	c35_JK3 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKC1 : public generic_gate {
public:
	c35_JKC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKC3 : public generic_gate {
public:
	c35_JKC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKCP1 : public generic_gate {
public:
	c35_JKCP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKCP3 : public generic_gate {
public:
	c35_JKCP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKP1 : public generic_gate {
public:
	c35_JKP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKP3 : public generic_gate {
public:
	c35_JKP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKS1 : public generic_gate {
public:
	c35_JKS1 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKS3 : public generic_gate {
public:
	c35_JKS3 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKSC1 : public generic_gate {
public:
	c35_JKSC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKSC3 : public generic_gate {
public:
	c35_JKSC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKSCP1 : public generic_gate {
public:
	c35_JKSCP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKSCP3 : public generic_gate {
public:
	c35_JKSCP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKSP1 : public generic_gate {
public:
	c35_JKSP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_JKSP3 : public generic_gate {
public:
	c35_JKSP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_MAJ31 : public generic_gate {
public:
	c35_MAJ31 (const char* name);
	void calculate_output(int o) ;
};

class c35_MAJ32 : public generic_gate {
public:
	c35_MAJ32 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX21 : public generic_gate {
public:
	c35_MUX21 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX22 : public generic_gate {
public:
	c35_MUX22 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX24 : public generic_gate {
public:
	c35_MUX24 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX26 : public generic_gate {
public:
	c35_MUX26 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX31 : public generic_gate {
public:
	c35_MUX31 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX32 : public generic_gate {
public:
	c35_MUX32 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX33 : public generic_gate {
public:
	c35_MUX33 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX34 : public generic_gate {
public:
	c35_MUX34 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX41 : public generic_gate {
public:
	c35_MUX41 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX42 : public generic_gate {
public:
	c35_MUX42 (const char* name);
	void calculate_output(int o) ;
};

class c35_MUX43 : public generic_gate {
public:
	c35_MUX43 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND20 : public generic_gate {
public:
	c35_NAND20 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND21 : public generic_gate {
public:
	c35_NAND21 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND22 : public generic_gate {
public:
	c35_NAND22 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND23 : public generic_gate {
public:
	c35_NAND23 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND24 : public generic_gate {
public:
	c35_NAND24 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND26 : public generic_gate {
public:
	c35_NAND26 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND28 : public generic_gate {
public:
	c35_NAND28 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND30 : public generic_gate {
public:
	c35_NAND30 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND31 : public generic_gate {
public:
	c35_NAND31 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND32 : public generic_gate {
public:
	c35_NAND32 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND33 : public generic_gate {
public:
	c35_NAND33 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND34 : public generic_gate {
public:
	c35_NAND34 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND40 : public generic_gate {
public:
	c35_NAND40 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND41 : public generic_gate {
public:
	c35_NAND41 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND42 : public generic_gate {
public:
	c35_NAND42 (const char* name);
	void calculate_output(int o) ;
};

class c35_NAND43 : public generic_gate {
public:
	c35_NAND43 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR20 : public generic_gate {
public:
	c35_NOR20 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR21 : public generic_gate {
public:
	c35_NOR21 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR22 : public generic_gate {
public:
	c35_NOR22 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR23 : public generic_gate {
public:
	c35_NOR23 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR24 : public generic_gate {
public:
	c35_NOR24 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR30 : public generic_gate {
public:
	c35_NOR30 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR31 : public generic_gate {
public:
	c35_NOR31 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR32 : public generic_gate {
public:
	c35_NOR32 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR33 : public generic_gate {
public:
	c35_NOR33 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR40 : public generic_gate {
public:
	c35_NOR40 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR41 : public generic_gate {
public:
	c35_NOR41 (const char* name);
	void calculate_output(int o) ;
};

class c35_NOR42 : public generic_gate {
public:
	c35_NOR42 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI210 : public generic_gate {
public:
	c35_OAI210 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI211 : public generic_gate {
public:
	c35_OAI211 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI2110 : public generic_gate {
public:
	c35_OAI2110 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI2111 : public generic_gate {
public:
	c35_OAI2111 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI2112 : public generic_gate {
public:
	c35_OAI2112 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI212 : public generic_gate {
public:
	c35_OAI212 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI220 : public generic_gate {
public:
	c35_OAI220 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI221 : public generic_gate {
public:
	c35_OAI221 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI222 : public generic_gate {
public:
	c35_OAI222 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI310 : public generic_gate {
public:
	c35_OAI310 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI311 : public generic_gate {
public:
	c35_OAI311 (const char* name);
	void calculate_output(int o) ;
};

class c35_OAI312 : public generic_gate {
public:
	c35_OAI312 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFC1 : public generic_gate {
public:
	c35_TFC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFC3 : public generic_gate {
public:
	c35_TFC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFCP1 : public generic_gate {
public:
	c35_TFCP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFCP3 : public generic_gate {
public:
	c35_TFCP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFEC1 : public generic_gate {
public:
	c35_TFEC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFEC3 : public generic_gate {
public:
	c35_TFEC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFECP1 : public generic_gate {
public:
	c35_TFECP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFECP3 : public generic_gate {
public:
	c35_TFECP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFEP1 : public generic_gate {
public:
	c35_TFEP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFEP3 : public generic_gate {
public:
	c35_TFEP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFP1 : public generic_gate {
public:
	c35_TFP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFP3 : public generic_gate {
public:
	c35_TFP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSC1 : public generic_gate {
public:
	c35_TFSC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSC3 : public generic_gate {
public:
	c35_TFSC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSCP1 : public generic_gate {
public:
	c35_TFSCP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSCP3 : public generic_gate {
public:
	c35_TFSCP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSEC1 : public generic_gate {
public:
	c35_TFSEC1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSEC3 : public generic_gate {
public:
	c35_TFSEC3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSECP1 : public generic_gate {
public:
	c35_TFSECP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSECP3 : public generic_gate {
public:
	c35_TFSECP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSEP1 : public generic_gate {
public:
	c35_TFSEP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSEP3 : public generic_gate {
public:
	c35_TFSEP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSP1 : public generic_gate {
public:
	c35_TFSP1 (const char* name);
	void calculate_output(int o) ;
};

class c35_TFSP3 : public generic_gate {
public:
	c35_TFSP3 (const char* name);
	void calculate_output(int o) ;
};

class c35_XNR20 : public generic_gate {
public:
	c35_XNR20 (const char* name);
	void calculate_output(int o) ;
};

class c35_XNR21 : public generic_gate {
public:
	c35_XNR21 (const char* name);
	void calculate_output(int o) ;
};

class c35_XNR22 : public generic_gate {
public:
	c35_XNR22 (const char* name);
	void calculate_output(int o) ;
};

class c35_XNR30 : public generic_gate {
public:
	c35_XNR30 (const char* name);
	void calculate_output(int o) ;
};

class c35_XNR31 : public generic_gate {
public:
	c35_XNR31 (const char* name);
	void calculate_output(int o) ;
};

class c35_XNR40 : public generic_gate {
public:
	c35_XNR40 (const char* name);
	void calculate_output(int o) ;
};

class c35_XNR41 : public generic_gate {
public:
	c35_XNR41 (const char* name);
	void calculate_output(int o) ;
};

class c35_XOR20 : public generic_gate {
public:
	c35_XOR20 (const char* name);
	void calculate_output(int o) ;
};

class c35_XOR21 : public generic_gate {
public:
	c35_XOR21 (const char* name);
	void calculate_output(int o) ;
};

class c35_XOR22 : public generic_gate {
public:
	c35_XOR22 (const char* name);
	void calculate_output(int o) ;
};

class c35_XOR30 : public generic_gate {
public:
	c35_XOR30 (const char* name);
	void calculate_output(int o) ;
};

class c35_XOR31 : public generic_gate {
public:
	c35_XOR31 (const char* name);
	void calculate_output(int o) ;
};

class c35_XOR40 : public generic_gate {
public:
	c35_XOR40 (const char* name);
	void calculate_output(int o) ;
};

class c35_XOR41 : public generic_gate {
public:
	c35_XOR41 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC1P : public generic_gate {
public:
	c35_BBC1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC4P : public generic_gate {
public:
	c35_BBC4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC4SMP : public generic_gate {
public:
	c35_BBC4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC8P : public generic_gate {
public:
	c35_BBC8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC8SMP : public generic_gate {
public:
	c35_BBC8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC8SP : public generic_gate {
public:
	c35_BBC8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC16P : public generic_gate {
public:
	c35_BBC16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC16SMP : public generic_gate {
public:
	c35_BBC16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC16SP : public generic_gate {
public:
	c35_BBC16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC24P : public generic_gate {
public:
	c35_BBC24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC24SMP : public generic_gate {
public:
	c35_BBC24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC24SP : public generic_gate {
public:
	c35_BBC24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD1P : public generic_gate {
public:
	c35_BBCD1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD4P : public generic_gate {
public:
	c35_BBCD4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD4SMP : public generic_gate {
public:
	c35_BBCD4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD8P : public generic_gate {
public:
	c35_BBCD8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD8SMP : public generic_gate {
public:
	c35_BBCD8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD8SP : public generic_gate {
public:
	c35_BBCD8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD16P : public generic_gate {
public:
	c35_BBCD16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD16SMP : public generic_gate {
public:
	c35_BBCD16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD16SP : public generic_gate {
public:
	c35_BBCD16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD24P : public generic_gate {
public:
	c35_BBCD24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD24SMP : public generic_gate {
public:
	c35_BBCD24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD24SP : public generic_gate {
public:
	c35_BBCD24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU1P : public generic_gate {
public:
	c35_BBCU1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU4P : public generic_gate {
public:
	c35_BBCU4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU4SMP : public generic_gate {
public:
	c35_BBCU4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU8P : public generic_gate {
public:
	c35_BBCU8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU8SMP : public generic_gate {
public:
	c35_BBCU8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU8SP : public generic_gate {
public:
	c35_BBCU8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU16P : public generic_gate {
public:
	c35_BBCU16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU16SMP : public generic_gate {
public:
	c35_BBCU16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU16SP : public generic_gate {
public:
	c35_BBCU16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU24P : public generic_gate {
public:
	c35_BBCU24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU24SMP : public generic_gate {
public:
	c35_BBCU24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU24SP : public generic_gate {
public:
	c35_BBCU24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS1P : public generic_gate {
public:
	c35_BBS1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS4P : public generic_gate {
public:
	c35_BBS4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS4SMP : public generic_gate {
public:
	c35_BBS4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS8P : public generic_gate {
public:
	c35_BBS8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS8SMP : public generic_gate {
public:
	c35_BBS8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS8SP : public generic_gate {
public:
	c35_BBS8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS16P : public generic_gate {
public:
	c35_BBS16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS16SMP : public generic_gate {
public:
	c35_BBS16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS16SP : public generic_gate {
public:
	c35_BBS16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS24P : public generic_gate {
public:
	c35_BBS24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS24SMP : public generic_gate {
public:
	c35_BBS24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS24SP : public generic_gate {
public:
	c35_BBS24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD1P : public generic_gate {
public:
	c35_BBSD1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD4P : public generic_gate {
public:
	c35_BBSD4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD4SMP : public generic_gate {
public:
	c35_BBSD4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD8P : public generic_gate {
public:
	c35_BBSD8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD8SMP : public generic_gate {
public:
	c35_BBSD8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD8SP : public generic_gate {
public:
	c35_BBSD8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD16P : public generic_gate {
public:
	c35_BBSD16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD16SMP : public generic_gate {
public:
	c35_BBSD16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD16SP : public generic_gate {
public:
	c35_BBSD16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD24P : public generic_gate {
public:
	c35_BBSD24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD24SMP : public generic_gate {
public:
	c35_BBSD24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD24SP : public generic_gate {
public:
	c35_BBSD24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU1P : public generic_gate {
public:
	c35_BBSU1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU4P : public generic_gate {
public:
	c35_BBSU4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU4SMP : public generic_gate {
public:
	c35_BBSU4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU8P : public generic_gate {
public:
	c35_BBSU8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU8SMP : public generic_gate {
public:
	c35_BBSU8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU8SP : public generic_gate {
public:
	c35_BBSU8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU16P : public generic_gate {
public:
	c35_BBSU16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU16SMP : public generic_gate {
public:
	c35_BBSU16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU16SP : public generic_gate {
public:
	c35_BBSU16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU24P : public generic_gate {
public:
	c35_BBSU24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU24SMP : public generic_gate {
public:
	c35_BBSU24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU24SP : public generic_gate {
public:
	c35_BBSU24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT1P : public generic_gate {
public:
	c35_BBT1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT4P : public generic_gate {
public:
	c35_BBT4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT4SMP : public generic_gate {
public:
	c35_BBT4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT8P : public generic_gate {
public:
	c35_BBT8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT8SMP : public generic_gate {
public:
	c35_BBT8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT8SP : public generic_gate {
public:
	c35_BBT8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT16P : public generic_gate {
public:
	c35_BBT16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT16SMP : public generic_gate {
public:
	c35_BBT16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT16SP : public generic_gate {
public:
	c35_BBT16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT24P : public generic_gate {
public:
	c35_BBT24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT24SMP : public generic_gate {
public:
	c35_BBT24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT24SP : public generic_gate {
public:
	c35_BBT24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD1P : public generic_gate {
public:
	c35_BBTD1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD4P : public generic_gate {
public:
	c35_BBTD4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD4SMP : public generic_gate {
public:
	c35_BBTD4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD8P : public generic_gate {
public:
	c35_BBTD8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD8SMP : public generic_gate {
public:
	c35_BBTD8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD8SP : public generic_gate {
public:
	c35_BBTD8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD16P : public generic_gate {
public:
	c35_BBTD16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD16SMP : public generic_gate {
public:
	c35_BBTD16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD16SP : public generic_gate {
public:
	c35_BBTD16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD24P : public generic_gate {
public:
	c35_BBTD24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD24SMP : public generic_gate {
public:
	c35_BBTD24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD24SP : public generic_gate {
public:
	c35_BBTD24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU1P : public generic_gate {
public:
	c35_BBTU1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU4P : public generic_gate {
public:
	c35_BBTU4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU4SMP : public generic_gate {
public:
	c35_BBTU4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU8P : public generic_gate {
public:
	c35_BBTU8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU8SMP : public generic_gate {
public:
	c35_BBTU8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU8SP : public generic_gate {
public:
	c35_BBTU8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU16P : public generic_gate {
public:
	c35_BBTU16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU16SMP : public generic_gate {
public:
	c35_BBTU16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU16SP : public generic_gate {
public:
	c35_BBTU16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU24P : public generic_gate {
public:
	c35_BBTU24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU24SMP : public generic_gate {
public:
	c35_BBTU24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU24SP : public generic_gate {
public:
	c35_BBTU24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT1P : public generic_gate {
public:
	c35_BT1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BT2P : public generic_gate {
public:
	c35_BT2P (const char* name);
	void calculate_output(int o) ;
};

class c35_BT4P : public generic_gate {
public:
	c35_BT4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BT4SMP : public generic_gate {
public:
	c35_BT4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT8P : public generic_gate {
public:
	c35_BT8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BT8SMP : public generic_gate {
public:
	c35_BT8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT8SP : public generic_gate {
public:
	c35_BT8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT12P : public generic_gate {
public:
	c35_BT12P (const char* name);
	void calculate_output(int o) ;
};

class c35_BT12SMP : public generic_gate {
public:
	c35_BT12SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT12SP : public generic_gate {
public:
	c35_BT12SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT16P : public generic_gate {
public:
	c35_BT16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BT16SMP : public generic_gate {
public:
	c35_BT16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT16SP : public generic_gate {
public:
	c35_BT16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT24P : public generic_gate {
public:
	c35_BT24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BT24SMP : public generic_gate {
public:
	c35_BT24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BT24SP : public generic_gate {
public:
	c35_BT24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU1P : public generic_gate {
public:
	c35_BU1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BU2P : public generic_gate {
public:
	c35_BU2P (const char* name);
	void calculate_output(int o) ;
};

class c35_BU4P : public generic_gate {
public:
	c35_BU4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BU4SMP : public generic_gate {
public:
	c35_BU4SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU8P : public generic_gate {
public:
	c35_BU8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BU8SMP : public generic_gate {
public:
	c35_BU8SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU8SP : public generic_gate {
public:
	c35_BU8SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU12P : public generic_gate {
public:
	c35_BU12P (const char* name);
	void calculate_output(int o) ;
};

class c35_BU12SMP : public generic_gate {
public:
	c35_BU12SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU12SP : public generic_gate {
public:
	c35_BU12SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU16P : public generic_gate {
public:
	c35_BU16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BU16SMP : public generic_gate {
public:
	c35_BU16SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU16SP : public generic_gate {
public:
	c35_BU16SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU24P : public generic_gate {
public:
	c35_BU24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BU24SMP : public generic_gate {
public:
	c35_BU24SMP (const char* name);
	void calculate_output(int o) ;
};

class c35_BU24SP : public generic_gate {
public:
	c35_BU24SP (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD1P : public generic_gate {
public:
	c35_BUDD1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD2P : public generic_gate {
public:
	c35_BUDD2P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD4P : public generic_gate {
public:
	c35_BUDD4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD8P : public generic_gate {
public:
	c35_BUDD8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD12P : public generic_gate {
public:
	c35_BUDD12P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD16P : public generic_gate {
public:
	c35_BUDD16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD24P : public generic_gate {
public:
	c35_BUDD24P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU1P : public generic_gate {
public:
	c35_BUDU1P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU2P : public generic_gate {
public:
	c35_BUDU2P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU4P : public generic_gate {
public:
	c35_BUDU4P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU8P : public generic_gate {
public:
	c35_BUDU8P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU12P : public generic_gate {
public:
	c35_BUDU12P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU16P : public generic_gate {
public:
	c35_BUDU16P (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU24P : public generic_gate {
public:
	c35_BUDU24P (const char* name);
	void calculate_output(int o) ;
};

class c35_CBU1P : public generic_gate {
public:
	c35_CBU1P (const char* name);
	void calculate_output(int o) ;
};

class c35_CBU2P : public generic_gate {
public:
	c35_CBU2P (const char* name);
	void calculate_output(int o) ;
};

class c35_ICCK2P : public generic_gate {
public:
	c35_ICCK2P (const char* name);
	void calculate_output(int o) ;
};

class c35_ICCK4P : public generic_gate {
public:
	c35_ICCK4P (const char* name);
	void calculate_output(int o) ;
};

class c35_ICCK8P : public generic_gate {
public:
	c35_ICCK8P (const char* name);
	void calculate_output(int o) ;
};

class c35_ICCK16P : public generic_gate {
public:
	c35_ICCK16P (const char* name);
	void calculate_output(int o) ;
};

class c35_ICDP : public generic_gate {
public:
	c35_ICDP (const char* name);
	void calculate_output(int o) ;
};

class c35_ICP : public generic_gate {
public:
	c35_ICP (const char* name);
	void calculate_output(int o) ;
};

class c35_ICUP : public generic_gate {
public:
	c35_ICUP (const char* name);
	void calculate_output(int o) ;
};

class c35_ISDP : public generic_gate {
public:
	c35_ISDP (const char* name);
	void calculate_output(int o) ;
};

class c35_ISP : public generic_gate {
public:
	c35_ISP (const char* name);
	void calculate_output(int o) ;
};

class c35_ISUP : public generic_gate {
public:
	c35_ISUP (const char* name);
	void calculate_output(int o) ;
};

class c35_ITCK2P : public generic_gate {
public:
	c35_ITCK2P (const char* name);
	void calculate_output(int o) ;
};

class c35_ITCK4P : public generic_gate {
public:
	c35_ITCK4P (const char* name);
	void calculate_output(int o) ;
};

class c35_ITCK8P : public generic_gate {
public:
	c35_ITCK8P (const char* name);
	void calculate_output(int o) ;
};

class c35_ITCK16P : public generic_gate {
public:
	c35_ITCK16P (const char* name);
	void calculate_output(int o) ;
};

class c35_ITDP : public generic_gate {
public:
	c35_ITDP (const char* name);
	void calculate_output(int o) ;
};

class c35_ITP : public generic_gate {
public:
	c35_ITP (const char* name);
	void calculate_output(int o) ;
};

class c35_ITUP : public generic_gate {
public:
	c35_ITUP (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC1P_V5 : public generic_gate {
public:
	c35_BBC1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC4P_V5 : public generic_gate {
public:
	c35_BBC4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC4SMP_V5 : public generic_gate {
public:
	c35_BBC4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC8P_V5 : public generic_gate {
public:
	c35_BBC8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC8SMP_V5 : public generic_gate {
public:
	c35_BBC8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC8SP_V5 : public generic_gate {
public:
	c35_BBC8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC16P_V5 : public generic_gate {
public:
	c35_BBC16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC16SMP_V5 : public generic_gate {
public:
	c35_BBC16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC16SP_V5 : public generic_gate {
public:
	c35_BBC16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC24P_V5 : public generic_gate {
public:
	c35_BBC24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC24SMP_V5 : public generic_gate {
public:
	c35_BBC24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBC24SP_V5 : public generic_gate {
public:
	c35_BBC24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD1P_V5 : public generic_gate {
public:
	c35_BBCD1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD4P_V5 : public generic_gate {
public:
	c35_BBCD4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD4SMP_V5 : public generic_gate {
public:
	c35_BBCD4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD8P_V5 : public generic_gate {
public:
	c35_BBCD8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD8SMP_V5 : public generic_gate {
public:
	c35_BBCD8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD8SP_V5 : public generic_gate {
public:
	c35_BBCD8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD16P_V5 : public generic_gate {
public:
	c35_BBCD16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD16SMP_V5 : public generic_gate {
public:
	c35_BBCD16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD16SP_V5 : public generic_gate {
public:
	c35_BBCD16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD24P_V5 : public generic_gate {
public:
	c35_BBCD24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD24SMP_V5 : public generic_gate {
public:
	c35_BBCD24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCD24SP_V5 : public generic_gate {
public:
	c35_BBCD24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU1P_V5 : public generic_gate {
public:
	c35_BBCU1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU4P_V5 : public generic_gate {
public:
	c35_BBCU4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU4SMP_V5 : public generic_gate {
public:
	c35_BBCU4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU8P_V5 : public generic_gate {
public:
	c35_BBCU8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU8SMP_V5 : public generic_gate {
public:
	c35_BBCU8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU8SP_V5 : public generic_gate {
public:
	c35_BBCU8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU16P_V5 : public generic_gate {
public:
	c35_BBCU16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU16SMP_V5 : public generic_gate {
public:
	c35_BBCU16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU16SP_V5 : public generic_gate {
public:
	c35_BBCU16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU24P_V5 : public generic_gate {
public:
	c35_BBCU24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU24SMP_V5 : public generic_gate {
public:
	c35_BBCU24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBCU24SP_V5 : public generic_gate {
public:
	c35_BBCU24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS1P_V5 : public generic_gate {
public:
	c35_BBS1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS4P_V5 : public generic_gate {
public:
	c35_BBS4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS4SMP_V5 : public generic_gate {
public:
	c35_BBS4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS8P_V5 : public generic_gate {
public:
	c35_BBS8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS8SMP_V5 : public generic_gate {
public:
	c35_BBS8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS8SP_V5 : public generic_gate {
public:
	c35_BBS8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS16P_V5 : public generic_gate {
public:
	c35_BBS16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS16SMP_V5 : public generic_gate {
public:
	c35_BBS16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS16SP_V5 : public generic_gate {
public:
	c35_BBS16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS24P_V5 : public generic_gate {
public:
	c35_BBS24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS24SMP_V5 : public generic_gate {
public:
	c35_BBS24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBS24SP_V5 : public generic_gate {
public:
	c35_BBS24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD1P_V5 : public generic_gate {
public:
	c35_BBSD1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD4P_V5 : public generic_gate {
public:
	c35_BBSD4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD4SMP_V5 : public generic_gate {
public:
	c35_BBSD4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD8P_V5 : public generic_gate {
public:
	c35_BBSD8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD8SMP_V5 : public generic_gate {
public:
	c35_BBSD8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD8SP_V5 : public generic_gate {
public:
	c35_BBSD8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD16P_V5 : public generic_gate {
public:
	c35_BBSD16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD16SMP_V5 : public generic_gate {
public:
	c35_BBSD16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD16SP_V5 : public generic_gate {
public:
	c35_BBSD16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD24P_V5 : public generic_gate {
public:
	c35_BBSD24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD24SMP_V5 : public generic_gate {
public:
	c35_BBSD24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSD24SP_V5 : public generic_gate {
public:
	c35_BBSD24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU1P_V5 : public generic_gate {
public:
	c35_BBSU1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU4P_V5 : public generic_gate {
public:
	c35_BBSU4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU4SMP_V5 : public generic_gate {
public:
	c35_BBSU4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU8P_V5 : public generic_gate {
public:
	c35_BBSU8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU8SMP_V5 : public generic_gate {
public:
	c35_BBSU8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU8SP_V5 : public generic_gate {
public:
	c35_BBSU8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU16P_V5 : public generic_gate {
public:
	c35_BBSU16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU16SMP_V5 : public generic_gate {
public:
	c35_BBSU16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU16SP_V5 : public generic_gate {
public:
	c35_BBSU16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU24P_V5 : public generic_gate {
public:
	c35_BBSU24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU24SMP_V5 : public generic_gate {
public:
	c35_BBSU24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBSU24SP_V5 : public generic_gate {
public:
	c35_BBSU24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT1P_V5 : public generic_gate {
public:
	c35_BBT1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT4P_V5 : public generic_gate {
public:
	c35_BBT4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT4SMP_V5 : public generic_gate {
public:
	c35_BBT4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT8P_V5 : public generic_gate {
public:
	c35_BBT8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT8SMP_V5 : public generic_gate {
public:
	c35_BBT8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT8SP_V5 : public generic_gate {
public:
	c35_BBT8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT16P_V5 : public generic_gate {
public:
	c35_BBT16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT16SMP_V5 : public generic_gate {
public:
	c35_BBT16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT16SP_V5 : public generic_gate {
public:
	c35_BBT16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT24P_V5 : public generic_gate {
public:
	c35_BBT24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT24SMP_V5 : public generic_gate {
public:
	c35_BBT24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBT24SP_V5 : public generic_gate {
public:
	c35_BBT24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD1P_V5 : public generic_gate {
public:
	c35_BBTD1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD4P_V5 : public generic_gate {
public:
	c35_BBTD4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD4SMP_V5 : public generic_gate {
public:
	c35_BBTD4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD8P_V5 : public generic_gate {
public:
	c35_BBTD8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD8SMP_V5 : public generic_gate {
public:
	c35_BBTD8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD8SP_V5 : public generic_gate {
public:
	c35_BBTD8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD16P_V5 : public generic_gate {
public:
	c35_BBTD16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD16SMP_V5 : public generic_gate {
public:
	c35_BBTD16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD16SP_V5 : public generic_gate {
public:
	c35_BBTD16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD24P_V5 : public generic_gate {
public:
	c35_BBTD24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD24SMP_V5 : public generic_gate {
public:
	c35_BBTD24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTD24SP_V5 : public generic_gate {
public:
	c35_BBTD24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU1P_V5 : public generic_gate {
public:
	c35_BBTU1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU4P_V5 : public generic_gate {
public:
	c35_BBTU4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU4SMP_V5 : public generic_gate {
public:
	c35_BBTU4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU8P_V5 : public generic_gate {
public:
	c35_BBTU8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU8SMP_V5 : public generic_gate {
public:
	c35_BBTU8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU8SP_V5 : public generic_gate {
public:
	c35_BBTU8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU16P_V5 : public generic_gate {
public:
	c35_BBTU16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU16SMP_V5 : public generic_gate {
public:
	c35_BBTU16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU16SP_V5 : public generic_gate {
public:
	c35_BBTU16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU24P_V5 : public generic_gate {
public:
	c35_BBTU24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU24SMP_V5 : public generic_gate {
public:
	c35_BBTU24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BBTU24SP_V5 : public generic_gate {
public:
	c35_BBTU24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT1P_V5 : public generic_gate {
public:
	c35_BT1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT2P_V5 : public generic_gate {
public:
	c35_BT2P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT4P_V5 : public generic_gate {
public:
	c35_BT4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT4SMP_V5 : public generic_gate {
public:
	c35_BT4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT8P_V5 : public generic_gate {
public:
	c35_BT8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT8SMP_V5 : public generic_gate {
public:
	c35_BT8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT8SP_V5 : public generic_gate {
public:
	c35_BT8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT12P_V5 : public generic_gate {
public:
	c35_BT12P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT12SMP_V5 : public generic_gate {
public:
	c35_BT12SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT12SP_V5 : public generic_gate {
public:
	c35_BT12SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT16P_V5 : public generic_gate {
public:
	c35_BT16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT16SMP_V5 : public generic_gate {
public:
	c35_BT16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT16SP_V5 : public generic_gate {
public:
	c35_BT16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT24P_V5 : public generic_gate {
public:
	c35_BT24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT24SMP_V5 : public generic_gate {
public:
	c35_BT24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BT24SP_V5 : public generic_gate {
public:
	c35_BT24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU1P_V5 : public generic_gate {
public:
	c35_BU1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU2P_V5 : public generic_gate {
public:
	c35_BU2P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU4P_V5 : public generic_gate {
public:
	c35_BU4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU4SMP_V5 : public generic_gate {
public:
	c35_BU4SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU8P_V5 : public generic_gate {
public:
	c35_BU8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU8SMP_V5 : public generic_gate {
public:
	c35_BU8SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU8SP_V5 : public generic_gate {
public:
	c35_BU8SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU12P_V5 : public generic_gate {
public:
	c35_BU12P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU12SMP_V5 : public generic_gate {
public:
	c35_BU12SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU12SP_V5 : public generic_gate {
public:
	c35_BU12SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU16P_V5 : public generic_gate {
public:
	c35_BU16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU16SMP_V5 : public generic_gate {
public:
	c35_BU16SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU16SP_V5 : public generic_gate {
public:
	c35_BU16SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU24P_V5 : public generic_gate {
public:
	c35_BU24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU24SMP_V5 : public generic_gate {
public:
	c35_BU24SMP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BU24SP_V5 : public generic_gate {
public:
	c35_BU24SP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD1P_V5 : public generic_gate {
public:
	c35_BUDD1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD2P_V5 : public generic_gate {
public:
	c35_BUDD2P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD4P_V5 : public generic_gate {
public:
	c35_BUDD4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD8P_V5 : public generic_gate {
public:
	c35_BUDD8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD12P_V5 : public generic_gate {
public:
	c35_BUDD12P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD16P_V5 : public generic_gate {
public:
	c35_BUDD16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDD24P_V5 : public generic_gate {
public:
	c35_BUDD24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU1P_V5 : public generic_gate {
public:
	c35_BUDU1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU2P_V5 : public generic_gate {
public:
	c35_BUDU2P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU4P_V5 : public generic_gate {
public:
	c35_BUDU4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU8P_V5 : public generic_gate {
public:
	c35_BUDU8P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU12P_V5 : public generic_gate {
public:
	c35_BUDU12P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU16P_V5 : public generic_gate {
public:
	c35_BUDU16P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_BUDU24P_V5 : public generic_gate {
public:
	c35_BUDU24P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_CBU1P_V5 : public generic_gate {
public:
	c35_CBU1P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_CBU2P_V5 : public generic_gate {
public:
	c35_CBU2P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ICCK2P_V5 : public generic_gate {
public:
	c35_ICCK2P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ICCK4P_V5 : public generic_gate {
public:
	c35_ICCK4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ICDP_V5 : public generic_gate {
public:
	c35_ICDP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ICP_V5 : public generic_gate {
public:
	c35_ICP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ICUP_V5 : public generic_gate {
public:
	c35_ICUP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ISDP_V5 : public generic_gate {
public:
	c35_ISDP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ISP_V5 : public generic_gate {
public:
	c35_ISP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ISUP_V5 : public generic_gate {
public:
	c35_ISUP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ITCK2P_V5 : public generic_gate {
public:
	c35_ITCK2P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ITCK4P_V5 : public generic_gate {
public:
	c35_ITCK4P_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ITDP_V5 : public generic_gate {
public:
	c35_ITDP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ITP_V5 : public generic_gate {
public:
	c35_ITP_V5 (const char* name);
	void calculate_output(int o) ;
};

class c35_ITUP_V5 : public generic_gate {
public:
	c35_ITUP_V5 (const char* name);
	void calculate_output(int o) ;
};

#endif
