#ifndef __C35_MEM_H
#define __C35_MEM_H


//////////////////////////////////////////////////////////////////////
// c35_memory.h: interface for the generic combinational logic.
//
// Author : Alberto BOSIO, Paolo BERNARDI
//
// Last Update : 15-12-2007
//////////////////////////////////////////////////////////////////////

#include "../../generic_gate.h"
#include "../../generic_memory.h"
#include "../common/truthtables.h"

using namespace std;

#define _SPLPLL_10M_16384x8m16_U    178		
//#define __DEBUG__

/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = -1
 * Z = -2
 * ...
*/

class SPLPLL_10M_16384x8m16 : public generic_memory {
public:
  
  // memory characteristics
  int gpram[16384][8];
  
  SPLPLL_10M_16384x8m16 (const char* name) : generic_memory(name) {
    inputs_number=26;
    outputs_number=8;
    outputs_name = "Q0,Q1,Q2,Q3,Q4,Q5,Q6,Q7";
    inputs_name = "CK,CSN,TBYPASS,WEN,A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,D0,D1,D2,D3,D4,D5,D6,D7";
    type=_SPLPLL_10M_16384x8m16_U;
    data_width = 8;
    add_width = 16384;
    add_log_width = 14;
    InitAfterConstr ();
  };    
    
    int calculate_addr(int* input_value) {
      int addr = 0;
#ifdef __DEBUG__
      cout << "\tinput_value " ;
#endif
      for( int i = 0 ; i <add_log_width ; i++ ) {
#ifdef __DEBUG__
	cout << input_value[i+4];
#endif
	addr *= 2;
	addr += input_value[i+4];
      }
#ifdef __DEBUG__
      cout  <<  "   -  corresponds to addr = " << std::hex <<  addr << std::dec <<  endl ;
#endif
      return addr;
    };
    
    void calculate_output(int o) {
      if (old_clk==0 && input_value[0]==1)
	if (input_value[2]==1)
	  temp_output_value[o]=input_value[o+18];			
	else 
	  if (input_value[1]==0) {	      	   
	    temp_output_value[o] = gpram[calculate_addr(input_value)][o];
	    if (input_value[3]==0) 
	      gpram[calculate_addr(input_value)][o]=input_value[o+18];	    	    
	  }
      propagate_output_flag = 0;
      return ;
    };
    
    void set_input (int port, int value) {
      generic_gate::set_input (port,value);
      if (port==0) {
	old_clk=input_value[0];
      }
    };
    
        
    int load_memory(const char* namefile) {
      int i = 0, j = 0;
      char app[25];
      FILE *fp;
      cout << "   + Memory instance " << name  << " is being filled --> " << namefile << "..";
      if(fp = fopen(namefile,"r")) {
	for (i=0 ; i<add_width; i++) {
		 fscanf(fp,"%s",app);
		 for (j=(data_width-1) ; j >= 0 ; j--) {		   
		   gpram[i][j]= app[j] - '0';
		 }
	} 
      }
      else return 0;
      cout << " done!" << endl;
      fclose(fp);
      return 1;
      
    };
    
    void write_content_2_video() {
      int i = 0, j = 0;
      FILE *fp;
      if(fp = fopen("compare.txt","w")) {
	for (i=0 ; i<add_width; i++) {
	  for (j=0 ; j<data_width; j++) 
	    fprintf(fp, "%d", gpram[i][j]) ;
	  fprintf(fp, "\n") ;
	}	   	    
      }
      fclose(fp);
      return;	  
    };
};



#endif
