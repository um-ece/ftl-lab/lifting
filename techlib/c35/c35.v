// Verilog UDP library for all c35 CORELIB voltages
// $Id: udp.v,v 1 2004/12/04 13:12:19 bka
// Owner: austriamicrosystems AG  HIT-Kit: Digital


primitive DFF_UDP (Q, D, C, S, R, notifier);
  output Q;
  input D,C,S,R;
  input notifier;
  reg Q;
 
  table
  // D  C    S   R ntf : Qt: Qt+1
     0 (01)  1   ?  ?  : ? : 0 ; // rising clk d = 0
     1 (01)  ?   1  ?  : ? : 1 ; // rising clk d = 1
     0 (0?)  1   ?  ?  : 0 : 0 ; // rising clk d = q
     1 (0?)  ?   1  ?  : 1 : 1 ; // rising clk d = q
     0 (?1)  1   ?  ?  : 0 : 0 ; // rising clk d = q
     1 (?1)  ?   1  ?  : 1 : 1 ; // rising clk d = q
     ? (1?)  1   1  ?  : ? : - ; // falling clk
     ? (?0)  1   1  ?  : ? : - ; // falling clk
     *  ?    1   1  ?  : ? : - ; // data change
     ?  ?  (?1)  1  ?  : ? : - ; // set inactive
     ?  ?    1 (?1) ?  : ? : - ; // reset inactive
     ?  ?    1   0  ?  : ? : 0 ; // reset
     ?  ?    0   1  ?  : ? : 1 ; // set
     ?  ?    0   0  ?  : ? : x ; // set && reset
     ?  ?    x   ?  ?  : ? : x ; //
     ?  ?    ?   x  ?  : ? : x ; //
     ?  ?    ?   ?  *  : ? : x ; // timing violation
  endtable
endprimitive

primitive LATCH_UDP (Q, G, C, S, R, notifier);
  output Q;
  input G,C,S,R;
  input notifier;
  reg Q;
 
  table
  // D  G  S  R ntf : Qt: Qt+1
     0  0  1  ?  ?  : ? : 0 ; // open gate d = 0
     1  0  ?  1  ?  : ? : 1 ; // open gate d = 1
     0  ?  1  ?  ?  : 0 : 0 ; // any gate d = q
     1  ?  ?  1  ?  : 1 : 1 ; // any gate d = q
     ?  1  1  1  ?  : ? : - ; // closed gate
     ?  ?  ?  0  ?  : ? : 0 ; // reset
     ?  ?  0  1  ?  : ? : 1 ; // set
     ?  ?  ?  ?  *  : ? : x ; // timing violation
  endtable
endprimitive

primitive MUX_UDP (Q, A, B, S);
  output Q;
  input A, B, S;
 
  table
  // A B S : Q
     0 0 ? : 0 ;
     1 1 ? : 1 ;
     0 ? 0 : 0 ;
     1 ? 0 : 1 ;
     ? 0 1 : 0 ;
     ? 1 1 : 1 ;
  endtable
endprimitive

primitive JK_UDP (JK, J, K, Q);
  output JK;
  input J,K,Q;
 
  table
  // J K Q : JK
     0 1 ? : 0 ; // reset
     1 0 ? : 1 ; // set
     0 ? 0 : 0 ; // no change
     1 ? 0 : 1 ; // toggle
     ? 1 1 : 0 ; // toggle
     ? 0 1 : 1 ; // no change
  endtable
endprimitive

// Description :  FULL ADDER SUM TERM

primitive U_ADDR2_S (S, A, B, CI);
    output S;
    input A, B, CI;

    table
    //  A   B   CI  :   S
        0   0   0   :   0 ;
        0   0   1   :   1 ;
        0   1   0   :   1 ;
        0   1   1   :   0 ;
        1   0   0   :   1 ;
        1   0   1   :   0 ;
        1   1   0   :   0 ;
        1   1   1   :   1 ;
    endtable
endprimitive 


// Description :  FULL ADDER CARRY OUT TERM

primitive U_ADDR2_C   (CO, A, B, CI);
    output CO;
    input A, B, CI;

    table
    //  A   B   CI  :   CO
        1   1   ?   :   1 ;
        1   ?   1   :   1 ;
        ?   1   1   :   1 ;
        0   0   ?   :   0 ;
        0   ?   0   :   0 ;
        ?   0   0   :   0 ;
    endtable
endprimitive   

// Description :  POSITIVE EDGE TRIGGERED D FLIP-FLOP ( Q OUTPUT UDP ).

primitive U_FD_P_NO (Q, D, CP, NOTIFIER_REG); 

    output Q;  
    input  NOTIFIER_REG,
           D, CP;
    reg    Q;  

    table
    //  D   CP    NOTIFIER_REG  :   Qt  :   Qt+1
        1   (01)     ?          :   ?   :   1;  // clocked data
        0   (01)     ?          :   ?   :   0;
        1   (?1)     ?          :   1   :   1;  // reducing pessimism
        0   (?1)     ?          :   0   :   0;
        1   (0?)     ?          :   1   :   1;
        0   (0?)     ?          :   0   :   0;
        ?   (1?)     ?          :   ?   :   -;  // no change on falling edge
        ?   (?0)     ?          :   ?   :   -;
        *    ?       ?          :   ?   :   -;  // ignore edges on data
        ?    ?       *          :   ?   :   x;
    endtable
endprimitive


// Description : POSITIVE EDGE TRIGGERED D FLIP-FLOP WITH ACTIVE LOW
//            ASYNCHRONOUS CLEAR ( Q OUTPUT UDP ).

primitive U_FD_P_RB_NO (Q, D, CP, RB, NOTIFIER_REG); 

    output Q;  
    input  NOTIFIER_REG,
           D, CP, RB;
    reg    Q;     

    table 
    //  D   CP      RB NOTIFIER_REG  :   Qt  :   Qt+1
        1   (01)    1     ?          :   ?   :   1;  // clocked data
        0   (01)    ?     ?          :   ?   :   0;
        1   (?1)    1     ?          :   1   :   1;  // reducing pessimism
        0   (?1)    ?     ?          :   0   :   0;                          
        1   (0?)    1     ?          :   1   :   1;  
        0   (0?)    ?     ?          :   0   :   0;  
        ?   (?0)    1     ?          :   ?   :   -;  // ignore falling clock
        ?   (1?)    1     ?          :   ?   :   -;  // ignore falling clock
        ?    ?      0     ?          :   ?   :   0;  // asynchronous clear
        ?    ?      x     ?          :   ?   :   x;  // pessimism
        *    ?      1     ?          :   ?   :   -;  // ignore the edges on data
        ?    ?    (?1)    ?          :   ?   :   -;  // ignore the edges on clear 
        ?    ?      ?     *          :   ?   :   x; 
    endtable
endprimitive


// Description : POSITIVE EDGE TRIGGERED D FLIP-FLOP WITH ACTIVE LOW
//            ASYNCHRONOUS SET AND CLEAR. ( Q OUTPUT UDP ).

primitive U_FD_P_RB_SB_NO (Q, D, CP, RB, SB, NOTIFIER_REG); 

    output Q;  
    input  NOTIFIER_REG,
           D, CP, RB, SB;
    reg    Q; 

    table 
    //  D   CP      RB  SB NOTIFIER_REG  :   Qt  :   Qt+1
        1   (01)    1   ?     ?          :   ?   :   1;  // clocked data  bk
        0   (01)    ?   1     ?          :   ?   :   0;  // clocked data  bk
        1   (?1)    1   ?     ?          :   1   :   1; 
        0   (?1)    ?   1     ?          :   0   :   0;
        1   (0?)    1   ?     ?          :   1   :   1; 
        0   (0?)    ?   1     ?          :   0   :   0;  
        ?   (?0)    1   1     ?          :   ?   :   -;  // ignore falling clock
        ?   (1?)    1   1     ?          :   ?   :   -;  // ignore falling clock
        ?   ?       0   1     ?          :   ?   :   0;  // asynchronous clear
        ?   ?       1   0     ?          :   ?   :   1;  // asynchronous set
        ?   ?       x   ?     ?          :   ?   :   x;  // pessimism
        ?   ?       ?   x     ?          :   ?   :   x;  // pessimism
        *   ?       1   1     ?          :   ?   :   -;  // ignore data edges 
        ?   ?     (?1)  1     ?          :   ?   :   -;  // ignore the edges on 
        ?   ?       1  (?1)   ?          :   ?   :   -;  // set and clear 
        ?   ?       0   0     ?          :   ?   :   x;  // set and clear 
        ?   ?       ?   ?     *          :   ?   :   x;
    endtable
endprimitive

    
// Description : POSITIVE EDGE TRIGGERED D FLIP-FLOP WITH ACTIVE LOW
//            ASYNCHRONOUS SET ( Q OUTPUT UDP )

primitive U_FD_P_SB_NO (Q, D, CP, SB, NOTIFIER_REG); 

    output Q;  
    input  NOTIFIER_REG,
           D, CP, SB;
    reg    Q; 

    table 
    //  D   CP      SB NOTIFIER_REG  :   Qt  :   Qt+1
        1   (01)    ?     ?          :   ?   :   1;  // clocked data
        0   (01)    1     ?          :   ?   :   0;
        1   (?1)    ?     ?          :   1   :   1;  // reducing pessimism
        0   (?1)    1     ?          :   0   :   0;                          
        1   (0?)    ?     ?          :   1   :   1;  
        0   (0?)    1     ?          :   0   :   0;  
        ?   (?0)    1     ?          :   ?   :   -;  // ignore falling clock
        ?   (1?)    1     ?          :   ?   :   -;  // ignore falling clock
        ?    ?      0     ?          :   ?   :   1;  // asynchronous clear
        ?    ?      x     ?          :   ?   :   x;  // pessimism
        *    ?      ?     ?          :   ?   :   -;  // ignore the data edges
        ?   ?     (?1)    ?          :   ?   :   -;  // ignore the edges on set
        ?   ?       ?     *          :   ?   :   x;
   endtable
endprimitive



// Description : POSITIVE EDGE TRIGGERED JK FLIP FLOP, STANDARD DRIVE ( Q OUTPUT UDP )
//  

primitive U_FJK_P_NO (Q, J, K, CP, NOTIFIER_REG); 
    output Q;
    reg    Q;  
    input  NOTIFIER_REG,
           J,K,
           CP;                                  // Clock.

    table
      //    J   K   CP  NOTIFIER_REG  :  Qtn   :   Qtn+1
            0   0   r      ?          :   ?    :     - ;      // Output retains the 
            0   1   r      ?          :   ?    :     0 ;      // reset
            1   0   r      ?          :   ?    :     1 ;      // set
            1   ?   p      ?          :   0    :     1 ;      // Clocked toggle.
            ?   1   p      ?          :   1    :     0 ;
            0   ?   p      ?          :   0    :     0 ;      // no change
            ?   0   p      ?          :   1    :     1 ;      // no change
            *   ?   ?      ?          :   ?    :     - ;      // Insensitive to 
            ?   *   ?      ?          :   ?    :     - ;      // transitions on J and K
                                                              // with steady clock.  
            ?   ?   n      ?          :   ?    :     - ;      //ignore falling clock.  
            ?   ?   ?      *          :   ?    :     x ;
    endtable
endprimitive


primitive U_FJK_P_RB_NO (Q, J, K, CP, RB, NOTIFIER_REG); 
    output Q;
    reg    Q;  
    input  NOTIFIER_REG,
           J,K,
            CP,                                  // CLOCK.
            RB;                                  // CLEAR INPUT.
// FUNCTION : POSITIVE EDGE TRIGGERED JK FLIP FLOP, WITH CLEAR 
//            ACTIVE LOW / STANDARD DRIVE ( Q OUTPUT UDP )


    table
      // J   K   CP  RB      NOTIFIER_REG  :  Qtn : Qtn+1

         0   0  (01)  1         ?          :   ?  :   - ;         // Output retains the 
                                                                  // present state if both 
                                                                  // J and K are 0.
         0   1  (01)  1         ?          :   ?  :   0 ;         // Clocked J and K.
         1   0  (01)  1         ?          :   ?  :   1 ;    

         ?   ?   ?    x         ?          :   ?  :   x ;         // pessimism

         1   1  (01)  1         ?          :   0  :   1 ;         // Clocked toggle.
         1   1  (01)  1         ?          :   1  :   0 ;

         0   0  (x1)  1         ?          :   ?  :   - ;         //possible clocked JK
         0   1  (x1)  1         ?          :   0  :   0 ;   
         1   0  (x1)  1         ?          :   1  :   1 ;
         0   0  (0x)  1         ?          :   ?  :   - ;
         0   1  (0x)  1         ?          :   0  :   0 ;
         1   0  (0x)  1         ?          :   1  :   1 ;

         *   ?   ?    1         ?          :   ?  :   - ;         // Insensitive to 
         ?   *   ?    1         ?          :   ?  :   - ;         // transitions on J and K
                                                                  // with steady clock.
         ?   ? (?0)   1         ?          :   ?  :   - ;         //ignore falling clock.
         ?   ? (1x)   1         ?          :   ?  :   - ; 

         ?   ?   ?    0         ?          :   ?  :   0 ;         // Clear. 

         ?   ?   ?   (?1)       ?          :   ?  :   - ;         // Insensitive to a rising
                                                                  // edge on CD.
         ?   ?   ?    ?         *          :   ?  :   x ;         


    endtable

endprimitive




// Description : POSITIVE EDGE TRIGGERED JK FLIP FLOP, WITH ACTIVE LOW 
//            ASYNCHRONOUS CLEAR AND  SET  ( Q OUTPUT UDP ).
 
primitive U_FJK_P_RB_SB_NO (Q, J, K, CP, RB, SB, NOTIFIER_REG); 
    output Q;
    reg    Q;  
    input  NOTIFIER_REG,
           J,K,
            CP,                                  // Clock.
            RB,                                  // Clear input
            SB;                                  // Set input  

    table
      // J   K   CP   RB   SB  NOTIFIER_REG  : Qtn : Qtn+1

         0   0   r    1    1      ?          :   ?    :     - ;      // Output retains the 
         0   1   r    ?    1      ?          :   ?    :     0 ;      // reset
         1   0   r    1    ?      ?          :   ?    :     1 ;      // set
         1   ?   p    1    ?      ?          :   0    :     1 ;      // Clocked toggle.
         ?   1   p    ?    1      ?          :   1    :     0 ;
         0   ?   p    ?    1      ?          :   0    :     0 ;      // no change
         ?   0   p    1    ?      ?          :   1    :     1 ;      // no change
         *   ?   ?    1    1      ?          :   ?    :     - ;      // Insensitive to 
         ?   *   ?    1    1      ?          :   ?    :     - ;      // transitions on J and K
                                                              // with steady clock.  
         ?   ?   n    1    1      ?          :   ?    :     - ;      //ignore falling clock.  
         ?   ?   ?    0    1      ?          :   ?    :     0 ;
         ?   ?   ?    1    0      ?          :   ?    :     1 ;
         ?   ?   ?    x    1      ?          :   ?    :     x ;
         ?   ?   ?    1    x      ?          :   ?    :     x ;
         ?   ?   ?    0    0      ?          :   ?    :     x ;
         ?   ?   ?    p    1      ?          :   ?    :     - ;
         ?   ?   ?    1    p      ?          :   ?    :     - ;
         ?   ?   ?    ?    ?      *          :   ?    :     x ; 

    endtable
endprimitive


// Description : POSITIVE EDGE TRIGGERED J K FLIP FLOP, WITH
//            ASYNCHRONOUS ACTIVE LOW SET  ( Q OUTPUT UDP )  

primitive U_FJK_P_SB_NO (Q, J, K, CP, SB, NOTIFIER_REG); 
    output Q;
    reg    Q;  
    input  NOTIFIER_REG,
           J,K,
            CP,                                  // CLOCK.
            SB;                                  // SET INPUT.

    table
      // J   K   CP  SB  NOTIFIER_REG  :  Qtn : Qtn+1
         0   0  (01)  1     ?          :   ?  :   - ;         // Output retains the 
                                                              // present state if both 
                                                              // J and K are 0.
         0   1  (01)  1     ?          :   ?  :   0 ;         // Clocked J and K.
         1   0  (01)  1     ?          :   ?  :   1 ;    
         1   0  (01)  x     ?          :   ?  :   1 ;         // pessimism
         ?   ?   ?    x     ?          :   1  :   1 ;         // pessimism
         1   1  (01)  1     ?          :   0  :   1 ;         // Clocked toggle.
         1   1  (01)  1     ?          :   1  :   0 ;
         1   ?  (01)  x     ?          :   0  :   1 ;         // pessimism
         0   0  (?1)  1     ?          :   ?  :   - ;         //possible clocked JK
         0   1  (?1)  1     ?          :   0  :   0 ;   
         1   0  (?1)  1     ?          :   1  :   1 ;
         0   0  (0?)  1     ?          :   ?  :   - ;
         0   1  (0?)  1     ?          :   0  :   0 ;
         1   0  (0?)  1     ?          :   1  :   1 ;
         *   ?   ?    1     ?          :   ?  :   - ;         // Insensitive to 
         ?   *   ?    1     ?          :   ?  :   - ;         // transitions on J and K
                                                              // with steady clock.
         ?   ? (?0)   1     ?          :   ?  :   - ;         //ignore falling clock.
         ?   ? (1?)   1     ?          :   ?  :   - ;                                         
         x   0   r    1     ?          :   1  :   1 ;         // reducing pessimism for unknown J 
         x   1   r    1     ?          :   1  :   0 ;         // reducing pessimism for unknown J 
         0   x   r    1     ?          :   0  :   0 ;         // reducing pessimism for unknown K 
         1   x   r    1     ?          :   0  :   1 ;         // reducing pessimism for unknown K 
         x   0  (?1)  1     ?          :   1  :   1 ;        //possible clocked with
         0   x  (?1)  1     ?          :   0  :   0 ;        //possible J & K                                 
         x   0  (0?)  1     ?          :   1  :   1 ;
         0   x  (0?)  1     ?          :   0  :   0 ;
         ?   ?   ?    0     ?          :   ?  :   1 ;         // SET. 
         ?   ?   ?   (?1)   ?          :   ?  :   - ;         // Insensitive to a rising
         ?   ?   ?    ?     *          :   ?  :   x ;         // edge on SB
    endtable
endprimitive


// Description :  TOGGLE FLIP FLOP WITH CLEAR ACTIVE LOW / STANDARD DRIVE ( Q OUTPUT UDP )

primitive U_FT_P_RB_NO (Q, CP, RB, NOTIFIER_REG); 
    output Q; 
    reg    Q;
    input  NOTIFIER_REG,
           CP,                                     // Clock.
           RB;                                     // Clear input

    table
      //   CP       RB   NOTIFIER_REG  :  QTN  :  QTN+1
           r        1       ?          :   0   :    1   ;     // Toggle condition.
           r        ?       ?          :   1   :    0   ;
          (?0)      1       ?          :   ?   :    -   ;
          (1?)      1       ?          :   ?   :    -   ;
           ?        0       ?          :   ?   :    0   ;  
           ?       (?1)     ?          :   ?   :    -   ;     // inact
           0        x       ?          :   ?   :    0   ;     // pessimism
           ?        ?       *          :   ?   :    x   ; 
    endtable
endprimitive


// Description : POSITIVE EDGE TRIGGERED TOGGLE FLIP FLOP WITH
//            ASYNCHRONOUS SET ( ACTIVE LOW ) /STANDARD DRIVE ( Q OUTPUT UDP ).

primitive U_FT_P_SB_NO (Q, CP, SB, NOTIFIER_REG);  

    output Q; 
    reg    Q;
    input  NOTIFIER_REG,
           CP,                                     // Clock.
           SB;                                     // Set input 

    table
      //   CP       SB   NOTIFIER_REG  :  QTN  :  QTN+1
           r        1       ?          :   0   :    1   ;     // Toggle condition.
           r        1       ?          :   1   :    0   ;
          (?1)      1       ?          :   0   :    1   ;     // Toggle condition.
          (?1)      1       ?          :   1   :    0   ;
          (?0)      1       ?          :   ?   :    -   ;
          (1?)      1       ?          :   ?   :    -   ;
           ?       (?1)     ?          :   ?   :    -   ;
           ?        0       ?          :   ?   :    1   ;     
           r        x       ?          :   ?   :    x   ;     // pessimism
           ?        x       ?          :   ?   :    x   ;     // pessimism
           ?        ?       *          :   ?   :    x   ; 
   endtable                                  
endprimitive

    
// Description : POSITIVE EDGE TRIGGERED TOGGLE FLIP FLOP WITH ACTIVE LOW CLEAR
//            AND TOGGLE ENABLE ( Q OUTPUT UDP ) .

primitive U_FT_P_TE_RB_NO (Q, TE, CP, RB, NOTIFIER_REG); 

    output Q; 
    reg    Q;
    input  NOTIFIER_REG,
           TE,
           CP,                                     // Clock
           RB;                                     // Clear input  

    table
      //   TE     CP       RB   NOTIFIER_REG  :  QTN  :  QTN+1
           1      r        1       ?          :   0   :    1   ;     // Toggle condition.
           1      r        1       ?          :   1   :    0   ;                         
           1     (?0)      1       ?          :   ?   :    -   ;
           1     (1?)      1       ?          :   ?   :    -   ;                         
           ?      ?       (?1)     ?          :   ?   :    -   ;
           ?      ?        0       ?          :   ?   :    0   ;  
           ?      r        x       ?          :   ?   :    x   ;     // pessimism
           ?      ?        x       ?          :   ?   :    x   ;
           0     (??)      1       ?          :   ?   :    -   ;  
           *      ?        1       ?          :   ?   :    -   ;  
           ?      ?        ?       *          :   ?   :    x   ;  
    endtable
endprimitive


// Description :  D-LATCH, GATED  ACTIVE LOW / STANDARD DRIVE ( Q OUTPUT UDP )
//

primitive U_LD_N_NO (Q, D, GN, NOTI_REG); 

    output Q; 
    reg    Q;                               
    input  D,                // data
           GN,               // clock
           NOTI_REG;         // NOTIFY_REG

   table
     //  D       GN    NOTI_REG : Qt  :   Qt+1
         ?        1     ?       :  ?  :    -   ;   // inact
         0        0     ?       :  ?  :    0   ;   // open
         1        0     ?       :  ?  :    1   ;   // open
         ?        ?     *       :  ?  :    x   ;
   endtable
endprimitive



// Description : NEGATIVE LEVEL SENSITIVE D-TYPE LATCH WITH ACTIVE LOW
//            ASYNCHRONOUS SET ( Q OUTPUT UDP ).

primitive U_LD_N_SB_NO (Q, D, GN, SB, NOTI_REG); 

    output Q; 
    reg    Q;                               
    input  D,               // DATA
           GN,              // CLOCK
           SB,              // SET ACTIVE LOW
           NOTI_REG;        // NOTIFY REG

   table
     //  D      GN       SB    NOTI_REG : Qt  :   Qt+1                     
         ?      1        1      ?       :  ?  :    -   ; //closed gate                 
         ?      ?        0      ?       :  ?  :    1   ; // asynchro SET  
         0      0        1      ?       :  ?  :    0   ; // open
         1      0        ?      ?       :  ?  :    1   ; // open
         1      ?        ?      ?       :  1  :    1   ;   
         ?      ?        ?      *       :  ?  :    x   ;                     
   endtable
endprimitive


// Description : D-LATCH, GATED  CLEAR DIRECT /GATE ACTIVE HIGH ( Q OUTPUT UDP ) 

primitive U_LD_P_RB_NO (Q, D, G, RB, NOTI_REG); 

    output Q; 
    reg    Q;                               
    input  D,               // DATA
           G,               // CLOCK
           RB,              // CLEAR ACTIVE LOW
           NOTI_REG;        // NOTIFY REG

   table
     //  D      G       RB    NOTI_REG : Qt  :   Qt+1
         ?      1        1      ?       :  ?  :    -   ; //closed gate                 
         ?      ?        0      ?       :  ?  :    0   ; // asynchro RESET  
         0      0        ?      ?       :  ?  :    0   ; // open
         1      0        1      ?       :  ?  :    1   ; // open
         0      ?        ?      ?       :  0  :    0   ;   
         ?      ?        ?      *       :  ?  :    x   ;    
   endtable
endprimitive


// Description :  TWO TO ONE MULTIPLEXER

primitive U_MUX_2_1 (Q, A, B, SL);
    output Q;
    input A, B, SL;

    table
    //  A   B   SL  :   Q
        0   0   ?   :   0 ;
        1   1   ?   :   1 ;
        0   ?   0   :   0 ;
        1   ?   0   :   1 ;
        ?   0   1   :   0 ;
        ?   1   1   :   1 ;
    endtable
endprimitive    

   
// Description :  TWO TO ONE MULTIPLEXER  WITH INVERTING OUTPUT

primitive U_MUX_2_1_INV (Y, D0, D1, S);

    input D0, D1, S;
    output Y;

    table
//   D0  D1  S  : Y 
     0   ?   0  : 1 ;
     1   ?   0  : 0 ;
     ?   0   1  : 1 ;
     ?   1   1  : 0 ;
     0   0   ?  : 1 ;
     1   1   ?  : 0 ;
     endtable
endprimitive


// Description :  THREE TO ONE MULTIPLEXER WITH 2 SELECT CONTROLS
//             AND INVERTING OUTPUT 

primitive U_MUX_3_2 (Y, D0, D1, D2, S0, S1);

    input D0, D1, D2, S0, S1;
    output Y;

    table
//   D0  D1  D2   S0 S1 : Y
     0   ?   ?    0  0  : 0 ; 
     1   ?   ?    0  0  : 1 ;                                  
     ?   0   ?    1  0  : 0 ; 
     ?   1   ?    1  0  : 1 ;                                
     ?   ?   0    ?  1  : 0 ; 
     ?   ?   1    ?  1  : 1 ;                                                                                         
     0   0   0    ?  ?  : 0 ;     //pessimism                       
     1   1   1    ?  ?  : 1 ; 
     0   0   ?    ?  0  : 0 ;     //pessimism
     1   1   ?    ?  0  : 1 ;     //pessimism
     0   ?   0    0  ?  : 0 ; 
     1   ?   1    0  ?  : 1 ; 
     ?   0   0    1  ?  : 0 ; 
     ?   1   1    1  ?  : 1 ; 
     endtable
endprimitive

// Description :  THREE TO ONE MULTIPLEXER WITH 2 SELECT CONTROLS
//             AND INVERTING OUTPUT 

primitive U_MUX_3_2_INV (Y, D0, D1, D2, S0, S1);

    input D0, D1, D2, S0, S1;
    output Y;

    table
//   D0  D1  D2   S0 S1 : Y                       
     0   ?   ?    0  0  : 1 ; 
     1   ?   ?    0  0  : 0 ; 
     ?   0   ?    1  0  : 1 ; 
     ?   1   ?    1  0  : 0 ;                                  
     ?   ?   0    ?  1  : 1 ; 
     ?   ?   1    ?  1  : 0 ;                                  
     0   0   0    ?  ?  : 1 ;     //pessimism                       
     1   1   1    ?  ?  : 0 ; 
     0   0   ?    ?  0  : 1 ;     //pessimism
     1   1   ?    ?  0  : 0 ;     //pessimism
     0   ?   0    0  ?  : 1 ; 
     1   ?   1    0  ?  : 0 ; 
     ?   0   0    1  ?  : 1 ; 
     ?   1   1    1  ?  : 0 ;                  
     endtable
endprimitive


// Description :  FOUR TO ONE MULTIPLEXER WITH 2 SELECT CONTROLS

primitive U_MUX_4_2 (Y, D0, D1, D2, D3, S0, S1);

    input D0, D1, D2, D3, S0, S1;
    output Y;
   
    table
//   D0  D1  D2 D3  S0  S1 : Y                       
     0   ?   ?  ?    0  0  : 0 ; 
     1   ?   ?  ?    0  0  : 1 ;                                  
     ?   0   ?  ?    1  0  : 0 ; 
     ?   1   ?  ?    1  0  : 1 ; 
     ?   ?   0  ?    0  1  : 0 ; 
     ?   ?   1  ?    0  1  : 1 ;                                  
     ?   ?   ?  0    1  1  : 0 ; 
     ?   ?   ?  1    1  1  : 1 ;                                  
     0   0   0   0   ?  ?  : 0 ; 
     1   1   1   1   ?  ?  : 1 ; 
     0   0   ?   ?   ?  0  : 0 ;    
     1   1   ?   ?   ?  0  : 1 ; 
     ?   ?   0   0   ?  1  : 0 ; 
     ?   ?   1   1   ?  1  : 1 ; 
     0   ?   0   ?   0  ?  : 0 ; 
     1   ?   1   ?   0  ?  : 1 ; 
     ?   0   ?   0   1  ?  : 0 ; 
     ?   1   ?   1   1  ?  : 1 ; 
     endtable
endprimitive


// Description :  FOUR TO ONE MULTIPLEXER WITH 2 SELECT CONTROLS
//             AND INVERTING OUTPUT    

primitive U_MUX_4_2_INV (Y, D0, D1, D2, D3, S0, S1);

    input D0, D1, D2, D3, S0, S1;
    output Y;

    table
//   D0  D1  D2 D3   S0 S1 : Y                      
     0   ?   ?  ?    0  0  : 1 ; 
     1   ?   ?  ?    0  0  : 0 ;                                  
     ?   0   ?  ?    1  0  : 1 ; 
     ?   1   ?  ?    1  0  : 0 ;                                  
     ?   ?   0  ?    0  1  : 1 ; 
     ?   ?   1  ?    0  1  : 0 ;                                  
     ?   ?   ?  0    1  1  : 1 ; 
     ?   ?   ?  1    1  1  : 0 ;                                  
     0   0   0   0   ?  ?  : 1 ;     //pessimism                       
     1   1   1   1   ?  ?  : 0 ; 
     0   0   ?   ?   ?  0  : 1 ;     //pessimism
     1   1   ?   ?   ?  0  : 0 ;     //pessimism
     ?   ?   0   0   ?  1  : 1 ;     //pessimism
     ?   ?   1   1   ?  1  : 0 ;     //pessimism
     0   ?   0   ?   0  ?  : 1 ; 
     1   ?   1   ?   0  ?  : 0 ; 
     ?   0   ?   0   1  ?  : 1 ; 
     ?   1   ?   1   1  ?  : 0 ; 
     endtable
endprimitive


// Description  : BOOL_SCAN_DFF with rising clock  q and qb outputs

primitive U_dfse_Q(Q,C,E,SE,D,SD,notifier);

output Q;
reg    Q;
input  C,E,SE,D,SD,notifier;

table
// C    E    SE   D    SD   nfr : - : Q;
   r    ?    1    ?    1    ?   : ? : 1;
   r    ?    1    ?    0    ?   : ? : 0;
   r    1    0    0    ?    ?   : ? : 0;
   r    1    0    1    ?    ?   : ? : 1;
   n    ?    ?    ?    ?    ?   : ? : -;
   ?    *    ?    ?    ?    ?   : ? : -;
   ?    ?    *    ?    ?    ?   : ? : -;
   ?    ?    ?    *    ?    ?   : ? : -;
   ?    ?    ?    ?    *    ?   : ? : -;
   (??) 0    0    ?    ?    ?   : ? : -;
   ?    ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : BOOL_SCAN_DFF with rising clock low clear q and qb outputs

primitive U_dfsec_Q(Q,C,RN,E,SE,D,SD,notifier);

output Q;
reg    Q;
input  C,RN,E,SE,D,SD,notifier;

table
// C    RN   E    SE   D    SD   nfr : - : Q;
   r    1    1    0    1    ?    ?   : ? : 1;
   r    1    1    0    0    ?    ?   : ? : 0;
   r    1    ?    1    ?    0    ?   : ? : 0;
   r    1    ?    1    ?    1    ?   : ? : 1;
   ?    0    ?    ?    ?    ?    ?   : ? : 0;
   n    1    ?    ?    ?    ?    ?   : ? : -;
   ?    x    ?    ?    ?    ?    ?   : ? : x;
   ?    (?1) ?    ?    ?    ?    ?   : ? : -;
   ?    1    *    ?    ?    ?    ?   : ? : -;
   ?    1    ?    *    ?    ?    ?   : ? : -;
   ?    1    ?    ?    *    ?    ?   : ? : -;
   ?    1    ?    ?    ?    *    ?   : ? : -;
   (??) 1    0    0    ?    ?    ?   : ? : -;
   ?    ?    ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : BOOL_SCAN_DFF with rising clock low preset q and qb outputs 

primitive U_dfsep_Q(Q,C,SE,E,SN,D,SD,notifier);

output Q;
reg    Q;
input  C,SE,E,SN,D,SD,notifier;

table
// C    SE   E    SN   D    SD   nfr : - : Q;
   r    0    1    1    0    ?    ?   : ? : 0;
   r    0    1    1    1    ?    ?   : ? : 1;
   r    1    ?    1    ?    0    ?   : ? : 0;
   r    1    ?    1    ?    1    ?   : ? : 1;
   ?    ?    ?    0    ?    ?    ?   : ? : 1;
   ?    ?    ?    x    ?    ?    ?   : ? : x;
   n    ?    ?    1    ?    ?    ?   : ? : -;
   ?    *    ?    1    ?    ?    ?   : ? : -;
   ?    ?    *    1    ?    ?    ?   : ? : -;
   ?    ?    ?    (?1) ?    ?    ?   : ? : -;
   ?    ?    ?    1    *    ?    ?   : ? : -;
   ?    ?    ?    1    ?    *    ?   : ? : -;
   (??) 0    0    1    ?    ?    ?   : ? : -;
   ?    ?    ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : SCAN_JKFF with rising clock  q and qb outputs

primitive U_jks_Q(Q,C,SE,J,K,SD,notifier);

output Q;
reg    Q;
input  C,SE,J,K,SD,notifier;

table
// C    SE   J    K    SD   nfr : - : Q;
   r    0    1    1    ?    ?   : 0 : 1;
   r    0    1    1    ?    ?   : 1 : 0;
   r    1    ?    ?    0    ?   : ? : 0;
   r    1    ?    ?    1    ?   : ? : 1;
   r    0    0    1    ?    ?   : ? : 0;
   r    0    1    0    ?    ?   : ? : 1;
   n    ?    ?    ?    ?    ?   : ? : -;
   ?    *    ?    ?    ?    ?   : ? : -;
   ?    ?    *    ?    ?    ?   : ? : -;
   ?    ?    ?    *    ?    ?   : ? : -;
   ?    ?    ?    ?    *    ?   : ? : -;
   (??) 0    0    0    0    ?   : ? : -;
   (??) 0    0    0    1    ?   : ? : -;
   ?    ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : SCAN_JKFF with rising clock low clear q and qb outputs

primitive U_jksc_Q(Q,C,RN,SE,J,K,SD,notifier);

output Q;
reg    Q;
input  C,RN,SE,J,K,SD,notifier;

table
// C    RN   SE   J    K    SD   nfr : - : Q;
   r    1    0    1    0    ?    ?   : ? : 1;
   r    1    0    0    1    ?    ?   : ? : 0;
   r    1    0    1    1    ?    ?   : 0 : 1;
   r    1    0    1    1    ?    ?   : 1 : 0;
   r    1    1    ?    ?    1    ?   : ? : 1;
   r    1    1    ?    ?    0    ?   : ? : 0;
   ?    0    ?    ?    ?    ?    ?   : ? : 0;
   n    1    ?    ?    ?    ?    ?   : ? : -;
   ?    (?1) ?    ?    ?    ?    ?   : ? : -;
   ?    x    ?    ?    ?    ?    ?   : ? : x;
   ?    1    *    ?    ?    ?    ?   : ? : -;
   ?    1    ?    *    ?    ?    ?   : ? : -;
   ?    1    ?    ?    *    ?    ?   : ? : -;
   ?    1    ?    ?    ?    *    ?   : ? : -;
   (??) 1    0    0    0    0    ?   : ? : -;
   (??) 1    0    0    0    1    ?   : ? : -;
   ?    ?    ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : SCAN_JKFF with rising clock low preset q and qb outputs

primitive U_jksp_Q(Q,C,SE,SN,J,K,SD,notifier);

output Q;
reg    Q;
input  C,SE,SN,J,K,SD,notifier;

table
// C    SE   SN   J    K    SD   nfr : - : Q;
   r    0    1    0    1    ?    ?   : ? : 0;
   r    0    1    1    1    ?    ?   : 0 : 1;
   r    0    1    1    1    ?    ?   : 1 : 0;
   r    1    1    ?    ?    1    ?   : ? : 1;
   r    0    1    1    0    ?    ?   : ? : 1;
   r    1    1    ?    ?    0    ?   : ? : 0;
   ?    ?    0    ?    ?    ?    ?   : ? : 1;
   n    ?    1    ?    ?    ?    ?   : ? : -;
   ?    *    1    ?    ?    ?    ?   : ? : -;
   ?    ?    (?1) ?    ?    ?    ?   : ? : -;
   ?    ?    x    ?    ?    ?    ?   : ? : x;
   ?    ?    1    *    ?    ?    ?   : ? : -;
   ?    ?    1    ?    *    ?    ?   : ? : -;
   ?    ?    1    ?    ?    *    ?   : ? : -;
   (??) 0    1    0    0    0    ?   : ? : -;
   (??) 0    1    0    0    1    ?   : ? : -;
   ?    ?    ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : TFF with rising edge clock,low preset,low clear,q and qb outputs

primitive U_tfcp_Q(Q,C,RN,SN,notifier);

output Q;
reg    Q;
input  C,RN,SN,notifier;

table
// C    RN   SN   nfr : - : Q;
   ?    1    0    ?   : ? : 1;
   ?    0    0    ?   : ? : 0;
   ?    0    1    ?   : ? : 0;
   r    1    1    ?   : 0 : 1;
   r    1    1    ?   : 1 : 0;
   n    1    1    ?   : ? : -;
   ?    (?1) 1    ?   : ? : -;
   ?    1    (?1) ?   : ? : -;
   ?    x    ?    ?   : ? : x;
   ?    ?    x    ?   : ? : x;
   ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : TFF with rising edge clock,low preset,low clear,high enable,q and qb outputs

primitive U_tfecp_Q(Q,C,RN,SN,T,notifier);

output Q;
reg    Q;
input  C,RN,SN,T,notifier;

table
// C    RN   SN   T    nfr : - : Q;
   ?    1    0    ?    ?   : ? : 1;
   ?    0    0    ?    ?   : ? : 0;
   ?    0    1    ?    ?   : ? : 0;
   r    1    1    1    ?   : 0 : 1;
   r    1    1    1    ?   : 1 : 0;
   n    1    1    ?    ?   : ? : -;
   ?    (?1) 1    ?    ?   : ? : -;
   ?    1    (?1) ?    ?   : ? : -;
   ?    x    ?    ?    ?   : ? : x;
   ?    ?    x    ?    ?   : ? : x;
   ?    1    1    *    ?   : ? : -;
   (??) 1    1    0    ?   : ? : -;
   ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : TFF with rising edge clock,low preset,high enable,q and qb outputs

primitive U_tfep_Q(Q,SN,C,T,notifier);

output Q;
reg    Q;
input  SN,C,T,notifier;

table
// SN   C    T    nfr : - : Q;
   1    r    1    ?   : 0 : 1;
   1    r    1    ?   : 1 : 0;
   0    ?    ?    ?   : ? : 1;
   (?1) ?    ?    ?   : ? : -;
   1    n    ?    ?   : ? : -;  
   1    ?    *    ?   : ? : -;
   1    (??) 0    ?   : ? : -;
   x    ?    ?    ?   : ? : x;
   ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : SCAN_TFF with rising clock low clear q and qb outputs

primitive U_tfsc_Q(Q,C,RN,SE,SD,notifier);

output Q;
reg    Q;
input  C,RN,SE,SD,notifier;

table
// C    RN   SE   SD   nfr : - : Q;
   r    1    0    ?    ?   : 0 : 1;
   r    1    0    ?    ?   : 1 : 0;
   ?    0    ?    ?    ?   : ? : 0;
   r    1    1    0    ?   : ? : 0;
   r    1    1    1    ?   : ? : 1;
   n    1    ?    ?    ?   : ? : -;
   ?    (?1) ?    ?    ?   : ? : -;
   ?    1    *    ?    ?   : ? : -;
   ?    1    ?    *    ?   : ? : -;
   ?    x    ?    ?    ?   : ? : x;
   ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive


// Description  : SCAN_TFF with rising clock low clear q and qb outputs

primitive U_tfsp_Q(Q,C,SE,SN,SD,notifier);

output Q;
reg    Q;
input  C,SE,SN,SD,notifier;

table
// C    SE   SN   SD   nfr : - : Q;
   r    1    1    0    ?   : ? : 0;
   r    0    1    ?    ?   : 0 : 1;
   r    0    1    ?    ?   : 1 : 0;
   r    1    1    1    ?   : ? : 1;
   ?    ?    0    ?    ?   : ? : 1;
   n    ?    1    ?    ?   : ? : -;
   ?    *    1    ?    ?   : ? : -;
   ?    ?    (?1) ?    ?   : ? : -;
   ?    ?    1    *    ?   : ? : -;
   r    ?    x    ?    ?   : ? : x;
   ?    ?    x    ?    ?   : ? : x;
   ?    ?    ?    ?    *   : ? : x;
endtable
endprimitive



// Description : D-LATCH, GATED  CLEAR DIRECT /GATE ACTIVE LOW ( Q OUTPUT UDP ) 

primitive U_LD_P_RB_SB_NO (Q, D, GB, RB, SB,NOTI_REG); 

    output Q; 
    reg    Q;                               
    input  D,               // DATA
           GB,              // CLOCK
           RB,              // CLEAR ACTIVE LOW
           SB,              // SET ACTIVE LOW
           NOTI_REG;        // NOTIFY REG

   table
     //  D       GB     RB    SB    NOTI_REG : Qt  :   Qt+1
         ?       1      1     1      ?       :  ?  :    -   ; // closed gate
         ?       ?      0     1      ?       :  ?  :    0   ; // asynchro clear
         ?       ?      1     0      ?       :  ?  :    1   ; // asynchro set
         1       0      1     ?      ?       :  ?  :    1   ; // set
         0       0      ?     1      ?       :  ?  :    0   ; // reset
         0       ?      ?     1      ?       :  0  :    0   ; // comb
         1       ?      1     ?      ?       :  1  :    1   ; // comb
         ?       ?      ?     ?      *       :  ?  :    x   ; // timing violation
   endtable
endprimitive


// Description  : BOOL_SCAN_DFF with rising clock low clear low pres q and qb outputs

primitive U_dfsecp_Q(Q,C,RN,SN,E,SE,D,SD,notifier);

output Q;
reg    Q;
input  C,RN,SN,E,SE,D,SD,notifier;

table
// C    RN   SN    E    SE   D    SD   nfr : - : Q;
   r    ?    1     1    0    0    ?    ?   : ? : 0;    //D=0 SE0
   r    1    ?     1    0    1    ?    ?   : ? : 1;    //D=1 SE0
   r    ?    1     ?    1    ?    0    ?   : ? : 0;    //SD=0 SE1
   r    1    ?     ?    1    ?    1    ?   : ? : 1;    //SD=1 SE1
   n    1    1     ?    ?    ?    ?    ?   : ? : -;    // falling DC
   ?    (?1) 1     ?    ?    ?    ?    ?   : ? : -;    // reset inactive
   ?    1    (?1)  ?    ?    ?    ?    ?   : ? : -;    // set inactive
   ?    1    1     ?    ?    *    ?    ?   : ? : -;    // data change
   ?    1    1     ?    ?    ?    *    ?   : ? : -;    // Sdata change
   ?    1    1     ?    *    ?    ?    ?   : ? : -;    // SE change
   ?    1    1     *    ?    ?    ?    ?   : ? : -;    // E change bka
   (??) 1    1     0    0    ?    ?    ?   : ? : -;    // E change bka
   ?    1    0     ?    ?    ?    ?    ?   : ? : 1;    // set
   ?    0    1     ?    ?    ?    ?    ?   : ? : 0;    // reset 
   ?    x    ?     ?    ?    ?    ?    ?   : ? : x;    // reset x
   ?    ?    x     ?    ?    ?    ?    ?   : ? : x;    // set x
   ?    ?    ?     ?    ?    ?    ?    *   : ? : x;    // timing violation
      
endtable
endprimitive


// Description  : SCAN_JKFF with rising clock low clear low pre q and qb outputs    

primitive U_jkscp_Q(Q,C,RN,SN,SE,J,K,SD,notifier);

output Q;
reg    Q;
input  C,RN,SN,SE,J,K,SD,notifier;

table
// C    RN   SN   SE   J    K    SD   nfr : - : Q;
   r    1    1    0    1    0    ?    ?   : ? : 1;   //SE0 J1
   r    1    1    0    0    1    ?    ?   : ? : 0;   //SE0 k1
   r    1    1    0    1    1    ?    ?   : 0 : 1;   //SE0 JK1
   r    1    1    0    1    1    ?    ?   : 1 : 0;   //SE0 JK1
   r    1    1    1    ?    ?    1    ?   : ? : 1;   //SE1 SD1
   r    1    1    1    ?    ?    0    ?   : ? : 0;   //SE1 SD0
   ?    1    0    ?    ?    ?    ?    ?   : ? : 1;   //set
   ?    0    1    ?    ?    ?    ?    ?   : ? : 0;   //reset
   ?    0    0    ?    ?    ?    ?    ?   : ? : 0;   //reset + set
   n    1    1    ?    ?    ?    ?    ?   : ? : -;   //falling
   ?    (?1) 1    ?    ?    ?    ?    ?   : ? : -;   //res inactive
   ?    1    (?1) ?    ?    ?    ?    ?   : ? : -;   //set inactive
   ?    1    1    *    ?    ?    ?    ?   : ? : -;   //change SE
   ?    1    1    ?    *    ?    ?    ?   : ? : -;   //change J
   ?    1    1    ?    ?    *    ?    ?   : ? : -;   //change K
   ?    1    1    ?    ?    ?    *    ?   : ? : -;   //change SD
   (??) 1    1    0    0    0    ?    ?   : ? : -;   //SE0 JK0
   ?    x    ?    ?    ?    ?    ?    ?   : ? : x;   //reset x
   ?    ?    x    ?    ?    ?    ?    ?   : ? : x;   //set x
   ?    ?    ?    ?    ?    ?    ?    *   : ? : x;   //timing vio
endtable
endprimitive


// Description  : SCAN_TFF with rising clock low clear q and qb outputs

primitive U_tfscp_Q(Q,C,RN,SN,SE,SD,notifier);

output Q;
reg    Q;
input  C,RN,SN,SE,SD,notifier;

table
// C    RN   SN   SE   SD   nfr : - : Q;
   r    1    1    0    ?    ?   : 0 : 1;   //toggle
   r    1    1    0    ?    ?   : 1 : 0;   //toggle
   ?    0    1    ?    ?    ?   : ? : 0;   //resn0
   ?    1    0    ?    ?    ?   : ? : 1;   //setn0
   ?    0    0    ?    ?    ?   : ? : 0;   //ressetn0
   r    1    1    1    0    ?   : ? : 0;   //se1 sd0
   r    1    1    1    1    ?   : ? : 1;   //se1 sd1
   n    1    1    ?    ?    ?   : ? : -;   //falling
   ?    (?1) 1    ?    ?    ?   : ? : -;   //resn inact
   ?    1    (?1) ?    ?    ?   : ? : -;   //setn inact
   ?    1    1    *    ?    ?   : ? : -;   //change se
   ?    1    1    ?    *    ?   : ? : -;   //cahnge sd
   ?    x    ?    ?    ?    ?   : ? : x;   //resnx
   ?    ?    x    ?    ?    ?   : ? : x;   //setnx
   ?    ?    ?    ?    ?    *   : ? : x;   //timing vio
endtable
endprimitive


// Description  : ESCAN_TFF with rising clock low clear q and qb outputs

primitive U_tfsec_Q(Q,C,RN,T,SE,SD,notifier);

output Q;
reg    Q;
input  C,RN,T,SE,SD,notifier;

table
// C    RN   T   SE   SD   nfr : - : Q;
   r    1    1    0    ?    ?   : 0 : 1;   //toggle
   r    1    1    0    ?    ?   : 1 : 0;   //toggle
   r    1    0    0    ?    ?   : ? : -;   //t0 no toggle
   ?    0    ?    ?    ?    ?   : ? : 0;   //resn0
   r    1    ?    1    0    ?   : ? : 0;   //se1 sd0
   r    1    ?    1    1    ?   : ? : 1;   //se1 sd1
   n    1    ?    ?    ?    ?   : ? : -;   //falling
   ?    (?1) ?    ?    ?    ?   : ? : -;   //resn inact
   ?    1    *    ?    ?    ?   : ? : -;   //change t
   ?    1    ?    *    ?    ?   : ? : -;   //change se
   ?    1    ?    ?    *    ?   : ? : -;   //change sd
   ?    x    ?    ?    ?    ?   : ? : x;   //resnx
   ?    ?    ?    ?    ?    *   : ? : x;   //timing vio
endtable
endprimitive


// Description  : SCAN_TFF with rising clock low clear q and qb outputs

primitive U_tfsecp_Q(Q,C,RN,SN,T,SE,SD,notifier);

output Q;
reg    Q;
input  C,RN,SN,T,SE,SD,notifier;

table
// C    RN   SN   T   SE   SD   nfr : - : Q;
   r    1    1    1   0    ?    ?   : 0 : 1;   //toggle
   r    1    1    1   0    ?    ?   : 1 : 0;   //toggle
   r    1    1    0   0    ?    ?   : ? : -;   //T0 notoggle
   ?    0    1    ?   ?    ?    ?   : ? : 0;   //resn0
   ?    1    0    ?   ?    ?    ?   : ? : 1;   //setn0
   ?    0    0    ?   ?    ?    ?   : ? : 0;   //ressetn0
   r    1    1    ?   1    0    ?   : ? : 0;   //se1 sd0
   r    1    1    ?   1    1    ?   : ? : 1;   //se1 sd1
   n    1    1    ?   ?    ?    ?   : ? : -;   //falling
   ?    (?1) 1    ?   ?    ?    ?   : ? : -;   //resn inact
   ?    1    (?1) ?   ?    ?    ?   : ? : -;   //setn inact
   ?    1    1    *   ?    ?    ?   : ? : -;   //change t
   ?    1    1    ?   *    ?    ?   : ? : -;   //change se
   ?    1    1    ?   ?    *    ?   : ? : -;   //change sd
   ?    x    ?    ?   ?    ?    ?   : ? : x;   //resnx
   ?    ?    x    ?   ?    ?    ?   : ? : x;   //setnx
   ?    ?    ?    ?   ?    ?    *   : ? : x;   //timing vio
endtable
endprimitive


// Description  : SCAN_TFF with rising clock low clear q and qb outputs

primitive U_tfsep_Q(Q,C,SN,T,SE,SD,notifier);

output Q;
reg    Q;
input  C,SN,T,SE,SD,notifier;

table
// C   SN   T   SE   SD   nfr : - : Q;
   r    1    1   0    ?    ?   : 0 : 1;   //toggle
   r    1    1   0    ?    ?   : 1 : 0;   //toggle
   r    1    0   0    ?    ?   : ? : -;   //T0 notoggle
   ?    0    ?   ?    ?    ?   : ? : 1;   //setn0
   r    1    ?   1    0    ?   : ? : 0;   //se1 sd0
   r    1    ?   1    1    ?   : ? : 1;   //se1 sd1
   n    1    ?   ?    ?    ?   : ? : -;   //falling
   ?   (?1)  ?   ?    ?    ?   : ? : -;   //setn inact
   ?    1    *   ?    ?    ?   : ? : -;   //change t
   ?    1    ?   *    ?    ?   : ? : -;   //change se
   ?    1    ?   ?    *    ?   : ? : -;   //change sd
   ?    x    ?   ?    ?    ?   : ? : x;   //setnx
   ?    ?    ?   ?    ?    *   : ? : x;   //timing vio
endtable
endprimitive







// Verilog simulation library for all c35 CORELIB voltages
// $Id: c35_CORELIB.v,v 3.70 2005/10/01 13:12:19 bka
// Owner: austriamicrosystems AG HIT-Kit: Digital

`celldefine
`timescale 1ns / 1ps

// Description : 1 bit HALF_ADDER

module ADD21 (A,B,CO,S);

output CO,S;
input A,B;

xor (S,B,A);
and (CO,B,A);

`ifdef functional
`else
specify
 (A => CO) = (1,1);
 (B => CO) = (1,1);
 (A => S ) = (1,1);
 (B => S ) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 1 bit HALF_ADDER

module ADD22 (A,B,CO,S);

output CO,S;
input A,B;

xor (S,B,A);
and (CO,B,A);

`ifdef functional
`else
specify
 (A => S ) = (1,1);
 (B => S ) = (1,1);
 (A => CO) = (1,1);
 (B => CO) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 1 bit FULL_ADDER

module ADD31 (A,B,CI,CO,S);

output CO,S;
input A,B,CI;

U_ADDR2_S (S,A,B,CI);
U_ADDR2_C (CO,A,B,CI);

`ifdef functional
`else
specify
 (A => S ) = (1,1);
 (B => S ) = (1,1);
 (CI => S ) = (1,1);
 (A => CO) = (1,1);
 (B => CO) = (1,1);
 (CI => CO) = (1,1); 
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 1 bit FULL_ADDER

module ADD32 (A,B,CI,CO,S);

output CO,S;
input A,B,CI;

U_ADDR2_S (S,A,B,CI);
U_ADDR2_C (CO,A,B,CI);

`ifdef functional
`else
specify
 (A => S ) = (1,1);
 (B => S ) = (1,1);
 (CI => S ) = (1,1);
 (A => CO) = (1,1);
 (B => CO) = (1,1);
 (CI => CO) = (1,1); 
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-1 AND-OR-INVERT

module AOI210 (A,B,C,Q);

output Q;
input A,B,C;

and (g_1_out,A,B);
nor (Q,g_1_out,C);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-1 AND-OR-INVERT

module AOI211 (A,B,C,Q);

output Q;
input A,B,C;

and (g_1_out,A,B);
nor (Q,g_1_out,C);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 1-1-2 AND-OR-INVERT

module AOI2110 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_3_out,B,A);
nor (Q,D,C,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 1-1-2 AND-OR-INVERT

module AOI2111 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_3_out,B,A);
nor (Q,D,C,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 1-1-2 AND-OR-INVERT

module AOI2112 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_3_out,B,A);
nor (Q,D,C,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-1 AND-OR-INVERT

module AOI212 (A,B,C,Q);

output Q;
input A,B,C;

and (g_1_out,A,B);
nor (Q,g_1_out,C);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-2 AND-OR-INVERT

module AOI220 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_1_out,B,A);
and (g_2_out,D,C);
nor (Q,g_1_out,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (D => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-2 AND-OR-INVERT

module AOI221 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_1_out,B,A);
and (g_2_out,D,C);
nor (Q,g_1_out,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (D => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-2 AND-OR-INVERT

module AOI222 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_1_out,B,A);
and (g_2_out,D,C);
nor (Q,g_1_out,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (D => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 3-1 AND-OR-INVERT

module AOI310 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_1_out,B,A,C);
nor (Q,g_1_out,D);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 3-1 AND-OR-INVERT

module AOI311 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_1_out,B,A,C);
nor (Q,g_1_out,D);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 3-1 AND-OR-INVERT

module AOI312 (A,B,C,D,Q);

output Q;
input A,B,C,D;

and (g_1_out,B,A,C);
nor (Q,g_1_out,D);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module BUF12 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module BUF15 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module BUF2 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module BUF4 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module BUF6 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module BUF8 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active high enable

module BUFE10 (A,E,Q);

output Q;
input A,E;

bufif1 (Q,A,E);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (E => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active high enable

module BUFE12 (A,E,Q);

output Q;
input A,E;

bufif1 (Q,A,E);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (E => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active high enable

module BUFE15 (A,E,Q);

output Q;
input A,E;

bufif1 (Q,A,E);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (E => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active high enable

module BUFE2 (A,E,Q);

output Q;
input A,E;

bufif1 (Q,A,E);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (E => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active high enable

module BUFE4 (A,E,Q);

output Q;
input A,E;

bufif1 (Q,A,E);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (E => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active high enable

module BUFE6 (A,E,Q);

output Q;
input A,E;

bufif1 (Q,A,E);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (E => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active high enable

module BUFE8 (A,E,Q);

output Q;
input A,E;

bufif1 (Q,A,E);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (E => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active low enable

module BUFT10 (A,EN,Q);

output Q;
input A,EN;

bufif0 (Q,A,EN);

`ifdef functional
`else
specify
 (EN => Q) = (1,1);
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active low enable

module BUFT12 (A,EN,Q);

output Q;
input A,EN;

bufif0 (Q,A,EN);

`ifdef functional
`else
specify
 (EN => Q) = (1,1);
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active low enable

module BUFT15 (A,EN,Q);

output Q;
input A,EN;

bufif0 (Q,A,EN);

`ifdef functional
`else
specify
 (EN => Q) = (1,1);
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active low enable

module BUFT2 (A,EN,Q);

output Q;
input A,EN;

bufif0 (Q,A,EN);

`ifdef functional
`else
specify
 (EN => Q) = (1,1);
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active low enable

module BUFT4 (A,EN,Q);

output Q;
input A,EN;

bufif0 (Q,A,EN);

`ifdef functional
`else
specify
 (EN => Q) = (1,1);
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active low enable

module BUFT6 (A,EN,Q);

output Q;
input A,EN;

bufif0 (Q,A,EN);

`ifdef functional
`else
specify
 (EN => Q) = (1,1);
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TRISTATE BUFFER with active low enable

module BUFT8 (A,EN,Q);

output Q;
input A,EN;

bufif0 (Q,A,EN);

`ifdef functional
`else
specify
 (EN => Q) = (1,1);
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module CLKBU12 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module CLKBU15 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module CLKBU2 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module CLKBU4 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module CLKBU6 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module CLKBU8 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN0 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN1 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN10 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN12 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN15 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN2 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN3 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN4 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN6 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module CLKIN8 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : DFF with rising edge clock,q and qb outputs

module DF1 (C,D,Q,QN);

output Q,QN;
input C,D;

`ifdef functional
U_FD_P_NO (buf_Q,D,C,1'b1);
`else
reg notifier;
U_FD_P_NO (buf_Q,D,C,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge D, posedge C, 0, notifier);
 $setup(negedge D, posedge C, 0, notifier);
 $hold(posedge C, negedge D, 0, notifier);
 $hold(posedge C, posedge D, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : DFF with rising edge clock,q and qb outputs

module DF3 (C,D,Q,QN);

output Q,QN;
input C,D;

`ifdef functional
U_FD_P_NO (buf_Q,D,C,1'b1);
`else
reg notifier;
U_FD_P_NO (buf_Q,D,C,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge D, posedge C, 0, notifier);
 $setup(negedge D, posedge C, 0, notifier);
 $hold(posedge C, negedge D, 0, notifier);
 $hold(posedge C, posedge D, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : DFF with rising edge clock,low clear,q and qb outputs

module DFC1 (C,D,Q,QN,RN);

output Q,QN;
input C,D,RN;

`ifdef functional
U_FD_P_RB_NO (buf_Q,D,C,RN,1'b1);
`else
reg notifier;
U_FD_P_RB_NO (buf_Q,D,C,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge C, 0, notifier);
 $setup(negedge D, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $hold(posedge C, negedge D, 0, notifier);
 $hold(posedge C, posedge D, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : DFF with rising edge clock,low clear,q and qb outputs

module DFC3 (C,D,Q,QN,RN);

output Q,QN;
input C,D,RN;

`ifdef functional
U_FD_P_RB_NO (buf_Q,D,C,RN,1'b1);
`else
reg notifier;
U_FD_P_RB_NO (buf_Q,D,C,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge C, 0, notifier);
 $setup(negedge D, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $hold(posedge C, negedge D, 0, notifier);
 $hold(posedge C, posedge D, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : DFF with rising edge clock,low preset,low clear,q and qb outputs

module DFCP1 (C,D,Q,QN,RN,SN);

output Q,QN;
input C,D,RN,SN;

`ifdef functional
U_FD_P_RB_SB_NO (buf_Q,D,C,RN,SN,1'b1);
`else
reg notifier;
U_FD_P_RB_SB_NO (buf_Q,D,C,RN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge C, 0, notifier);
 $setup(negedge D, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, negedge D, 0, notifier);
 $hold(posedge C, posedge D, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
 endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : DFF with rising edge clock,low preset,low clear,q and qb outputs

module DFCP3 (C,D,Q,QN,RN,SN);

output Q,QN;
input C,D,RN,SN;

`ifdef functional
U_FD_P_RB_SB_NO (buf_Q,D,C,RN,SN,1'b1);
`else
reg notifier;
U_FD_P_RB_SB_NO (buf_Q,D,C,RN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge C, 0, notifier);
 $setup(negedge D, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, negedge D, 0, notifier);
 $hold(posedge C, posedge D, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
 endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_DFF with rising clock q and qb outputs

module DFE1 (C,D,E,Q,QN);

output Q,QN;
input C,D,E;

buf (Q,buf_Q);
not (QN,buf_Q);

U_MUX_2_1 (BOOL_OUT,buf_Q,D,E);
`ifdef functional
U_FD_P_NO (buf_Q,BOOL_OUT,C,1'b1);
`else
reg notifier;
U_FD_P_NO (buf_Q,BOOL_OUT,C,notifier);
`endif

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge D, posedge C &&& (E), 0, notifier);
 $setup(negedge D, posedge C &&& (E), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $hold(posedge C &&& (E), negedge D, 0, notifier);
 $hold(posedge C &&& (E), posedge D, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_DFF with rising clock q and qb outputs

module DFE3 (C,D,E,Q,QN);

output Q,QN;
input C,D,E;

buf (Q,buf_Q);
not (QN,buf_Q);

U_MUX_2_1 (BOOL_OUT,buf_Q,D,E);
`ifdef functional
U_FD_P_NO (buf_Q,BOOL_OUT,C,1'b1);
`else
reg notifier;
U_FD_P_NO (buf_Q,BOOL_OUT,C,notifier);
`endif

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge D, posedge C &&& (E), 0, notifier);
 $setup(negedge D, posedge C &&& (E), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $hold(posedge C &&& (E), negedge D, 0, notifier);
 $hold(posedge C &&& (E), posedge D, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_DFF with rising clock low clear q and qb outputs

module DFEC1 (C,D,E,Q,QN,RN);

output Q,QN;
input C,D,E,RN;

buf (Q,buf_Q);
not (QN,buf_Q);

U_MUX_2_1 (BOOL_OUT,buf_Q,D,E);
`ifdef functional
U_FD_P_RB_NO (buf_Q,BOOL_OUT,C,RN,1'b1);
`else
reg notifier;
U_FD_P_RB_NO (buf_Q,BOOL_OUT,C,RN,notifier);
`endif

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (E), 0, notifier);
 $setup(negedge D, posedge C &&& (E), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $hold(posedge C &&& (E), negedge D, 0, notifier);
 $hold(posedge C &&& (E), posedge D, 0, notifier);  
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_DFF with rising clock low clear q and qb outputs

module DFEC3 (C,D,E,Q,QN,RN);

output Q,QN;
input C,D,E,RN;

buf (Q,buf_Q);
not (QN,buf_Q);

U_MUX_2_1 (BOOL_OUT,buf_Q,D,E);
`ifdef functional
U_FD_P_RB_NO (buf_Q,BOOL_OUT,C,RN,1'b1);
`else
reg notifier;
U_FD_P_RB_NO (buf_Q,BOOL_OUT,C,RN,notifier);
`endif

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (E), 0, notifier);
 $setup(negedge D, posedge C &&& (E), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $hold(posedge C &&& (E), negedge D, 0, notifier);
 $hold(posedge C &&& (E), posedge D, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_DFF with rising clock low preset,low clear q and qb outputs

module DFECP1 (C,D,E,Q,QN,RN,SN);

output Q,QN;
input C,D,E,RN,SN;

buf (Q,buf_Q);
not (QN,buf_Q);

U_MUX_2_1 (BOOL_OUT,buf_Q,D,E);
`ifdef functional
U_FD_P_RB_SB_NO (buf_Q,BOOL_OUT,C,RN,SN,1'b1);
`else
reg notifier;
U_FD_P_RB_SB_NO (buf_Q,BOOL_OUT,C,RN,SN,notifier);
`endif

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge C &&& (E), 0, notifier);
 $setup(negedge D, posedge C &&& (E), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (E), negedge D, 0, notifier);
 $hold(posedge C &&& (E), posedge D, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
 endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_DFF with rising clock low preset,low clear q and qb outputs

module DFECP3 (C,D,E,Q,QN,RN,SN);

output Q,QN;
input C,D,E,RN,SN;

buf (Q,buf_Q);
not (QN,buf_Q);

U_MUX_2_1 (BOOL_OUT,buf_Q,D,E);
`ifdef functional
U_FD_P_RB_SB_NO (buf_Q,BOOL_OUT,C,RN,SN,1'b1);
`else
reg notifier;
U_FD_P_RB_SB_NO (buf_Q,BOOL_OUT,C,RN,SN,notifier);
`endif

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge C &&& (E), 0, notifier);
 $setup(negedge D, posedge C &&& (E), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (E), negedge D, 0, notifier);
 $hold(posedge C &&& (E), posedge D, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier); 
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
 endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_DFF with rising clock low preset q and qb outputs

module DFEP1 (C,D,E,Q,QN,SN);

output Q,QN;
input C,D,E,SN;

buf (Q,buf_Q);
not (QN,buf_Q);

U_MUX_2_1 (BOOL_OUT,buf_Q,D,E);
`ifdef functional
U_FD_P_SB_NO (buf_Q,BOOL_OUT,C,SN,1'b1);
`else
reg notifier;
U_FD_P_SB_NO (buf_Q,BOOL_OUT,C,SN,notifier);
`endif

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (E), 0, notifier);
 $setup(negedge D, posedge C &&& (E), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (E), negedge D, 0, notifier);
 $hold(posedge C &&& (E), posedge D, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
 endspecify 
`endif
endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_DFF with rising clock low preset q and qb outputs

module DFEP3 (C,D,E,Q,QN,SN);

output Q,QN;
input C,D,E,SN;

buf (Q,buf_Q);
not (QN,buf_Q);

U_MUX_2_1 (BOOL_OUT,buf_Q,D,E);
`ifdef functional
U_FD_P_SB_NO (buf_Q,BOOL_OUT,C,SN,1'b1);
`else
reg notifier;
U_FD_P_SB_NO (buf_Q,BOOL_OUT,C,SN,notifier);
`endif


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (E), 0, notifier);
 $setup(negedge D, posedge C &&& (E), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (E), negedge D, 0, notifier);
 $hold(posedge C &&& (E), posedge D, 0, notifier);  
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
 endspecify 
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : DFF with rising edge clock,low preset,q and qb outputs

module DFP1 (C,D,Q,QN,SN);

output Q,QN;
input C,D,SN;

`ifdef functional
U_FD_P_SB_NO (buf_Q,D,C,SN,1'b1);
`else
reg notifier;
U_FD_P_SB_NO (buf_Q,D,C,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge C, 0, notifier);
 $setup(negedge D, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, negedge D, 0, notifier);
 $hold(posedge C, posedge D, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : DFF with rising edge clock,low preset,q and qb outputs

module DFP3 (C,D,Q,QN,SN);

output Q,QN;
input C,D,SN;

`ifdef functional
U_FD_P_SB_NO (buf_Q,D,C,SN,1'b1);
`else
reg notifier;
U_FD_P_SB_NO (buf_Q,D,C,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge C, 0, notifier);
 $setup(negedge D, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, negedge D, 0, notifier);
 $hold(posedge C, posedge D, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_DFF with rising edge clock,q and qb outputs

module DFS1 (C,D,Q,QN,SD,SE);

output Q,QN;
input C,D,SD,SE;

`ifdef functional
U_FD_P_NO (buf_Q,mux_out,C,1'b1);
`else
reg notifier;
U_FD_P_NO (buf_Q,mux_out,C,notifier);
`endif

U_MUX_2_1 (mux_out,D,SD,SE);
buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge D, posedge C &&& (~SE), 0, notifier);
 $setup(negedge D, posedge C &&& (~SE), 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), posedge D, 0, notifier);
 $hold(posedge C &&& (~SE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_DFF with rising edge clock,q and qb outputs

module DFS3 (C,D,Q,QN,SD,SE);

output Q,QN;
input C,D,SD,SE;

`ifdef functional
U_FD_P_NO (buf_Q,mux_out,C,1'b1);
`else
reg notifier;
U_FD_P_NO (buf_Q,mux_out,C,notifier);
`endif

U_MUX_2_1 (mux_out,D,SD,SE);
buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge D, posedge C &&& (~SE), 0, notifier);
 $setup(negedge D, posedge C &&& (~SE), 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), posedge D, 0, notifier);
 $hold(posedge C &&& (~SE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_DFF with rising edge clock,low clear,q and qb outputs

module DFSC1 (C,D,Q,QN,RN,SD,SE);

output Q,QN;
input C,D,RN,SD,SE;

`ifdef functional
U_FD_P_RB_NO (buf_Q,mux_out,C,RN,1'b1);
`else
reg notifier;
U_FD_P_RB_NO (buf_Q,mux_out,C,RN,notifier);
`endif

U_MUX_2_1 (mux_out,D,SD,SE);
buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (~SE), 0, notifier);
 $setup(negedge D, posedge C &&& (~SE), 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), posedge D, 0, notifier);
 $hold(posedge C &&& (~SE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_DFF with rising edge clock,low clear,q and qb outputs

module DFSC3 (C,D,Q,QN,RN,SD,SE);

output Q,QN;
input C,D,RN,SD,SE;

`ifdef functional
U_FD_P_RB_NO (buf_Q,mux_out,C,RN,1'b1);
`else
reg notifier;
U_FD_P_RB_NO (buf_Q,mux_out,C,RN,notifier);
`endif

U_MUX_2_1 (mux_out,D,SD,SE);
buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (~SE), 0, notifier);
 $setup(negedge D, posedge C &&& (~SE), 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), posedge D, 0, notifier);
 $hold(posedge C &&& (~SE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_DFF with rising edge clock,low preset,low clear,q and qb outputs

module DFSCP1 (C,D,Q,QN,RN,SD,SE,SN);

output Q,QN;
input C,D,RN,SD,SE,SN;

`ifdef functional
U_FD_P_RB_SB_NO (buf_Q,mux_out,C,RN,SN,1'b1);
`else
reg notifier;
U_FD_P_RB_SB_NO (buf_Q,mux_out,C,RN,SN,notifier);
`endif

U_MUX_2_1 (mux_out,D,SD,SE);

buf (Q,buf_Q);
not (QN,buf_Q);


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge C &&& (~SE), 0, notifier);
 $setup(negedge D, posedge C &&& (~SE), 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), posedge D, 0, notifier);
 $hold(posedge C &&& (~SE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_DFF with rising edge clock,low preset,low clear,q and qb outputs

module DFSCP3 (C,D,Q,QN,RN,SD,SE,SN);

output Q,QN;
input C,D,RN,SD,SE,SN;

`ifdef functional
U_FD_P_RB_SB_NO (buf_Q,mux_out,C,RN,SN,1'b1);
`else
reg notifier;
U_FD_P_RB_SB_NO (buf_Q,mux_out,C,RN,SN,notifier);
`endif

U_MUX_2_1 (mux_out,D,SD,SE);
buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge C &&& (~SE), 0, notifier);
 $setup(negedge D, posedge C &&& (~SE), 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), posedge D, 0, notifier);
 $hold(posedge C &&& (~SE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_SCAN_DFF with rising clock q and qb outputs

module DFSE1 (C,D,E,Q,QN,SD,SE);

output Q,QN;
input C,D,E,SD,SE;

`ifdef functional
U_dfse_Q (buf_Q,C,E,SE,D,SD,1'b0);
`else
reg notifier;
U_dfse_Q (buf_Q,C,E,SE,D,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);
and (EandNSE, E, ~SE);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(negedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (EandNSE), posedge D, 0, notifier);
 $hold(posedge C &&& (EandNSE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine



//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_SCAN_DFF with rising clock q and qb outputs

module DFSE3 (C,D,E,Q,QN,SD,SE);

output Q,QN;
input C,D,E,SD,SE;

`ifdef functional
U_dfse_Q (buf_Q,C,E,SE,D,SD,1'b0);
`else
reg notifier;
U_dfse_Q (buf_Q,C,E,SE,D,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);
and (EandNSE, E, ~SE);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(negedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (EandNSE), posedge D, 0, notifier);
 $hold(posedge C &&& (EandNSE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_SCAN_DFF with rising clock low clear q and qb outputs

module DFSEC1 (C,D,E,Q,QN,RN,SD,SE);

output Q,QN;
input C,D,E,RN,SD,SE;

`ifdef functional
U_dfsec_Q (buf_Q,C,RN,E,SE,D,SD,1'b0);
`else
reg notifier;
U_dfsec_Q (buf_Q,C,RN,E,SE,D,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);
and (EandNSE, E, ~SE);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(negedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (EandNSE), posedge D, 0, notifier);
 $hold(posedge C &&& (EandNSE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_SCAN_DFF with rising clock low clear q and qb outputs

module DFSEC3 (C,D,E,Q,QN,RN,SD,SE);

output Q,QN;
input C,D,E,RN,SD,SE;

`ifdef functional
U_dfsec_Q (buf_Q,C,RN,E,SE,D,SD,1'b0);
`else
reg notifier;
U_dfsec_Q (buf_Q,C,RN,E,SE,D,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);
and (EandNSE, E, ~SE);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(negedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (EandNSE), posedge D, 0, notifier);
 $hold(posedge C &&& (EandNSE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_SCAN_DFF with rising clock low preset,low clear q and qb outputs

module DFSECP1 (C,D,E,Q,QN,RN,SD,SE,SN);

output Q,QN;
input C,D,E,RN,SD,SE,SN;

`ifdef functional
U_dfsecp_Q (buf_Q,C,RN,SN,E,SE,D,SD,1'b0);
`else
reg notifier;
U_dfsecp_Q (buf_Q,C,RN,SN,E,SE,D,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);
and (EandNSE, E, ~SE);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(negedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (EandNSE), posedge D, 0, notifier);
 $hold(posedge C &&& (EandNSE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_SCAN_DFF with rising clock low preset,low clear q and qb outputs

module DFSECP3 (C,D,E,Q,QN,RN,SD,SE,SN);

output Q,QN;
input C,D,E,RN,SD,SE,SN;

`ifdef functional
U_dfsecp_Q (buf_Q,C,RN,SN,E,SE,D,SD,1'b0);
`else
reg notifier;
U_dfsecp_Q (buf_Q,C,RN,SN,E,SE,D,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);
and (EandNSE, E, ~SE);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(negedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (EandNSE), posedge D, 0, notifier);
 $hold(posedge C &&& (EandNSE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_SCAN_DFF with rising clock low preset q and qb outputs

module DFSEP1 (C,D,E,Q,QN,SD,SE,SN);

output Q,QN;
input C,D,E,SD,SE,SN;

`ifdef functional
U_dfsep_Q (buf_Q,C,SE,E,SN,D,SD,1'b0);
`else
reg notifier;
U_dfsep_Q (buf_Q,C,SE,E,SN,D,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);
and (EandNSE, E, ~SE);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(negedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (EandNSE), posedge D, 0, notifier);
 $hold(posedge C &&& (EandNSE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : BOOL_SCAN_DFF with rising clock low preset q and qb outputs

module DFSEP3 (C,D,E,Q,QN,SD,SE,SN);

output Q,QN;
input C,D,E,SD,SE,SN;

`ifdef functional
U_dfsep_Q (buf_Q,C,SE,E,SN,D,SD,1'b0);
`else
reg notifier;
U_dfsep_Q (buf_Q,C,SE,E,SN,D,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);
and (EandNSE, E, ~SE);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(negedge D, posedge C &&& (EandNSE), 0, notifier);
 $setup(posedge E, posedge C, 0, notifier);
 $setup(negedge E, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier); 
 $hold(posedge C &&& (EandNSE), posedge D, 0, notifier);
 $hold(posedge C &&& (EandNSE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, negedge E, 0, notifier);
 $hold(posedge C, posedge E, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_DFF with rising edge clock,low preset,q and qb outputs

module DFSP1 (C,D,Q,QN,SD,SE,SN);

output Q,QN;
input C,D,SD,SE,SN;

`ifdef functional
U_FD_P_SB_NO (buf_Q,mux_out,C,SN,1'b1);
`else
reg notifier;
U_FD_P_SB_NO (buf_Q,mux_out,C,SN,notifier);
`endif

U_MUX_2_1 (mux_out,D,SD,SE);
buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (~SE), 0, notifier);
 $setup(negedge D, posedge C &&& (~SE), 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), posedge D, 0, notifier);
 $hold(posedge C &&& (~SE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_DFF with rising edge clock,low preset,q and qb outputs

module DFSP3 (C,D,Q,QN,SD,SE,SN);

output Q,QN;
input C,D,SD,SE,SN;

`ifdef functional
U_FD_P_SB_NO (buf_Q,mux_out,C,SN,1'b1);
`else
reg notifier;
U_FD_P_SB_NO (buf_Q,mux_out,C,SN,notifier);
`endif

U_MUX_2_1 (mux_out,D,SD,SE);
buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge C &&& (~SE), 0, notifier);
 $setup(negedge D, posedge C &&& (~SE), 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), posedge D, 0, notifier);
 $hold(posedge C &&& (~SE), negedge D, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,q and qb outputs

module DL1 (D,GN,Q,QN);

output Q,QN;
input D,GN;

`ifdef functional
U_LD_N_NO (buf_Q,D,GN,1'b1);
`else
reg notifier;
U_LD_N_NO (buf_Q,D,GN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (D => Q) = (1,1);
 (D => QN) = (1,1);
 (GN => Q) = (1,1);
 (GN => QN) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,q and qb outputs

module DL3 (D,GN,Q,QN);

output Q,QN;
input D,GN;

`ifdef functional
U_LD_N_NO (buf_Q,D,GN,1'b1);
`else
reg notifier;
U_LD_N_NO (buf_Q,D,GN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (D => Q) = (1,1);
 (D => QN) = (1,1);
 (GN => Q) = (1,1);
 (GN => QN) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);  
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low clear,q and qb outputs

module DLC1 (D,GN,Q,QN,RN);

output Q,QN;
input D,GN,RN;

`ifdef functional
U_LD_P_RB_NO (buf_Q,D,GN,RN,1'b1);
`else
reg notifier;
U_LD_P_RB_NO (buf_Q,D,GN,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (D => Q) = (1,1);
 (D => QN) = (1,1);
 (GN => Q) = (1,1);
 (GN => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge RN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge RN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low clear,q and qb outputs

module DLC3 (D,GN,Q,QN,RN);

output Q,QN;
input D,GN,RN;

`ifdef functional
U_LD_P_RB_NO (buf_Q,D,GN,RN,1'b1);
`else
reg notifier;
U_LD_P_RB_NO (buf_Q,D,GN,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (D => Q) = (1,1);
 (D => QN) = (1,1);
 (GN => Q) = (1,1);
 (GN => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge RN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge RN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low preset,low clear,q and qb outputs

module DLCP1 (D,GN,Q,QN,RN,SN);

output Q,QN;
input D,GN,RN,SN;

`ifdef functional
U_LD_P_RB_SB_NO (buf_Q,D,GN,RN,SN,1'b1);
`else
reg notifier;
U_LD_P_RB_SB_NO (buf_Q,D,GN,RN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (D => Q) = (1,1);
 (D => QN) = (1,1);
 (GN => Q) = (1,1);
 (GN => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge RN, posedge GN, 0, notifier);
 $recovery(posedge SN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge RN, 0, notifier);
 $hold(posedge GN, posedge SN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low preset,low clear,q and qb outputs

module DLCP3 (D,GN,Q,QN,RN,SN);

output Q,QN;
input D,GN,RN,SN;

`ifdef functional
U_LD_P_RB_SB_NO (buf_Q,D,GN,RN,SN,1'b1);
`else
reg notifier;
U_LD_P_RB_SB_NO (buf_Q,D,GN,RN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (D => Q) = (1,1);
 (D => QN) = (1,1);
 (GN => Q) = (1,1);
 (GN => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge RN, posedge GN, 0, notifier);
 $recovery(posedge SN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge RN, 0, notifier);
 $hold(posedge GN, posedge SN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low preset,low clear,q output

module DLCPQ1 (D,GN,Q,RN,SN);

output Q;
input D,GN,RN,SN;

`ifdef functional
U_LD_P_RB_SB_NO (Q,D,GN,RN,SN,1'b1);
`else
reg notifier;
U_LD_P_RB_SB_NO (Q,D,GN,RN,SN,notifier);
`endif

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (GN => Q) = (1,1);
 (RN => Q) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge RN, posedge GN, 0, notifier);
 $recovery(posedge SN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge RN, 0, notifier);
 $hold(posedge GN, posedge SN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low preset,low clear,q output

module DLCPQ3 (D,GN,Q,RN,SN);

output Q;
input D,GN,RN,SN;

`ifdef functional
U_LD_P_RB_SB_NO (Q,D,GN,RN,SN,1'b1);
`else
reg notifier;
U_LD_P_RB_SB_NO (Q,D,GN,RN,SN,notifier);
`endif

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (GN => Q) = (1,1);
 (RN => Q) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge RN, posedge GN, 0, notifier);
 $recovery(posedge SN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge RN, 0, notifier);
 $hold(posedge GN, posedge SN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low clear,q output

module DLCQ1 (D,GN,Q,RN);

output Q;
input D,GN,RN;

`ifdef functional
U_LD_P_RB_NO (Q,D,GN,RN,1'b1);
`else
reg notifier;
U_LD_P_RB_NO (Q,D,GN,RN,notifier);
`endif

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (GN => Q) = (1,1);
 (RN => Q) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge RN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge RN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low clear,q output

module DLCQ3 (D,GN,Q,RN);

output Q;
input D,GN,RN;

`ifdef functional
U_LD_P_RB_NO (Q,D,GN,RN,1'b1);
`else
reg notifier;
U_LD_P_RB_NO (Q,D,GN,RN,notifier);
`endif

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (GN => Q) = (1,1);
 (RN => Q) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge RN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge RN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low preset,q and qb outputs

module DLP1 (D,GN,Q,QN,SN);

output Q,QN;
input D,GN,SN;

`ifdef functional
U_LD_N_SB_NO (buf_Q,D,GN,SN,1'b1);
`else
reg notifier;
U_LD_N_SB_NO (buf_Q,D,GN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (D => QN) = (1,1);
 (GN => Q) = (1,1);
 (GN => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge SN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge SN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low preset,q and qb outputs

module DLP3 (D,GN,Q,QN,SN);

output Q,QN;
input D,GN,SN;

`ifdef functional
U_LD_N_SB_NO (buf_Q,D,GN,SN,1'b1);
`else
reg notifier;
U_LD_N_SB_NO (buf_Q,D,GN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (D => QN) = (1,1);
 (GN => Q) = (1,1);
 (GN => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge SN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge SN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low preset,q output

module DLPQ1 (D,GN,Q,SN);

output Q;
input D,GN,SN;

`ifdef functional
U_LD_N_SB_NO (Q,D,GN,SN,1'b1);
`else
reg notifier;
U_LD_N_SB_NO (Q,D,GN,SN,notifier);
`endif

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (GN => Q) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge SN, posedge GN, 0, notifier);
 $hold(posedge GN, posedge SN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,low preset,q output

module DLPQ3 (D,GN,Q,SN);

output Q;
input D,GN,SN;

`ifdef functional
U_LD_N_SB_NO (Q,D,GN,SN,1'b1);
`else
reg notifier;
U_LD_N_SB_NO (Q,D,GN,SN,notifier);
`endif

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (GN => Q) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $recovery(posedge SN, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $hold(posedge GN, posedge SN, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,q output

module DLQ1 (D,GN,Q);

output Q;
input D,GN;

`ifdef functional
U_LD_N_NO (Q,D,GN,1'b1);
`else
reg notifier;
U_LD_N_NO (Q,D,GN,notifier);
`endif

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (GN => Q) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : LATCH with low enable,q output

module DLQ3 (D,GN,Q);

output Q;
input D,GN;

`ifdef functional
U_LD_N_NO (Q,D,GN,1'b1);
`else
reg notifier;
U_LD_N_NO (Q,D,GN,notifier);
`endif

`ifdef functional
`else
 specify
 (D => Q) = (1,1);
 (GN => Q) = (1,1);
 $setup(posedge D, posedge GN, 0, notifier);
 $setup(negedge D, posedge GN, 0, notifier);
 $hold(posedge GN, negedge D, 0, notifier);
 $hold(posedge GN, posedge D, 0, notifier);
 $width(negedge GN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module DLY12 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module DLY22 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module DLY32 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : BUFFER

module DLY42 (A,Q);

output Q;
input A;

buf (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 2-2-2 AND-OR-INVERT

module IMAJ30 (A,B,C,Q);

output Q;
input A,B,C;

and (g_1_out,B,A);
and (g_2_out,C,B);
and (g_3_out,C,A);
nor (Q,g_1_out,g_2_out,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 2-2-2 AND-OR-INVERT

module IMAJ31 (A,B,C,Q);

output Q;
input A,B,C;

and (g_1_out,B,A);
and (g_2_out,C,B);
and (g_3_out,C,A);
nor (Q,g_1_out,g_2_out,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 to 1 INVERTING MULTIPLEXER

module IMUX20 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1_INV (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 to 1 INVERTING MULTIPLEXER

module IMUX21 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1_INV (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 to 1 INVERTING MULTIPLEXER

module IMUX22 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1_INV (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 to 1 INVERTING MULTIPLEXER

module IMUX23 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1_INV (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps
 


module IMUX24 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1_INV (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 to 1 INVERTING MULTIPLEXER

module IMUX30 (A,B,C,Q,S0,S1);

output Q;
input A,B,C,S0,S1;

U_MUX_3_2_INV (Q,A,B,C,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 to 1 INVERTING MULTIPLEXER

module IMUX31 (A,B,C,Q,S0,S1);

output Q;
input A,B,C,S0,S1;

U_MUX_3_2_INV (Q,A,B,C,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 to 1 INVERTING MULTIPLEXER

module IMUX32 (A,B,C,Q,S0,S1);

output Q;
input A,B,C,S0,S1;

U_MUX_3_2_INV (Q,A,B,C,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 to 1 INVERTING MULTIPLEXER

module IMUX33 (A,B,C,Q,S0,S1);

output Q;
input A,B,C,S0,S1;

U_MUX_3_2_INV (Q,A,B,C,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 to 1 INVERTING MULTIPLEXER

module IMUX40 (A,B,C,D,Q,S0,S1);

output Q;
input A,B,C,D,S0,S1;

U_MUX_4_2_INV (Q,A,B,C,D,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1); 
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 to 1 INVERTING MULTIPLEXER

module IMUX41 (A,B,C,D,Q,S0,S1);

output Q;
input A,B,C,D,S0,S1;

U_MUX_4_2_INV (Q,A,B,C,D,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 to 1 INVERTING MULTIPLEXER

module IMUX42 (A,B,C,D,Q,S0,S1);

output Q;
input A,B,C,D,S0,S1;

U_MUX_4_2_INV (Q,A,B,C,D,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV0 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV1 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV10 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV12 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV15 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV2 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV3 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV4 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV6 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : INVERTER

module INV8 (A,Q);

output Q;
input A;

not (Q,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : JKFF with rising edge clock,j and k,q and qb outputs

module JK1 (C,J,K,Q,QN);

output Q,QN;
input C,J,K;

`ifdef functional
U_FJK_P_NO (buf_Q,J,K,C,1'b1);
`else
reg notifier;
U_FJK_P_NO (buf_Q,J,K,C,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge J, posedge C, 0, notifier);
 $setup(negedge J, posedge C, 0, notifier);
 $setup(posedge K, posedge C, 0, notifier);
 $setup(negedge K, posedge C, 0, notifier);
 $hold(posedge C, negedge J, 0, notifier);
 $hold(posedge C, posedge J, 0, notifier);
 $hold(posedge C, negedge K, 0, notifier);
 $hold(posedge C, posedge K, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : JKFF with rising edge clock,j and k,q and qb outputs

module JK3 (C,J,K,Q,QN);

output Q,QN;
input C,J,K;

`ifdef functional
U_FJK_P_NO (buf_Q,J,K,C,1'b1);
`else
reg notifier;
U_FJK_P_NO (buf_Q,J,K,C,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge J, posedge C, 0, notifier);
 $setup(negedge J, posedge C, 0, notifier);
 $setup(posedge K, posedge C, 0, notifier);
 $setup(negedge K, posedge C, 0, notifier);
 $hold(posedge C, negedge J, 0, notifier);
 $hold(posedge C, posedge J, 0, notifier);
 $hold(posedge C, negedge K, 0, notifier);
 $hold(posedge C, posedge K, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : JKFF with rising edge clock,low clear,j and k,q and qb outputs

module JKC1 (C,J,K,Q,QN,RN);

output Q,QN;
input C,J,K,RN;

`ifdef functional
U_FJK_P_RB_NO (buf_Q,J,K,C,RN,1'b1);
`else
reg notifier;
U_FJK_P_RB_NO (buf_Q,J,K,C,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge J, posedge C, 0, notifier);
 $setup(negedge J, posedge C, 0, notifier);
 $setup(posedge K, posedge C, 0, notifier);
 $setup(negedge K, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $hold(posedge C, negedge J, 0, notifier);
 $hold(posedge C, posedge J, 0, notifier);
 $hold(posedge C, negedge K, 0, notifier);
 $hold(posedge C, posedge K, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : JKFF with rising edge clock,low clear,j and k,q and qb outputs

module JKC3 (C,J,K,Q,QN,RN);

output Q,QN;
input C,J,K,RN;

`ifdef functional
U_FJK_P_RB_NO (buf_Q,J,K,C,RN,1'b1);
`else
reg notifier;
U_FJK_P_RB_NO (buf_Q,J,K,C,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge J, posedge C, 0, notifier);
 $setup(negedge J, posedge C, 0, notifier);
 $setup(posedge K, posedge C, 0, notifier);
 $setup(negedge K, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $hold(posedge C, negedge J, 0, notifier);
 $hold(posedge C, posedge J, 0, notifier);
 $hold(posedge C, negedge K, 0, notifier);
 $hold(posedge C, posedge K, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : JKFF with rising edge clock,low preset,low clear,j and k,q and qb outputs

module JKCP1 (C,J,K,Q,QN,RN,SN);

output Q,QN;
input C,J,K,RN,SN;

`ifdef functional
U_FJK_P_RB_SB_NO (buf_Q,J,K,C,RN,SN,1'b1);
`else
reg notifier;
U_FJK_P_RB_SB_NO (buf_Q,J,K,C,RN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge J, posedge C, 0, notifier);
 $setup(negedge J, posedge C, 0, notifier);
 $setup(posedge K, posedge C, 0, notifier);
 $setup(negedge K, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, negedge J, 0, notifier);
 $hold(posedge C, posedge J, 0, notifier);
 $hold(posedge C, negedge K, 0, notifier);
 $hold(posedge C, posedge K, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : JKFF with rising edge clock,low preset,low clear,j and k,q and qb outputs

module JKCP3 (C,J,K,Q,QN,RN,SN);

output Q,QN;
input C,J,K,RN,SN;

`ifdef functional
U_FJK_P_RB_SB_NO (Q,J,K,C,RN,SN,1'b1);
`else
reg notifier;
U_FJK_P_RB_SB_NO (buf_Q,J,K,C,RN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge J, posedge C, 0, notifier);
 $setup(negedge J, posedge C, 0, notifier);
 $setup(posedge K, posedge C, 0, notifier);
 $setup(negedge K, posedge C, 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, negedge J, 0, notifier);
 $hold(posedge C, posedge J, 0, notifier);
 $hold(posedge C, negedge K, 0, notifier);
 $hold(posedge C, posedge K, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : JKFF with rising edge clock,low preset,j and k,q and qb outputs

module JKP1 (C,J,K,Q,QN,SN);

output Q,QN;
input C,J,K,SN;

`ifdef functional
U_FJK_P_SB_NO (buf_Q,J,K,C,SN,1'b1);
`else
reg notifier;
U_FJK_P_SB_NO (buf_Q,J,K,C,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge J, posedge C, 0, notifier);
 $setup(negedge J, posedge C, 0, notifier);
 $setup(posedge K, posedge C, 0, notifier);
 $setup(negedge K, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, negedge J, 0, notifier);
 $hold(posedge C, posedge J, 0, notifier);
 $hold(posedge C, negedge K, 0, notifier);
 $hold(posedge C, posedge K, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : JKFF with rising edge clock,low preset,j and k,q and qb outputs

module JKP3 (C,J,K,Q,QN,SN);

output Q,QN;
input C,J,K,SN;

`ifdef functional
U_FJK_P_SB_NO (buf_Q,J,K,C,SN,1'b1);
`else
reg notifier;
U_FJK_P_SB_NO (buf_Q,J,K,C,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge J, posedge C, 0, notifier);
 $setup(negedge J, posedge C, 0, notifier);
 $setup(posedge K, posedge C, 0, notifier);
 $setup(negedge K, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, negedge J, 0, notifier);
 $hold(posedge C, posedge J, 0, notifier);
 $hold(posedge C, negedge K, 0, notifier);
 $hold(posedge C, posedge K, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
 endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_JKFF with rising clock q and qb outputs

module JKS1 (C,J,K,Q,QN,SD,SE);

output Q,QN;
input C,J,K,SD,SE;

`ifdef functional
U_jks_Q (buf_Q,C,SE,J,K,SD,1'b0);
`else
reg notifier;
U_jks_Q (buf_Q,C,SE,J,K,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge J, posedge C &&& (~SE), 0, notifier);
 $setup(negedge J, posedge C &&& (~SE), 0, notifier);
 $setup(posedge K, posedge C &&& (~SE), 0, notifier);
 $setup(negedge K, posedge C &&& (~SE), 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), negedge J, 0, notifier);
 $hold(posedge C &&& (~SE), posedge J, 0, notifier);  
 $hold(posedge C &&& (~SE), negedge K, 0, notifier);
 $hold(posedge C &&& (~SE), posedge K, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_JKFF with rising clock q and qb outputs

module JKS3 (C,J,K,Q,QN,SD,SE);

output Q,QN;
input C,J,K,SD,SE;

`ifdef functional
U_jks_Q (buf_Q,C,SE,J,K,SD,1'b0);
`else
reg notifier;
U_jks_Q (buf_Q,C,SE,J,K,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 $setup(posedge J, posedge C &&& (~SE), 0, notifier);
 $setup(negedge J, posedge C &&& (~SE), 0, notifier);
 $setup(posedge K, posedge C &&& (~SE), 0, notifier);
 $setup(negedge K, posedge C &&& (~SE), 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), negedge J, 0, notifier);
 $hold(posedge C &&& (~SE), posedge J, 0, notifier);  
 $hold(posedge C &&& (~SE), negedge K, 0, notifier);
 $hold(posedge C &&& (~SE), posedge K, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_JKFF with rising clock low clear q and qb outputs

module JKSC1 (C,J,K,Q,QN,RN,SD,SE);

output Q,QN;
input C,J,K,RN,SD,SE;

`ifdef functional
U_jksc_Q (buf_Q,C,RN,SE,J,K,SD,1'b0);
`else
reg notifier;
U_jksc_Q (buf_Q,C,RN,SE,J,K,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge J, posedge C &&& (~SE), 0, notifier);
 $setup(negedge J, posedge C &&& (~SE), 0, notifier);
 $setup(posedge K, posedge C &&& (~SE), 0, notifier);
 $setup(negedge K, posedge C &&& (~SE), 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), negedge J, 0, notifier);
 $hold(posedge C &&& (~SE), posedge J, 0, notifier);  
 $hold(posedge C &&& (~SE), negedge K, 0, notifier);
 $hold(posedge C &&& (~SE), posedge K, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_JKFF with rising clock low clear q and qb outputs

module JKSC3 (C,J,K,Q,QN,RN,SD,SE);

output Q,QN;
input C,J,K,RN,SD,SE;

`ifdef functional
U_jksc_Q (buf_Q,C,RN,SE,J,K,SD,1'b0);
`else
reg notifier;
U_jksc_Q (buf_Q,C,RN,SE,J,K,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $setup(posedge J, posedge C &&& (~SE), 0, notifier);
 $setup(negedge J, posedge C &&& (~SE), 0, notifier);
 $setup(posedge K, posedge C &&& (~SE), 0, notifier);
 $setup(negedge K, posedge C &&& (~SE), 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), negedge J, 0, notifier);
 $hold(posedge C &&& (~SE), posedge J, 0, notifier);  
 $hold(posedge C &&& (~SE), negedge K, 0, notifier);
 $hold(posedge C &&& (~SE), posedge K, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_JKFF with rising clock low clear,low preset q and qb outputs

module JKSCP1 (C,J,K,Q,QN,RN,SD,SE,SN);

output Q,QN;
input C,J,K,RN,SD,SE,SN;


`ifdef functional
U_jkscp_Q (buf_Q,C,RN,SN,SE,J,K,SD,1'b0);
`else
reg notifier;
U_jkscp_Q (buf_Q,C,RN,SN,SE,J,K,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge J, posedge C &&& (~SE), 0, notifier);
 $setup(negedge J, posedge C &&& (~SE), 0, notifier);
 $setup(posedge K, posedge C &&& (~SE), 0, notifier);
 $setup(negedge K, posedge C &&& (~SE), 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), negedge J, 0, notifier);
 $hold(posedge C &&& (~SE), posedge J, 0, notifier);  
 $hold(posedge C &&& (~SE), negedge K, 0, notifier);
 $hold(posedge C &&& (~SE), posedge K, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_JKFF with rising clock low clear,low preset q and qb outputs

module JKSCP3 (C,J,K,Q,QN,RN,SD,SE,SN);

output Q,QN;
input C,J,K,RN,SD,SE,SN;

`ifdef functional
U_jkscp_Q (buf_Q,C,RN,SN,SE,J,K,SD,1'b0);
`else
reg notifier;
U_jkscp_Q (buf_Q,C,RN,SN,SE,J,K,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $setup(posedge J, posedge C &&& (~SE), 0, notifier);
 $setup(negedge J, posedge C &&& (~SE), 0, notifier);
 $setup(posedge K, posedge C &&& (~SE), 0, notifier);
 $setup(negedge K, posedge C &&& (~SE), 0, notifier);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), negedge J, 0, notifier);
 $hold(posedge C &&& (~SE), posedge J, 0, notifier);  
 $hold(posedge C &&& (~SE), negedge K, 0, notifier);
 $hold(posedge C &&& (~SE), posedge K, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine



//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_JKFF with rising clock low preset q and qb outputs

module JKSP1 (C,J,K,Q,QN,SD,SE,SN);

output Q,QN;
input C,J,K,SD,SE,SN;

`ifdef functional
U_jksp_Q (buf_Q,C,SE,SN,J,K,SD,1'b0);
`else
reg notifier;
U_jksp_Q (buf_Q,C,SE,SN,J,K,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge J, posedge C &&& (~SE), 0, notifier);
 $setup(negedge J, posedge C &&& (~SE), 0, notifier);
 $setup(posedge K, posedge C &&& (~SE), 0, notifier);
 $setup(negedge K, posedge C &&& (~SE), 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), negedge J, 0, notifier);
 $hold(posedge C &&& (~SE), posedge J, 0, notifier);  
 $hold(posedge C &&& (~SE), negedge K, 0, notifier);
 $hold(posedge C &&& (~SE), posedge K, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_JKFF with rising clock low preset q and qb outputs

module JKSP3 (C,J,K,Q,QN,SD,SE,SN);

output Q,QN;
input C,J,K,SD,SE,SN;

`ifdef functional
U_jksp_Q (buf_Q,C,SE,SN,J,K,SD,1'b0);
`else
reg notifier;
U_jksp_Q (buf_Q,C,SE,SN,J,K,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge J, posedge C &&& (~SE), 0, notifier);
 $setup(negedge J, posedge C &&& (~SE), 0, notifier);
 $setup(posedge K, posedge C &&& (~SE), 0, notifier);
 $setup(negedge K, posedge C &&& (~SE), 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C &&& (~SE), negedge J, 0, notifier);
 $hold(posedge C &&& (~SE), posedge J, 0, notifier);  
 $hold(posedge C &&& (~SE), negedge K, 0, notifier);
 $hold(posedge C &&& (~SE), posedge K, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 2-2-2 AND-OR

module MAJ31 (A,B,C,Q);

output Q;
input A,B,C;

and (g_1_out,B,A);
and (g_2_out,C,B);
and (g_3_out,C,A);
or (Q,g_1_out,g_2_out,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 2-2-2 AND-OR

module MAJ32 (A,B,C,Q);

output Q;
input A,B,C;

and (g_1_out,B,A);
and (g_2_out,C,B);
and (g_3_out,C,A);
or (Q,g_1_out,g_2_out,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 to 1 MULTIPLEXER

module MUX21 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1 (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 to 1 MULTIPLEXER

module MUX22 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1 (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 to 1 MULTIPLEXER

module MUX24 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1 (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 to 1 MULTIPLEXER

module MUX26 (A,B,Q,S);

output Q;
input A,B,S;

U_MUX_2_1 (Q,A,B,S);

`ifdef functional
`else
specify
 (S => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 to 1 MULTIPLEXER

module MUX31 (A,B,C,Q,S0,S1);

output Q;
input A,B,C,S0,S1;

U_MUX_3_2 (Q,A,B,C,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 to 1 MULTIPLEXER

module MUX32 (A,B,C,Q,S0,S1);

output Q;
input A,B,C,S0,S1;

U_MUX_3_2 (Q,A,B,C,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 to 1 MULTIPLEXER

module MUX33 (A,B,C,Q,S0,S1);

output Q;
input A,B,C,S0,S1;

U_MUX_3_2 (Q,A,B,C,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 to 1 MULTIPLEXER

module MUX34 (A,B,C,Q,S0,S1);

output Q;
input A,B,C,S0,S1;

U_MUX_3_2 (Q,A,B,C,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 to 1 MULTIPLEXER

module MUX41 (A,B,C,D,Q,S0,S1);

output Q;
input A,B,C,D,S0,S1;

U_MUX_4_2 (Q,A,B,C,D,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 to 1 MULTIPLEXER

module MUX42 (A,B,C,D,Q,S0,S1);

output Q;
input A,B,C,D,S0,S1;

U_MUX_4_2 (Q,A,B,C,D,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 to 1 MULTIPLEXER

module MUX43 (A,B,C,D,Q,S0,S1);

output Q;
input A,B,C,D,S0,S1;

U_MUX_4_2 (Q,A,B,C,D,S0,S1);

`ifdef functional
`else
specify
 (S0 => Q) = (1,1);
 (S1 => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NAND

module NAND20 (A,B,Q);

output Q;
input A,B;

nand (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NAND

module NAND21 (A,B,Q);

output Q;
input A,B;

nand (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NAND

module NAND22 (A,B,Q);

output Q;
input A,B;

nand (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NAND

module NAND23 (A,B,Q);

output Q;
input A,B;

nand (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NAND

module NAND24 (A,B,Q);

output Q;
input A,B;

nand (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NAND

module NAND26 (A,B,Q);

output Q;
input A,B;

nand (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NAND

module NAND28 (A,B,Q);

output Q;
input A,B;

nand (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NAND

module NAND30 (A,B,C,Q);

output Q;
input A,B,C;

nand (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NAND

module NAND31 (A,B,C,Q);

output Q;
input A,B,C;

nand (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NAND

module NAND32 (A,B,C,Q);

output Q;
input A,B,C;

nand (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NAND

module NAND33 (A,B,C,Q);

output Q;
input A,B,C;

nand (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NAND

module NAND34 (A,B,C,Q);

output Q;
input A,B,C;

nand (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input NAND

module NAND40 (A,B,C,D,Q);

output Q;
input A,B,C,D;

nand (Q,D,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input NAND

module NAND41 (A,B,C,D,Q);

output Q;
input A,B,C,D;

nand (Q,D,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input NAND

module NAND42 (A,B,C,D,Q);

output Q;
input A,B,C,D;

nand (Q,D,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input NAND

module NAND43 (A,B,C,D,Q);

output Q;
input A,B,C,D;

nand (Q,D,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NOR

module NOR20 (A,B,Q);

output Q;
input A,B;

nor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NOR

module NOR21 (A,B,Q);

output Q;
input A,B;

nor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NOR

module NOR22 (A,B,Q);

output Q;
input A,B;

nor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NOR

module NOR23 (A,B,Q);

output Q;
input A,B;

nor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input NOR

module NOR24 (A,B,Q);

output Q;
input A,B;

nor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NOR

module NOR30 (A,B,C,Q);

output Q;
input A,B,C;

nor (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NOR

module NOR31 (A,B,C,Q);

output Q;
input A,B,C;

nor (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NOR

module NOR32 (A,B,C,Q);

output Q;
input A,B,C;

nor (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input NOR

module NOR33 (A,B,C,Q);

output Q;
input A,B,C;

nor (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input NOR

module NOR40 (A,B,C,D,Q);

output Q;
input A,B,C,D;

nor (Q,D,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input NOR

module NOR41 (A,B,C,D,Q);

output Q;
input A,B,C,D;

nor (Q,D,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input NOR

module NOR42 (A,B,C,D,Q);

output Q;
input A,B,C,D;

nor (Q,D,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 1-2 OR-AND-INVERT

module OAI210 (A,B,C,Q);

output Q;
input A,B,C;

or (g_2_out,A,B);
nand (Q,C,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 1-2 OR-AND-INVERT

module OAI211 (A,B,C,Q);

output Q;
input A,B,C;

or (g_2_out,A,B);
nand (Q,C,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 1-1-2 OR-AND-INVERT

module OAI2110 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_3_out,B,A);
nand (Q,D,C,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 1-1-2 OR-AND-INVERT

module OAI2111 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_3_out,B,A);
nand (Q,D,C,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 wide 1-1-2 OR-AND-INVERT

module OAI2112 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_3_out,B,A);
nand (Q,D,C,g_3_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 1-2 OR-AND-INVERT

module OAI212 (A,B,C,Q);

output Q;
input A,B,C;

or (g_2_out,A,B);
nand (Q,C,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-2 OR-AND-INVERT

module OAI220 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_1_out,D,C);
or (g_2_out,B,A);
nand (Q,g_1_out,g_2_out);

`ifdef functional
`else
specify
 (C => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-2 OR-AND-INVERT

module OAI221 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_1_out,D,C);
or (g_2_out,B,A);
nand (Q,g_1_out,g_2_out);

`ifdef functional
`else
specify
 (C => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 2-2 OR-AND-INVERT

module OAI222 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_1_out,D,C);
or (g_2_out,B,A);
nand (Q,g_1_out,g_2_out);

`ifdef functional
`else
specify
 (C => Q) = (1,1);
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 1-3 OR-AND-INVERT

module OAI310 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_2_out,B,A,C);
nand (Q,D,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 1-3 OR-AND-INVERT

module OAI311 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_2_out,B,A,C);
nand (Q,D,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 wide 1-3 OR-AND-INVERT

module OAI312 (A,B,C,D,Q);

output Q;
input A,B,C,D;

or (g_2_out,B,A,C);
nand (Q,D,g_2_out);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low clear,q and qb outputs

module TFC1 (C,Q,QN,RN);

output Q,QN;
input C,RN;

`ifdef functional
U_FT_P_RB_NO (buf_Q,C,RN,1'b1);
`else
reg notifier;
U_FT_P_RB_NO (buf_Q,C,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low clear,q and qb outputs

module TFC3 (C,Q,QN,RN);

output Q,QN;
input C,RN;

`ifdef functional
U_FT_P_RB_NO (buf_Q,C,RN,1'b1);
`else
reg notifier;
U_FT_P_RB_NO (buf_Q,C,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low preset,low clear,q and qb outputs

module TFCP1 (C,Q,QN,RN,SN);

output Q,QN;
input C,RN,SN;

`ifdef functional
U_tfcp_Q (buf_Q,C,RN,SN,1'b0);
`else
reg notifier;
U_tfcp_Q (buf_Q,C,RN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low preset,low clear,q and qb outputs

module TFCP3 (C,Q,QN,RN,SN);

output Q,QN;
input C,RN,SN;

`ifdef functional
U_tfcp_Q (buf_Q,C,RN,SN,1'b0);
`else
reg notifier;
U_tfcp_Q (buf_Q,C,RN,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low clear,high enable,q and qb outputs

module TFEC1 (C,Q,QN,RN,T);

output Q,QN;
input C,RN,T;

`ifdef functional
U_FT_P_TE_RB_NO (buf_Q,T,C,RN,1'b1);
`else
reg notifier;
U_FT_P_TE_RB_NO (buf_Q,T,C,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge T, posedge C, 0, notifier);
 $setup(negedge T, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge T, 0, notifier);
 $hold(posedge C, negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low clear,high enable,q and qb outputs

module TFEC3 (C,Q,QN,RN,T);

output Q,QN;
input C,RN,T;

`ifdef functional
U_FT_P_TE_RB_NO (buf_Q,T,C,RN,1'b1);
`else
reg notifier;
U_FT_P_TE_RB_NO (buf_Q,T,C,RN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge T, posedge C, 0, notifier);
 $setup(negedge T, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge T, 0, notifier);
 $hold(posedge C, negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low preset,low clear,high enable,q and qb outputs

module TFECP1 (C,Q,QN,RN,SN,T);

output Q,QN;
input C,RN,SN,T;

`ifdef functional
U_tfecp_Q (buf_Q,C,RN,SN,T,1'b0);
`else
reg notifier;
U_tfecp_Q (buf_Q,C,RN,SN,T,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $setup(posedge T, posedge C, 0, notifier);
 $setup(negedge T, posedge C, 0, notifier);
 $hold(posedge C, posedge T, 0, notifier);
 $hold(posedge C, negedge T, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

module TFECP3 (C,Q,QN,RN,SN,T);

output Q,QN;
input C,RN,SN,T;

`ifdef functional
U_tfecp_Q (buf_Q,C,RN,SN,T,1'b0);
`else
reg notifier;
U_tfecp_Q (buf_Q,C,RN,SN,T,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $setup(posedge T, posedge C, 0, notifier);
 $setup(negedge T, posedge C, 0, notifier);
 $hold(posedge C, posedge T, 0, notifier);
 $hold(posedge C, negedge T, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine




//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low preset,high enable,q and qb outputs

module TFEP1 (C,Q,QN,SN,T);

output Q,QN;
input C,SN,T;

`ifdef functional
U_tfep_Q (buf_Q,SN,C,T,1'b0);
`else
reg notifier;
U_tfep_Q (buf_Q,SN,C,T,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $recovery(posedge SN, posedge C, 0, notifier);
 $setup(posedge T, posedge C, 0, notifier);
 $setup(negedge T, posedge C, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $hold(posedge C, posedge T, 0, notifier);
 $hold(posedge C, negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine



//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low preset,high enable,q and qb outputs

module TFEP3 (C,Q,QN,SN,T);

output Q,QN;
input C,SN,T;

`ifdef functional
U_tfep_Q (buf_Q,SN,C,T,1'b0);
`else
reg notifier;
U_tfep_Q (buf_Q,SN,C,T,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $recovery(posedge SN, posedge C, 0, notifier);
 $setup(posedge T, posedge C, 0, notifier);
 $setup(negedge T, posedge C, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $hold(posedge C, posedge T, 0, notifier);
 $hold(posedge C, negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low preset,q and qb outputs

module TFP1 (C,Q,QN,SN);

output Q,QN;
input C,SN;

`ifdef functional
U_FT_P_SB_NO (buf_Q,C,SN,1'b1);
`else
reg notifier;
U_FT_P_SB_NO (buf_Q,C,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : TFF with rising edge clock,low preset,q and qb outputs

module TFP3 (C,Q,QN,SN);

output Q,QN;
input C,SN;

`ifdef functional
U_FT_P_SB_NO (buf_Q,C,SN,1'b1);
`else
reg notifier;
U_FT_P_SB_NO (buf_Q,C,SN,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_TFF with rising clock low clear q and qb outputs

module TFSC1 (C,Q,QN,RN,SD,SE);

output Q,QN;
input C,RN,SD,SE;

`ifdef functional
U_tfsc_Q (buf_Q,C,RN,SE,SD,1'b0);
`else
reg notifier;
U_tfsc_Q (buf_Q,C,RN,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine



//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_TFF with rising clock low clear q and qb outputs

module TFSC3 (C,Q,QN,RN,SD,SE);

output Q,QN;
input C,RN,SD,SE;

`ifdef functional
U_tfsc_Q (buf_Q,C,RN,SE,SD,1'b0);
`else
reg notifier;
U_tfsc_Q (buf_Q,C,RN,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : UNKNOWN

module TFSCP1 (C,Q,QN,RN,SD,SE,SN);

output Q,QN;
input C,RN,SD,SE,SN;

`ifdef functional
U_tfscp_Q (buf_Q,C,RN,SN,SE,SD,1'b0);
`else
reg notifier;
U_tfscp_Q (buf_Q,C,RN,SN,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : UNKNOWN

module TFSCP3 (C,Q,QN,RN,SD,SE,SN);

output Q,QN;
input C,RN,SD,SE,SN;

`ifdef functional
U_tfscp_Q (buf_Q,C,RN,SN,SE,SD,1'b0);
`else
reg notifier;
U_tfscp_Q (buf_Q,C,RN,SN,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine



//
`celldefine
`timescale 1ns / 1ps

// Description : UNKNOWN

module TFSEC1 (C,Q,QN,RN,SD,SE,T);

output Q,QN;
input C,RN,SD,SE,T;

`ifdef functional
U_tfsec_Q (buf_Q,C,RN,T,SE,SD,1'b0);
`else
reg notifier;
U_tfsec_Q (buf_Q,C,RN,T,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $setup(posedge T, posedge C &&& (~SE), 0, notifier);
 $setup(negedge T, posedge C &&& (~SE), 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C &&& (~SE), posedge T, 0, notifier);
 $hold(posedge C &&& (~SE), negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : UNKNOWN

module TFSEC3 (C,Q,QN,RN,SD,SE,T);

output Q,QN;
input C,RN,SD,SE,T;

`ifdef functional
U_tfsec_Q (buf_Q,C,RN,T,SE,SD,1'b0);
`else
reg notifier;
U_tfsec_Q (buf_Q,C,RN,T,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $setup(posedge T, posedge C &&& (~SE), 0, notifier);
 $setup(negedge T, posedge C &&& (~SE), 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C &&& (~SE), posedge T, 0, notifier);
 $hold(posedge C &&& (~SE), negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : UNKNOWN

module TFSECP1 (C,Q,QN,RN,SD,SE,SN,T);

output Q,QN;
input C,RN,SD,SE,SN,T;

`ifdef functional
U_tfsecp_Q (buf_Q,C,RN,SN,T,SE,SD,1'b0);
`else
reg notifier;
U_tfsecp_Q (buf_Q,C,RN,SN,T,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $setup(posedge T, posedge C &&& (~SE), 0, notifier);
 $setup(negedge T, posedge C &&& (~SE), 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C &&& (~SE), posedge T, 0, notifier);
 $hold(posedge C &&& (~SE), negedge T, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : UNKNOWN

module TFSECP3 (C,Q,QN,RN,SD,SE,SN,T);

output Q,QN;
input C,RN,SD,SE,SN,T;

`ifdef functional
U_tfsecp_Q (buf_Q,C,RN,SN,T,SE,SD,1'b0);
`else
reg notifier;
U_tfsecp_Q (buf_Q,C,RN,SN,T,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (RN => Q) = (1,1);
 (RN => QN) = (1,1);
 (SN => QN) = (1,1);
 (SN => Q) = (1,1);
 $recovery(posedge RN, posedge C, 0, notifier);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $setup(posedge T, posedge C &&& (~SE), 0, notifier);
 $setup(negedge T, posedge C &&& (~SE), 0, notifier);
 $hold(posedge C, posedge RN, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C &&& (~SE), posedge T, 0, notifier);
 $hold(posedge C &&& (~SE), negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge RN, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : UNKNOWN

module TFSEP1 (C,Q,QN,SD,SE,SN,T);

output Q,QN;
input C,SD,SE,SN,T;

`ifdef functional
U_tfsep_Q (buf_Q,C,SN,T,SE,SD,1'b0);
`else
reg notifier;
U_tfsep_Q (buf_Q,C,SN,T,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);


`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $setup(posedge T, posedge C &&& (~SE), 0, notifier);
 $setup(negedge T, posedge C &&& (~SE), 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C &&& (~SE), posedge T, 0, notifier);
 $hold(posedge C &&& (~SE), negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : UNKNOWN

module TFSEP3 (C,Q,QN,SD,SE,SN,T);

output Q,QN;
input C,SD,SE,SN,T;

`ifdef functional
U_tfsep_Q (buf_Q,C,SN,T,SE,SD,1'b0);
`else
reg notifier;
U_tfsep_Q (buf_Q,C,SN,T,SE,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $setup(posedge T, posedge C &&& (~SE), 0, notifier);
 $setup(negedge T, posedge C &&& (~SE), 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $hold(posedge C &&& (~SE), posedge T, 0, notifier);
 $hold(posedge C &&& (~SE), negedge T, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_TFF with rising clock low clear q and qb outputs

module TFSP1 (C,Q,QN,SD,SE,SN);

output Q,QN;
input C,SD,SE,SN;

`ifdef functional
U_tfsp_Q (buf_Q,C,SE,SN,SD,1'b0);
`else
reg notifier;
U_tfsp_Q (buf_Q,C,SE,SN,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine


//
`celldefine
`timescale 1ns / 1ps

// Description : SCAN_TFF with rising clock low clear q and qb outputs

module TFSP3 (C,Q,QN,SD,SE,SN);

output Q,QN;
input C,SD,SE,SN;

`ifdef functional
U_tfsp_Q (buf_Q,C,SE,SN,SD,1'b0);
`else
reg notifier;
U_tfsp_Q (buf_Q,C,SE,SN,SD,notifier);
`endif

buf (Q,buf_Q);
not (QN,buf_Q);

`ifdef functional
`else
 specify
 (C => Q) = (1,1);
 (C => QN) = (1,1);
 (SN => Q) = (1,1);
 (SN => QN) = (1,1);
 $setup(posedge SD, posedge C &&& (SE), 0, notifier);
 $setup(negedge SD, posedge C &&& (SE), 0, notifier);
 $setup(posedge SE, posedge C, 0, notifier);
 $setup(negedge SE, posedge C, 0, notifier);
 $recovery(posedge SN, posedge C, 0, notifier);
 $hold(posedge C, posedge SN, 0, notifier);
 $hold(posedge C &&& (SE), posedge SD, 0, notifier);
 $hold(posedge C &&& (SE), negedge SD, 0, notifier);
 $hold(posedge C, negedge SE, 0, notifier);
 $hold(posedge C, posedge SE, 0, notifier);
 $width(posedge C, 1, 0, notifier);
 $width(negedge C, 1, 0, notifier);
 $width(negedge SN, 1, 0, notifier);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input XNOR

module XNR20 (A,B,Q);

output Q;
input A,B;

xnor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input XNOR

module XNR21 (A,B,Q);

output Q;
input A,B;

xnor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input XNOR

module XNR22 (A,B,Q);

output Q;
input A,B;

xnor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input XNOR

module XNR30 (A,B,C,Q);

output Q;
input A,B,C;

xnor (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input XNOR

module XNR31 (A,B,C,Q);

output Q;
input A,B,C;

xnor (Q,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input XNOR

module XNR40 (A,B,C,D,Q);

output Q;
input A,B,C,D;

xnor (Q,D,C,B,A);

`ifdef functional
`else
specify
 (B => Q) = (1,1);
 (A => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input XNOR

module XNR41 (A,B,C,D,Q);

output Q;
input A,B,C,D;

xnor (Q,D,C,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input XOR

module XOR20 (A,B,Q);

output Q;
input A,B;

xor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input XOR

module XOR21 (A,B,Q);

output Q;
input A,B;

xor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 2 input XOR

module XOR22 (A,B,Q);

output Q;
input A,B;

xor (Q,B,A);

`ifdef functional
`else
specify
 (A => Q) = (1,1);
 (B => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input XOR

module XOR30 (A,B,C,Q);

output Q;
input A,B,C;

xor (Q,C,B,A);

`ifdef functional
`else
specify
 (B => Q) = (1,1);
 (A => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 3 input XOR

module XOR31 (A,B,C,Q);

output Q;
input A,B,C;

xor (Q,C,B,A);

`ifdef functional
`else
specify
 (B => Q) = (1,1);
 (A => Q) = (1,1);
 (C => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input XOR

module XOR40 (A,B,C,D,Q);

output Q;
input A,B,C,D;

xor (Q,D,C,B,A);

`ifdef functional
`else
specify
 (B => Q) = (1,1);
 (A => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine

//
`celldefine
`timescale 1ns / 1ps

// Description : 4 input XOR

module XOR41 (A,B,C,D,Q);

output Q;
input A,B,C,D;

xor (Q,D,C,B,A);

`ifdef functional
`else
specify
 (B => Q) = (1,1);
 (A => Q) = (1,1);
 (C => Q) = (1,1);
 (D => Q) = (1,1);
endspecify
`endif

endmodule
`endcelldefine


module BUSHD (A);
 inout A;

 buf (A_buf, A);
 buf (pull1, pull0)(A, A_buf);
 
endmodule


module TIE0 (Q);
 output Q;

 buf (Q,0);
 
endmodule


module TIE1 (Q);
 output Q;

 buf (Q,1);
 
endmodule


































// Verilog simulation library for c35 IOLIB_4M 3.3V
// $Id: csx_IOLIB_3M.v,v 1.1 2001/09/25 12:07:16 mbo Exp mbo $
// Owner: austriamicrosystems AG  HIT-Kit: Digital
`timescale 1ns/1ps
`celldefine 
module BBC1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU1P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU4P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU4SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU8P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU8SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU8SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU16P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU16SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU16SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU24P (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU24SMP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU24SP (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BT1P (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT2P (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT4P (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT4SMP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT8P (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT8SMP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT8SP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT12P (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT12SMP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT12SP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT16P (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT16SMP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT16SP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT24P (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT24SMP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT24SP (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU1P (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU2P (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU4P (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU4SMP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU8P (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU8SMP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU8SP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU12P (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU12SMP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU12SP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU16P (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU16SMP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU16SP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU24P (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU24SMP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU24SP (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD1P (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD2P (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD4P (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD8P (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD12P (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD16P (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD24P (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU1P (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU2P (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU4P (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU8P (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU12P (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU16P (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU24P (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module CBU1P (A, Y);
  input A;
  output Y;

  buf (Y, A);

`ifdef functional
`else
  specify
    (A => Y) = (1,1);
  endspecify
`endif

endmodule

module CBU2P (A, Y);
  input A;
  output Y;

  buf (Y, A);

`ifdef functional
`else
  specify
    (A => Y) = (1,1);
  endspecify
`endif

endmodule

module GND3ALLP(A);
  inout A;
 
endmodule
 
module GND3IP(A);
  inout A;
 
endmodule
 
module GND3OP(A);
  inout A;
 
endmodule
 
module GND3RP(A);
  inout A;
 
endmodule
 
module ICCK2P (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICCK4P (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICCK8P (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICCK16P (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICDP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICUP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ISDP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ISP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ISUP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITCK2P (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITCK4P (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITCK8P (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITCK16P (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITDP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITUP (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module VDD3ALLP(A);
  inout A;
 
endmodule
 
module VDD3IP(A);
  inout A;
 
endmodule
 
module VDD3OP(A);
  inout A;
 
endmodule
 
module VDD3RP(A);
  inout A;
 
endmodule
 
`endcelldefine



// Verilog simulation library for c35 IOLIBV5_4M 3.3V
// $Id: csx_IOLIBV5_3M.v,v 1.2 2001/10/30 09:43:46 mbo Exp mbo $
// Owner: austriamicrosystems AG  HIT-Kit: Digital
`timescale 1ns/1ps
`celldefine 
module BBC1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBC24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCD24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBCU24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBS24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSD24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBSU24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBT24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTD24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU1P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU4P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU4SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU8P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU8SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU8SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU16P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU16SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU16SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU24P_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU24SMP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BBTU24SP_V5 (A, EN, PAD, Y);
  input A;
  input EN;
  inout PAD;
  output Y;

  buf (Y, PAD);
  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module BT1P_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT2P_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT4P_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT4SMP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT8P_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT8SMP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT8SP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT12P_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT12SMP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT12SP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT16P_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT16SMP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT16SP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT24P_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT24SMP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BT24SP_V5 (A, EN, PAD);
  input A;
  input EN;
  output PAD;

  bufif0 (PAD, A, EN);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
    (EN => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU1P_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU2P_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU4P_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU4SMP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU8P_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU8SMP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU8SP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU12P_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU12SMP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU12SP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU16P_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU16SMP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU16SP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU24P_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU24SMP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BU24SP_V5 (A, PAD);
  input A;
  output PAD;

  buf (PAD, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD1P_V5 (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD2P_V5 (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD4P_V5 (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD8P_V5 (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD12P_V5 (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD16P_V5 (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDD24P_V5 (A, PAD);
  input A;
  output PAD;

  bufif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU1P_V5 (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU2P_V5 (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU4P_V5 (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU8P_V5 (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU12P_V5 (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU16P_V5 (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module BUDU24P_V5 (A, PAD);
  input A;
  output PAD;

  notif0 (PAD, 0, A);

`ifdef functional
`else
  specify
    (A => PAD) = (1,1);
  endspecify
`endif

endmodule

module CBU1P_V5 (A, Y);
  input A;
  output Y;

  buf (Y, A);

`ifdef functional
`else
  specify
    (A => Y) = (1,1);
  endspecify
`endif

endmodule

module CBU2P_V5 (A, Y);
  input A;
  output Y;

  buf (Y, A);

`ifdef functional
`else
  specify
    (A => Y) = (1,1);
  endspecify
`endif

endmodule

module GND5ALLP_V5(A);
  inout A;
 
endmodule
 
module GND5IP_V5(A);
  inout A;
 
endmodule
 
module GND5OP_V5(A);
  inout A;
 
endmodule
 
module GND5RP_V5(A);
  inout A;
 
endmodule
 
module ICCK2P_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICCK4P_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICDP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ICUP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ISDP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ISP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ISUP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITCK2P_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITCK4P_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITDP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module ITUP_V5 (PAD, Y);
  input PAD;
  output Y;

  buf (Y, PAD);

`ifdef functional
`else
  specify
    (PAD => Y) = (1,1);
  endspecify
`endif

endmodule

module VDD3IP_V5(A);
  inout A;
 
endmodule
 
module VDD3RP_V5(A);
  inout A;
 
 
endmodule
 
module VDD5ALLP_V5(A);
  inout A;
 
endmodule
 
module VDD5OP_V5(A);
  inout A;
 
endmodule
 
module VDD5RP_V5(A);
  inout A;
 
endmodule
 
`endcelldefine

