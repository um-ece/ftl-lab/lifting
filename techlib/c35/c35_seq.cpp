#include "c35_seq.h"

using namespace std;

#define _C35_DFF_GATE	100
#define _C35_TFF_GATE	101


c35_dff::c35_dff (const char* name, int hasEnable, int hasReset, int hasSet, int isScan, string gateType) : generic_ff(name) {
	int i=2;

	inputs_number=2+hasEnable+hasReset+hasSet+2*isScan;
	outputs_number=2;
	inputs_name = "C,D,";
	if (hasEnable==1) {
		inputs_name += "E,";
		enablePosition=i++;
	}
	if (hasReset==1) {
		inputs_name += "RN,";
		resetPosition=i++;
	}
	if (hasSet==1) {
		inputs_name += "SN,";
		setPosition=i++;
	}
	if (isScan==1) {
		inputs_name += "SD,SE,";
	}
	inputs_name=inputs_name.substr(0, inputs_name.length()-1);
	outputs_name = "Q,QN";
	type=_C35_DFF_GATE;
	this->hasEnable=hasEnable;
	this->hasReset=hasReset;
	this->hasSet=hasSet;
	this->isScan=isScan;
	setPhysicalParameters (gateType);
	InitAfterConstr ();
};


// This faunction has to be plaved in the file "c35_FF_parms.cpp"
// And the file has to be added in the Makefile
void c35_dff::setPhysicalParameters (string gateType) {
if (gateType=="DF1") {
input_capa[0] = 4.354; 
input_capa[1] = 6.918; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DF3") {
input_capa[0] = 4.354; 
input_capa[1] = 6.917; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFC1") {
input_capa[0] = 4.334; 
input_capa[1] = 6.074; 
input_capa[2] = 12.577; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFC3") {
input_capa[0] = 4.334; 
input_capa[1] = 6.603; 
input_capa[2] = 12.937; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFCP1") {
input_capa[0] = 4.480; 
input_capa[1] = 5.792; 
input_capa[2] = 18.841; 
input_capa[3] = 16.225; 
gate_size[0] = 25.200 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFCP3") {
input_capa[0] = 4.480; 
input_capa[1] = 5.787; 
input_capa[2] = 18.748; 
input_capa[3] = 16.894; 
gate_size[0] = 25.200 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFE1") {
input_capa[0] = 4.346; 
input_capa[1] = 10.384; 
input_capa[2] = 7.457; 
gate_size[0] = 25.200 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFE3") {
input_capa[0] = 4.346; 
input_capa[1] = 10.223; 
input_capa[2] = 7.559; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFEC1") {
input_capa[0] = 4.408; 
input_capa[1] = 10.574; 
input_capa[2] = 7.876; 
input_capa[3] = 13.083; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFEC3") {
input_capa[0] = 4.409; 
input_capa[1] = 10.492; 
input_capa[2] = 7.868; 
input_capa[3] = 13.068; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFECP1") {
input_capa[0] = 4.344; 
input_capa[1] = 10.729; 
input_capa[2] = 7.421; 
input_capa[3] = 18.431; 
input_capa[4] = 16.865; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFECP3") {
input_capa[0] = 4.342; 
input_capa[1] = 10.665; 
input_capa[2] = 7.223; 
input_capa[3] = 17.644; 
input_capa[4] = 16.129; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFEP1") {
input_capa[0] = 4.393; 
input_capa[1] = 10.512; 
input_capa[2] = 6.758; 
input_capa[3] = 11.922; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFEP3") {
input_capa[0] = 4.394; 
input_capa[1] = 10.431; 
input_capa[2] = 6.730; 
input_capa[3] = 11.907; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFP1") {
input_capa[0] = 4.645; 
input_capa[1] = 6.824; 
input_capa[2] = 14.153; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFP3") {
input_capa[0] = 4.645; 
input_capa[1] = 6.826; 
input_capa[2] = 14.220; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFS1") {
input_capa[0] = 4.344; 
input_capa[1] = 4.824; 
input_capa[2] = 11.256; 
input_capa[3] = 7.839; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFS3") {
input_capa[0] = 4.344; 
input_capa[1] = 4.829; 
input_capa[2] = 10.664; 
input_capa[3] = 7.487; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSC1") {
input_capa[0] = 4.407; 
input_capa[1] = 4.666; 
input_capa[2] = 13.007; 
input_capa[3] = 10.748; 
input_capa[4] = 7.695; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSC3") {
input_capa[0] = 4.407; 
input_capa[1] = 4.658; 
input_capa[2] = 12.986; 
input_capa[3] = 10.663; 
input_capa[4] = 7.745; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSCP1") {
input_capa[0] = 4.344; 
input_capa[1] = 4.521; 
input_capa[2] = 18.626; 
input_capa[3] = 10.477; 
input_capa[4] = 7.563; 
input_capa[5] = 15.878; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSCP3") {
input_capa[0] = 4.341; 
input_capa[1] = 4.535; 
input_capa[2] = 18.586; 
input_capa[3] = 10.917; 
input_capa[4] = 7.438; 
input_capa[5] = 16.124; 
gate_size[0] = 32.200 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSE1") {
input_capa[0] = 4.341; 
input_capa[1] = 11.057; 
input_capa[2] = 7.567; 
input_capa[3] = 10.709; 
input_capa[4] = 7.852; 
gate_size[0] = 32.200 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSE3") {
input_capa[0] = 4.341; 
input_capa[1] = 10.967; 
input_capa[2] = 7.549; 
input_capa[3] = 10.986; 
input_capa[4] = 7.824; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSEC1") {
input_capa[0] = 4.405; 
input_capa[1] = 10.581; 
input_capa[2] = 7.875; 
input_capa[3] = 13.072; 
input_capa[4] = 10.951; 
input_capa[5] = 7.524; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSEC3") {
input_capa[0] = 4.405; 
input_capa[1] = 10.229; 
input_capa[2] = 7.557; 
input_capa[3] = 12.490; 
input_capa[4] = 10.978; 
input_capa[5] = 7.781; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSECP1") {
input_capa[0] = 4.343; 
input_capa[1] = 10.741; 
input_capa[2] = 7.355; 
input_capa[3] = 17.114; 
input_capa[4] = 10.958; 
input_capa[5] = 7.672; 
input_capa[6] = 16.834; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSECP3") {
input_capa[0] = 4.342; 
input_capa[1] = 10.665; 
input_capa[2] = 7.349; 
input_capa[3] = 17.815; 
input_capa[4] = 10.246; 
input_capa[5] = 7.523; 
input_capa[6] = 16.053; 
gate_size[0] = 36.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSEP1") {
input_capa[0] = 4.393; 
input_capa[1] = 10.522; 
input_capa[2] = 7.036; 
input_capa[3] = 11.097; 
input_capa[4] = 7.496; 
input_capa[5] = 12.467; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSEP3") {
input_capa[0] = 4.393; 
input_capa[1] = 10.439; 
input_capa[2] = 6.723; 
input_capa[3] = 11.223; 
input_capa[4] = 6.797; 
input_capa[5] = 11.770; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSP1") {
input_capa[0] = 4.119; 
input_capa[1] = 4.490; 
input_capa[2] = 11.221; 
input_capa[3] = 7.829; 
input_capa[4] = 14.532; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="DFSP3") {
input_capa[0] = 4.119; 
input_capa[1] = 4.493; 
input_capa[2] = 11.186; 
input_capa[3] = 7.848; 
input_capa[4] = 14.520; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
} 
}

void c35_dff::calculate_output(int o) {
	extern int current_time;
	int mytemp, modified=0;
	//printf("\n*************input_value = %d, old_clk = %d \n",input_value[0], old_clk);
	if (hasReset==1 && input_value[resetPosition]==0) {
		mytemp = 0;
		modified=1;
	} else if (hasSet==1 && input_value[setPosition]==0) {
		mytemp = 1;		     			
		modified=1;
#ifdef __TIMING_SIMULATION__
	} else if (old_clk==0 && input_value[0]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
			(hasSet==0 || input_value[setPosition]==1) &&  (hasEnable==0 ||  input_value[enablePosition]==1)  )   {
		mytemp = DFF_TRUTH_TABLE[input_value[1]];
		modified=1;
		
		if(current_time - setup_time[data_port][0][old_data][1] < last_input_change[data_port]) {
			mytemp = 2;
#ifdef __DEBUG__			
			printf("WARNING : setup time error!\n");
			printf("current_time = %d ;setup_time[%d][0][%d][1] = %d; last_input_change[%d] = %d",current_time,data_port,old_data,setup_time[data_port][0][old_data][1],data_port,last_input_change[data_port]);
			cout << " --> " << name <<"\n"<<endl;
#endif			
		}
		//propagate_output_flag = 0;
	} else if ((old_data != input_value[1]) && 
				//(current_time - hold_time[data_port][0][input_value[1]][1] - clk_width[0][1]< last_input_change[0]) &&
				(current_time - hold_time[data_port][0][input_value[1]][1] < last_input_change[0]) &&
				(current_time != 0) && (current_time != last_input_change[0]) &&(last_input_change[0]!=0)){
		//propagate_output_flag = 0;
		mytemp = 2;
		modified=1;
#ifdef __DEBUG__		
		printf("WARNING : hold time error!\n");
		printf("current_time = %d ;hold_time[%d][0][%d][1] = %d;clk_width[0][1] = %d; last_input_change[0] = %d",current_time,data_port,old_data,hold_time[data_port][0][input_value[1]][1],clk_width[0][1],last_input_change[0]);
		cout << " --> " << name <<"\n"<<endl;
#endif		
#else
	} else if (old_clk==0 && input_value[0]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
			(hasSet==0 || input_value[setPosition]==1) &&  (hasEnable==0 ||  input_value[enablePosition]==1)  )   {
		mytemp = DFF_TRUTH_TABLE[input_value[1]];
		//printf ("New FF value: %d\n", mytemp);
		modified=1;
		propagate_output_flag = 0;
	} else {
		propagate_output_flag = 0;
#endif
	}

	// This function is called just for combinational part of the flip flop (async set and reset, that are active low)
	if (modified==1) {
		if (o==0) {
			temp_output_value[0]=mytemp;
		} else if (o==1) {
			temp_output_value[1]=NOT_TRUTH_TABLE[mytemp];
		} else {
			printf ("ERROR: calculate_output for a Flip-Flop with o=%d.\n", o);
			exit(-1);
		}
		//printf("FF gate ++++++++++++++++++++ temp_output_value[%d] = %d \n",o,temp_output_value[o]);
	}
};
	
#ifdef __TIMING_SIMULATION__
void c35_dff::set_input_timing (int port, int value) {
	if (FF_data_input_flag[port]==1) {
		data_port=port;
	}
	generic_gate::set_input (port,value);
	if (port == 0 && input_value[0] == 1) {
		last_input_change[port]=current_time;
	} else if (port != 0){
		last_input_change[port]=current_time;
	}
	//printf("###################### last_input_change[%d] = %d\n",port,last_input_change[port]);
	//cout << " --> " << name <<"\n"<<endl;
	if (port==0) {
		old_clk=input_value[0];
	}
	if (FF_data_input_flag[port]==1) {
		//printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!FF_data_input_flag[%d] = 1\n",port);
		old_data=input_value[port];
	}
};
#endif

#ifndef __TIMING_SIMULATION__
void c35_dff::set_input_logic (int port, int value) {
	generic_gate::set_input (port,value);
	if (port==0) {
		old_clk=input_value[0];
	}
};
#endif

int c35_dff::is_clock (int port) {
	//generic_gate::is_clock (port); //---------lu 04/09/2012
	
	if (port==0) return 1;
	return 0;
};

void c35_dff::load_FF(int value) {
	if (stuck_at_output[0]==0) {
		temp_output_value[0]=value;
		output_value[0]=value;
		propagate_output(0);
	}
	if (stuck_at_output[1]==0) {
		temp_output_value[1]=DFF_NOT_TRUTH_TABLE[value];
		output_value[1]=DFF_NOT_TRUTH_TABLE[value];
		propagate_output(1);
	}
};

void c35_dff::bit_flip_FF() {
	temp_output_value[0]=DFF_NOT_TRUTH_TABLE[temp_output_value[0]];
	output_value[0]=temp_output_value[0];
	propagate_output(0);
	temp_output_value[1]=DFF_NOT_TRUTH_TABLE[temp_output_value[1]];
	output_value[1]=temp_output_value[1];
	propagate_output(1);
};



c35_tff::c35_tff (const char* name, int hasEnable, int hasReset, int hasSet, int isScan, string gateType) : generic_ff(name) {
	int i=1;

	inputs_number=1+hasEnable+hasReset+hasSet+2*isScan;
	outputs_number=2;
	inputs_name = "C,";
	if (hasEnable==1) {
		inputs_name += "T,";
		enablePosition=i++;
	}
	if (hasReset==1) {
		inputs_name += "RN,";
		resetPosition=i++;
	}
	if (hasSet==1) {
		inputs_name += "SN,";
		setPosition=i++;
	}
	if (isScan==1) {
		inputs_name += "SD,SE,";
		sdPosition=i++;
		sePosition=i++;
	}
	inputs_name=inputs_name.substr(0, inputs_name.length()-1);
	outputs_name = "Q,QN";
	type=_C35_DFF_GATE;
	this->hasEnable=hasEnable;
	this->hasReset=hasReset;
	this->hasSet=hasSet;
	this->isScan=isScan;
	setPhysicalParameters (gateType);
	InitAfterConstr ();
};
void c35_tff::setPhysicalParameters (string gateType) {
if (gateType=="TFC1") {
input_capa[0] = 4.443; 
input_capa[1] = 15.237; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFC3") {
input_capa[0] = 4.443; 
input_capa[1] = 14.846; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFCP1") {
input_capa[0] = 4.360; 
input_capa[1] = 17.923; 
input_capa[2] = 14.498; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFCP3") {
input_capa[0] = 4.358; 
input_capa[1] = 18.818; 
input_capa[2] = 14.809; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFEC1") {
input_capa[0] = 4.406; 
input_capa[1] = 12.915; 
input_capa[2] = 7.685; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFEC3") {
input_capa[0] = 4.407; 
input_capa[1] = 12.925; 
input_capa[2] = 7.675; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFECP1") {
input_capa[0] = 4.342; 
input_capa[1] = 17.769; 
input_capa[2] = 15.900; 
input_capa[3] = 6.685; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFECP3") {
input_capa[0] = 4.340; 
input_capa[1] = 18.673; 
input_capa[2] = 16.077; 
input_capa[3] = 6.819; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFEP1") {
input_capa[0] = 4.393; 
input_capa[1] = 12.080; 
input_capa[2] = 5.479; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFEP3") {
input_capa[0] = 4.394; 
input_capa[1] = 12.069; 
input_capa[2] = 5.511; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFP1") {
input_capa[0] = 4.483; 
input_capa[1] = 14.026; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFP3") {
input_capa[0] = 4.482; 
input_capa[1] = 13.952; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSC1") {
input_capa[0] = 4.433; 
input_capa[1] = 15.343; 
input_capa[2] = 10.279; 
input_capa[3] = 7.299; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSC3") {
input_capa[0] = 4.383; 
input_capa[1] = 15.374; 
input_capa[2] = 11.194; 
input_capa[3] = 6.764; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSCP1") {
input_capa[0] = 4.348; 
input_capa[1] = 18.271; 
input_capa[2] = 10.597; 
input_capa[3] = 7.670; 
input_capa[4] = 14.740; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSCP3") {
input_capa[0] = 4.347; 
input_capa[1] = 18.095; 
input_capa[2] = 10.483; 
input_capa[3] = 7.686; 
input_capa[4] = 14.569; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSEC1") {
input_capa[0] = 4.479; 
input_capa[1] = 13.331; 
input_capa[2] = 11.909; 
input_capa[3] = 7.346; 
input_capa[4] = 8.125; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSEC3") {
input_capa[0] = 4.480; 
input_capa[1] = 13.336; 
input_capa[2] = 11.911; 
input_capa[3] = 7.344; 
input_capa[4] = 8.034; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSECP1") {
input_capa[0] = 4.375; 
input_capa[1] = 18.140; 
input_capa[2] = 10.970; 
input_capa[3] = 7.327; 
input_capa[4] = 15.915; 
input_capa[5] = 6.693; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSECP3") {
input_capa[0] = 4.375; 
input_capa[1] = 18.408; 
input_capa[2] = 10.938; 
input_capa[3] = 6.926; 
input_capa[4] = 16.841; 
input_capa[5] = 6.840; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSEP1") {
input_capa[0] = 4.485; 
input_capa[1] = 11.086; 
input_capa[2] = 6.622; 
input_capa[3] = 12.351; 
input_capa[4] = 5.378; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSEP3") {
input_capa[0] = 4.485; 
input_capa[1] = 11.078; 
input_capa[2] = 6.620; 
input_capa[3] = 12.342; 
input_capa[4] = 5.385; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSP1") {
input_capa[0] = 4.118; 
input_capa[1] = 11.147; 
input_capa[2] = 8.066; 
input_capa[3] = 14.102; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
} else if (gateType=="TFSP3") {
input_capa[0] = 4.119; 
input_capa[1] = 10.530; 
input_capa[2] = 8.042; 
input_capa[3] = 14.159; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
}
}
void c35_tff::calculate_output(int o) {
	// This function is called just for combinational part of the flip flop (async set and reset, that are active low)
	if (o==0) {
		if (hasReset==1 && input_value[resetPosition]==0) {
			temp_output_value[0] = 0;
		} else if (hasSet==1 && input_value[setPosition]==0) {
			temp_output_value[0] = 1;		     			
		} else if (old_clk==0 && input_value[0]==1 && isScan==1 && input_value[sePosition]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
				(hasSet==0 || input_value[setPosition]==1) )   {
			temp_output_value[0] = input_value[sdPosition];
			propagate_output_flag = 0;
		} else if (old_clk==0 && input_value[0]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
				(hasSet==0 || input_value[setPosition]==1) &&  (hasEnable==0 ||  input_value[enablePosition]==1)  )   {
			temp_output_value[0] = DFF_NOT_TRUTH_TABLE[temp_output_value[0]];
			//propagate_output_flag = 0;
		} else {
			//propagate_output_flag = 0;
		}
	} else if (o==1) {
		if (hasReset==1 && input_value[resetPosition]==0) {
			temp_output_value[1] = 1;
		} else if (hasSet==1 && input_value[setPosition]==0) {
			temp_output_value[1] = 0;
		} else if (old_clk==0 && input_value[0]==1 && isScan==1 && input_value[sePosition]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
				(hasSet==0 || input_value[setPosition]==1) )   {
			temp_output_value[0] = DFF_NOT_TRUTH_TABLE[input_value[sdPosition]];
			propagate_output_flag = 0;
		} else if (old_clk==0 && input_value[0]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
				(hasSet==0 || input_value[setPosition]==1)  &&  (hasEnable==0 ||  input_value[enablePosition]==1)  ) {
			temp_output_value[1] = DFF_NOT_TRUTH_TABLE[temp_output_value[1]];
			//propagate_output_flag = 0;
		} else {
			//propagate_output_flag = 0;
		}
	} else {
		printf ("ERROR: calculate_output for a Flip-Flop with o=%d.\n", o);
		exit(-1);
	}
};

void c35_tff::set_input (int port, int value) {
	generic_gate::set_input (port,value);
	if (port==0) {
		old_clk=input_value[0];
	}
#ifdef __TIMING_SIMULATION__
	if (FF_data_input_flag[port]==1) {
		old_data=input_value[port];
	}
#endif
}

int c35_tff::is_clock (int port) {
	//generic_gate::is_clock (port); //---------lu 04/09/2012
	if (port==0) return 1;
	return 0;
}

void c35_tff::load_FF(int value) {
	if (stuck_at_output[0]==0) {
		temp_output_value[0]=value;
		output_value[0]=value;
		input_value[sdPosition]=value;
	}
	if (stuck_at_output[1]==0) {
		temp_output_value[1]=DFF_NOT_TRUTH_TABLE[value];
		output_value[1]=DFF_NOT_TRUTH_TABLE[value];
	}
};

void c35_tff::bit_flip_FF() {
	temp_output_value[0]=DFF_NOT_TRUTH_TABLE[temp_output_value[0]];
	output_value[0]=temp_output_value[0];
	propagate_output(0);
	temp_output_value[1]=DFF_NOT_TRUTH_TABLE[temp_output_value[1]];
	output_value[1]=temp_output_value[1];
	propagate_output(1);
};


