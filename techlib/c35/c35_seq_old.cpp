#include "c35_seq.h"

using namespace std;

#define _C35_DFF_GATE	100
#define _C35_TFF_GATE	101


c35_dff::c35_dff (const char* name, int hasEnable, int hasReset, int hasSet, int isScan, string gateType) : generic_ff(name) {
	int i=2;

	inputs_number=2+hasEnable+hasReset+hasSet+2*isScan;
	outputs_number=2;
	inputs_name = "C,D,";
	if (hasEnable==1) {
		inputs_name += "E,";
		enablePosition=i++;
	}
	if (hasReset==1) {
		inputs_name += "RN,";
		resetPosition=i++;
	}
	if (hasSet==1) {
		inputs_name += "SN,";
		setPosition=i++;
	}
	if (isScan==1) {
		inputs_name += "SD,SE,";
	}
	inputs_name=inputs_name.substr(0, inputs_name.length()-1);
	outputs_name = "Q,QN";
	type=_C35_DFF_GATE;
	this->hasEnable=hasEnable;
	this->hasReset=hasReset;
	this->hasSet=hasSet;
	this->isScan=isScan;
	setPhysicalParameters (gateType);
	InitAfterConstr ();
};


// This faunction has to be plaved in the file "c35_FF_parms.cpp"
// And the file has to be added in the Makefile
void c35_dff::setPhysicalParameters (string gateType) {
	if (gateType=="DF1") {
		input_capa[0] = 4.354; 
		input_capa[1] = 6.918; 
		gate_size[0] = 21.000 ;
		gate_size[1] = 13.000 ;
	} else if (gateTYpe=="DF3") {
		input_capa[0] = 4.354; 
		input_capa[1] = 6.917; 
		gate_size[0] = 21.000 ;
		gate_size[1] = 13.000 ;
	}
}

void c35_dff::calculate_output(int o) {
	extern int current_time;
	int mytemp, modified=0;
	//printf("\n*************input_value = %d, old_clk = %d \n",input_value[0], old_clk);
	if (hasReset==1 && input_value[resetPosition]==0) {
		mytemp = 0;
		modified=1;
	} else if (hasSet==1 && input_value[setPosition]==0) {
		mytemp = 1;		     			
		modified=1;
#ifdef __TIMING_SIMULATION__
	} else if (old_clk==0 && input_value[0]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
			(hasSet==0 || input_value[setPosition]==1) &&  (hasEnable==0 ||  input_value[enablePosition]==1)  )   {
		mytemp = DFF_TRUTH_TABLE[input_value[1]];
		modified=1;
		
		if(current_time - setup_time[data_port][0][old_data][1] < last_input_change[data_port]) {
			mytemp = 2;
			printf("WARNING : setup time error!\n");
			printf("current_time = %d ;setup_time[%d][0][%d][1] = %d; last_input_change[%d] = %d",current_time,data_port,old_data,setup_time[data_port][0][old_data][1],data_port,last_input_change[data_port]);
			cout << " --> " << name <<"\n"<<endl;
		}
		//propagate_output_flag = 0;
	} else if ((old_data != input_value[1]) && 
				//(current_time - hold_time[data_port][0][input_value[1]][1] - clk_width[0][1]< last_input_change[0]) &&
				(current_time - hold_time[data_port][0][input_value[1]][1] < last_input_change[0]) &&
				(current_time != 0) && (current_time != last_input_change[0]) &&(last_input_change[0]!=0)){
		//propagate_output_flag = 0;
		mytemp = 2;
		modified=1;
		printf("WARNING : hold time error!\n");
		printf("current_time = %d ;hold_time[%d][0][%d][1] = %d;clk_width[0][1] = %d; last_input_change[0] = %d",current_time,data_port,old_data,hold_time[data_port][0][input_value[1]][1],clk_width[0][1],last_input_change[0]);
		cout << " --> " << name <<"\n"<<endl;
#else
	} else if (old_clk==0 && input_value[0]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
			(hasSet==0 || input_value[setPosition]==1) &&  (hasEnable==0 ||  input_value[enablePosition]==1)  )   {
		mytemp = DFF_TRUTH_TABLE[input_value[1]];
		//printf ("New FF value: %d\n", mytemp);
		modified=1;
		propagate_output_flag = 0;
	} else {
		propagate_output_flag = 0;
#endif
	}

	// This function is called just for combinational part of the flip flop (async set and reset, that are active low)
	if (modified==1) {
		if (o==0) {
			temp_output_value[0]=mytemp;
		} else if (o==1) {
			temp_output_value[1]=NOT_TRUTH_TABLE[mytemp];
		} else {
			printf ("ERROR: calculate_output for a Flip-Flop with o=%d.\n", o);
			exit(-1);
		}
		//printf("FF gate ++++++++++++++++++++ temp_output_value[%d] = %d \n",o,temp_output_value[o]);
	}
};
	
#ifdef __TIMING_SIMULATION__
void c35_dff::set_input_timing (int port, int value) {
	if (FF_data_input_flag[port]==1) {
		data_port=port;
	}
	generic_gate::set_input (port,value);
	if (port == 0 && input_value[0] == 1) {
		last_input_change[port]=current_time;
	} else if (port != 0){
		last_input_change[port]=current_time;
	}
	//printf("###################### last_input_change[%d] = %d\n",port,last_input_change[port]);
	//cout << " --> " << name <<"\n"<<endl;
	if (port==0) {
		old_clk=input_value[0];
	}
	if (FF_data_input_flag[port]==1) {
		//printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!FF_data_input_flag[%d] = 1\n",port);
		old_data=input_value[port];
	}
};
#endif

#ifndef __TIMING_SIMULATION__
void c35_dff::set_input_logic (int port, int value) {
	generic_gate::set_input (port,value);
	if (port==0) {
		old_clk=input_value[0];
	}
};
#endif

int c35_dff::is_clock (int port) {
	//generic_gate::is_clock (port); //---------lu 04/09/2012
	
	if (port==0) return 1;
	return 0;
};

void c35_dff::load_FF(int value) {
	if (stuck_at_output[0]==0) {
		temp_output_value[0]=value;
		output_value[0]=value;
		propagate_output(0);
	}
	if (stuck_at_output[1]==0) {
		temp_output_value[1]=DFF_NOT_TRUTH_TABLE[value];
		output_value[1]=DFF_NOT_TRUTH_TABLE[value];
		propagate_output(1);
	}
};

void c35_dff::bit_flip_FF() {
	temp_output_value[0]=DFF_NOT_TRUTH_TABLE[temp_output_value[0]];
	output_value[0]=temp_output_value[0];
	propagate_output(0);
	temp_output_value[1]=DFF_NOT_TRUTH_TABLE[temp_output_value[1]];
	output_value[1]=temp_output_value[1];
	propagate_output(1);
};



c35_tff::c35_tff (const char* name, int hasEnable, int hasReset, int hasSet, int isScan, string gateType) : generic_ff(name) {
	int i=1;

	inputs_number=1+hasEnable+hasReset+hasSet+2*isScan;
	outputs_number=2;
	inputs_name = "C,";
	if (hasEnable==1) {
		inputs_name += "T,";
		enablePosition=i++;
	}
	if (hasReset==1) {
		inputs_name += "RN,";
		resetPosition=i++;
	}
	if (hasSet==1) {
		inputs_name += "SN,";
		setPosition=i++;
	}
	if (isScan==1) {
		inputs_name += "SD,SE,";
		sdPosition=i++;
		sePosition=i++;
	}
	inputs_name=inputs_name.substr(0, inputs_name.length()-1);
	outputs_name = "Q,QN";
	type=_C35_DFF_GATE;
	this->hasEnable=hasEnable;
	this->hasReset=hasReset;
	this->hasSet=hasSet;
	this->isScan=isScan;
	setPhysicalParameters (gateType);
	InitAfterConstr ();
};

void c35_tff::calculate_output(int o) {
	// This function is called just for combinational part of the flip flop (async set and reset, that are active low)
	if (o==0) {
		if (hasReset==1 && input_value[resetPosition]==0) {
			temp_output_value[0] = 0;
		} else if (hasSet==1 && input_value[setPosition]==0) {
			temp_output_value[0] = 1;		     			
		} else if (old_clk==0 && input_value[0]==1 && isScan==1 && input_value[sePosition]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
				(hasSet==0 || input_value[setPosition]==1) )   {
			temp_output_value[0] = input_value[sdPosition];
			propagate_output_flag = 0;
		} else if (old_clk==0 && input_value[0]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
				(hasSet==0 || input_value[setPosition]==1) &&  (hasEnable==0 ||  input_value[enablePosition]==1)  )   {
			temp_output_value[0] = DFF_NOT_TRUTH_TABLE[temp_output_value[0]];
			//propagate_output_flag = 0;
		} else {
			//propagate_output_flag = 0;
		}
	} else if (o==1) {
		if (hasReset==1 && input_value[resetPosition]==0) {
			temp_output_value[1] = 1;
		} else if (hasSet==1 && input_value[setPosition]==0) {
			temp_output_value[1] = 0;
		} else if (old_clk==0 && input_value[0]==1 && isScan==1 && input_value[sePosition]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
				(hasSet==0 || input_value[setPosition]==1) )   {
			temp_output_value[0] = DFF_NOT_TRUTH_TABLE[input_value[sdPosition]];
			propagate_output_flag = 0;
		} else if (old_clk==0 && input_value[0]==1 && (hasReset==0 || input_value[resetPosition]==1)  && 
				(hasSet==0 || input_value[setPosition]==1)  &&  (hasEnable==0 ||  input_value[enablePosition]==1)  ) {
			temp_output_value[1] = DFF_NOT_TRUTH_TABLE[temp_output_value[1]];
			//propagate_output_flag = 0;
		} else {
			//propagate_output_flag = 0;
		}
	} else {
		printf ("ERROR: calculate_output for a Flip-Flop with o=%d.\n", o);
		exit(-1);
	}
};

void c35_tff::set_input (int port, int value) {
	generic_gate::set_input (port,value);
	if (port==0) {
		old_clk=input_value[0];
	}
#ifdef __TIMING_SIMULATION__
	if (FF_data_input_flag[port]==1) {
		old_data=input_value[port];
	}
#endif
}

int c35_tff::is_clock (int port) {
	//generic_gate::is_clock (port); //---------lu 04/09/2012
	if (port==0) return 1;
	return 0;
}

void c35_tff::load_FF(int value) {
	if (stuck_at_output[0]==0) {
		temp_output_value[0]=value;
		output_value[0]=value;
		input_value[sdPosition]=value;
	}
	if (stuck_at_output[1]==0) {
		temp_output_value[1]=DFF_NOT_TRUTH_TABLE[value];
		output_value[1]=DFF_NOT_TRUTH_TABLE[value];
	}
};

void c35_tff::bit_flip_FF() {
	temp_output_value[0]=DFF_NOT_TRUTH_TABLE[temp_output_value[0]];
	output_value[0]=temp_output_value[0];
	propagate_output(0);
	temp_output_value[1]=DFF_NOT_TRUTH_TABLE[temp_output_value[1]];
	output_value[1]=temp_output_value[1];
	propagate_output(1);
};


