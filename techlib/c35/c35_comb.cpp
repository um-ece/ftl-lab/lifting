
#define _TYPE_COMBINATIONAL 1
#include "c35_comb.h"

using namespace std;

c35_ADD21 :: c35_ADD21 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "A,B";
	outputs_name = "CO,S";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.798; 
input_capa[1] = 11.083; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_ADD21 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_ADD21_S[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_ADD21_CO[input_value[1]][input_value[0]];
	}
};

c35_ADD22 :: c35_ADD22 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "A,B";
	outputs_name = "CO,S";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 14.689; 
input_capa[1] = 19.106; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_ADD22 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_ADD22_S[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_ADD22_CO[input_value[1]][input_value[0]];
	}
};

c35_ADD31 :: c35_ADD31 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "A,B,CI";
	outputs_name = "CO,S";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 26.591; 
input_capa[1] = 25.110; 
input_capa[2] = 17.827; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_ADD31 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_ADD31_S[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_ADD31_CO[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_ADD32 :: c35_ADD32 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "A,B,CI";
	outputs_name = "CO,S";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 43.898; 
input_capa[1] = 38.850; 
input_capa[2] = 30.264; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_ADD32 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_ADD32_S[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_ADD32_CO[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_AOI210 :: c35_AOI210 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.938; 
input_capa[1] = 6.149; 
input_capa[2] = 5.687; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI210 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI210_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI211 :: c35_AOI211 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.111; 
input_capa[1] = 11.285; 
input_capa[2] = 8.576; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI211 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI211_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI2110 :: c35_AOI2110 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.064; 
input_capa[1] = 7.919; 
input_capa[2] = 7.245; 
input_capa[3] = 7.276; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI2110 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI2110_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI2111 :: c35_AOI2111 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.498; 
input_capa[1] = 13.550; 
input_capa[2] = 10.900; 
input_capa[3] = 11.435; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI2111 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI2111_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI2112 :: c35_AOI2112 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.557; 
input_capa[1] = 25.818; 
input_capa[2] = 22.307; 
input_capa[3] = 20.887; 
gate_size[0] = 9.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI2112 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI2112_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI212 :: c35_AOI212 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 12.499; 
input_capa[1] = 18.959; 
input_capa[2] = 19.255; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI212 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI212_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI220 :: c35_AOI220 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.970; 
input_capa[1] = 6.502; 
input_capa[2] = 5.294; 
input_capa[3] = 6.550; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI220 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI220_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI221 :: c35_AOI221 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.092; 
input_capa[1] = 11.193; 
input_capa[2] = 8.600; 
input_capa[3] = 10.533; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI221 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI221_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI222 :: c35_AOI222 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 14.475; 
input_capa[1] = 19.303; 
input_capa[2] = 15.636; 
input_capa[3] = 19.667; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI222 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI222_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI310 :: c35_AOI310 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.107; 
input_capa[1] = 6.584; 
input_capa[2] = 5.848; 
input_capa[3] = 6.017; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI310 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI310_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI311 :: c35_AOI311 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 12.204; 
input_capa[1] = 10.368; 
input_capa[2] = 8.715; 
input_capa[3] = 8.824; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI311 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI311_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_AOI312 :: c35_AOI312 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 23.085; 
input_capa[1] = 17.541; 
input_capa[2] = 16.457; 
input_capa[3] = 15.783; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_AOI312 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_AOI312_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_BUF12 :: c35_BUF12 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 14.766; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUF12 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUF12_Q[input_value[0]];
};

c35_BUF15 :: c35_BUF15 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.753; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUF15 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUF15_Q[input_value[0]];
};

c35_BUF2 :: c35_BUF2 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 3.349; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUF2 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUF2_Q[input_value[0]];
};

c35_BUF4 :: c35_BUF4 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.554; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUF4 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUF4_Q[input_value[0]];
};

c35_BUF6 :: c35_BUF6 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.343; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUF6 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUF6_Q[input_value[0]];
};

c35_BUF8 :: c35_BUF8 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.424; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUF8 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUF8_Q[input_value[0]];
};

c35_BUFE10 :: c35_BUFE10 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,E";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 24.591; 
input_capa[1] = 35.156; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFE10 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFE10_Q[input_value[1]][input_value[0]];
};

c35_BUFE12 :: c35_BUFE12 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,E";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 29.629; 
input_capa[1] = 41.453; 
gate_size[0] = 25.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFE12 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFE12_Q[input_value[1]][input_value[0]];
};

c35_BUFE15 :: c35_BUFE15 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,E";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 36.720; 
input_capa[1] = 50.718; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFE15 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFE15_Q[input_value[1]][input_value[0]];
};

c35_BUFE2 :: c35_BUFE2 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,E";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.192; 
input_capa[1] = 9.467; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFE2 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFE2_Q[input_value[1]][input_value[0]];
};

c35_BUFE4 :: c35_BUFE4 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,E";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.558; 
input_capa[1] = 15.928; 
gate_size[0] = 12.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFE4 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFE4_Q[input_value[1]][input_value[0]];
};

c35_BUFE6 :: c35_BUFE6 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,E";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 15.313; 
input_capa[1] = 21.217; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFE6 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFE6_Q[input_value[1]][input_value[0]];
};

c35_BUFE8 :: c35_BUFE8 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,E";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.528; 
input_capa[1] = 28.981; 
gate_size[0] = 19.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFE8 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFE8_Q[input_value[1]][input_value[0]];
};

c35_BUFT10 :: c35_BUFT10 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 25.330; 
input_capa[1] = 33.531; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFT10 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFT10_Q[input_value[1]][input_value[0]];
};

c35_BUFT12 :: c35_BUFT12 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 29.428; 
input_capa[1] = 42.275; 
gate_size[0] = 25.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFT12 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFT12_Q[input_value[1]][input_value[0]];
};

c35_BUFT15 :: c35_BUFT15 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 36.008; 
input_capa[1] = 50.688; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFT15 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFT15_Q[input_value[1]][input_value[0]];
};

c35_BUFT2 :: c35_BUFT2 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.292; 
input_capa[1] = 9.726; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFT2 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFT2_Q[input_value[1]][input_value[0]];
};

c35_BUFT4 :: c35_BUFT4 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.222; 
input_capa[1] = 15.814; 
gate_size[0] = 12.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFT4 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFT4_Q[input_value[1]][input_value[0]];
};

c35_BUFT6 :: c35_BUFT6 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 14.682; 
input_capa[1] = 21.349; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFT6 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFT6_Q[input_value[1]][input_value[0]];
};

c35_BUFT8 :: c35_BUFT8 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.514; 
input_capa[1] = 28.951; 
gate_size[0] = 19.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_BUFT8 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUFT8_Q[input_value[1]][input_value[0]];
};

c35_CLKBU12 :: c35_CLKBU12 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 13.749; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKBU12 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKBU12_Q[input_value[0]];
};

c35_CLKBU15 :: c35_CLKBU15 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 19.296; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKBU15 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKBU15_Q[input_value[0]];
};

c35_CLKBU2 :: c35_CLKBU2 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 3.923; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKBU2 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKBU2_Q[input_value[0]];
};

c35_CLKBU4 :: c35_CLKBU4 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.497; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKBU4 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKBU4_Q[input_value[0]];
};

c35_CLKBU6 :: c35_CLKBU6 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.612; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKBU6 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKBU6_Q[input_value[0]];
};

c35_CLKBU8 :: c35_CLKBU8 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 9.419; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKBU8 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKBU8_Q[input_value[0]];
};

c35_CLKIN0 :: c35_CLKIN0 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.535; 
gate_size[0] = 2.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN0 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN0_Q[input_value[0]];
};

c35_CLKIN1 :: c35_CLKIN1 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.138; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN1 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN1_Q[input_value[0]];
};

c35_CLKIN10 :: c35_CLKIN10 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 45.671; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN10 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN10_Q[input_value[0]];
};

c35_CLKIN12 :: c35_CLKIN12 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 53.377; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN12 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN12_Q[input_value[0]];
};

c35_CLKIN15 :: c35_CLKIN15 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 65.454; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN15 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN15_Q[input_value[0]];
};

c35_CLKIN2 :: c35_CLKIN2 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 9.510; 
gate_size[0] = 2.800 ;
gate_size[1] = 13.000 ;
#ifdef __TIMING_SIMULATION__
number_pnj = 4;
location_pnj[0][0]=1000; location_pnj[0][1]=10000;
bottom_left_pnj[0][0]=300;bottom_left_pnj[0][1]=8300;
		netlilst_pnj[0].node_name="inh_vdd";
		netlilst_pnj[0].cmos_name="mp2";
		netlilst_pnj[0].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[0].name_subcell="xi3";
		netlilst_pnj[0].type_subcell="invb_core";	
location_pnj[1][0]=2000; location_pnj[1][1]=10000;
bottom_left_pnj[1][0]=1600;bottom_left_pnj[1][1]=8300;
		netlilst_pnj[1].node_name="out";
		netlilst_pnj[1].cmos_name="mp2";
		netlilst_pnj[1].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[1].name_subcell="xi3";
		netlilst_pnj[1].type_subcell="invb_core";
location_pnj[2][0]=800; location_pnj[2][1]=2400;
bottom_left_pnj[2][0]=300;bottom_left_pnj[2][1]=1550;
		netlilst_pnj[2].node_name="inh_gnd";
		netlilst_pnj[2].cmos_name="mn2";
		netlilst_pnj[2].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[2].name_subcell="xi3";
		netlilst_pnj[2].type_subcell="invb_core";
location_pnj[3][0]=2000; location_pnj[3][1]=2400;
bottom_left_pnj[3][0]=1600;bottom_left_pnj[3][1]=1550;
		netlilst_pnj[3].node_name="out";
		netlilst_pnj[3].cmos_name="mn2";
		netlilst_pnj[3].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[3].name_subcell="xi3";
		netlilst_pnj[3].type_subcell="invb_core";
#endif
	InitAfterConstr ();
};

void c35_CLKIN2 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN2_Q[input_value[0]];
};

c35_CLKIN3 :: c35_CLKIN3 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 13.949; 
gate_size[0] = 2.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN3 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN3_Q[input_value[0]];
};

c35_CLKIN4 :: c35_CLKIN4 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 18.669; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN4 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN4_Q[input_value[0]];
};

c35_CLKIN6 :: c35_CLKIN6 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 27.463; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN6 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN6_Q[input_value[0]];
};

c35_CLKIN8 :: c35_CLKIN8 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 36.025; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_CLKIN8 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CLKIN8_Q[input_value[0]];
};

c35_DF1 :: c35_DF1 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "C,D";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.354; 
input_capa[1] = 6.918; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DF1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DF1_QN[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DF1_Q[input_value[1]][input_value[0]];
	}
};

c35_DF3 :: c35_DF3 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "C,D";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.354; 
input_capa[1] = 6.917; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DF3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DF3_QN[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DF3_Q[input_value[1]][input_value[0]];
	}
};

c35_DFC1 :: c35_DFC1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,D,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.334; 
input_capa[1] = 6.074; 
input_capa[2] = 12.577; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFC1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFC1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFC3 :: c35_DFC3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,D,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.334; 
input_capa[1] = 6.603; 
input_capa[2] = 12.937; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFC3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFC3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFCP1 :: c35_DFCP1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,D,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.480; 
input_capa[1] = 5.792; 
input_capa[2] = 18.841; 
input_capa[3] = 16.225; 
gate_size[0] = 25.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFCP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFCP1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFCP1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFCP3 :: c35_DFCP3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,D,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.480; 
input_capa[1] = 5.787; 
input_capa[2] = 18.748; 
input_capa[3] = 16.894; 
gate_size[0] = 25.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFCP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFCP3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFCP3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFE1 :: c35_DFE1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,D,E";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.346; 
input_capa[1] = 10.384; 
input_capa[2] = 7.457; 
gate_size[0] = 25.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFE1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFE1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFE1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFE3 :: c35_DFE3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,D,E";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.346; 
input_capa[1] = 10.223; 
input_capa[2] = 7.559; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFE3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFE3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFE3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFEC1 :: c35_DFEC1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,D,E,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.408; 
input_capa[1] = 10.574; 
input_capa[2] = 7.876; 
input_capa[3] = 13.083; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFEC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFEC1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFEC1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFEC3 :: c35_DFEC3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,D,E,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.409; 
input_capa[1] = 10.492; 
input_capa[2] = 7.868; 
input_capa[3] = 13.068; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFEC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFEC3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFEC3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFECP1 :: c35_DFECP1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,D,E,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.344; 
input_capa[1] = 10.729; 
input_capa[2] = 7.421; 
input_capa[3] = 18.431; 
input_capa[4] = 16.865; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFECP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFECP1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFECP1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFECP3 :: c35_DFECP3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,D,E,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.342; 
input_capa[1] = 10.665; 
input_capa[2] = 7.223; 
input_capa[3] = 17.644; 
input_capa[4] = 16.129; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFECP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFECP3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFECP3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFEP1 :: c35_DFEP1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,D,E,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.393; 
input_capa[1] = 10.512; 
input_capa[2] = 6.758; 
input_capa[3] = 11.922; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFEP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFEP1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFEP1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFEP3 :: c35_DFEP3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,D,E,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.394; 
input_capa[1] = 10.431; 
input_capa[2] = 6.730; 
input_capa[3] = 11.907; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFEP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFEP3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFEP3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFP1 :: c35_DFP1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,D,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.645; 
input_capa[1] = 6.824; 
input_capa[2] = 14.153; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFP1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFP1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFP3 :: c35_DFP3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,D,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.645; 
input_capa[1] = 6.826; 
input_capa[2] = 14.220; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFP3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFP3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFS1 :: c35_DFS1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,D,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.344; 
input_capa[1] = 4.824; 
input_capa[2] = 11.256; 
input_capa[3] = 7.839; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFS1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFS1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFS1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFS3 :: c35_DFS3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,D,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.344; 
input_capa[1] = 4.829; 
input_capa[2] = 10.664; 
input_capa[3] = 7.487; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFS3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFS3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFS3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSC1 :: c35_DFSC1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,D,RN,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.407; 
input_capa[1] = 4.666; 
input_capa[2] = 13.007; 
input_capa[3] = 10.748; 
input_capa[4] = 7.695; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSC1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSC1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSC3 :: c35_DFSC3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,D,RN,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.407; 
input_capa[1] = 4.658; 
input_capa[2] = 12.986; 
input_capa[3] = 10.663; 
input_capa[4] = 7.745; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSC3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSC3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSCP1 :: c35_DFSCP1 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,D,RN,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.344; 
input_capa[1] = 4.521; 
input_capa[2] = 18.626; 
input_capa[3] = 10.477; 
input_capa[4] = 7.563; 
input_capa[5] = 15.878; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSCP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSCP1_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSCP1_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSCP3 :: c35_DFSCP3 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,D,RN,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.341; 
input_capa[1] = 4.535; 
input_capa[2] = 18.586; 
input_capa[3] = 10.917; 
input_capa[4] = 7.438; 
input_capa[5] = 16.124; 
gate_size[0] = 32.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSCP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSCP3_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSCP3_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSE1 :: c35_DFSE1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,D,E,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.341; 
input_capa[1] = 11.057; 
input_capa[2] = 7.567; 
input_capa[3] = 10.709; 
input_capa[4] = 7.852; 
gate_size[0] = 32.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSE1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSE1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSE1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSE3 :: c35_DFSE3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,D,E,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.341; 
input_capa[1] = 10.967; 
input_capa[2] = 7.549; 
input_capa[3] = 10.986; 
input_capa[4] = 7.824; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSE3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSE3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSE3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSEC1 :: c35_DFSEC1 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,D,E,RN,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.405; 
input_capa[1] = 10.581; 
input_capa[2] = 7.875; 
input_capa[3] = 13.072; 
input_capa[4] = 10.951; 
input_capa[5] = 7.524; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSEC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSEC1_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSEC1_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSEC3 :: c35_DFSEC3 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,D,E,RN,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.405; 
input_capa[1] = 10.229; 
input_capa[2] = 7.557; 
input_capa[3] = 12.490; 
input_capa[4] = 10.978; 
input_capa[5] = 7.781; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSEC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSEC3_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSEC3_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSECP1 :: c35_DFSECP1 (const char* name) : generic_gate(name) {
	inputs_number = 7;
	outputs_number = 2;
	inputs_name = "C,D,E,RN,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.343; 
input_capa[1] = 10.741; 
input_capa[2] = 7.355; 
input_capa[3] = 17.114; 
input_capa[4] = 10.958; 
input_capa[5] = 7.672; 
input_capa[6] = 16.834; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSECP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSECP1_QN[input_value[6]][input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSECP1_Q[input_value[6]][input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSECP3 :: c35_DFSECP3 (const char* name) : generic_gate(name) {
	inputs_number = 7;
	outputs_number = 2;
	inputs_name = "C,D,E,RN,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.342; 
input_capa[1] = 10.665; 
input_capa[2] = 7.349; 
input_capa[3] = 17.815; 
input_capa[4] = 10.246; 
input_capa[5] = 7.523; 
input_capa[6] = 16.053; 
gate_size[0] = 36.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSECP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSECP3_QN[input_value[6]][input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSECP3_Q[input_value[6]][input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSEP1 :: c35_DFSEP1 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,D,E,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.393; 
input_capa[1] = 10.522; 
input_capa[2] = 7.036; 
input_capa[3] = 11.097; 
input_capa[4] = 7.496; 
input_capa[5] = 12.467; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSEP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSEP1_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSEP1_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSEP3 :: c35_DFSEP3 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,D,E,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.393; 
input_capa[1] = 10.439; 
input_capa[2] = 6.723; 
input_capa[3] = 11.223; 
input_capa[4] = 6.797; 
input_capa[5] = 11.770; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSEP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSEP3_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSEP3_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSP1 :: c35_DFSP1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,D,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.119; 
input_capa[1] = 4.490; 
input_capa[2] = 11.221; 
input_capa[3] = 7.829; 
input_capa[4] = 14.532; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSP1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSP1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DFSP3 :: c35_DFSP3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,D,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.119; 
input_capa[1] = 4.493; 
input_capa[2] = 11.186; 
input_capa[3] = 7.848; 
input_capa[4] = 14.520; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DFSP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DFSP3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DFSP3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DL1 :: c35_DL1 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "D,GN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.251; 
input_capa[1] = 8.893; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DL1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DL1_QN[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DL1_Q[input_value[1]][input_value[0]];
	}
};

c35_DL3 :: c35_DL3 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "D,GN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.347; 
input_capa[1] = 8.439; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DL3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DL3_QN[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DL3_Q[input_value[1]][input_value[0]];
	}
};

c35_DLC1 :: c35_DLC1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "D,GN,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.771; 
input_capa[1] = 8.444; 
input_capa[2] = 12.473; 
gate_size[0] = 16.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DLC1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DLC1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DLC3 :: c35_DLC3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "D,GN,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.771; 
input_capa[1] = 8.444; 
input_capa[2] = 12.471; 
gate_size[0] = 16.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DLC3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DLC3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DLCP1 :: c35_DLCP1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "D,GN,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.770; 
input_capa[1] = 8.297; 
input_capa[2] = 14.946; 
input_capa[3] = 7.982; 
gate_size[0] = 18.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLCP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DLCP1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DLCP1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DLCP3 :: c35_DLCP3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "D,GN,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.770; 
input_capa[1] = 8.295; 
input_capa[2] = 14.226; 
input_capa[3] = 7.734; 
gate_size[0] = 18.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLCP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DLCP3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DLCP3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DLCPQ1 :: c35_DLCPQ1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "D,GN,RN,SN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.382; 
input_capa[1] = 8.279; 
input_capa[2] = 8.146; 
input_capa[3] = 8.054; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLCPQ1 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLCPQ1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_DLCPQ3 :: c35_DLCPQ3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "D,GN,RN,SN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.382; 
input_capa[1] = 8.279; 
input_capa[2] = 8.421; 
input_capa[3] = 7.452; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLCPQ3 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLCPQ3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_DLCQ1 :: c35_DLCQ1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "D,GN,RN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.935; 
input_capa[1] = 8.400; 
input_capa[2] = 5.659; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLCQ1 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLCQ1_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_DLCQ3 :: c35_DLCQ3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "D,GN,RN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.928; 
input_capa[1] = 7.666; 
input_capa[2] = 5.654; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLCQ3 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLCQ3_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_DLP1 :: c35_DLP1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "D,GN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.825; 
input_capa[1] = 8.624; 
input_capa[2] = 8.435; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DLP1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DLP1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DLP3 :: c35_DLP3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "D,GN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.826; 
input_capa[1] = 8.623; 
input_capa[2] = 8.460; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_DLP3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_DLP3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_DLPQ1 :: c35_DLPQ1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "D,GN,SN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.825; 
input_capa[1] = 9.224; 
input_capa[2] = 7.401; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLPQ1 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLPQ1_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_DLPQ3 :: c35_DLPQ3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "D,GN,SN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.826; 
input_capa[1] = 9.218; 
input_capa[2] = 7.888; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLPQ3 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLPQ3_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_DLQ1 :: c35_DLQ1 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "D,GN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.388; 
input_capa[1] = 8.867; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLQ1 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLQ1_Q[input_value[1]][input_value[0]];
};

c35_DLQ3 :: c35_DLQ3 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "D,GN";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.941; 
input_capa[1] = 9.058; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLQ3 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLQ3_Q[input_value[1]][input_value[0]];
};

c35_DLY12 :: c35_DLY12 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.634; 
gate_size[0] = 12.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLY12 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLY12_Q[input_value[0]];
};

c35_DLY22 :: c35_DLY22 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 3.860; 
gate_size[0] = 16.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLY22 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLY22_Q[input_value[0]];
};

c35_DLY32 :: c35_DLY32 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 3.974; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLY32 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLY32_Q[input_value[0]];
};

c35_DLY42 :: c35_DLY42 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 3.923; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_DLY42 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_DLY42_Q[input_value[0]];
};

c35_IMAJ30 :: c35_IMAJ30 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 12.151; 
input_capa[1] = 11.571; 
input_capa[2] = 5.219; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMAJ30 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMAJ30_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_IMAJ31 :: c35_IMAJ31 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.974; 
input_capa[1] = 19.640; 
input_capa[2] = 8.608; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMAJ31 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMAJ31_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX20 :: c35_IMUX20 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.645; 
input_capa[1] = 6.133; 
input_capa[2] = 10.389; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX20 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX20_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX21 :: c35_IMUX21 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.420; 
input_capa[1] = 10.703; 
input_capa[2] = 12.796; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX21 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX21_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX22 :: c35_IMUX22 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.079; 
input_capa[1] = 21.371; 
input_capa[2] = 23.209; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX22 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX22_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX23 :: c35_IMUX23 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 29.250; 
input_capa[1] = 21.616; 
input_capa[2] = 39.334; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX23 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX23_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX24 :: c35_IMUX24 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 35.772; 
input_capa[1] = 24.958; 
input_capa[2] = 49.952; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX24 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX24_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX30 :: c35_IMUX30 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 1;
	inputs_name = "A,B,C,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.004; 
input_capa[1] = 8.448; 
input_capa[2] = 6.067; 
input_capa[3] = 11.294; 
input_capa[4] = 9.253; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX30 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX30_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX31 :: c35_IMUX31 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 1;
	inputs_name = "A,B,C,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 15.675; 
input_capa[1] = 16.713; 
input_capa[2] = 11.301; 
input_capa[3] = 18.902; 
input_capa[4] = 13.606; 
gate_size[0] = 16.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX31 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX31_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX32 :: c35_IMUX32 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 1;
	inputs_name = "A,B,C,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 29.786; 
input_capa[1] = 21.696; 
input_capa[2] = 14.154; 
input_capa[3] = 37.901; 
input_capa[4] = 26.160; 
gate_size[0] = 18.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX32 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX32_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX33 :: c35_IMUX33 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 1;
	inputs_name = "A,B,C,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 41.957; 
input_capa[1] = 43.271; 
input_capa[2] = 29.706; 
input_capa[3] = 41.896; 
input_capa[4] = 32.101; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX33 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX33_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX40 :: c35_IMUX40 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 1;
	inputs_name = "A,B,C,D,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.116; 
input_capa[1] = 8.803; 
input_capa[2] = 8.834; 
input_capa[3] = 8.209; 
input_capa[4] = 21.560; 
input_capa[5] = 9.273; 
gate_size[0] = 16.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX40 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX40_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX41 :: c35_IMUX41 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 1;
	inputs_name = "A,B,C,D,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 14.616; 
input_capa[1] = 14.956; 
input_capa[2] = 14.002; 
input_capa[3] = 14.613; 
input_capa[4] = 36.201; 
input_capa[5] = 13.887; 
gate_size[0] = 19.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX41 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX41_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_IMUX42 :: c35_IMUX42 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 1;
	inputs_name = "A,B,C,D,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 27.447; 
input_capa[1] = 19.585; 
input_capa[2] = 28.772; 
input_capa[3] = 20.275; 
input_capa[4] = 77.631; 
input_capa[5] = 23.721; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_IMUX42 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_IMUX42_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_INV0 :: c35_INV0 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.615; 
gate_size[0] = 2.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV0 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV0_Q[input_value[0]];
};

c35_INV1 :: c35_INV1 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.404; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV1 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV1_Q[input_value[0]];
};

c35_INV10 :: c35_INV10 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 49.292; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV10 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV10_Q[input_value[0]];
};

c35_INV12 :: c35_INV12 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 57.682; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV12 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV12_Q[input_value[0]];
};

c35_INV15 :: c35_INV15 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 70.856; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV15 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV15_Q[input_value[0]];
};

c35_INV2 :: c35_INV2 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.325; 
gate_size[0] = 2.800 ;
gate_size[1] = 13.000 ;
#ifdef __TIMING_SIMULATION__
number_pnj = 4;
location_pnj[0][0]=900; location_pnj[0][1]=10000;
bottom_left_pnj[0][0]=300;bottom_left_pnj[0][1]=8300;
		netlilst_pnj[0].node_name="inh_vdd";
		netlilst_pnj[0].cmos_name="mp1";
		netlilst_pnj[0].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[0].name_subcell="xi3";
		netlilst_pnj[0].type_subcell="inv_core";	
location_pnj[1][0]=2000; location_pnj[1][1]=10000;
bottom_left_pnj[1][0]=1600;bottom_left_pnj[1][1]=8300;
		netlilst_pnj[1].node_name="out";
		netlilst_pnj[1].cmos_name="mp1";
		netlilst_pnj[1].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[1].name_subcell="xi3";
		netlilst_pnj[1].type_subcell="inv_core";
location_pnj[2][0]=800; location_pnj[2][1]=2400;
bottom_left_pnj[2][0]=300;bottom_left_pnj[2][1]=1550;
		netlilst_pnj[2].node_name="inh_gnd";
		netlilst_pnj[2].cmos_name="mn1";
		netlilst_pnj[2].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[2].name_subcell="xi3";
		netlilst_pnj[2].type_subcell="inv_core";
location_pnj[3][0]=2000; location_pnj[3][1]=2400;
bottom_left_pnj[3][0]=1600;bottom_left_pnj[3][1]=1550;
		netlilst_pnj[3].node_name="out";
		netlilst_pnj[3].cmos_name="mn1";
		netlilst_pnj[3].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[3].name_subcell="xi3";
		netlilst_pnj[3].type_subcell="inv_core";
#endif
	InitAfterConstr ();
};

void c35_INV2 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV2_Q[input_value[0]];
};

c35_INVP :: c35_INVP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.325; 
gate_size[0] = 2.800 ;
gate_size[1] = 13.000 ;
#ifdef __TIMING_SIMULATION__
number_pnj = 4;
location_pnj[0][0]=900; location_pnj[0][1]=10000;
bottom_left_pnj[0][0]=300;bottom_left_pnj[0][1]=8300;
		netlilst_pnj[0].node_name="inh_vdd";
		netlilst_pnj[0].cmos_name="mp1";
		netlilst_pnj[0].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[0].name_subcell="xi3";
		netlilst_pnj[0].type_subcell="inv_core";	
location_pnj[1][0]=2000; location_pnj[1][1]=10000;
bottom_left_pnj[1][0]=1600;bottom_left_pnj[1][1]=8300;
		netlilst_pnj[1].node_name="out";
		netlilst_pnj[1].cmos_name="mp1";
		netlilst_pnj[1].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[1].name_subcell="xi3";
		netlilst_pnj[1].type_subcell="inv_core";
location_pnj[2][0]=800; location_pnj[2][1]=2400;
bottom_left_pnj[2][0]=300;bottom_left_pnj[2][1]=1550;
		netlilst_pnj[2].node_name="inh_gnd";
		netlilst_pnj[2].cmos_name="mn1";
		netlilst_pnj[2].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[2].name_subcell="xi3";
		netlilst_pnj[2].type_subcell="inv_core";
location_pnj[3][0]=2000; location_pnj[3][1]=2400;
bottom_left_pnj[3][0]=1600;bottom_left_pnj[3][1]=1550;
		netlilst_pnj[3].node_name="out";
		netlilst_pnj[3].cmos_name="mn1";
		netlilst_pnj[3].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[3].name_subcell="xi3";
		netlilst_pnj[3].type_subcell="inv_core";
#endif
	InitAfterConstr ();
};

void c35_INVP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INVP_Q[input_value[0]];
};

c35_INVN :: c35_INVN (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.325; 
gate_size[0] = 2.800 ;
gate_size[1] = 13.000 ;
#ifdef __TIMING_SIMULATION__
number_pnj = 4;
location_pnj[0][0]=900; location_pnj[0][1]=10000;
bottom_left_pnj[0][0]=300;bottom_left_pnj[0][1]=8300;
		netlilst_pnj[0].node_name="inh_vdd";
		netlilst_pnj[0].cmos_name="mp1";
		netlilst_pnj[0].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[0].name_subcell="xi3";
		netlilst_pnj[0].type_subcell="inv_core";	
location_pnj[1][0]=2000; location_pnj[1][1]=10000;
bottom_left_pnj[1][0]=1600;bottom_left_pnj[1][1]=8300;
		netlilst_pnj[1].node_name="out";
		netlilst_pnj[1].cmos_name="mp1";
		netlilst_pnj[1].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[1].name_subcell="xi3";
		netlilst_pnj[1].type_subcell="inv_core";
location_pnj[2][0]=800; location_pnj[2][1]=2400;
bottom_left_pnj[2][0]=300;bottom_left_pnj[2][1]=1550;
		netlilst_pnj[2].node_name="inh_gnd";
		netlilst_pnj[2].cmos_name="mn1";
		netlilst_pnj[2].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[2].name_subcell="xi3";
		netlilst_pnj[2].type_subcell="inv_core";
location_pnj[3][0]=2000; location_pnj[3][1]=2400;
bottom_left_pnj[3][0]=1600;bottom_left_pnj[3][1]=1550;
		netlilst_pnj[3].node_name="out";
		netlilst_pnj[3].cmos_name="mn1";
		netlilst_pnj[3].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[3].name_subcell="xi3";
		netlilst_pnj[3].type_subcell="inv_core";
#endif
	InitAfterConstr ();
};

void c35_INVN :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INVN_Q[input_value[0]];
};

c35_INV3 :: c35_INV3 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 15.264; 
gate_size[0] = 2.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV3 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV3_Q[input_value[0]];
};

c35_INV4 :: c35_INV4 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.886; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV4 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV4_Q[input_value[0]];
};

c35_INV6 :: c35_INV6 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 29.790; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV6 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV6_Q[input_value[0]];
};

c35_INV8 :: c35_INV8 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 38.572; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_INV8 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_INV8_Q[input_value[0]];
};

c35_JK1 :: c35_JK1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,J,K";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.352; 
input_capa[1] = 6.687; 
input_capa[2] = 6.426; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JK1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JK1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JK1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JK3 :: c35_JK3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,J,K";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.352; 
input_capa[1] = 6.679; 
input_capa[2] = 6.391; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JK3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JK3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JK3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKC1 :: c35_JKC1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,J,K,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.410; 
input_capa[1] = 6.298; 
input_capa[2] = 6.327; 
input_capa[3] = 12.944; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKC1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKC1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKC3 :: c35_JKC3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,J,K,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.410; 
input_capa[1] = 6.305; 
input_capa[2] = 6.369; 
input_capa[3] = 13.360; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKC3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKC3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKCP1 :: c35_JKCP1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,J,K,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.164; 
input_capa[1] = 6.617; 
input_capa[2] = 5.583; 
input_capa[3] = 17.732; 
input_capa[4] = 16.020; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKCP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKCP1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKCP1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKCP3 :: c35_JKCP3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,J,K,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.165; 
input_capa[1] = 6.647; 
input_capa[2] = 5.451; 
input_capa[3] = 18.007; 
input_capa[4] = 15.200; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKCP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKCP3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKCP3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKP1 :: c35_JKP1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,J,K,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.393; 
input_capa[1] = 6.666; 
input_capa[2] = 5.508; 
input_capa[3] = 12.247; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKP1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKP1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKP3 :: c35_JKP3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,J,K,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.394; 
input_capa[1] = 6.669; 
input_capa[2] = 5.307; 
input_capa[3] = 12.738; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKP3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKP3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKS1 :: c35_JKS1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,J,K,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.471; 
input_capa[1] = 6.368; 
input_capa[2] = 6.520; 
input_capa[3] = 11.892; 
input_capa[4] = 7.330; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKS1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKS1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKS1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKS3 :: c35_JKS3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,J,K,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.470; 
input_capa[1] = 6.408; 
input_capa[2] = 6.317; 
input_capa[3] = 11.013; 
input_capa[4] = 7.524; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKS3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKS3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKS3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKSC1 :: c35_JKSC1 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,J,K,RN,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.480; 
input_capa[1] = 6.522; 
input_capa[2] = 6.483; 
input_capa[3] = 12.581; 
input_capa[4] = 11.058; 
input_capa[5] = 7.109; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKSC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKSC1_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKSC1_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKSC3 :: c35_JKSC3 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,J,K,RN,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.480; 
input_capa[1] = 6.527; 
input_capa[2] = 6.466; 
input_capa[3] = 13.360; 
input_capa[4] = 11.912; 
input_capa[5] = 7.398; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKSC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKSC3_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKSC3_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKSCP1 :: c35_JKSCP1 (const char* name) : generic_gate(name) {
	inputs_number = 7;
	outputs_number = 2;
	inputs_name = "C,J,K,RN,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.131; 
input_capa[1] = 6.773; 
input_capa[2] = 6.130; 
input_capa[3] = 18.019; 
input_capa[4] = 11.535; 
input_capa[5] = 6.696; 
input_capa[6] = 15.765; 
gate_size[0] = 36.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKSCP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKSCP1_QN[input_value[6]][input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKSCP1_Q[input_value[6]][input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKSCP3 :: c35_JKSCP3 (const char* name) : generic_gate(name) {
	inputs_number = 7;
	outputs_number = 2;
	inputs_name = "C,J,K,RN,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.131; 
input_capa[1] = 6.771; 
input_capa[2] = 6.123; 
input_capa[3] = 17.813; 
input_capa[4] = 11.098; 
input_capa[5] = 6.772; 
input_capa[6] = 15.428; 
gate_size[0] = 36.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKSCP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKSCP3_QN[input_value[6]][input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKSCP3_Q[input_value[6]][input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKSP1 :: c35_JKSP1 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,J,K,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.485; 
input_capa[1] = 6.794; 
input_capa[2] = 6.013; 
input_capa[3] = 11.860; 
input_capa[4] = 6.600; 
input_capa[5] = 11.895; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKSP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKSP1_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKSP1_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_JKSP3 :: c35_JKSP3 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,J,K,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.485; 
input_capa[1] = 6.793; 
input_capa[2] = 6.006; 
input_capa[3] = 11.868; 
input_capa[4] = 6.905; 
input_capa[5] = 12.465; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_JKSP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_JKSP3_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_JKSP3_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_MAJ31 :: c35_MAJ31 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.974; 
input_capa[1] = 19.640; 
input_capa[2] = 8.608; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MAJ31 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MAJ31_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_MAJ32 :: c35_MAJ32 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.890; 
input_capa[1] = 19.535; 
input_capa[2] = 8.567; 
gate_size[0] = 9.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MAJ32 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MAJ32_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX21 :: c35_MUX21 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.420; 
input_capa[1] = 10.703; 
input_capa[2] = 12.796; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX21 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX21_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX22 :: c35_MUX22 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 20.079; 
input_capa[1] = 21.371; 
input_capa[2] = 23.209; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX22 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX22_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX24 :: c35_MUX24 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 35.772; 
input_capa[1] = 24.958; 
input_capa[2] = 49.952; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX24 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX24_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX26 :: c35_MUX26 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,S";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 29.298; 
input_capa[1] = 21.980; 
input_capa[2] = 38.176; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX26 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX26_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX31 :: c35_MUX31 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 1;
	inputs_name = "A,B,C,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 15.675; 
input_capa[1] = 16.713; 
input_capa[2] = 11.301; 
input_capa[3] = 18.902; 
input_capa[4] = 13.606; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX31 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX31_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX32 :: c35_MUX32 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 1;
	inputs_name = "A,B,C,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 29.786; 
input_capa[1] = 21.696; 
input_capa[2] = 14.154; 
input_capa[3] = 37.901; 
input_capa[4] = 26.160; 
gate_size[0] = 18.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX32 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX32_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX33 :: c35_MUX33 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 1;
	inputs_name = "A,B,C,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 41.957; 
input_capa[1] = 43.271; 
input_capa[2] = 29.706; 
input_capa[3] = 41.896; 
input_capa[4] = 32.101; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX33 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX33_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX34 :: c35_MUX34 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 1;
	inputs_name = "A,B,C,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 42.231; 
input_capa[1] = 41.000; 
input_capa[2] = 30.033; 
input_capa[3] = 41.866; 
input_capa[4] = 33.413; 
gate_size[0] = 30.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX34 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX34_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX41 :: c35_MUX41 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 1;
	inputs_name = "A,B,C,D,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 14.616; 
input_capa[1] = 14.956; 
input_capa[2] = 14.002; 
input_capa[3] = 14.613; 
input_capa[4] = 36.201; 
input_capa[5] = 13.887; 
gate_size[0] = 18.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX41 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX41_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX42 :: c35_MUX42 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 1;
	inputs_name = "A,B,C,D,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 27.447; 
input_capa[1] = 19.585; 
input_capa[2] = 28.772; 
input_capa[3] = 20.275; 
input_capa[4] = 77.631; 
input_capa[5] = 23.721; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX42 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX42_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_MUX43 :: c35_MUX43 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 1;
	inputs_name = "A,B,C,D,S0,S1";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 28.564; 
input_capa[1] = 20.054; 
input_capa[2] = 26.767; 
input_capa[3] = 20.044; 
input_capa[4] = 79.188; 
input_capa[5] = 23.678; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_MUX43 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_MUX43_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND20 :: c35_NAND20 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.063; 
input_capa[1] = 5.672; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND20 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND20_Q[input_value[1]][input_value[0]];
};

c35_NAND21 :: c35_NAND21 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.660; 
input_capa[1] = 8.564; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND21 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND21_Q[input_value[1]][input_value[0]];
};

c35_NAND22 :: c35_NAND22 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 9.222; 
input_capa[1] = 15.094; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
#ifdef __TIMING_SIMULATION__
number_pnj = 6;
location_pnj[0][0]=900; location_pnj[0][1]=10000;
bottom_left_pnj[0][0]=300;bottom_left_pnj[0][1]=8400;
		netlilst_pnj[0].node_name="inh_vdd";
		netlilst_pnj[0].cmos_name="mp2";
		netlilst_pnj[0].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[0].name_subcell="xi1";
		netlilst_pnj[0].type_subcell="nand2_core";	
location_pnj[1][0]=2200; location_pnj[1][1]=10000;
bottom_left_pnj[1][0]=1200;bottom_left_pnj[1][1]=8400;
		netlilst_pnj[1].node_name="out";
		netlilst_pnj[1].cmos_name="mp2";
		netlilst_pnj[1].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[1].name_subcell="xi1";
		netlilst_pnj[1].type_subcell="nand2_core";
location_pnj[2][0]=3500; location_pnj[2][1]=10000;
bottom_left_pnj[2][0]=2600;bottom_left_pnj[2][1]=8400;
		netlilst_pnj[2].node_name="inh_vdd";
		netlilst_pnj[2].cmos_name="mp1";
		netlilst_pnj[2].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[2].name_subcell="xi1";
		netlilst_pnj[2].type_subcell="nand2_core";
location_pnj[3][0]=600; location_pnj[3][1]=2300;
bottom_left_pnj[3][0]=300;bottom_left_pnj[3][1]=1500;
		netlilst_pnj[3].node_name="inh_gnd";
		netlilst_pnj[3].cmos_name="mn2";
		netlilst_pnj[3].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[3].name_subcell="xi1";
		netlilst_pnj[3].type_subcell="nand2_core";
location_pnj[4][0]=2500; location_pnj[4][1]=2900;
bottom_left_pnj[4][0]=2600;bottom_left_pnj[4][1]=1500;
		netlilst_pnj[4].node_name="net13";
		netlilst_pnj[4].cmos_name="mn2";
		netlilst_pnj[4].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[4].name_subcell="xi1";
		netlilst_pnj[4].type_subcell="nand2_core";
location_pnj[5][0]=3600; location_pnj[5][1]=3500;
bottom_left_pnj[5][0]=3400;bottom_left_pnj[5][1]=1500;
		netlilst_pnj[5].node_name="out";
		netlilst_pnj[5].cmos_name="mn1";
		netlilst_pnj[5].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[5].name_subcell="xi1";
		netlilst_pnj[5].type_subcell="nand2_core";
#endif
	InitAfterConstr ();
};

void c35_NAND22 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND22_Q[input_value[1]][input_value[0]];
};

c35_NAND23 :: c35_NAND23 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 15.335; 
input_capa[1] = 22.284; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND23 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND23_Q[input_value[1]][input_value[0]];
};

c35_NAND24 :: c35_NAND24 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 18.891; 
input_capa[1] = 29.104; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND24 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND24_Q[input_value[1]][input_value[0]];
};

c35_NAND26 :: c35_NAND26 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 28.106; 
input_capa[1] = 42.231; 
gate_size[0] = 12.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND26 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND26_Q[input_value[1]][input_value[0]];
};

c35_NAND28 :: c35_NAND28 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 38.282; 
input_capa[1] = 58.626; 
gate_size[0] = 14.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND28 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND28_Q[input_value[1]][input_value[0]];
};

c35_NAND30 :: c35_NAND30 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.120; 
input_capa[1] = 4.751; 
input_capa[2] = 6.572; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND30 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND30_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND31 :: c35_NAND31 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.105; 
input_capa[1] = 7.239; 
input_capa[2] = 9.769; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND31 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND31_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND32 :: c35_NAND32 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 12.069; 
input_capa[1] = 14.483; 
input_capa[2] = 20.768; 
gate_size[0] = 9.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND32 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND32_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND33 :: c35_NAND33 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 15.284; 
input_capa[1] = 16.816; 
input_capa[2] = 27.565; 
gate_size[0] = 9.800 ;
gate_size[1] = 13.000 ;
#ifdef __TIMING_SIMULATION__
number_pnj = 8;
location_pnj[0][0]=900; location_pnj[0][1]=9000;
bottom_left_pnj[0][0]=300;bottom_left_pnj[0][1]=7500;
		netlilst_pnj[0].node_name="inh_vdd";
		netlilst_pnj[0].cmos_name="mp1";
		netlilst_pnj[0].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[0].name_subcell="xi1";
		netlilst_pnj[0].type_subcell="nand3_core";	
location_pnj[1][0]=2000; location_pnj[1][1]=9000;
bottom_left_pnj[1][0]=1200;bottom_left_pnj[1][1]=7500;
		netlilst_pnj[1].node_name="out";
		netlilst_pnj[1].cmos_name="mp1";
		netlilst_pnj[1].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[1].name_subcell="xi1";
		netlilst_pnj[1].type_subcell="nand3_core";
location_pnj[2][0]=3500; location_pnj[2][1]=9000;
bottom_left_pnj[2][0]=3000;bottom_left_pnj[2][1]=7500;
		netlilst_pnj[2].node_name="inh_vdd";
		netlilst_pnj[2].cmos_name="mp2";
		netlilst_pnj[2].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[2].name_subcell="xi1";
		netlilst_pnj[2].type_subcell="nand3_core";
location_pnj[3][0]=4900; location_pnj[3][1]=9000;
bottom_left_pnj[3][0]=4000;bottom_left_pnj[3][1]=7500;
		netlilst_pnj[3].node_name="out";
		netlilst_pnj[3].cmos_name="mp3";
		netlilst_pnj[3].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[3].name_subcell="xi1";
		netlilst_pnj[3].type_subcell="nand3_core";
location_pnj[4][0]=2000; location_pnj[4][1]=1200;
bottom_left_pnj[4][0]=300;bottom_left_pnj[4][1]=900;
		netlilst_pnj[4].node_name="inh_gnd";
		netlilst_pnj[4].cmos_name="mn3";
		netlilst_pnj[4].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[4].name_subcell="xi1";
		netlilst_pnj[4].type_subcell="nand3_core";
location_pnj[5][0]=4800; location_pnj[5][1]=2000;
bottom_left_pnj[5][0]=7300;bottom_left_pnj[5][1]=900;
		netlilst_pnj[5].node_name="net10";
		netlilst_pnj[5].cmos_name="mn2";
		netlilst_pnj[5].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[5].name_subcell="xi1";
		netlilst_pnj[5].type_subcell="nand3_core";
location_pnj[6][0]=5500; location_pnj[6][1]=2700;
bottom_left_pnj[6][0]=8100;bottom_left_pnj[6][1]=900;
		netlilst_pnj[6].node_name="net6";
		netlilst_pnj[6].cmos_name="mn1";
		netlilst_pnj[6].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[6].name_subcell="xi1";
		netlilst_pnj[6].type_subcell="nand3_core";
location_pnj[7][0]=8500; location_pnj[7][1]=3600;
bottom_left_pnj[7][0]=8900;bottom_left_pnj[7][1]=900;
		netlilst_pnj[7].node_name="out";
		netlilst_pnj[7].cmos_name="mn1";
		netlilst_pnj[7].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[7].name_subcell="xi1";
		netlilst_pnj[7].type_subcell="nand3_core";
#endif
	InitAfterConstr ();
};

void c35_NAND33 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND33_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND34 :: c35_NAND34 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 21.040; 
input_capa[1] = 22.156; 
input_capa[2] = 37.052; 
gate_size[0] = 12.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND34 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND34_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND40 :: c35_NAND40 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.367; 
input_capa[1] = 5.221; 
input_capa[2] = 5.118; 
input_capa[3] = 6.637; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND40 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND40_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND41 :: c35_NAND41 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.694; 
input_capa[1] = 7.490; 
input_capa[2] = 8.612; 
input_capa[3] = 12.330; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND41 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND41_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND42 :: c35_NAND42 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 22.378; 
input_capa[1] = 14.148; 
input_capa[2] = 14.111; 
input_capa[3] = 13.466; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND42 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND42_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_NAND43 :: c35_NAND43 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 31.713; 
input_capa[1] = 20.704; 
input_capa[2] = 21.726; 
input_capa[3] = 19.232; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NAND43 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NAND43_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_NOR20 :: c35_NOR20 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.453; 
input_capa[1] = 5.801; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR20 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR20_Q[input_value[1]][input_value[0]];
};

c35_NOR21 :: c35_NOR21 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.722; 
input_capa[1] = 8.859; 
gate_size[0] = 4.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR21 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR21_Q[input_value[1]][input_value[0]];
};

c35_NOR22 :: c35_NOR22 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 16.234; 
input_capa[1] = 16.595; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
#ifdef __TIMING_SIMULATION__
number_pnj = 6;
location_pnj[0][0]=1600; location_pnj[0][1]=10300;
bottom_left_pnj[0][0]=300;bottom_left_pnj[0][1]=7250;
		netlilst_pnj[0].node_name="inh_vdd";
		netlilst_pnj[0].cmos_name="mp2";
		netlilst_pnj[0].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[0].name_subcell="xi1";
		netlilst_pnj[0].type_subcell="nor2_core";	
location_pnj[1][0]=3000; location_pnj[1][1]=9000;
bottom_left_pnj[1][0]=1150;bottom_left_pnj[1][1]=7250;
		netlilst_pnj[1].node_name="net17";
		netlilst_pnj[1].cmos_name="mp2";
		netlilst_pnj[1].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[1].name_subcell="xi1";
		netlilst_pnj[1].type_subcell="nor2_core";
location_pnj[2][0]=4800; location_pnj[2][1]=8200;
bottom_left_pnj[2][0]=1950;bottom_left_pnj[2][1]=7250;
		netlilst_pnj[2].node_name="out";
		netlilst_pnj[2].cmos_name="mp1";
		netlilst_pnj[2].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[2].name_subcell="xi1";
		netlilst_pnj[2].type_subcell="nor2_core";
location_pnj[3][0]=1500; location_pnj[3][1]=3000;
bottom_left_pnj[3][0]=1100;bottom_left_pnj[3][1]=2000;
		netlilst_pnj[3].node_name="inh_gnd";
		netlilst_pnj[3].cmos_name="mn2";
		netlilst_pnj[3].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[3].name_subcell="xi1";
		netlilst_pnj[3].type_subcell="nor2_core";
location_pnj[4][0]=2800; location_pnj[4][1]=3000;
bottom_left_pnj[4][0]=2300;bottom_left_pnj[4][1]=2000;
		netlilst_pnj[4].node_name="out";
		netlilst_pnj[4].cmos_name="mn2";
		netlilst_pnj[4].type_pn=1; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[4].name_subcell="xi1";
		netlilst_pnj[4].type_subcell="nor2_core";
location_pnj[5][0]=4100; location_pnj[5][1]=3000;
bottom_left_pnj[5][0]=3700;bottom_left_pnj[5][1]=2000;
		netlilst_pnj[5].node_name="inh_gnd";
		netlilst_pnj[5].cmos_name="mn1";
		netlilst_pnj[5].type_pn=3; // 1-> drain, 2-> gate, 3-> source
		netlilst_pnj[5].name_subcell="xi1";
		netlilst_pnj[5].type_subcell="nor2_core";
#endif
	InitAfterConstr ();
};

void c35_NOR22 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR22_Q[input_value[1]][input_value[0]];
};

c35_NOR23 :: c35_NOR23 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 24.288; 
input_capa[1] = 23.276; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR23 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR23_Q[input_value[1]][input_value[0]];
};

c35_NOR24 :: c35_NOR24 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 32.911; 
input_capa[1] = 30.931; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR24 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR24_Q[input_value[1]][input_value[0]];
};

c35_NOR30 :: c35_NOR30 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.788; 
input_capa[1] = 7.748; 
input_capa[2] = 7.455; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR30 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR30_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NOR31 :: c35_NOR31 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 11.554; 
input_capa[1] = 12.074; 
input_capa[2] = 12.079; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR31 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR31_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NOR32 :: c35_NOR32 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 21.493; 
input_capa[1] = 22.554; 
input_capa[2] = 21.070; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR32 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR32_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NOR33 :: c35_NOR33 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 33.167; 
input_capa[1] = 32.379; 
input_capa[2] = 31.075; 
gate_size[0] = 9.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR33 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR33_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_NOR40 :: c35_NOR40 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.464; 
input_capa[1] = 8.192; 
input_capa[2] = 9.041; 
input_capa[3] = 8.983; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR40 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR40_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_NOR41 :: c35_NOR41 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 14.157; 
input_capa[1] = 14.372; 
input_capa[2] = 15.138; 
input_capa[3] = 13.300; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR41 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR41_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_NOR42 :: c35_NOR42 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 27.981; 
input_capa[1] = 28.708; 
input_capa[2] = 27.870; 
input_capa[3] = 27.969; 
gate_size[0] = 12.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_NOR42 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_NOR42_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI210 :: c35_OAI210 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 6.341; 
input_capa[1] = 6.368; 
input_capa[2] = 4.712; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI210 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI210_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI211 :: c35_OAI211 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.686; 
input_capa[1] = 11.140; 
input_capa[2] = 6.040; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI211 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI211_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI2110 :: c35_OAI2110 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 5.357; 
input_capa[1] = 5.707; 
input_capa[2] = 4.890; 
input_capa[3] = 6.462; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI2110 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI2110_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI2111 :: c35_OAI2111 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.499; 
input_capa[1] = 8.970; 
input_capa[2] = 7.002; 
input_capa[3] = 10.127; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI2111 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI2111_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI2112 :: c35_OAI2112 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 17.526; 
input_capa[1] = 16.807; 
input_capa[2] = 12.718; 
input_capa[3] = 19.558; 
gate_size[0] = 11.200 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI2112 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI2112_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI212 :: c35_OAI212 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 19.055; 
input_capa[1] = 18.766; 
input_capa[2] = 9.950; 
gate_size[0] = 5.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI212 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI212_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI220 :: c35_OAI220 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.872; 
input_capa[1] = 5.475; 
input_capa[2] = 5.747; 
input_capa[3] = 6.205; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI220 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI220_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI221 :: c35_OAI221 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.969; 
input_capa[1] = 8.591; 
input_capa[2] = 10.820; 
input_capa[3] = 10.916; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI221 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI221_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI222 :: c35_OAI222 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 14.901; 
input_capa[1] = 14.557; 
input_capa[2] = 20.494; 
input_capa[3] = 18.782; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI222 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI222_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI310 :: c35_OAI310 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.363; 
input_capa[1] = 7.768; 
input_capa[2] = 7.657; 
input_capa[3] = 4.375; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI310 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI310_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI311 :: c35_OAI311 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 13.062; 
input_capa[1] = 13.704; 
input_capa[2] = 13.527; 
input_capa[3] = 6.505; 
gate_size[0] = 7.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI311 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI311_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_OAI312 :: c35_OAI312 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 25.869; 
input_capa[1] = 25.879; 
input_capa[2] = 25.110; 
input_capa[3] = 10.079; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_OAI312 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_OAI312_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_TFC1 :: c35_TFC1 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "C,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.443; 
input_capa[1] = 15.237; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFC1_QN[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFC1_Q[input_value[1]][input_value[0]];
	}
};

c35_TFC3 :: c35_TFC3 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "C,RN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.443; 
input_capa[1] = 14.846; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFC3_QN[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFC3_Q[input_value[1]][input_value[0]];
	}
};

c35_TFCP1 :: c35_TFCP1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.360; 
input_capa[1] = 17.923; 
input_capa[2] = 14.498; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFCP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFCP1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFCP1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFCP3 :: c35_TFCP3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,RN,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.358; 
input_capa[1] = 18.818; 
input_capa[2] = 14.809; 
gate_size[0] = 23.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFCP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFCP3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFCP3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFEC1 :: c35_TFEC1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,RN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.406; 
input_capa[1] = 12.915; 
input_capa[2] = 7.685; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFEC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFEC1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFEC1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFEC3 :: c35_TFEC3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,RN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.407; 
input_capa[1] = 12.925; 
input_capa[2] = 7.675; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFEC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFEC3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFEC3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFECP1 :: c35_TFECP1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,RN,SN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.342; 
input_capa[1] = 17.769; 
input_capa[2] = 15.900; 
input_capa[3] = 6.685; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFECP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFECP1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFECP1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFECP3 :: c35_TFECP3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,RN,SN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.340; 
input_capa[1] = 18.673; 
input_capa[2] = 16.077; 
input_capa[3] = 6.819; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFECP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFECP3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFECP3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFEP1 :: c35_TFEP1 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,SN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.393; 
input_capa[1] = 12.080; 
input_capa[2] = 5.479; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFEP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFEP1_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFEP1_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFEP3 :: c35_TFEP3 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 2;
	inputs_name = "C,SN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.394; 
input_capa[1] = 12.069; 
input_capa[2] = 5.511; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFEP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFEP3_QN[input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFEP3_Q[input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFP1 :: c35_TFP1 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "C,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.483; 
input_capa[1] = 14.026; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFP1_QN[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFP1_Q[input_value[1]][input_value[0]];
	}
};

c35_TFP3 :: c35_TFP3 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 2;
	inputs_name = "C,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.482; 
input_capa[1] = 13.952; 
gate_size[0] = 22.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFP3_QN[input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFP3_Q[input_value[1]][input_value[0]];
	}
};

c35_TFSC1 :: c35_TFSC1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,RN,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.433; 
input_capa[1] = 15.343; 
input_capa[2] = 10.279; 
input_capa[3] = 7.299; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSC1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSC1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSC3 :: c35_TFSC3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,RN,SD,SE";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.383; 
input_capa[1] = 15.374; 
input_capa[2] = 11.194; 
input_capa[3] = 6.764; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSC3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSC3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSCP1 :: c35_TFSCP1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,RN,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.348; 
input_capa[1] = 18.271; 
input_capa[2] = 10.597; 
input_capa[3] = 7.670; 
input_capa[4] = 14.740; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSCP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSCP1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSCP1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSCP3 :: c35_TFSCP3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,RN,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.347; 
input_capa[1] = 18.095; 
input_capa[2] = 10.483; 
input_capa[3] = 7.686; 
input_capa[4] = 14.569; 
gate_size[0] = 29.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSCP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSCP3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSCP3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSEC1 :: c35_TFSEC1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,RN,SD,SE,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.479; 
input_capa[1] = 13.331; 
input_capa[2] = 11.909; 
input_capa[3] = 7.346; 
input_capa[4] = 8.125; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSEC1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSEC1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSEC1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSEC3 :: c35_TFSEC3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,RN,SD,SE,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.480; 
input_capa[1] = 13.336; 
input_capa[2] = 11.911; 
input_capa[3] = 7.344; 
input_capa[4] = 8.034; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSEC3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSEC3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSEC3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSECP1 :: c35_TFSECP1 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,RN,SD,SE,SN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.375; 
input_capa[1] = 18.140; 
input_capa[2] = 10.970; 
input_capa[3] = 7.327; 
input_capa[4] = 15.915; 
input_capa[5] = 6.693; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSECP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSECP1_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSECP1_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSECP3 :: c35_TFSECP3 (const char* name) : generic_gate(name) {
	inputs_number = 6;
	outputs_number = 2;
	inputs_name = "C,RN,SD,SE,SN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.375; 
input_capa[1] = 18.408; 
input_capa[2] = 10.938; 
input_capa[3] = 6.926; 
input_capa[4] = 16.841; 
input_capa[5] = 6.840; 
gate_size[0] = 35.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSECP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSECP3_QN[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSECP3_Q[input_value[5]][input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSEP1 :: c35_TFSEP1 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,SD,SE,SN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.485; 
input_capa[1] = 11.086; 
input_capa[2] = 6.622; 
input_capa[3] = 12.351; 
input_capa[4] = 5.378; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSEP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSEP1_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSEP1_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSEP3 :: c35_TFSEP3 (const char* name) : generic_gate(name) {
	inputs_number = 5;
	outputs_number = 2;
	inputs_name = "C,SD,SE,SN,T";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.485; 
input_capa[1] = 11.078; 
input_capa[2] = 6.620; 
input_capa[3] = 12.342; 
input_capa[4] = 5.385; 
gate_size[0] = 33.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSEP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSEP3_QN[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSEP3_Q[input_value[4]][input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSP1 :: c35_TFSP1 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.118; 
input_capa[1] = 11.147; 
input_capa[2] = 8.066; 
input_capa[3] = 14.102; 
gate_size[0] = 26.600 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSP1 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSP1_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSP1_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_TFSP3 :: c35_TFSP3 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 2;
	inputs_name = "C,SD,SE,SN";
	outputs_name = "Q,QN";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 4.119; 
input_capa[1] = 10.530; 
input_capa[2] = 8.042; 
input_capa[3] = 14.159; 
gate_size[0] = 28.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_TFSP3 :: calculate_output(int o) {
	if (o == 1) {
	temp_output_value[o] = TRUTH_TABLE_TFSP3_QN[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
	if (o == 0) {
	temp_output_value[o] = TRUTH_TABLE_TFSP3_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
	}
};

c35_XNR20 :: c35_XNR20 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 9.712; 
input_capa[1] = 11.638; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XNR20 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XNR20_Q[input_value[1]][input_value[0]];
};

c35_XNR21 :: c35_XNR21 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 16.332; 
input_capa[1] = 15.309; 
gate_size[0] = 8.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XNR21 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XNR21_Q[input_value[1]][input_value[0]];
};

c35_XNR22 :: c35_XNR22 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 30.581; 
input_capa[1] = 29.112; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XNR22 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XNR22_Q[input_value[1]][input_value[0]];
};

c35_XNR30 :: c35_XNR30 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.131; 
input_capa[1] = 10.020; 
input_capa[2] = 9.009; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XNR30 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XNR30_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_XNR31 :: c35_XNR31 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.841; 
input_capa[1] = 10.088; 
input_capa[2] = 13.912; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XNR31 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XNR31_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_XNR40 :: c35_XNR40 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.425; 
input_capa[1] = 8.969; 
input_capa[2] = 8.236; 
input_capa[3] = 9.663; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XNR40 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XNR40_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_XNR41 :: c35_XNR41 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.815; 
input_capa[1] = 9.045; 
input_capa[2] = 9.418; 
input_capa[3] = 10.096; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XNR41 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XNR41_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_XOR20 :: c35_XOR20 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 10.137; 
input_capa[1] = 12.465; 
gate_size[0] = 9.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XOR20 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XOR20_Q[input_value[1]][input_value[0]];
};

c35_XOR21 :: c35_XOR21 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 15.350; 
input_capa[1] = 18.237; 
gate_size[0] = 9.800 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XOR21 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XOR21_Q[input_value[1]][input_value[0]];
};

c35_XOR22 :: c35_XOR22 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,B";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 28.974; 
input_capa[1] = 30.390; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XOR22 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XOR22_Q[input_value[1]][input_value[0]];
};

c35_XOR30 :: c35_XOR30 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.009; 
input_capa[1] = 10.317; 
input_capa[2] = 9.351; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XOR30 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XOR30_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_XOR31 :: c35_XOR31 (const char* name) : generic_gate(name) {
	inputs_number = 3;
	outputs_number = 1;
	inputs_name = "A,B,C";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 7.978; 
input_capa[1] = 9.731; 
input_capa[2] = 15.249; 
gate_size[0] = 15.400 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XOR31 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XOR31_Q[input_value[2]][input_value[1]][input_value[0]];
};

c35_XOR40 :: c35_XOR40 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.587; 
input_capa[1] = 9.222; 
input_capa[2] = 8.183; 
input_capa[3] = 9.697; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XOR40 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XOR40_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_XOR41 :: c35_XOR41 (const char* name) : generic_gate(name) {
	inputs_number = 4;
	outputs_number = 1;
	inputs_name = "A,B,C,D";
	outputs_name = "Q";
	type = _TYPE_COMBINATIONAL;
input_capa[0] = 8.761; 
input_capa[1] = 9.172; 
input_capa[2] = 8.474; 
input_capa[3] = 9.316; 
gate_size[0] = 21.000 ;
gate_size[1] = 13.000 ;
	InitAfterConstr ();
};

void c35_XOR41 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_XOR41_Q[input_value[3]][input_value[2]][input_value[1]][input_value[0]];
};

c35_BBC1P :: c35_BBC1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC1P_Y[input_value[1]][input_value[0]];
};

c35_BBC4P :: c35_BBC4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC4P_Y[input_value[1]][input_value[0]];
};

c35_BBC4SMP :: c35_BBC4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBC8P :: c35_BBC8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC8P_Y[input_value[1]][input_value[0]];
};

c35_BBC8SMP :: c35_BBC8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBC8SP :: c35_BBC8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC8SP_Y[input_value[1]][input_value[0]];
};

c35_BBC16P :: c35_BBC16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC16P_Y[input_value[1]][input_value[0]];
};

c35_BBC16SMP :: c35_BBC16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBC16SP :: c35_BBC16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC16SP_Y[input_value[1]][input_value[0]];
};

c35_BBC24P :: c35_BBC24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC24P_Y[input_value[1]][input_value[0]];
};

c35_BBC24SMP :: c35_BBC24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBC24SP :: c35_BBC24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC24SP_Y[input_value[1]][input_value[0]];
};

c35_BBCD1P :: c35_BBCD1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD1P_Y[input_value[1]][input_value[0]];
};

c35_BBCD4P :: c35_BBCD4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD4P_Y[input_value[1]][input_value[0]];
};

c35_BBCD4SMP :: c35_BBCD4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBCD8P :: c35_BBCD8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD8P_Y[input_value[1]][input_value[0]];
};

c35_BBCD8SMP :: c35_BBCD8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBCD8SP :: c35_BBCD8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD8SP_Y[input_value[1]][input_value[0]];
};

c35_BBCD16P :: c35_BBCD16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD16P_Y[input_value[1]][input_value[0]];
};

c35_BBCD16SMP :: c35_BBCD16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBCD16SP :: c35_BBCD16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD16SP_Y[input_value[1]][input_value[0]];
};

c35_BBCD24P :: c35_BBCD24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD24P_Y[input_value[1]][input_value[0]];
};

c35_BBCD24SMP :: c35_BBCD24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBCD24SP :: c35_BBCD24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD24SP_Y[input_value[1]][input_value[0]];
};

c35_BBCU1P :: c35_BBCU1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU1P_Y[input_value[1]][input_value[0]];
};

c35_BBCU4P :: c35_BBCU4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU4P_Y[input_value[1]][input_value[0]];
};

c35_BBCU4SMP :: c35_BBCU4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBCU8P :: c35_BBCU8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU8P_Y[input_value[1]][input_value[0]];
};

c35_BBCU8SMP :: c35_BBCU8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBCU8SP :: c35_BBCU8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU8SP_Y[input_value[1]][input_value[0]];
};

c35_BBCU16P :: c35_BBCU16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU16P_Y[input_value[1]][input_value[0]];
};

c35_BBCU16SMP :: c35_BBCU16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBCU16SP :: c35_BBCU16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU16SP_Y[input_value[1]][input_value[0]];
};

c35_BBCU24P :: c35_BBCU24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU24P_Y[input_value[1]][input_value[0]];
};

c35_BBCU24SMP :: c35_BBCU24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBCU24SP :: c35_BBCU24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU24SP_Y[input_value[1]][input_value[0]];
};

c35_BBS1P :: c35_BBS1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS1P_Y[input_value[1]][input_value[0]];
};

c35_BBS4P :: c35_BBS4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS4P_Y[input_value[1]][input_value[0]];
};

c35_BBS4SMP :: c35_BBS4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBS8P :: c35_BBS8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS8P_Y[input_value[1]][input_value[0]];
};

c35_BBS8SMP :: c35_BBS8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBS8SP :: c35_BBS8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS8SP_Y[input_value[1]][input_value[0]];
};

c35_BBS16P :: c35_BBS16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS16P_Y[input_value[1]][input_value[0]];
};

c35_BBS16SMP :: c35_BBS16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBS16SP :: c35_BBS16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS16SP_Y[input_value[1]][input_value[0]];
};

c35_BBS24P :: c35_BBS24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS24P_Y[input_value[1]][input_value[0]];
};

c35_BBS24SMP :: c35_BBS24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBS24SP :: c35_BBS24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS24SP_Y[input_value[1]][input_value[0]];
};

c35_BBSD1P :: c35_BBSD1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD1P_Y[input_value[1]][input_value[0]];
};

c35_BBSD4P :: c35_BBSD4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD4P_Y[input_value[1]][input_value[0]];
};

c35_BBSD4SMP :: c35_BBSD4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBSD8P :: c35_BBSD8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD8P_Y[input_value[1]][input_value[0]];
};

c35_BBSD8SMP :: c35_BBSD8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBSD8SP :: c35_BBSD8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD8SP_Y[input_value[1]][input_value[0]];
};

c35_BBSD16P :: c35_BBSD16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD16P_Y[input_value[1]][input_value[0]];
};

c35_BBSD16SMP :: c35_BBSD16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBSD16SP :: c35_BBSD16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD16SP_Y[input_value[1]][input_value[0]];
};

c35_BBSD24P :: c35_BBSD24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD24P_Y[input_value[1]][input_value[0]];
};

c35_BBSD24SMP :: c35_BBSD24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBSD24SP :: c35_BBSD24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD24SP_Y[input_value[1]][input_value[0]];
};

c35_BBSU1P :: c35_BBSU1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU1P_Y[input_value[1]][input_value[0]];
};

c35_BBSU4P :: c35_BBSU4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU4P_Y[input_value[1]][input_value[0]];
};

c35_BBSU4SMP :: c35_BBSU4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBSU8P :: c35_BBSU8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU8P_Y[input_value[1]][input_value[0]];
};

c35_BBSU8SMP :: c35_BBSU8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBSU8SP :: c35_BBSU8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU8SP_Y[input_value[1]][input_value[0]];
};

c35_BBSU16P :: c35_BBSU16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU16P_Y[input_value[1]][input_value[0]];
};

c35_BBSU16SMP :: c35_BBSU16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBSU16SP :: c35_BBSU16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU16SP_Y[input_value[1]][input_value[0]];
};

c35_BBSU24P :: c35_BBSU24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU24P_Y[input_value[1]][input_value[0]];
};

c35_BBSU24SMP :: c35_BBSU24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBSU24SP :: c35_BBSU24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU24SP_Y[input_value[1]][input_value[0]];
};

c35_BBT1P :: c35_BBT1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT1P_Y[input_value[1]][input_value[0]];
};

c35_BBT4P :: c35_BBT4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT4P_Y[input_value[1]][input_value[0]];
};

c35_BBT4SMP :: c35_BBT4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBT8P :: c35_BBT8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT8P_Y[input_value[1]][input_value[0]];
};

c35_BBT8SMP :: c35_BBT8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBT8SP :: c35_BBT8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT8SP_Y[input_value[1]][input_value[0]];
};

c35_BBT16P :: c35_BBT16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT16P_Y[input_value[1]][input_value[0]];
};

c35_BBT16SMP :: c35_BBT16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBT16SP :: c35_BBT16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT16SP_Y[input_value[1]][input_value[0]];
};

c35_BBT24P :: c35_BBT24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT24P_Y[input_value[1]][input_value[0]];
};

c35_BBT24SMP :: c35_BBT24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBT24SP :: c35_BBT24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT24SP_Y[input_value[1]][input_value[0]];
};

c35_BBTD1P :: c35_BBTD1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD1P_Y[input_value[1]][input_value[0]];
};

c35_BBTD4P :: c35_BBTD4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD4P_Y[input_value[1]][input_value[0]];
};

c35_BBTD4SMP :: c35_BBTD4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBTD8P :: c35_BBTD8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD8P_Y[input_value[1]][input_value[0]];
};

c35_BBTD8SMP :: c35_BBTD8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBTD8SP :: c35_BBTD8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD8SP_Y[input_value[1]][input_value[0]];
};

c35_BBTD16P :: c35_BBTD16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD16P_Y[input_value[1]][input_value[0]];
};

c35_BBTD16SMP :: c35_BBTD16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBTD16SP :: c35_BBTD16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD16SP_Y[input_value[1]][input_value[0]];
};

c35_BBTD24P :: c35_BBTD24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD24P_Y[input_value[1]][input_value[0]];
};

c35_BBTD24SMP :: c35_BBTD24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBTD24SP :: c35_BBTD24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD24SP_Y[input_value[1]][input_value[0]];
};

c35_BBTU1P :: c35_BBTU1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU1P_Y[input_value[1]][input_value[0]];
};

c35_BBTU4P :: c35_BBTU4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU4P_Y[input_value[1]][input_value[0]];
};

c35_BBTU4SMP :: c35_BBTU4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU4SMP_Y[input_value[1]][input_value[0]];
};

c35_BBTU8P :: c35_BBTU8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU8P_Y[input_value[1]][input_value[0]];
};

c35_BBTU8SMP :: c35_BBTU8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU8SMP_Y[input_value[1]][input_value[0]];
};

c35_BBTU8SP :: c35_BBTU8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU8SP_Y[input_value[1]][input_value[0]];
};

c35_BBTU16P :: c35_BBTU16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU16P_Y[input_value[1]][input_value[0]];
};

c35_BBTU16SMP :: c35_BBTU16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU16SMP_Y[input_value[1]][input_value[0]];
};

c35_BBTU16SP :: c35_BBTU16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU16SP_Y[input_value[1]][input_value[0]];
};

c35_BBTU24P :: c35_BBTU24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU24P_Y[input_value[1]][input_value[0]];
};

c35_BBTU24SMP :: c35_BBTU24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU24SMP_Y[input_value[1]][input_value[0]];
};

c35_BBTU24SP :: c35_BBTU24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU24SP_Y[input_value[1]][input_value[0]];
};

c35_BT1P :: c35_BT1P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT1P_PAD[input_value[1]][input_value[0]];
};

c35_BT2P :: c35_BT2P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT2P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT2P_PAD[input_value[1]][input_value[0]];
};

c35_BT4P :: c35_BT4P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT4P_PAD[input_value[1]][input_value[0]];
};

c35_BT4SMP :: c35_BT4SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT4SMP_PAD[input_value[1]][input_value[0]];
};

c35_BT8P :: c35_BT8P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT8P_PAD[input_value[1]][input_value[0]];
};

c35_BT8SMP :: c35_BT8SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT8SMP_PAD[input_value[1]][input_value[0]];
};

c35_BT8SP :: c35_BT8SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT8SP_PAD[input_value[1]][input_value[0]];
};

c35_BT12P :: c35_BT12P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT12P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT12P_PAD[input_value[1]][input_value[0]];
};

c35_BT12SMP :: c35_BT12SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT12SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT12SMP_PAD[input_value[1]][input_value[0]];
};

c35_BT12SP :: c35_BT12SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT12SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT12SP_PAD[input_value[1]][input_value[0]];
};

c35_BT16P :: c35_BT16P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT16P_PAD[input_value[1]][input_value[0]];
};

c35_BT16SMP :: c35_BT16SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT16SMP_PAD[input_value[1]][input_value[0]];
};

c35_BT16SP :: c35_BT16SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT16SP_PAD[input_value[1]][input_value[0]];
};

c35_BT24P :: c35_BT24P (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT24P_PAD[input_value[1]][input_value[0]];
};

c35_BT24SMP :: c35_BT24SMP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT24SMP_PAD[input_value[1]][input_value[0]];
};

c35_BT24SP :: c35_BT24SP (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT24SP_PAD[input_value[1]][input_value[0]];
};

c35_BU1P :: c35_BU1P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU1P_PAD[input_value[0]];
};

c35_BU2P :: c35_BU2P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU2P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU2P_PAD[input_value[0]];
};

c35_BU4P :: c35_BU4P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU4P_PAD[input_value[0]];
};

c35_BU4SMP :: c35_BU4SMP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU4SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU4SMP_PAD[input_value[0]];
};

c35_BU8P :: c35_BU8P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU8P_PAD[input_value[0]];
};

c35_BU8SMP :: c35_BU8SMP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU8SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU8SMP_PAD[input_value[0]];
};

c35_BU8SP :: c35_BU8SP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU8SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU8SP_PAD[input_value[0]];
};

c35_BU12P :: c35_BU12P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU12P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU12P_PAD[input_value[0]];
};

c35_BU12SMP :: c35_BU12SMP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU12SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU12SMP_PAD[input_value[0]];
};

c35_BU12SP :: c35_BU12SP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU12SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU12SP_PAD[input_value[0]];
};

c35_BU16P :: c35_BU16P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU16P_PAD[input_value[0]];
};

c35_BU16SMP :: c35_BU16SMP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU16SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU16SMP_PAD[input_value[0]];
};

c35_BU16SP :: c35_BU16SP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU16SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU16SP_PAD[input_value[0]];
};

c35_BU24P :: c35_BU24P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU24P_PAD[input_value[0]];
};

c35_BU24SMP :: c35_BU24SMP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU24SMP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU24SMP_PAD[input_value[0]];
};

c35_BU24SP :: c35_BU24SP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU24SP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU24SP_PAD[input_value[0]];
};

c35_BUDD1P :: c35_BUDD1P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD1P_PAD[input_value[0]];
};

c35_BUDD2P :: c35_BUDD2P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD2P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD2P_PAD[input_value[0]];
};

c35_BUDD4P :: c35_BUDD4P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD4P_PAD[input_value[0]];
};

c35_BUDD8P :: c35_BUDD8P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD8P_PAD[input_value[0]];
};

c35_BUDD12P :: c35_BUDD12P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD12P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD12P_PAD[input_value[0]];
};

c35_BUDD16P :: c35_BUDD16P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD16P_PAD[input_value[0]];
};

c35_BUDD24P :: c35_BUDD24P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD24P_PAD[input_value[0]];
};

c35_BUDU1P :: c35_BUDU1P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU1P_PAD[input_value[0]];
};

c35_BUDU2P :: c35_BUDU2P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU2P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU2P_PAD[input_value[0]];
};

c35_BUDU4P :: c35_BUDU4P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU4P_PAD[input_value[0]];
};

c35_BUDU8P :: c35_BUDU8P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU8P_PAD[input_value[0]];
};

c35_BUDU12P :: c35_BUDU12P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU12P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU12P_PAD[input_value[0]];
};

c35_BUDU16P :: c35_BUDU16P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU16P_PAD[input_value[0]];
};

c35_BUDU24P :: c35_BUDU24P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU24P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU24P_PAD[input_value[0]];
};

c35_CBU1P :: c35_CBU1P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_CBU1P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CBU1P_Y[input_value[0]];
};

c35_CBU2P :: c35_CBU2P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_CBU2P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CBU2P_Y[input_value[0]];
};

c35_ICCK2P :: c35_ICCK2P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICCK2P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICCK2P_Y[input_value[0]];
};

c35_ICCK4P :: c35_ICCK4P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICCK4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICCK4P_Y[input_value[0]];
};

c35_ICCK8P :: c35_ICCK8P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICCK8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICCK8P_Y[input_value[0]];
};

c35_ICCK16P :: c35_ICCK16P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICCK16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICCK16P_Y[input_value[0]];
};

c35_ICDP :: c35_ICDP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICDP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICDP_Y[input_value[0]];
};

c35_ICP :: c35_ICP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICP_Y[input_value[0]];
};

c35_ICUP :: c35_ICUP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICUP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICUP_Y[input_value[0]];
};

c35_ISDP :: c35_ISDP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ISDP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ISDP_Y[input_value[0]];
};

c35_ISP :: c35_ISP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ISP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ISP_Y[input_value[0]];
};

c35_ISUP :: c35_ISUP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ISUP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ISUP_Y[input_value[0]];
};

c35_ITCK2P :: c35_ITCK2P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITCK2P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITCK2P_Y[input_value[0]];
};

c35_ITCK4P :: c35_ITCK4P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITCK4P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITCK4P_Y[input_value[0]];
};

c35_ITCK8P :: c35_ITCK8P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITCK8P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITCK8P_Y[input_value[0]];
};

c35_ITCK16P :: c35_ITCK16P (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITCK16P :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITCK16P_Y[input_value[0]];
};

c35_ITDP :: c35_ITDP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITDP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITDP_Y[input_value[0]];
};

c35_ITP :: c35_ITP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITP_Y[input_value[0]];
};

c35_ITUP :: c35_ITUP (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITUP :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITUP_Y[input_value[0]];
};

c35_BBC1P_V5 :: c35_BBC1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC4P_V5 :: c35_BBC4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC4SMP_V5 :: c35_BBC4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC8P_V5 :: c35_BBC8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC8SMP_V5 :: c35_BBC8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC8SP_V5 :: c35_BBC8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC16P_V5 :: c35_BBC16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC16SMP_V5 :: c35_BBC16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC16SP_V5 :: c35_BBC16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC24P_V5 :: c35_BBC24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC24SMP_V5 :: c35_BBC24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBC24SP_V5 :: c35_BBC24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBC24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBC24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD1P_V5 :: c35_BBCD1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD4P_V5 :: c35_BBCD4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD4SMP_V5 :: c35_BBCD4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD8P_V5 :: c35_BBCD8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD8SMP_V5 :: c35_BBCD8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD8SP_V5 :: c35_BBCD8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD16P_V5 :: c35_BBCD16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD16SMP_V5 :: c35_BBCD16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD16SP_V5 :: c35_BBCD16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD24P_V5 :: c35_BBCD24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD24SMP_V5 :: c35_BBCD24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCD24SP_V5 :: c35_BBCD24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCD24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCD24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU1P_V5 :: c35_BBCU1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU4P_V5 :: c35_BBCU4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU4SMP_V5 :: c35_BBCU4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU8P_V5 :: c35_BBCU8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU8SMP_V5 :: c35_BBCU8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU8SP_V5 :: c35_BBCU8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU16P_V5 :: c35_BBCU16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU16SMP_V5 :: c35_BBCU16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU16SP_V5 :: c35_BBCU16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU24P_V5 :: c35_BBCU24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU24SMP_V5 :: c35_BBCU24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBCU24SP_V5 :: c35_BBCU24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBCU24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBCU24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS1P_V5 :: c35_BBS1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS4P_V5 :: c35_BBS4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS4SMP_V5 :: c35_BBS4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS8P_V5 :: c35_BBS8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS8SMP_V5 :: c35_BBS8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS8SP_V5 :: c35_BBS8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS16P_V5 :: c35_BBS16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS16SMP_V5 :: c35_BBS16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS16SP_V5 :: c35_BBS16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS24P_V5 :: c35_BBS24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS24SMP_V5 :: c35_BBS24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBS24SP_V5 :: c35_BBS24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBS24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBS24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD1P_V5 :: c35_BBSD1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD4P_V5 :: c35_BBSD4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD4SMP_V5 :: c35_BBSD4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD8P_V5 :: c35_BBSD8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD8SMP_V5 :: c35_BBSD8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD8SP_V5 :: c35_BBSD8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD16P_V5 :: c35_BBSD16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD16SMP_V5 :: c35_BBSD16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD16SP_V5 :: c35_BBSD16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD24P_V5 :: c35_BBSD24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD24SMP_V5 :: c35_BBSD24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSD24SP_V5 :: c35_BBSD24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSD24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSD24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU1P_V5 :: c35_BBSU1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU4P_V5 :: c35_BBSU4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU4SMP_V5 :: c35_BBSU4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU8P_V5 :: c35_BBSU8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU8SMP_V5 :: c35_BBSU8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU8SP_V5 :: c35_BBSU8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU16P_V5 :: c35_BBSU16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU16SMP_V5 :: c35_BBSU16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU16SP_V5 :: c35_BBSU16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU24P_V5 :: c35_BBSU24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU24SMP_V5 :: c35_BBSU24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBSU24SP_V5 :: c35_BBSU24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBSU24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBSU24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT1P_V5 :: c35_BBT1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT4P_V5 :: c35_BBT4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT4SMP_V5 :: c35_BBT4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT8P_V5 :: c35_BBT8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT8SMP_V5 :: c35_BBT8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT8SP_V5 :: c35_BBT8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT16P_V5 :: c35_BBT16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT16SMP_V5 :: c35_BBT16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT16SP_V5 :: c35_BBT16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT24P_V5 :: c35_BBT24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT24SMP_V5 :: c35_BBT24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBT24SP_V5 :: c35_BBT24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBT24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBT24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD1P_V5 :: c35_BBTD1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD4P_V5 :: c35_BBTD4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD4SMP_V5 :: c35_BBTD4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD8P_V5 :: c35_BBTD8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD8SMP_V5 :: c35_BBTD8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD8SP_V5 :: c35_BBTD8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD16P_V5 :: c35_BBTD16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD16SMP_V5 :: c35_BBTD16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD16SP_V5 :: c35_BBTD16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD24P_V5 :: c35_BBTD24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD24SMP_V5 :: c35_BBTD24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTD24SP_V5 :: c35_BBTD24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTD24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTD24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU1P_V5 :: c35_BBTU1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU1P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU4P_V5 :: c35_BBTU4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU4P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU4SMP_V5 :: c35_BBTU4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU4SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU8P_V5 :: c35_BBTU8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU8P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU8SMP_V5 :: c35_BBTU8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU8SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU8SP_V5 :: c35_BBTU8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU8SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU16P_V5 :: c35_BBTU16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU16P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU16SMP_V5 :: c35_BBTU16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU16SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU16SP_V5 :: c35_BBTU16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU16SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU24P_V5 :: c35_BBTU24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU24P_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU24SMP_V5 :: c35_BBTU24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU24SMP_V5_Y[input_value[1]][input_value[0]];
};

c35_BBTU24SP_V5 :: c35_BBTU24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BBTU24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BBTU24SP_V5_Y[input_value[1]][input_value[0]];
};

c35_BT1P_V5 :: c35_BT1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT1P_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT2P_V5 :: c35_BT2P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT2P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT2P_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT4P_V5 :: c35_BT4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT4P_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT4SMP_V5 :: c35_BT4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT4SMP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT8P_V5 :: c35_BT8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT8P_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT8SMP_V5 :: c35_BT8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT8SMP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT8SP_V5 :: c35_BT8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT8SP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT12P_V5 :: c35_BT12P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT12P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT12P_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT12SMP_V5 :: c35_BT12SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT12SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT12SMP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT12SP_V5 :: c35_BT12SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT12SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT12SP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT16P_V5 :: c35_BT16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT16P_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT16SMP_V5 :: c35_BT16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT16SMP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT16SP_V5 :: c35_BT16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT16SP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT24P_V5 :: c35_BT24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT24P_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT24SMP_V5 :: c35_BT24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT24SMP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BT24SP_V5 :: c35_BT24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 2;
	outputs_number = 1;
	inputs_name = "A,EN";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BT24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BT24SP_V5_PAD[input_value[1]][input_value[0]];
};

c35_BU1P_V5 :: c35_BU1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU1P_V5_PAD[input_value[0]];
};

c35_BU2P_V5 :: c35_BU2P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU2P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU2P_V5_PAD[input_value[0]];
};

c35_BU4P_V5 :: c35_BU4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU4P_V5_PAD[input_value[0]];
};

c35_BU4SMP_V5 :: c35_BU4SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU4SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU4SMP_V5_PAD[input_value[0]];
};

c35_BU8P_V5 :: c35_BU8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU8P_V5_PAD[input_value[0]];
};

c35_BU8SMP_V5 :: c35_BU8SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU8SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU8SMP_V5_PAD[input_value[0]];
};

c35_BU8SP_V5 :: c35_BU8SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU8SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU8SP_V5_PAD[input_value[0]];
};

c35_BU12P_V5 :: c35_BU12P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU12P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU12P_V5_PAD[input_value[0]];
};

c35_BU12SMP_V5 :: c35_BU12SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU12SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU12SMP_V5_PAD[input_value[0]];
};

c35_BU12SP_V5 :: c35_BU12SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU12SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU12SP_V5_PAD[input_value[0]];
};

c35_BU16P_V5 :: c35_BU16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU16P_V5_PAD[input_value[0]];
};

c35_BU16SMP_V5 :: c35_BU16SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU16SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU16SMP_V5_PAD[input_value[0]];
};

c35_BU16SP_V5 :: c35_BU16SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU16SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU16SP_V5_PAD[input_value[0]];
};

c35_BU24P_V5 :: c35_BU24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU24P_V5_PAD[input_value[0]];
};

c35_BU24SMP_V5 :: c35_BU24SMP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU24SMP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU24SMP_V5_PAD[input_value[0]];
};

c35_BU24SP_V5 :: c35_BU24SP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BU24SP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BU24SP_V5_PAD[input_value[0]];
};

c35_BUDD1P_V5 :: c35_BUDD1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD1P_V5_PAD[input_value[0]];
};

c35_BUDD2P_V5 :: c35_BUDD2P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD2P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD2P_V5_PAD[input_value[0]];
};

c35_BUDD4P_V5 :: c35_BUDD4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD4P_V5_PAD[input_value[0]];
};

c35_BUDD8P_V5 :: c35_BUDD8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD8P_V5_PAD[input_value[0]];
};

c35_BUDD12P_V5 :: c35_BUDD12P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD12P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD12P_V5_PAD[input_value[0]];
};

c35_BUDD16P_V5 :: c35_BUDD16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD16P_V5_PAD[input_value[0]];
};

c35_BUDD24P_V5 :: c35_BUDD24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDD24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDD24P_V5_PAD[input_value[0]];
};

c35_BUDU1P_V5 :: c35_BUDU1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU1P_V5_PAD[input_value[0]];
};

c35_BUDU2P_V5 :: c35_BUDU2P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU2P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU2P_V5_PAD[input_value[0]];
};

c35_BUDU4P_V5 :: c35_BUDU4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU4P_V5_PAD[input_value[0]];
};

c35_BUDU8P_V5 :: c35_BUDU8P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU8P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU8P_V5_PAD[input_value[0]];
};

c35_BUDU12P_V5 :: c35_BUDU12P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU12P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU12P_V5_PAD[input_value[0]];
};

c35_BUDU16P_V5 :: c35_BUDU16P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU16P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU16P_V5_PAD[input_value[0]];
};

c35_BUDU24P_V5 :: c35_BUDU24P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "PAD";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_BUDU24P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_BUDU24P_V5_PAD[input_value[0]];
};

c35_CBU1P_V5 :: c35_CBU1P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_CBU1P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CBU1P_V5_Y[input_value[0]];
};

c35_CBU2P_V5 :: c35_CBU2P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "A";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_CBU2P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_CBU2P_V5_Y[input_value[0]];
};

c35_ICCK2P_V5 :: c35_ICCK2P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICCK2P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICCK2P_V5_Y[input_value[0]];
};

c35_ICCK4P_V5 :: c35_ICCK4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICCK4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICCK4P_V5_Y[input_value[0]];
};

c35_ICDP_V5 :: c35_ICDP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICDP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICDP_V5_Y[input_value[0]];
};

c35_ICP_V5 :: c35_ICP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICP_V5_Y[input_value[0]];
};

c35_ICUP_V5 :: c35_ICUP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ICUP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ICUP_V5_Y[input_value[0]];
};

c35_ISDP_V5 :: c35_ISDP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ISDP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ISDP_V5_Y[input_value[0]];
};

c35_ISP_V5 :: c35_ISP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ISP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ISP_V5_Y[input_value[0]];
};

c35_ISUP_V5 :: c35_ISUP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ISUP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ISUP_V5_Y[input_value[0]];
};

c35_ITCK2P_V5 :: c35_ITCK2P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITCK2P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITCK2P_V5_Y[input_value[0]];
};

c35_ITCK4P_V5 :: c35_ITCK4P_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITCK4P_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITCK4P_V5_Y[input_value[0]];
};

c35_ITDP_V5 :: c35_ITDP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITDP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITDP_V5_Y[input_value[0]];
};

c35_ITP_V5 :: c35_ITP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITP_V5 :: calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITP_V5_Y[input_value[0]];
};

c35_ITUP_V5::c35_ITUP_V5 (const char* name) : generic_gate(name) {
	inputs_number = 1;
	outputs_number = 1;
	inputs_name = "PAD";
	outputs_name = "Y";
	type = _TYPE_COMBINATIONAL;
	InitAfterConstr ();
};

void c35_ITUP_V5::calculate_output(int o) {
	temp_output_value[o] = TRUTH_TABLE_ITUP_V5_Y[input_value[0]];
};

