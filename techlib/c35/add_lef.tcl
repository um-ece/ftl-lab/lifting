#!/usr/bin/tclsh8.4

set i 1
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}

if {([regexp -- "-ic" $list_arg] == 1)} {
	set cpp_name [lindex $list_arg [expr [lsearch $list_arg "-ic"] + 1]]
	set cpp_file [open $cpp_name "r"]
}
if {([regexp -- "-il" $list_arg] == 1)} {
	set lef_name [lindex $list_arg [expr [lsearch $list_arg "-il"] + 1]]
	
}
if {([regexp -- "-o" $list_arg] == 1)} {
	set output_name [lindex $list_arg [expr [lsearch $list_arg "-o"] + 1]]
	set output_file [open $output_name "w"]
	set output_flag 1
}
set flag_gate 0
set output_flag 0
gets $cpp_file Line;
while {([eof $cpp_file] != 1)} {
	if {([regexp  "generic_gate" $Line] == 1)} {
		set temp_Line [split $Line " "]
		set temp_Line [split $temp_Line "::"]
		set gate_type [lindex $temp_Line 0]
		regsub c35_ $gate_type "" gate_type
		set flag_gate 1
#		puts $gate_type
	}
	
	if {([regexp  "InitAfterConstr" $Line] == 1)} {
	
		set lef_file [open $lef_name "r"]
		set lib_gate_flag 0
		set flag_close_lib 0
		gets $lef_file lib_Line;
		set temp_gate_type $gate_type
#		set temp_gate_type [string tolower $temp_gate_type]
		regsub " " $temp_gate_type "" temp_gate_type
		set temp_gate_type_name $temp_gate_type
#		append temp_gate_type ".cell"
		
		
#		puts $temp_gate_type
		
		while {([eof $lef_file] != 1)&&($flag_close_lib ==0)} {
			if {([regexp  -nocase "MACRO $temp_gate_type" $lib_Line] == 1)} {
				
					set lib_gate_flag 1
#					
			}
			if {([regexp  -nocase "END $temp_gate_type" $lib_Line] == 1)} {
				
					set lib_gate_flag 0
#					puts $lib_Line
					set flag_close_lib 1
			}
			
			
			if {([regexp  "SIZE" $lib_Line] == 1)&&($lib_gate_flag==1)} {
#			puts $lib_Line
#			append capa_value [lindex $lib_Line 2]
			puts "gate_size\[0\] = [lindex $lib_Line 1] ;"
			puts "gate_size\[1\] = [lindex $lib_Line 3] ;"
			if { $output_flag == 1} {
				puts $output_file "gate_size\[0\] = [lindex $lib_Line 1] ;"
				puts $output_file "gate_size\[1\] = [lindex $lib_Line 3] ;"
				}
			}
			
		gets $lef_file lib_Line;
		}
		close $lef_file
#		puts "capa of $gate_type : $capa_value"
#puts $n
#		unset capa_value
	}
puts $Line
if { $output_flag == 1} {
	puts $output_file $Line
	}
gets $cpp_file Line;
}
close $cpp_file
exit