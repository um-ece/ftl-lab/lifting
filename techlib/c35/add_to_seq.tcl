#!/usr/bin/tclsh8.4

set i 1
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}
#set ff_flag_gate 0
set output_flag 0
if {([regexp -- "-ic" $list_arg] == 1)} {
	set comb_name [lindex $list_arg [expr [lsearch $list_arg "-ic"] + 1]]
#	set comb_file [open $comb_name "r"]
}
if {([regexp -- "-il" $list_arg] == 1)} {
	set list_name [lindex $list_arg [expr [lsearch $list_arg "-il"] + 1]]
	set list_file [open $list_name "r"]
}
if {([regexp -- "-o" $list_arg] == 1)} {
	set output_name [lindex $list_arg [expr [lsearch $list_arg "-o"] + 1]]
	set output_file [open $output_name "w"]
	set output_flag 1
}

gets $list_file Line;
while {([eof $list_file] != 1)} {
	set ff_name $Line
	set temp_ff_name "c35_"
	append temp_ff_name $ff_name 
	set flag_close_comb 0
	set ff_gate_flag 0
#	puts $temp_ff_name
	
	set comb_file [open $comb_name "r"]
	gets $comb_file comb_Line;
	while {([eof $comb_file] != 1)&&($flag_close_comb ==0)} {
			if {([regexp  -nocase "$temp_ff_name :: $temp_ff_name" $comb_Line] == 1)} {
				
					set ff_gate_flag 1
					puts "\} else if \(gateTYpe==\"$ff_name\"\) \{"
			}
			if {([regexp  -nocase "input_capa" $comb_Line] == 1)&&($ff_gate_flag==1)} {		
					puts $comb_Line			
			}
			if {([regexp  -nocase "gate_size" $comb_Line] == 1)&&($ff_gate_flag==1)} {		
					puts $comb_Line			
			}
			if {([regexp  "InitAfterConstr" $comb_Line] == 1)&&($ff_gate_flag==1)} {
				set ff_gate_flag 0
				set flag_close_comb 1
				
			}
			gets $comb_file comb_Line;
	}
	close $comb_file
	gets $list_file Line;
}
close $list_file
puts "\}"

if { $output_flag == 1} {
	puts $output_file $Line
	}

exit