#ifndef __TRUTHTABLES_H
#define __TRUTHTABLES_H

const int AND_TRUTH_TABLE[4][4] = { -1,-1,0,-1,    -1,-1,0,-1,     0,0,0,0,    -1,-1,0,1 };
const int OR_TRUTH_TABLE[4][4] = { -1,-1,-1,1,    -1,-1,-1,1,     -1,-1,0,1,    1,1,1,1 };
const int XOR_TRUTH_TABLE[4][4] = { -1,-1,-1,-1,    -1,-1,-1,-1,     -1,-1,0,1,    -1,-1,1,0 };
const int DFF_TRUTH_TABLE[4] = {0,1,2,2 };
const int DFF_NOT_TRUTH_TABLE[4] = {1,0,2,2 };
const int NOT_TRUTH_TABLE[4] = {1,0,2,2 };


#endif
