#ifndef __PI_H
#define __PI_H

//////////////////////////////////////////////////////////////////////
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-06-2007
//////////////////////////////////////////////////////////////////////

#include "../../generic_gate.h"


class pi : public generic_gate {

public:
	pi(const char* name) : generic_gate(name) {
		inputs_number=1;
		outputs_number=1;
		type=-1;
		//strcpy (output_line_name,name);
	};

	void calculate_output(int o) {
		temp_output_value[o]=input_value[o];
	};

	void calculate_output() {
		calculate_output(0);
	};

	void set_input (int value) {
		generic_gate::set_input (0, value);
	};

	int read_input () { return input_value[0]; };

	virtual void print_gates (ostream &os) {
		os << " " <<  name << " " ;
	}
#ifdef __PROBA_CALCULATION__
	void set_input_probability (int value, float prob) {
		output_probabilities[0][value]=prob;
	}
#endif

};

#endif

