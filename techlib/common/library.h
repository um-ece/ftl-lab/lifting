#ifndef __LIBRARY_H
#define __LIBRARY_H

#include <map>
#include <string.h>
#include "truthtables.h"

//////////////////////////////////////////////////////////////////////
// library.h: interface for the parser
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 06-09-2007
//////////////////////////////////////////////////////////////////////

using namespace std;

class generic_gate;
class generic_ff;
class generic_memory;

class library {
public:
	char name[255];

	library (char *n) {
		strcpy (name, n);
	};

	virtual int getGateType (string gateType) = 0;

	virtual generic_gate* createCombGate (string gateType, const char* gateName) = 0;

	virtual generic_ff* createSeqGate (string gateType, const char* gateName) = 0;

	virtual generic_memory* createMemory (string gateType, const char* gateName) = 0;
};



#endif
