#ifndef __ZERO_H
#define __ZERO_H

//////////////////////////////////////////////////////////////////////
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-06-2007
//////////////////////////////////////////////////////////////////////

#include "../../generic_gate.h"


class zero : public generic_gate {

public:
	zero(void) : generic_gate("GND") {
	  inputs_number=0;
	  outputs_number=1;
	  
	  propagate_output_during_init = 1 ; // in case of constant, during init, the output is both calculated and propagated				
	};

	void calculate_output(int o) {
	  output_is_changed[0]=1;
	  temp_output_value[0]=0;
	  output_value[0]=0;
	};
};

#endif

