#ifndef __PO_H
#define __PO_H

//////////////////////////////////////////////////////////////////////
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-06-2007
//////////////////////////////////////////////////////////////////////

#include "../../generic_gate.h"


class po : public generic_gate {

public:
	po(const char* name) : generic_gate(name) {
		inputs_number=1;
		outputs_number=1;
		type=-2;
	};

	void calculate_output(int o) {
		temp_output_value[o]=input_value[o];
	};

	void calculate_output() {
		calculate_output(0);
	};

	int read_output () { return output_value[0]; };

	virtual void print_gates (ostream &os) {
		os << " " <<  name << " " ;
	}


};

#endif

