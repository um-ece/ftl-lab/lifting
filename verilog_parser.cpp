
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison LALR(1) parsers in C++
   
      Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Free Software
   Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* First part of user declarations.  */


/* Line 311 of lalr1.cc  */
#line 41 "verilog_parser.cpp"


#include "verilog_parser.hpp"

/* User implementation prologue.  */


/* Line 317 of lalr1.cc  */
#line 50 "verilog_parser.cpp"
/* Unqualified %code blocks.  */

/* Line 318 of lalr1.cc  */
#line 66 "verilog_parser.yy"

     # include "./verilog_driver.h"
     


/* Line 318 of lalr1.cc  */
#line 61 "verilog_parser.cpp"

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* FIXME: INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#define YYUSE(e) ((void) (e))

/* Enable debugging if requested.  */
#if YYDEBUG

/* A pseudo ostream that takes yydebug_ into account.  */
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)	\
do {							\
  if (yydebug_)						\
    {							\
      *yycdebug_ << Title << ' ';			\
      yy_symbol_print_ ((Type), (Value), (Location));	\
      *yycdebug_ << std::endl;				\
    }							\
} while (false)

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug_)				\
    yy_reduce_print_ (Rule);		\
} while (false)

# define YY_STACK_PRINT()		\
do {					\
  if (yydebug_)				\
    yystack_print_ ();			\
} while (false)

#else /* !YYDEBUG */

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_REDUCE_PRINT(Rule)
# define YY_STACK_PRINT()

#endif /* !YYDEBUG */

#define yyerrok		(yyerrstatus_ = 0)
#define yyclearin	(yychar = yyempty_)

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)


/* Line 380 of lalr1.cc  */
#line 1 "[Bison:b4_percent_define_default]"

namespace yy {

/* Line 380 of lalr1.cc  */
#line 130 "verilog_parser.cpp"
#if YYERROR_VERBOSE

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  verilog_parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              /* Fall through.  */
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }

#endif

  /// Build a parser object.
  verilog_parser::verilog_parser (verilog_driver& driver_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      driver (driver_yyarg)
  {
  }

  verilog_parser::~verilog_parser ()
  {
  }

#if YYDEBUG
  /*--------------------------------.
  | Print this symbol on YYOUTPUT.  |
  `--------------------------------*/

  inline void
  verilog_parser::yy_symbol_value_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yyvaluep);
    switch (yytype)
      {
        case 13: /* "\"identifier\"" */

/* Line 449 of lalr1.cc  */
#line 84 "verilog_parser.yy"
	{ debug_stream () << *(yyvaluep->sval); };

/* Line 449 of lalr1.cc  */
#line 207 "verilog_parser.cpp"
	break;
      case 14: /* "\"number\"" */

/* Line 449 of lalr1.cc  */
#line 87 "verilog_parser.yy"
	{ debug_stream () << (yyvaluep->ival); };

/* Line 449 of lalr1.cc  */
#line 216 "verilog_parser.cpp"
	break;
       default:
	  break;
      }
  }


  void
  verilog_parser::yy_symbol_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    *yycdebug_ << (yytype < yyntokens_ ? "token" : "nterm")
	       << ' ' << yytname_[yytype] << " ("
	       << *yylocationp << ": ";
    yy_symbol_value_print_ (yytype, yyvaluep, yylocationp);
    *yycdebug_ << ')';
  }
#endif

  void
  verilog_parser::yydestruct_ (const char* yymsg,
			   int yytype, semantic_type* yyvaluep, location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yymsg);
    YYUSE (yyvaluep);

    YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

    switch (yytype)
      {
        case 13: /* "\"identifier\"" */

/* Line 480 of lalr1.cc  */
#line 85 "verilog_parser.yy"
	{ delete (yyvaluep->sval); };

/* Line 480 of lalr1.cc  */
#line 255 "verilog_parser.cpp"
	break;

	default:
	  break;
      }
  }

  void
  verilog_parser::yypop_ (unsigned int n)
  {
    yystate_stack_.pop (n);
    yysemantic_stack_.pop (n);
    yylocation_stack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  verilog_parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  verilog_parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  verilog_parser::debug_level_type
  verilog_parser::debug_level () const
  {
    return yydebug_;
  }

  void
  verilog_parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif

  int
  verilog_parser::parse ()
  {
    /// Lookahead and lookahead in internal form.
    int yychar = yyempty_;
    int yytoken = 0;

    /* State.  */
    int yyn;
    int yylen = 0;
    int yystate = 0;

    /* Error handling.  */
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// Semantic value of the lookahead.
    semantic_type yylval;
    /// Location of the lookahead.
    location_type yylloc;
    /// The locations where the error started and ended.
    location_type yyerror_range[2];

    /// $$.
    semantic_type yyval;
    /// @$.
    location_type yyloc;

    int yyresult;

    YYCDEBUG << "Starting parse" << std::endl;


    /* User initialization code.  */
    
/* Line 553 of lalr1.cc  */
#line 49 "verilog_parser.yy"
{
       // Initialize the initial location.  
       yylloc.begin.filename = yylloc.end.filename = &driver.file;
     }

/* Line 553 of lalr1.cc  */
#line 341 "verilog_parser.cpp"

    /* Initialize the stacks.  The initial state will be pushed in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystate_stack_ = state_stack_type (0);
    yysemantic_stack_ = semantic_stack_type (0);
    yylocation_stack_ = location_stack_type (0);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* New state.  */
  yynewstate:
    yystate_stack_.push (yystate);
    YYCDEBUG << "Entering state " << yystate << std::endl;

    /* Accept?  */
    if (yystate == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    /* Backup.  */
  yybackup:

    /* Try to take a decision without lookahead.  */
    yyn = yypact_[yystate];
    if (yyn == yypact_ninf_)
      goto yydefault;

    /* Read a lookahead token.  */
    if (yychar == yyempty_)
      {
	YYCDEBUG << "Reading a token: ";
	yychar = yylex (&yylval, &yylloc, driver);
      }


    /* Convert token to internal form.  */
    if (yychar <= yyeof_)
      {
	yychar = yytoken = yyeof_;
	YYCDEBUG << "Now at end of input." << std::endl;
      }
    else
      {
	yytoken = yytranslate_ (yychar);
	YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
      }

    /* If the proper action on seeing token YYTOKEN is to reduce or to
       detect an error, take that action.  */
    yyn += yytoken;
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yytoken)
      goto yydefault;

    /* Reduce or error.  */
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
	if (yyn == 0 || yyn == yytable_ninf_)
	goto yyerrlab;
	yyn = -yyn;
	goto yyreduce;
      }

    /* Shift the lookahead token.  */
    YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

    /* Discard the token being shifted.  */
    yychar = yyempty_;

    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* Count tokens shifted since error; after three, turn off error
       status.  */
    if (yyerrstatus_)
      --yyerrstatus_;

    yystate = yyn;
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystate];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    /* If YYLEN is nonzero, implement the default value of the action:
       `$$ = $1'.  Otherwise, use the top of the stack.

       Otherwise, the following line sets YYVAL to garbage.
       This behavior is undocumented and Bison
       users should not rely upon it.  */
    if (yylen)
      yyval = yysemantic_stack_[yylen - 1];
    else
      yyval = yysemantic_stack_[0];

    {
      slice<location_type, location_stack_type> slice (yylocation_stack_, yylen);
      YYLLOC_DEFAULT (yyloc, slice, yylen);
    }
    YY_REDUCE_PRINT (yyn);
    switch (yyn)
      {
	  case 6:

/* Line 678 of lalr1.cc  */
#line 102 "verilog_parser.yy"
    {
			
		
			// driver.add_lib_cell (); 
#ifdef __DEBUG__
				cout << " cell library module, not yet managed " << endl;
#endif

			
		}
    break;

  case 38:

/* Line 678 of lalr1.cc  */
#line 171 "verilog_parser.yy"
    { 


			driver.circuit = NULL;
			//cout << "pp" ;
			driver.u_val = 0;
			driver.i_val = 0;	
			driver.module_name = *(yysemantic_stack_[(6) - (2)].sval);  // used only for check
			driver.module_number++;	

			driver.circuit = new generic_cell ( (*(yysemantic_stack_[(6) - (2)].sval)).c_str() ,0);  // cretes a new circuite
		
			//driver.incomp_hash.reserve(270);	

			if (driver.circuit == NULL)  {


				cerr << "Memory Error,  current_module ( " << *(yysemantic_stack_[(6) - (2)].sval) << " ) cannot be created ... " << endl;
				exit (-1);
		
			}


			// PB - using one & zero classes


		_unconnect unc_gate;


		one* my_one;

		my_one = new one (); // create new POWER SUPPLY connection

		if (my_one == NULL)  {


			cerr << "Memory Error,  current POWER_SUPPLY cannot be created ... " << endl;
			exit (-1);

		}

		unc_gate.line = "VCC";
		unc_gate.pos = 0;
		unc_gate.ref = my_one;				

		driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
		//PB
		//cout << "pp" ;
		
		driver.u_val++;
		driver.unconn_index[unc_gate.line] = driver.u_val ; 			

		//_PB


		// driver.circuit->add_generic_gate (my_one); // insert the new VCC in the circuit

		driver.circuit->add_const (my_one); // insert the new VCC in the circuit as a const




		
		zero* my_zero;

		my_zero = new zero (); // create new GND connection

		if (my_zero == NULL)  {


			cerr << "Memory Error,  current GROUND cannot be created ... " << endl;
			exit (-1);

		}

		unc_gate.line = "GND";
		unc_gate.pos = 0;
		unc_gate.ref = my_zero;				

		driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
		//PB
		//cout << "pp" ;
		driver.u_val++;
		driver.unconn_index[unc_gate.line] = driver.u_val ; 			
		//_PB



		// driver.circuit->add_generic_gate (my_zero); // insert the new VCC in the circuit
		driver.circuit->add_const (my_zero); // insert the new VCC in the circuit as a const

			
	 	// _PB - end using one & zero classe

/* */

			// driver.add_modules (); 
			
	
			

		}
    break;

  case 39:

/* Line 678 of lalr1.cc  */
#line 275 "verilog_parser.yy"
    {}
    break;

  case 40:

/* Line 678 of lalr1.cc  */
#line 276 "verilog_parser.yy"
    {}
    break;

  case 43:

/* Line 678 of lalr1.cc  */
#line 281 "verilog_parser.yy"
    {}
    break;

  case 44:

/* Line 678 of lalr1.cc  */
#line 283 "verilog_parser.yy"
    {

			
			pi* my_pi;


			_unconnect unc_gate;
			char  current_name[255];
			string name;
			int i;


			if ((yysemantic_stack_[(8) - (3)].ival) > (yysemantic_stack_[(8) - (5)].ival)) {
				for (i = (yysemantic_stack_[(8) - (3)].ival) ; i >= (yysemantic_stack_[(8) - (5)].ival) ; i-- )	{ 
					
					sprintf (current_name,"%s[%d]",(yysemantic_stack_[(8) - (7)].sval)->c_str(),  i) ;

#ifdef __DEBUG__
						cout << "new input : " << current_name << endl;
#endif
					
					name = current_name;

					my_pi = new pi (name.c_str()); // create new primary input

					if (my_pi == NULL)  {


						cerr << "Memory Error,  current Primary Input  ( " << name << " ) cannot be created ... " << endl;
						exit (-1);
		
					}

					unc_gate.line = name;
					unc_gate.pos = 0;
					unc_gate.ref = my_pi;				

					driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
					//PB
					driver.u_val++;
					//cout << "pp" ;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 

					//_PB
					driver.circuit->add_input (my_pi); // set the new PI as PI of the previous in the circuit

					

				
				}
			}
			else {

				for (i = (yysemantic_stack_[(8) - (3)].ival) ; i <= (yysemantic_stack_[(8) - (5)].ival) ; i++ )	{

					sprintf (current_name,"%s[%d]",(yysemantic_stack_[(8) - (7)].sval)->c_str(),  i) ;

#ifdef __DEBUG__
						cout << "new input : " << current_name << endl;
#endif

					name = current_name;

					my_pi = new pi (name.c_str()); // create new primary input

					if (my_pi == NULL)  {


						cerr << "Memory Error,  current Primary Input  ( " << name << " ) cannot be created ... " << endl;
						exit (-1);
		
					}


					unc_gate.line = name;
					unc_gate.pos = 0;
					unc_gate.ref = my_pi;				
					
					driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array									
					//PB
					//cout << "pp" ;	
					driver.u_val++;	
					driver.unconn_index.insert( make_pair( unc_gate.line , driver.u_val  ) ); 					

					//_PB
					driver.circuit->add_input (my_pi); // set the new PI as PI of the previous in the circuit
				
				}
			}
			
		}
    break;

  case 47:

/* Line 678 of lalr1.cc  */
#line 380 "verilog_parser.yy"
    {
		

		_unconnect unc_gate;

		pi* my_pi;

		my_pi = new pi ((*(yysemantic_stack_[(1) - (1)].sval)).c_str()); // create new primary input

		if (my_pi == NULL)  {


			cerr << "Memory Error,  current Primary Input  ( " << *(yysemantic_stack_[(1) - (1)].sval) << " ) cannot be created ... " << endl;
			exit (-1);

		}

		unc_gate.line = *(yysemantic_stack_[(1) - (1)].sval);
		unc_gate.pos = 0;
		unc_gate.ref = my_pi;				

		driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
					//PB
					//cout << "pp" ;
					driver.u_val++;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 			

					//_PB
		driver.circuit->add_input (my_pi); // set the new PI as PI of the previous in the circuit
		


	   }
    break;

  case 48:

/* Line 678 of lalr1.cc  */
#line 414 "verilog_parser.yy"
    {   

		_unconnect unc_gate;
		pi* my_pi;

		my_pi = new pi ( (*(yysemantic_stack_[(2) - (2)].sval)).c_str()); // create new primary input

		if (my_pi == NULL)  {


			cerr << "Memory Error,  current Primary Input  ( " << *(yysemantic_stack_[(2) - (2)].sval) << " ) cannot be created ... " << endl;
			exit (-1);

		}

		unc_gate.line = *(yysemantic_stack_[(2) - (2)].sval);
		unc_gate.pos = 0;
		unc_gate.ref = my_pi;				

		driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
					//PB
					//cout << "pp" ;
					driver.u_val++;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 			
					//_PB
		driver.circuit->add_input (my_pi); // set the new PI as PI of the previous in the circuit






	   }
    break;

  case 50:

/* Line 678 of lalr1.cc  */
#line 451 "verilog_parser.yy"
    {
			

			po* my_po;
			char  current_name[255];
			string name ;
			int i;
			
			// split the po array 

			if ((yysemantic_stack_[(8) - (3)].ival) > (yysemantic_stack_[(8) - (5)].ival)) {
				for (i = (yysemantic_stack_[(8) - (3)].ival) ; i >= (yysemantic_stack_[(8) - (5)].ival) ; i-- )	{
					
					sprintf (current_name,"%s[%d]",(yysemantic_stack_[(8) - (7)].sval)->c_str(),  i) ;

#ifdef __DEBUG__
						cout << "new input : " << current_name << endl;
#endif

					name = current_name;


					my_po = new po (name.c_str()); // create new primary input

					if (my_po == NULL)  {


						cerr << "Memory Error,  current Primary Output  ( " << name << " ) cannot be created ... " << endl;
						exit (-1);
		
					}


					// driver.unconnected_gate.push_back (my_pi); // push it into unconnected gate array      // DA VERIFICARE SE DEVO METTERLI O MENO IN QUESTO VETTORE
					driver.circuit->add_output (my_po); // set the new PO as PO of the previous in the circuit



				}
			}
			else {

				for (i = (yysemantic_stack_[(8) - (3)].ival) ; i <= (yysemantic_stack_[(8) - (5)].ival) ; i++ )	{

					sprintf (current_name,"%s[%d]",(yysemantic_stack_[(8) - (7)].sval)->c_str(),  i) ;

#ifdef __DEBUG__
						cout << "new input : " << current_name << endl;
#endif


					name = current_name;

					my_po = new po (name.c_str()); // create new primary input

					if (my_po == NULL)  {


						cerr << "Memory Error,  current Primary Output  ( " << name << " ) cannot be created ... " << endl;
						exit (-1);
		
					}


					// driver.unconnected_gate.push_back (my_pi); // push it into unconnected gate array      // DA VERIFICARE SE DEVO METTERLI O MENO IN QUESTO VETTORE
					driver.circuit->add_output (my_po); // set the new PO as PO of the previous in the circuit
				

				}
			}
			


		}
    break;

  case 53:

/* Line 678 of lalr1.cc  */
#line 531 "verilog_parser.yy"
    {


			po* my_po;

			my_po = new po ( (*(yysemantic_stack_[(1) - (1)].sval)).c_str()); // create new primary output

			if (my_po == NULL)  {


				cerr << "Memory Error,  current Primary Output  ( " << *(yysemantic_stack_[(1) - (1)].sval) << " ) cannot be created ... " << endl;
				exit (-1);

			}


			// driver.unconnected_gate.push_back (my_pi); // push it into unconnected gate array      // DA VERIFICARE SE DEVO METTERLI O MENO IN QUESTO VETTORE
			driver.circuit->add_output (my_po); // set the new PO as PO of the previous in the circuit
				




			

		     }
    break;

  case 54:

/* Line 678 of lalr1.cc  */
#line 558 "verilog_parser.yy"
    {

	
			po* my_po;


			my_po = new po ( (*(yysemantic_stack_[(2) - (2)].sval)).c_str()); // create new primary output

			if (my_po == NULL)  {


				cerr << "Memory Error,  current Primary Output  ( " << *(yysemantic_stack_[(2) - (2)].sval) << " ) cannot be created ... " << endl;
				exit (-1);

			}


			// driver.unconnected_gate.push_back (my_pi); // push it into unconnected gate array      // DA VERIFICARE SE DEVO METTERLI O MENO IN QUESTO VETTORE
			driver.circuit->add_output (my_po); // set the new PO as PO of the previous in the circuit
				


		}
    break;

  case 55:

/* Line 678 of lalr1.cc  */
#line 584 "verilog_parser.yy"
    {

		// aggiungere il codice di creazione dei gate vero e proprio 
#ifdef __DEBUG__
			cout << " parsing gate " << endl;
#endif
		

		int my_gate_id, pos, terminal_index, input_pos;
		const char* gate_name= (yysemantic_stack_[(6) - (2)].sval)->c_str () ;

#ifdef __DEBUG__
			cout << " parsing gate " << gate_name  << endl;
#endif


		_incomplete inc_gate;
		_unconnect unc_gate;

		//library lib;   // instantiate the current library   currentlib/librrary.h

		generic_gate *p_gate = NULL;
		generic_ff* s_gate = NULL;
		//PB
		generic_memory* m_gate = NULL;
		//_PB		
		generic_gate *input_gate;

		generic_cell *lib_cell, *mod_cell;


		my_gate_id = driver.lib->getGateType (*(yysemantic_stack_[(6) - (1)].sval));

		tokenizer t_input(',');
		tokenizer t_output(',');

#ifdef __DEBUG__
			cout << " my_gate_id   " <<  my_gate_id   << endl;
#endif
	
		

		switch (my_gate_id) {
			case 1: // Gate combinatiorio

				p_gate=driver.lib->createCombGate((*(yysemantic_stack_[(6) - (1)].sval)), gate_name);   // instatiate the current gate 
				
				
				driver.circuit->add_generic_gate (p_gate);

				break;

			case 2:  { // FF
					s_gate=driver.lib->createSeqGate((*(yysemantic_stack_[(6) - (1)].sval)), gate_name);  // 
					
					
					p_gate = s_gate;
					
					driver.circuit->add_FF_noscan (s_gate);


					break;
				}
			case 3: { // FF scan


					s_gate=driver.lib->createSeqGate((*(yysemantic_stack_[(6) - (1)].sval)), gate_name);  // 

					p_gate = s_gate;
					
					driver.circuit->add_FF_scan (s_gate);


					break;
				}
			//PB
			case 4: { // memory
					m_gate=driver.lib->createMemory((*(yysemantic_stack_[(6) - (1)].sval)), gate_name);  // 
					
					
					p_gate = m_gate;
					
					driver.circuit->add_memory (m_gate);

					break;
				}	
			//_PB
			default: {
				cerr << " Unkonwn gate ( " << *(yysemantic_stack_[(6) - (1)].sval) << " ) " << endl;
				exit (-1);
				break; 

			}		
				
		}


#ifdef __DEBUG__
			cout << "gate name : " << *(yysemantic_stack_[(6) - (1)].sval) << " gate number : " << driver.lib->getGateType (*(yysemantic_stack_[(6) - (1)].sval)) << " number of terminal : " << driver.terminal.size () << endl;
#endif



	//if (my_gate_id == 1)	{  // comb
		
#ifdef __DEBUG__
			cout << " processing current gate :  " << p_gate->name << endl;
#endif
		// for each input 
		
		if (!driver.terminal_name.empty ()) {   // case of notation .I()

			
			t_input.tokeinze (p_gate->inputs_name);
			t_output.tokeinze (p_gate->outputs_name);

			string t_name;
			string type_name;
			for (terminal_index = 0; terminal_index < driver.terminal_name.size (); terminal_index++) {
			
				// t_name = driver.terminal_name[terminal_index].substr (1, driver.terminal_name[terminal_index].length () );  // remove '.'
				t_name = driver.terminal_name[terminal_index];
				type_name =  *(yysemantic_stack_[(6) - (1)].sval);	
#ifdef __DEBUG__
				cout << " term = " << driver.terminal_name[terminal_index] << endl;
				cout << " signal name = " << driver.terminal[terminal_index] << endl;
				cout << " t_name  = " << t_name << endl;
				cout << " type_name = "<< type_name <<endl;
#endif
				pos = t_input.result[t_name];
				if (pos >  0)  { // input terminal
					
					
					p_gate->input_line_name[pos -1] = driver.terminal[terminal_index];
					p_gate->gate_type = type_name;
					//printf("gate_type = %s \n", type_name.c_str());
#ifdef __DEBUG__
					cout << " input terminal pos = " << pos << ", p_gate->input_line_name[pos -1] = " << p_gate->input_line_name[pos -1] << endl;
#endif
					//PB
					/* if(strcmp(driver.terminal[terminal_index].c_str(), "VCC" ) == 0){
						//p_gate->input_value[pos-1] = 1;
						for (int p = 0 ; p < p_gate->outputs_number ; p++ )
							p_gate->output_value[p] = 0;	
						p_gate->ref_inputs[pos-1].p_gg = NULL;
						p_gate->ref_inputs[pos-1].port = pos-1;
						//RISKIO
						p_gate->set_input(pos-1, 1 );
						//p_gate->propagate_output_during_init = 1;
						
					}
					else
						if(strcmp(driver.terminal[terminal_index].c_str(), "GND" ) == 0 ){
							//p_gate->input_value[pos-1] = 0;
							for (int p = 0 ; p < p_gate->outputs_number ; p++ )
								p_gate->output_value[p] = 0;	
							p_gate->ref_inputs[pos-1].p_gg = NULL;
							p_gate->ref_inputs[pos-1].port = pos-1;
							//RISKIO					
							p_gate->set_input(pos-1, 0 );		
							//p_gate->propagate_output_during_init = 1;
						} */
					//_PB	
					
					input_gate = driver.get_unconnected_gate (driver.terminal[terminal_index].c_str(), &input_pos); 										
					char tmp;

					
					
					if (input_gate != NULL)	 {

#ifdef __DEBUG__
						cout << " input  gate :  " << input_gate->name << " ( " << input_pos << " )  si connette a  " << p_gate->name  << " in pos  " <<  pos-1 << endl;
#endif

						input_gate->add_output (input_pos, p_gate, pos-1);

#ifdef __DEBUG__
						cout << " qui arriva " << input_gate->name << endl;						
#endif
						
	
					}
					else { // one input has yet defined

						
#ifdef __DEBUG__
						cerr << "Not ordered netlist" << endl ;
#endif

						inc_gate.line = driver.terminal[terminal_index]; // put not complete gate in the list 
						inc_gate.ref = p_gate;	
						inc_gate.pos = pos-1;
						driver.incomplete_gate.push_back (inc_gate);
					//PB	
					
					//cout << "terra " << inc_gate.line << " luna " << driver.i_val << " sole " << (inc_gate.line[0] + inc_gate.line[1] + inc_gate.line[inc_gate.line.size()-1] + inc_gate.line[inc_gate.line.size()-2] ) % 263 << endl;
					// driver.incomp_index[inc_gate.line] = driver.i_val ; 			
					_hash_mnemonic_key app = { inc_gate.line, driver.i_val };
					// cout << " # " << app.line << " @ " << app.v_pos << endl;
					driver.incomp_hash[ (inc_gate.line[0] + inc_gate.line[1] + inc_gate.line[inc_gate.line.size()-1] + inc_gate.line[inc_gate.line.size()-2] ) % 263].mylist.push_back(app);
					driver.i_val++;	
					//_PB
					}



				}
				else if ( (pos = t_output.result[t_name]) > 0 )  { // output terminal
				p_gate->gate_type = type_name;
#ifdef __DEBUG__
					cout << " output pos = " << pos << endl;
#endif
					
					if (p_gate->output_line_name == NULL) {
						cerr << " Parser, error when processing  " << p_gate->name << " . output_line_name has no element " << endl;
						exit (-1);
					}
					p_gate->output_line_name[pos-1] = driver.terminal[terminal_index]; // copy the output line name
					// OKKIO	
											
#ifdef __DEBUG__
					cout << " output pos = " << pos << endl;
#endif

					unc_gate.line = driver.terminal[terminal_index]; // put not complete gate in the list 
					unc_gate.ref = p_gate;	
					unc_gate.pos = pos-1;
					driver.unconnected_gate.push_back (unc_gate);
					//PB
					//cout << "pp" ;
					driver.u_val++;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 			

					//_PB

					driver.check_po (p_gate, pos-1); 
					
#ifdef __DEBUG__

					cout << " check inc gate ( " << p_gate->name << " ) terminal " << 	driver.terminal[terminal_index].c_str() << endl;
#endif					
					
					driver.check_incomplete_gate (driver.terminal[terminal_index].c_str(), p_gate, pos-1); // checl if there some incomplete nodes

				}
				else {
					// error or the case of constatn 1 or 0 - the last case is not trated yet


					cerr << "Unknown terminal ( " << t_name << " ) when processing gate ( " << p_gate->name << " ) " <<  endl;
					exit (-1);

				}
				
			}



			
		}
		else {  // whitout notation .T

	/*		// assumin only ine output line in position 0 of the terminal
		
			p_gate->output_line_name[pos-1] = driver.terminal[0]; // copy the output line name
				
			#ifdef __DEBUG__
				cout << " out line  name : " << p_gate->output_line_name[0] << endl;
				cout << " gate  name : " << p_gate->name << endl;
			#endif

			driver.check_po (p_gate,0);
	
			driver.check_incomplete_gate (driver.terminal[0].c_str(), p_gate,0); // checl if there some incomplete nodes




			for (int i = 1; i < driver.terminal.size (); i++) {
				input_gate = driver.get_unconnected_gate (driver.terminal[i].c_str(),i-1);
				
				if (input_gate != NULL)	 {

					// cout << " input  gate :  " << input_gate->name << endl;
					input_gate->add_output (0,p_gate, i-1);


				}
				else { // one input has yet defined
					// cerr << "Not ordered netlist" << endl ; exit (-1);
					// cerr << "Not ordered netlist" << endl ;

					inc_gate.line = driver.terminal[i]; // put not complete gate in the list 
					inc_gate.ref = p_gate;	
					inc_gate.pos = i-1;
					driver.incomplete_gate.push_back (inc_gate);
					//PB
					//cout << "terra " <<  inc_gate.line << " luna " << driver.i_val++ << endl;
					//driver.incomp_index[inc_gate.line] = driver.i_val ; 			
					_hash_menmonic_key app = { inc_gate.line, driver.i_val };
					driver.incomp_hash[ (inc_gate.line[0] + inc_gate.line[1] + inc_gate.line[inc_gate.line.size()-1] + inc_gate.line[inc_gate.line.size()-2]) % 263].push_back(app);
					driver.i_val++;
					//_PB
				}
			}
		
			unc_gate.line = driver.terminal[terminal_index]; // put not complete gate in the list 
			unc_gate.ref = p_gate;	
			unc_gate.pos = pos-1;
			driver.unconnected_gate.push_back (unc_gate);
					//PB
					//cout << "pp" ;
					driver.u_val++;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 			

					//_PB
			driver.circuit->add_generic_gate (p_gate);

			
*/
		}

		
	//}

	  driver.terminal.clear ();	
	  driver.terminal_name.clear ();	

}
    break;

  case 58:

/* Line 678 of lalr1.cc  */
#line 919 "verilog_parser.yy"
    {

		
	/*	driver.terminal.clear ();	
		driver.terminal_name.clear ();	*/
		

		driver.terminal.push_back (*(yysemantic_stack_[(1) - (1)].sval));
		
	   }
    break;

  case 59:

/* Line 678 of lalr1.cc  */
#line 931 "verilog_parser.yy"
    {
			
		
		driver.terminal.push_back (*(yysemantic_stack_[(2) - (2)].sval));

	   }
    break;

  case 60:

/* Line 678 of lalr1.cc  */
#line 940 "verilog_parser.yy"
    {

/* -----\/----- EXCLUDED -----\/-----

		driver.terminal.clear ();	
		driver.terminal_name.clear ();	
 -----/\----- EXCLUDED -----/\----- */
		driver.terminal.push_back (*(yysemantic_stack_[(5) - (4)].sval));    // signal_name
		driver.terminal_name.push_back (*(yysemantic_stack_[(5) - (2)].sval));  // .A
		

	  }
    break;

  case 61:

/* Line 678 of lalr1.cc  */
#line 954 "verilog_parser.yy"
    {
		
		driver.terminal.push_back (*(yysemantic_stack_[(6) - (5)].sval));    // signal_name
		driver.terminal_name.push_back (*(yysemantic_stack_[(6) - (3)].sval));  // .A
	  }
    break;

  case 62:

/* Line 678 of lalr1.cc  */
#line 963 "verilog_parser.yy"
    {


/* -----\/----- EXCLUDED -----\/-----
		driver.terminal.clear ();	
		driver.terminal_name.clear ();	
 -----/\----- EXCLUDED -----/\----- */
		driver.terminal.push_back ("  ");    // signal_name
		driver.terminal_name.push_back (*(yysemantic_stack_[(4) - (2)].sval));  // .A
		

	  }
    break;

  case 63:

/* Line 678 of lalr1.cc  */
#line 977 "verilog_parser.yy"
    {
		
		driver.terminal.push_back (" ");    // signal_name
		driver.terminal_name.push_back (*(yysemantic_stack_[(5) - (3)].sval));  // .A
	  }
    break;

  case 64:

/* Line 678 of lalr1.cc  */
#line 987 "verilog_parser.yy"
    {

		// example  1'b1 1'b0    
		// no control on the parallesim is made		

		driver.terminal.push_back (*(yysemantic_stack_[(5) - (4)].sval));    // signal_name
		driver.terminal_name.push_back (*(yysemantic_stack_[(5) - (2)].sval)); // .A

	  }
    break;

  case 65:

/* Line 678 of lalr1.cc  */
#line 999 "verilog_parser.yy"
    {

		// example  1'b1   or 1'b0    
		// no control on the parallesim is made

		driver.terminal.push_back (*(yysemantic_stack_[(6) - (5)].sval));    // signal_name
		driver.terminal_name.push_back (*(yysemantic_stack_[(6) - (3)].sval));
	  }
    break;

  case 66:

/* Line 678 of lalr1.cc  */
#line 1009 "verilog_parser.yy"
    {	
		char str[50];


		#ifdef __PBDEBUG__
		cout << "Ecco un bus della memoria " << *(yysemantic_stack_[(7) - (2)].sval) << endl ;
		#endif
		for (int j = 0; j < driver.k ; j++){
			sprintf(str,"%s%d", (*(yysemantic_stack_[(7) - (2)].sval)).c_str(),j);			
	    		driver.terminal_name.push_back (str);  // .A
			#ifdef __PBDEBUG__
			cout << "pushpush " << *(yysemantic_stack_[(7) - (2)].sval)<<j << endl ;
			#endif
		}
	   }
    break;

  case 67:

/* Line 678 of lalr1.cc  */
#line 1026 "verilog_parser.yy"
    {
		char str[50];
		#ifdef __PBDEBUG__
		cout << "Ecco un bus della memoria" << *(yysemantic_stack_[(8) - (3)].sval) << endl  ;
		#endif
		for (int j = 0; j < driver.k ; j++){
			sprintf(str,"%s%d", (*(yysemantic_stack_[(8) - (3)].sval)).c_str(),j);		
		    	driver.terminal_name.push_back (str);  // .A
			
			#ifdef __PBDEBUG__
			cout << "pushpush " << str << endl ;
			#endif
		}
	   }
    break;

  case 70:

/* Line 678 of lalr1.cc  */
#line 1050 "verilog_parser.yy"
    {

	  driver.k=0;
	  #ifdef __PBDEBUG__
		cout << "{"  << *(yysemantic_stack_[(1) - (1)].sval)   ;
	  #endif
	  driver.terminal.push_back (*(yysemantic_stack_[(1) - (1)].sval));
	  driver.k++;
	  }
    break;

  case 71:

/* Line 678 of lalr1.cc  */
#line 1061 "verilog_parser.yy"
    {
	  #ifdef __PBDEBUG__
		cout << " , "  << *(yysemantic_stack_[(2) - (2)].sval) << endl  ;
	  #endif
	  driver.terminal.push_back (*(yysemantic_stack_[(2) - (2)].sval));
	  driver.k++;


	  }
    break;



/* Line 678 of lalr1.cc  */
#line 1437 "verilog_parser.cpp"
	default:
          break;
      }
    YY_SYMBOL_PRINT ("-> $$ =", yyr1_[yyn], &yyval, &yyloc);

    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();

    yysemantic_stack_.push (yyval);
    yylocation_stack_.push (yyloc);

    /* Shift the result of the reduction.  */
    yyn = yyr1_[yyn];
    yystate = yypgoto_[yyn - yyntokens_] + yystate_stack_[0];
    if (0 <= yystate && yystate <= yylast_
	&& yycheck_[yystate] == yystate_stack_[0])
      yystate = yytable_[yystate];
    else
      yystate = yydefgoto_[yyn - yyntokens_];
    goto yynewstate;

  /*------------------------------------.
  | yyerrlab -- here on detecting error |
  `------------------------------------*/
  yyerrlab:
    /* If not already recovering from an error, report this error.  */
    if (!yyerrstatus_)
      {
	++yynerrs_;
	error (yylloc, yysyntax_error_ (yystate, yytoken));
      }

    yyerror_range[0] = yylloc;
    if (yyerrstatus_ == 3)
      {
	/* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

	if (yychar <= yyeof_)
	  {
	  /* Return failure if at end of input.  */
	  if (yychar == yyeof_)
	    YYABORT;
	  }
	else
	  {
	    yydestruct_ ("Error: discarding", yytoken, &yylval, &yylloc);
	    yychar = yyempty_;
	  }
      }

    /* Else will try to reuse lookahead token after shifting the error
       token.  */
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;

    yyerror_range[0] = yylocation_stack_[yylen - 1];
    /* Do not reclaim the symbols of the rule which action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    yystate = yystate_stack_[0];
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;	/* Each real token shifted decrements this.  */

    for (;;)
      {
	yyn = yypact_[yystate];
	if (yyn != yypact_ninf_)
	{
	  yyn += yyterror_;
	  if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
	    {
	      yyn = yytable_[yyn];
	      if (0 < yyn)
		break;
	    }
	}

	/* Pop the current state because it cannot handle the error token.  */
	if (yystate_stack_.height () == 1)
	YYABORT;

	yyerror_range[0] = yylocation_stack_[0];
	yydestruct_ ("Error: popping",
		     yystos_[yystate],
		     &yysemantic_stack_[0], &yylocation_stack_[0]);
	yypop_ ();
	yystate = yystate_stack_[0];
	YY_STACK_PRINT ();
      }

    yyerror_range[1] = yylloc;
    // Using YYLLOC is tempting, but would change the location of
    // the lookahead.  YYLOC is available though.
    YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yyloc);

    /* Shift the error token.  */
    YY_SYMBOL_PRINT ("Shifting", yystos_[yyn],
		     &yysemantic_stack_[0], &yylocation_stack_[0]);

    yystate = yyn;
    goto yynewstate;

    /* Accept.  */
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    /* Abort.  */
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (yychar != yyempty_)
      yydestruct_ ("Cleanup: discarding lookahead", yytoken, &yylval, &yylloc);

    /* Do not reclaim the symbols of the rule which action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (yystate_stack_.height () != 1)
      {
	yydestruct_ ("Cleanup: popping",
		   yystos_[yystate_stack_[0]],
		   &yysemantic_stack_[0],
		   &yylocation_stack_[0]);
	yypop_ ();
      }

    return yyresult;
  }

  // Generate an error message.
  std::string
  verilog_parser::yysyntax_error_ (int yystate, int tok)
  {
    std::string res;
    YYUSE (yystate);
#if YYERROR_VERBOSE
    int yyn = yypact_[yystate];
    if (yypact_ninf_ < yyn && yyn <= yylast_)
      {
	/* Start YYX at -YYN if negative to avoid negative indexes in
	   YYCHECK.  */
	int yyxbegin = yyn < 0 ? -yyn : 0;

	/* Stay within bounds of both yycheck and yytname.  */
	int yychecklim = yylast_ - yyn + 1;
	int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
	int count = 0;
	for (int x = yyxbegin; x < yyxend; ++x)
	  if (yycheck_[x + yyn] == x && x != yyterror_)
	    ++count;

	// FIXME: This method of building the message is not compatible
	// with internationalization.  It should work like yacc.c does it.
	// That is, first build a string that looks like this:
	// "syntax error, unexpected %s or %s or %s"
	// Then, invoke YY_ on this string.
	// Finally, use the string as a format to output
	// yytname_[tok], etc.
	// Until this gets fixed, this message appears in English only.
	res = "syntax error, unexpected ";
	res += yytnamerr_ (yytname_[tok]);
	if (count < 5)
	  {
	    count = 0;
	    for (int x = yyxbegin; x < yyxend; ++x)
	      if (yycheck_[x + yyn] == x && x != yyterror_)
		{
		  res += (!count++) ? ", expecting " : " or ";
		  res += yytnamerr_ (yytname_[x]);
		}
	  }
      }
    else
#endif
      res = YY_("syntax error");
    return res;
  }


  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
  const signed char verilog_parser::yypact_ninf_ = -62;
  const signed char
  verilog_parser::yypact_[] =
  {
       -62,    23,   -62,    -1,    -8,   -62,   -62,   -62,   -62,    10,
       7,    27,   -62,   -62,    14,    26,   -62,    -9,    32,    13,
      34,    47,   -62,    33,    38,   -62,    37,   -62,   -62,    42,
     -62,    43,    20,    40,   -62,    48,   -62,   -62,    44,   -62,
      45,   -62,   -62,    50,   -62,    46,    21,    49,    56,   -62,
      51,    52,   -62,    53,   -62,   -62,    54,   -62,    57,    28,
      58,   -62,    36,    55,    59,    61,   -62,    62,   -62,   -62,
      60,   -62,    64,    29,   -62,    63,   -62,    65,    68,    66,
      69,   -62,    70,   -62,   -62,    72,   -62,    71,   -62,    67,
      73,    74,    75,   -62,   -62,   -62,    78,    77,    76,     3,
     -62,    80,    83,   -62,    -7,    82,    85,   -62,   -62,    84,
     -62,    87,   -62,    81,   -62,    86,    -3,    -2,    88,    89,
     -62,   -62,    90,    91,   -62,   -62,   -62,   -62,   -11,   -62,
     -62,   -10,   -62,    92,    93,   -62,    94,   -62,   -62,   -62
  };

  /* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
     doesn't specify something else to do.  Zero means the default is an
     error.  */
  const unsigned char
  verilog_parser::yydefact_[] =
  {
         3,     0,     1,     0,     0,     2,     5,     4,    35,     0,
       0,    33,     6,    40,    46,     9,    34,     0,     0,     0,
      52,    25,    32,    42,     0,    39,     0,    47,    43,     0,
      45,     0,     0,    13,     8,    17,    41,    38,     0,    48,
       0,    53,    49,     0,    51,     0,     0,    29,    37,    24,
       0,     0,    54,     0,    14,    10,     0,    12,     0,     0,
      21,    16,     0,     0,     0,     0,    15,     0,    30,    26,
       0,    28,     0,     0,     7,     0,    36,     0,     0,     0,
       0,    31,     0,    22,    18,     0,    20,     0,    44,     0,
       0,     0,     0,    23,    57,    50,     0,     0,     0,     0,
      11,     0,     0,    58,     0,     0,     0,    56,    27,     0,
      59,     0,    55,     0,    19,     0,     0,     0,     0,     0,
      62,    69,     0,     0,    63,    69,    64,    60,     0,    65,
      61,     0,    70,     0,     0,    68,     0,    71,    66,    67
  };

  /* YYPGOTO[NTERM-NUM].  */
  const signed char
  verilog_parser::yypgoto_[] =
  {
       -62,   -62,   -62,   -62,    99,   -62,   -62,   -62,   -62,   -62,
     -62,   -62,   -62,   -62,   -62,   -62,   -62,   -62,   -62,   -62,
     -62,   -62,   -62,   -62,   -62,   -62,   -62,   -62,   -62,   -62,
     -62,   -62,   -61,   -62
  };

  /* YYDEFGOTO[NTERM-NUM].  */
  const short int
  verilog_parser::yydefgoto_[] =
  {
        -1,     1,     5,     6,     7,    21,    34,    46,    57,    48,
      61,    73,    86,    35,    49,    59,    71,    15,    11,    62,
       8,    17,    25,    16,    19,    30,    22,    32,    44,    76,
      99,   107,   128,   135
  };

  /* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule which
     number is the opposite.  If zero, do what YYDEFACT says.  */
  const signed char verilog_parser::yytable_ninf_ = -1;
  const unsigned char
  verilog_parser::yytable_[] =
  {
       118,   122,   132,   132,    23,    10,   110,     4,   133,   133,
     119,   123,    24,   134,   136,   111,   103,    12,   120,   124,
     121,   125,   104,     2,   105,   106,    27,    13,    28,     3,
      18,     4,    29,    41,    54,    42,    55,    14,    20,    43,
      56,    68,    83,    69,    84,    74,    26,    70,    85,    75,
      31,    33,    36,    37,    38,    39,    45,    40,    50,    47,
      53,    60,    51,    52,   131,    58,    64,    66,    77,    63,
      65,    67,     0,    81,    72,    79,    87,    78,    82,    80,
      88,    89,    95,    91,    90,    93,    96,    92,     0,    98,
     101,    94,    97,   100,   102,   108,   109,   112,   113,   114,
     115,   116,     9,     0,     0,   137,   117,     0,     0,   126,
     127,   129,   130,     0,   138,   139
  };

  /* YYCHECK.  */
  const signed char
  verilog_parser::yycheck_[] =
  {
         3,     3,    13,    13,    13,    13,    13,     8,    19,    19,
      13,    13,    21,    24,    24,    22,    13,     7,    21,    21,
      23,    23,    19,     0,    21,    22,    13,    20,    15,     6,
      16,     8,    19,    13,    13,    15,    15,    10,    12,    19,
      19,    13,    13,    15,    15,     9,    14,    19,    19,    13,
      16,     4,    19,    15,    17,    13,    16,    14,    14,    11,
      14,     5,    17,    13,   125,    16,    14,    13,    13,    18,
      17,    14,    -1,    13,    16,    14,    13,    18,    14,    17,
      15,    13,    15,    14,    18,    13,    13,    17,    -1,    14,
      13,    20,    18,    15,    18,    15,    13,    15,    13,    15,
      13,    20,     3,    -1,    -1,    13,    20,    -1,    -1,    21,
      21,    21,    21,    -1,    21,    21
  };

  /* STOS_[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
  const unsigned char
  verilog_parser::yystos_[] =
  {
         0,    26,     0,     6,     8,    27,    28,    29,    45,    29,
      13,    43,     7,    20,    10,    42,    48,    46,    16,    49,
      12,    30,    51,    13,    21,    47,    14,    13,    15,    19,
      50,    16,    52,     4,    31,    38,    19,    15,    17,    13,
      14,    13,    15,    19,    53,    16,    32,    11,    34,    39,
      14,    17,    13,    14,    13,    15,    19,    33,    16,    40,
       5,    35,    44,    18,    14,    17,    13,    14,    13,    15,
      19,    41,    16,    36,     9,    13,    54,    13,    18,    14,
      17,    13,    14,    13,    15,    19,    37,    13,    15,    13,
      18,    14,    17,    13,    20,    15,    13,    18,    14,    55,
      15,    13,    18,    13,    19,    21,    22,    56,    15,    13,
      13,    22,    15,    13,    15,    13,    20,    20,     3,    13,
      21,    23,     3,    13,    21,    23,    21,    21,    57,    21,
      21,    57,    13,    19,    24,    58,    24,    13,    21,    21
  };

#if YYDEBUG
  /* TOKEN_NUMBER_[YYLEX-NUM] -- Internal symbol number corresponding
     to YYLEX-NUM.  */
  const unsigned short int
  verilog_parser::yytoken_number_[] =
  {
         0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,    59,    91,    58,    93,    44,
      40,    41,    46,   123,   125
  };
#endif

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
  const unsigned char
  verilog_parser::yyr1_[] =
  {
         0,    25,    26,    26,    27,    27,    28,    29,    30,    30,
      31,    31,    32,    32,    33,    33,    34,    34,    35,    35,
      36,    36,    37,    37,    38,    38,    39,    39,    40,    40,
      41,    41,    42,    42,    43,    43,    44,    44,    45,    46,
      46,    47,    47,    48,    48,    49,    49,    50,    50,    51,
      51,    52,    52,    53,    53,    54,    55,    55,    56,    56,
      56,    56,    56,    56,    56,    56,    56,    56,    57,    57,
      58,    58
  };

  /* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
  const unsigned char
  verilog_parser::yyr2_[] =
  {
         0,     2,     2,     0,     1,     1,     3,     8,     2,     0,
       3,     8,     2,     0,     1,     2,     2,     0,     3,     8,
       2,     0,     1,     2,     2,     0,     3,     8,     2,     0,
       1,     2,     2,     0,     2,     0,     2,     0,     6,     2,
       0,     2,     1,     3,     8,     2,     0,     1,     2,     3,
       8,     2,     0,     1,     2,     6,     2,     0,     1,     2,
       5,     6,     4,     5,     5,     6,     7,     8,     2,     0,
       1,     2
  };

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
  /* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
     First, the terminals, then, starting at \a yyntokens_, nonterminals.  */
  const char*
  const verilog_parser::yytname_[] =
  {
    "\"end of file\"", "error", "$undefined", "\"bit\"", "\"inputoutp\"",
  "\"register\"", "\"celldefine\"", "\"endcelldefine\"", "\"module\"",
  "\"end module\"", "\"input\"", "\"wire\"", "\"output\"",
  "\"identifier\"", "\"number\"", "';'", "'['", "':'", "']'", "','", "'('",
  "')'", "'.'", "'{'", "'}'", "$accept", "sentences", "statement", "cells",
  "modules", "inout_lists", "inout_list", "inout_list_id", "inout_id",
  "reg_lists", "reg_list", "reg_list_id", "reg_id", "wire_lists",
  "wire_list", "wire_list_id", "wire_id", "output_lists", "input_lists",
  "gates", "begin_module", "parameter_list", "param", "input_list",
  "pi_list", "pi", "output_list", "po_list", "po", "gate", "terminal_list",
  "terminal", "bus_list", "bus", 0
  };
#endif

#if YYDEBUG
  /* YYRHS -- A `-1'-separated list of the rules' RHS.  */
  const verilog_parser::rhs_number_type
  verilog_parser::yyrhs_[] =
  {
        26,     0,    -1,    26,    27,    -1,    -1,    29,    -1,    28,
      -1,     6,    29,     7,    -1,    45,    43,    42,    30,    38,
      34,    44,     9,    -1,    30,    31,    -1,    -1,     4,    32,
      15,    -1,     4,    16,    14,    17,    14,    18,    13,    15,
      -1,    32,    33,    -1,    -1,    13,    -1,    19,    13,    -1,
      34,    35,    -1,    -1,     5,    36,    15,    -1,     5,    16,
      14,    17,    14,    18,    13,    15,    -1,    36,    37,    -1,
      -1,    13,    -1,    19,    13,    -1,    38,    39,    -1,    -1,
      11,    40,    15,    -1,    11,    16,    14,    17,    14,    18,
      13,    15,    -1,    40,    41,    -1,    -1,    13,    -1,    19,
      13,    -1,    42,    51,    -1,    -1,    43,    48,    -1,    -1,
      44,    54,    -1,    -1,     8,    13,    20,    46,    21,    15,
      -1,    46,    47,    -1,    -1,    13,    19,    -1,    13,    -1,
      10,    49,    15,    -1,    10,    16,    14,    17,    14,    18,
      13,    15,    -1,    49,    50,    -1,    -1,    13,    -1,    19,
      13,    -1,    12,    52,    15,    -1,    12,    16,    14,    17,
      14,    18,    13,    15,    -1,    52,    53,    -1,    -1,    13,
      -1,    19,    13,    -1,    13,    13,    20,    55,    21,    15,
      -1,    55,    56,    -1,    -1,    13,    -1,    19,    13,    -1,
      22,    13,    20,    13,    21,    -1,    19,    22,    13,    20,
      13,    21,    -1,    22,    13,    20,    21,    -1,    19,    22,
      13,    20,    21,    -1,    22,    13,    20,     3,    21,    -1,
      19,    22,    13,    20,     3,    21,    -1,    22,    13,    20,
      23,    57,    24,    21,    -1,    19,    22,    13,    20,    23,
      57,    24,    21,    -1,    57,    58,    -1,    -1,    13,    -1,
      19,    13,    -1
  };

  /* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
     YYRHS.  */
  const unsigned char
  verilog_parser::yyprhs_[] =
  {
         0,     0,     3,     6,     7,     9,    11,    15,    24,    27,
      28,    32,    41,    44,    45,    47,    50,    53,    54,    58,
      67,    70,    71,    73,    76,    79,    80,    84,    93,    96,
      97,    99,   102,   105,   106,   109,   110,   113,   114,   121,
     124,   125,   128,   130,   134,   143,   146,   147,   149,   152,
     156,   165,   168,   169,   171,   174,   181,   184,   185,   187,
     190,   196,   203,   208,   214,   220,   227,   235,   244,   247,
     248,   250
  };

  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
  const unsigned short int
  verilog_parser::yyrline_[] =
  {
         0,    95,    95,    96,    98,    98,   101,   114,   116,   117,
     119,   120,   123,   124,   126,   127,   131,   132,   134,   135,
     138,   139,   141,   142,   147,   148,   150,   151,   154,   155,
     157,   158,   161,   162,   164,   165,   167,   168,   170,   275,
     276,   278,   279,   281,   282,   376,   377,   379,   413,   448,
     449,   528,   529,   531,   557,   583,   910,   911,   918,   930,
     939,   953,   962,   975,   985,   997,  1008,  1025,  1046,  1047,
    1049,  1060
  };

  // Print the state stack on the debug stream.
  void
  verilog_parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (state_stack_type::const_iterator i = yystate_stack_.begin ();
	 i != yystate_stack_.end (); ++i)
      *yycdebug_ << ' ' << *i;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  verilog_parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    /* Print the symbols being reduced, and their result.  */
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
	       << " (line " << yylno << "):" << std::endl;
    /* The symbols being reduced.  */
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
		       yyrhs_[yyprhs_[yyrule] + yyi],
		       &(yysemantic_stack_[(yynrhs) - (yyi + 1)]),
		       &(yylocation_stack_[(yynrhs) - (yyi + 1)]));
  }
#endif // YYDEBUG

  /* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
  verilog_parser::token_number_type
  verilog_parser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
           0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      20,    21,     2,     2,    19,     2,    22,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    17,    15,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    16,     2,    18,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    23,     2,    24,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14
    };
    if ((unsigned int) t <= yyuser_token_number_max_)
      return translate_table[t];
    else
      return yyundef_token_;
  }

  const int verilog_parser::yyeof_ = 0;
  const int verilog_parser::yylast_ = 115;
  const int verilog_parser::yynnts_ = 34;
  const int verilog_parser::yyempty_ = -2;
  const int verilog_parser::yyfinal_ = 2;
  const int verilog_parser::yyterror_ = 1;
  const int verilog_parser::yyerrcode_ = 256;
  const int verilog_parser::yyntokens_ = 25;

  const unsigned int verilog_parser::yyuser_token_number_max_ = 269;
  const verilog_parser::token_number_type verilog_parser::yyundef_token_ = 2;


/* Line 1054 of lalr1.cc  */
#line 1 "[Bison:b4_percent_define_default]"

} // yy

/* Line 1054 of lalr1.cc  */
#line 1976 "verilog_parser.cpp"


/* Line 1056 of lalr1.cc  */
#line 1078 "verilog_parser.yy"


     void
     yy::verilog_parser::error (const yy::verilog_parser::location_type& l,
                               const std::string& m)
     {
       driver.error (l, m);
       exit (-1);
     }



