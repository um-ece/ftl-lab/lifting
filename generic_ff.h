//////////////////////////////////////////////////////////////////////
// generic_ff.h: interface for the generic_ff class.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-06-2007
//////////////////////////////////////////////////////////////////////
#ifndef __generic_ff
#define __generic_ff

#include <string>
#include <iostream>
#include <vector>
#include "generic_gate.h"

class generic_ff : public generic_gate {
public:
	int old_clk;
	int old_data;
//	int last_input_change[MAX_INPUTS];
	int data_port;
//	 int clk_width[MAX_INPUTS][2]; //edge_width[clock]
//	 int hold_time[MAX_INPUTS][MAX_INPUTS][2][2]; //hold_time[data_input][clock_input][data: 1->posedge;0->negedge][clock: 1->posedge;0->negedge]
//	 int setup_time[MAX_INPUTS][MAX_INPUTS][2][2]; //setup_time[data_input][clock_input][data: 1->posedge;0->negedge][clock: 1->posedge;0->negedge]

	generic_ff (const char* name) : generic_gate(name) {
		old_clk=2;
		old_data=2;
#ifdef __LOGIC_LEVELS__
		isFF=1;
#endif
	};
	virtual void load_FF(int)=0;
	virtual void bit_flip_FF()=0;
#ifdef __PROBA_CALCULATION__
	void set_FF_probability (int value, float prob) {
		output_probabilities[0][value]=prob;
		if (outputs_number>1) {
			output_probabilities[1][(value==0?1:(value==1?0:value))]=prob;
		}
	}
#endif
};

#endif

