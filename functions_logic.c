#ifndef __TIMING_SIMULATION__
#include "functions.h"
#include "generic_gate.h"


int read_input_patterns(char* filename)
{
	FILE *f;
	int rowSize=50+(top_level->FF_scan_number>top_level->input_number?top_level->FF_scan_number:top_level->input_number);
	int set_input_number=top_level->input_number; 
	char s[20], p[rowSize];
	int i=0,j,k,qt,type,qto,row_number;

	// Get number of input patterns
	if ((f=fopen(filename,"r"))==NULL) return -1;
	input_patterns_number=0;
	row_number=0;
	while (fgets(p, rowSize, f)) {
		if (strlen(p)>2 ) {
			row_number++;
			if ((p[0]=='P' && p[1]=='I') || (p[0]=='S' && p[1]=='C')) {
				input_patterns_number++;
			}
		}
	}
	fclose (f);


	input_patterns = (int **) malloc (input_patterns_number * sizeof(int*));
	output_patterns = (int **) malloc (input_patterns_number * sizeof(int*));
	golden_output_patterns = (int **) malloc (input_patterns_number * sizeof(int*));

	if ((f=fopen(filename,"r"))==NULL) return -1;
	for (i=0; i<row_number; i++) {
		fscanf (f, "%s %s", s, p);
		if (strcmp(s,"PI")==0) {
			// It's a primary input
			qt=top_level->input_number;
			qto=top_level->output_number;
			type=APPLY_PRIMARY_INPUT;
			
		} else {
			// It's a scan chain
			qt=top_level->FF_scan_number;
			qto=top_level->FF_scan_number;
			type=LOAD_SCAN_CHAIN;
			
		}
		
		input_patterns[i]=(int*) malloc ((1+qt) * sizeof(int));
		output_patterns[i]=(int*) malloc ((1+qto) * sizeof(int));
		golden_output_patterns[i]=(int*) malloc (qto * sizeof(int));
		input_patterns[i][0]=type;
		for (j=0; j<qt; j++) {
			if (p[j]=='0') {
				input_patterns[i][j+1]=0;
			} else if (p[j]=='1') {
				input_patterns[i][j+1]=1;
			} else {
				input_patterns[i][j+1]=2;
			}
		}
	}
}
void print_output()
{
	int i,j,qt1,qt2;
	char c;
	FILE *f;

	f=fopen (param_output_filename, "w");
	if (f==NULL) {
		printf ("\nERROR: cannot open output files for writing. Output aborted.\n");
		return;
	}

	for (i=0; i<input_patterns_number; i++) {
		if (input_patterns[i][0]==APPLY_PRIMARY_INPUT) {
			qt1=top_level->input_number;
			qt2=top_level->output_number;
			fprintf (f, "PO ");
		} else {
			qt1=top_level->FF_scan_number;
			qt2=top_level->FF_scan_number;
			fprintf (f, "SC ");
		}
		for (j=0; j<qt1; j++) {
			switch (input_patterns[i][j+1]) {
				case 0: c='0'; break;
				case 1: c='1'; break;
				case 2: c='x'; break;
				case 3: c='Z'; break;
			}
			fprintf (f, "%c",c);
		}
		fprintf (f, " -> ");
		for (j=0; j<qt2; j++) {
			switch (output_patterns[i][j]) {
				case 0: c='0'; break;
				case 1: c='1'; break;
				case 2: c='x'; break;
				case 3: c='Z'; break;
				//case 4: c='s'; break;
				//case 5: c='h'; break;	
			}
			fprintf (f, "%c",c);
		}
		
		fprintf (f, "\n");
	}
	fclose (f);
}


void logic_simulation(int printFlag, int executeInit)
{
	int i,j,k,l,m,n,pre_rising=0;
	int flag_fallow_fault=0;

	if (executeInit==1) {
		top_level->init();
	}

	if (printFlag==1) {
		cout << endl << "Running simulation of " << input_patterns_number  << " patterns.. \n";
		fflush(stdout);
	}

	// LOGIC SIMULATION
	for (i=0; i<input_patterns_number; i++) {
#ifdef __DEBUG__
		cout << endl << "Applying Pattern (" << i+1 << "/" << input_patterns_number << ") --> ";
#endif
		if (input_patterns[i][0]==APPLY_PRIMARY_INPUT) {      
#ifdef __DEBUG__
			for (j=0; j<top_level->input_number; j++) {
				cout  << top_level->list_PI[j]->name << " = " << input_patterns[i][j+1] << " ; " ;
			}
			printf("\n");
#endif

			//top_level->propagate_output_FF();

			// Set primary input values
			for (j=0; j<top_level->input_number; j++) {	
				top_level->set_input (j, input_patterns[i][j+1]);
			}


			//PB - moved early before set primary input values
			// Clock cycle
			top_level->propagate_output_FF();
			//_PB

			// Bit flip (if bitflip_enable==1)
			if (bitflip_enable==1) {
				if (i==bitflip_time) {
					bitflip_location->bit_flip_FF();
				}
			}
			// Read primary output
			for (j=0; j<top_level->output_number; j++) {
				output_patterns[i][j]=top_level->read_output(j);
			}
		} else {
			// Read scan chain
			for (j=0; j<top_level->FF_scan_number; j++) {
				output_patterns[i][j]=top_level->list_FF_scan[j]->read_output(0);
			}
			// Load new scan chain
			for (j=0; j<top_level->FF_scan_number; j++) {
				top_level->list_FF_scan[j]->load_FF(input_patterns[i][j+2]);
			}
		}
	}
	if (printFlag==1) {
		printf ("done! \n");
	}

}



#endif

