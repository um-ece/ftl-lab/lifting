//////////////////////////////////////////////////////////////////////
// generic_memory.h: interface for the generic_ff class.
//
// Author : Alberto BOSIO, Paolo BERNARDI
//
// Last Update : 11-06-2007
//////////////////////////////////////////////////////////////////////
#ifndef __generic_memory
#define __generic_memory

#include <string>
#include <iostream>
#include <vector>
#include "generic_gate.h"

class generic_memory : public generic_gate {
public:

        // attributi della memoria
 
        int data_width;
        int add_width;
	int add_log_width;

	int old_clk;

        generic_memory (const char* name) : generic_gate(name) {
	  	old_clk=-1;		
	};

	virtual int load_memory(const char* namefile)=0;
	virtual void write_content_2_video()=0;
	
};

#endif

