############### Makefile ################
# The following defines can be used:
# __TIMING_SIMULATION__ to enable tlifting
# __SLACK_CALCULATION__ to enable the function that calculate the available slack time for each gate (it requires __TIMING_SIMULATION__)
# __PROBA_CALCULATION__ to enable the functions that calculate the probability of each net to have 0 or 1 (it requires not to have __TIMING_SIMULATION__)
# __LOGIC_LEVELS__ to enable the functions the calculate the depth of each net
# __DEBUG__ to enable debug messages
# __DEBUG3__ to enable other debug messages
# __DEBUG_PROBA_1__ to enable PROBA debug messages
# __DEBUG_PROBA_2__ to enable more PROBA debug messages
# __DEBUG_SLACK__ to enable SLACK debug messages
# __DEBUG_SLACK_2__ to enable more SLACK debug messages
# __DEBUG_LOGICLEVEL_1__ to enable logic level messages

CC= g++ -Wno-write-strings -D __TIMING_SIMULATION__ 
RM= rm
BISON= /usr/bin/bison

LIB= ./c35
CLIB_C35= ./techlib/c35/
PARSER=./verilog-parser/


# verilog_parser.cpp :  verilog_parser.yy
#	$(BISON) --verbose -d -o verilog_parser.cpp verilog_parser.yy

# verilog_scanner.c : verilog_scanner.flex
#	flex -overilog_scanner.c verilog_scanner.flex

verilog_driver.o : verilog_driver.cpp verilog_driver.h verilog_parser.cpp 
	$(CC) -c -g verilog_driver.cpp

verilog_parser.o : verilog_parser.cpp verilog_parser.hpp verilog_scanner.c
	$(CC) -c -g verilog_parser.cpp

verilog_scanner.o : 	verilog_scanner.c 
	$(CC) -c -g verilog_scanner.c

c35_comb.o : $(CLIB_C35)c35_comb.cpp $(CLIB_C35)c35_comb.h $(CLIB_C35)c35_library.h
	$(CC) -c -g $(CLIB_C35)c35_comb.cpp

c35_seq.o : $(CLIB_C35)c35_seq.cpp $(CLIB_C35)c35_seq.h $(CLIB_C35)c35_library.h
	$(CC) -c -g $(CLIB_C35)c35_seq.cpp

generic_gate.o : generic_gate.cpp generic_gate.h
	$(CC) -c -g generic_gate.cpp

generic_cell.o : generic_cell.cpp generic_cell.h generic_gate.h
	$(CC) -c -g generic_cell.cpp

generic_fault.o : generic_fault.cpp generic_fault.h
	$(CC) -c -g generic_fault.cpp

input_pattern.o : input_pattern.cpp input_pattern.h
	$(CC) -c -g input_pattern.cpp

functions_timing.o : functions_timing.cpp functions.h
	$(CC) -c -g functions_timing.cpp

functions_logic.o : functions_logic.c functions.h
	$(CC) -c -g functions_logic.c

functions.o : functions.c functions.h
	$(CC) -c -g functions.c

read_timing.o : read_timing.cpp
	$(CC) -c -g read_timing.cpp
def_parser.o : def_parser.c
	$(CC) -c -g def_parser.c	

globalVariables.o : globalVariables.c
	$(CC) -c -g globalVariables.c

manageFaults.o : manageFaults.c stuckat_fault.h
	$(CC) -c -g manageFaults.c

main.o : main.cpp stuckat_fault.h globalVariables.c 
	$(CC) -c -g main.cpp

# FOR TIMING:
OBJtiming=verilog_driver.o verilog_parser.o verilog_scanner.o c35_comb.o c35_seq.o generic_cell.o generic_gate.o input_pattern.o generic_fault.o functions.o functions_timing.o globalVariables.o manageFaults.o read_timing.o def_parser.o main.o

dir: 
	   $(RM) ./techlib/currentlib
	   ln -s $(LIB) ./techlib/currentlib 

tlifting:  $(OBJtiming)
	   $(CC) $(OBJtiming) -o tlifting

clean:
	$(RM) *.o >> /dev/null
	$(RM) *~ >> /dev/null

install:
	sudo cp tlifting /usr/local/bin/


#--------------------------------------#
