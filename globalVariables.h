#ifndef __GLOBAL_VARIABLES_
#define __GLOBAL_VARIABLES_

#include "stuckat_fault.h"
#include "generic_cell.h"
#include "generic_ff.h"
#include "simulation_event.h"

#include <map>
#include <string>
#include <queue>

class generic_cell;
class generic_gate;
class generic_ff;


extern generic_cell* top_level;

extern int** input_patterns;
extern int** output_patterns;
extern int** golden_output_patterns;
extern int input_patterns_number;

extern int detected_faults;

extern int bitflip_enable;
extern int bitflip_time;
extern generic_ff* bitflip_location;

extern char *faultlist_coverage;
extern int faultlist_number;
extern int use_external_faultlist;

extern stuckat_fault** stuckat_faultlist;


extern char param_netlist_filename[1024];
extern char param_input_filename[1024];
extern char param_output_filename[1024];
extern int param_fault_simulation;
//extern int param_force_sequential_algorithm;
extern int param_generate_full_fault_dictionary;
extern int param_write_fault_statistic;
extern char param_fault_statistic_filename[1024];
extern int param_write_truth_fault_table;
extern FILE *param_truth_fault_table_file;
extern int param_bitflip_mintime;
extern int param_bitflip_experiments;
extern int param_saf_observe_last_input_only;
extern char param_faultlist_filename[1024];
extern int param_print_fs_result;
extern char param_print_fs_result_dir[1024];
extern map<string, int> external_faultlist;
extern int param_library;
extern int param_use_errorDetection;
extern char constant_fault_statistic[1000];
extern int param_write_fault_list;
extern char param_fault_list_filename[1024];
extern char param_timing_filename[1000];
extern char param_def_filename[1000];
extern int timing_type;
extern int timing_factor;
extern int set_input_no;
extern int set_clock_no;
extern int clock_no;
//extern char set_input_name[1024];
extern int end_time;
extern int clock_flag;
extern int set_input_exist; 
#ifdef __SLACK_CALCULATION__
extern int param_slack_time_calculation;
#endif
#ifdef __PROBA_CALCULATION__
extern int param_proba_calculation;
extern int param_proba_calculation_max_ttsize;
extern int max_logic_level_toSimulate;
#ifdef __DEBUG_PROBA_1__
extern long double proba[10];
extern long double proba_moins1;
extern long double proba_moins01;
extern long double proba_moins001;
#endif
#endif
#ifdef __LOGIC_LEVELS__
extern int max_levels;
#endif

extern int faultCombinations_number;
extern int max_faultCombinations;
extern vector<int> *faultCombinations;
extern vector<int> *faultBegin;
extern vector<int> *faultEnd;

// This variable is used for double stuck-at fault simulation
extern string fault_to_exclude;

// AB 
extern int param_passfail_dictionary; //default 1, the created dictionary is passfail. if 0 then the full dictionary is created


//PB
extern char memory_name[30][1024];
extern char memory_file[30][1024];
extern int memory_number;
//_PB

#ifdef __TIMING_SIMULATION__
// VARIABLES FOR TIMING SIMULATION
extern char fault_output_filename[1024];
extern char waveform_output_filename[1024];
extern char laser_fault_filename[1024];
extern char w_laser_fault_filename[1024];
extern char hspice_libname[1024];
extern char flag_hspice_lib;
extern char hspice_output_filename[1024];
extern priority_queue <simulation_event> timing_queue;
extern int current_time;
//extern int gate_number;
extern vector <string> list_subgate;
extern vector <string> temp_list_subgate;
extern vector <string> list_input_netname;
extern vector <string> list_waveform_netname;
extern vector <string> netlist_cap_name;
extern vector <float> netlist_cap_value;
extern vector <string> netlist_cap_netname;
extern int subgate_number;
extern int fault_start;
extern int fault_end;
extern int Hspice_wholetime_sim_flag;
extern int flag_force_sim_time;
extern FILE* output_hspice_file;
extern FILE* output_laser_list;
extern char tcl_command[1024];
extern int flag_hspice;
extern int flag_wf;
extern int flag_wf_all;
extern int flag_laser_fault,flag_hspice_output;
extern int simulation_start, simulation_end;
extern int no_nett;
extern int hspice_level;
extern int flag_read_def;
extern int flag_write_laser_fault;
// Laser's parameters
extern int laser_center[2];
extern int laser_radius;
extern int laser_start;
extern int laser_end;
extern float laser_power;
#endif

#endif
