//////////////////////////////////////////////////////////////////////
// generic_cell.h: interface for the generic_cell class.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-06-2007
//////////////////////////////////////////////////////////////////////
#ifndef __generic_cell
#define __generic_cell

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include "generic_gate.h"
#include "generic_ff.h"
//PB
#include "generic_memory.h"
//_PB
#include "techlib/common/pi.h"
#include "techlib/common/po.h"

using namespace std;

class generic_gate;

/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = -1
 * Z = -2
 * ...
*/

class generic_cell {

public:

	// INFORMATION
	char name[255]; // module name
	int celldefine;  // 1 means that current modules is a library cell, 0 otherwise

	vector <pi *> list_PI;
	vector <po *> list_PO;   // in case of basic gate we have only one PO

	vector <generic_ff*> list_FF_noscan;   // seq elements
	vector <generic_ff*> list_FF_scan;   // seq elements
	vector <generic_gate*> list_logic_gate; // comb elements
	//PB
	vector <generic_memory*> list_memory; // memory elements	
	vector <generic_gate*> list_const; // list of reference to cell, if size of list_cell equal to 0 then the current cell is a leaf
	//PB
	vector <generic_cell*> list_cell; // list of reference to cell, if size of list_cell equal to 0 then the current cell is a leaf

	int input_number;
	int output_number;
	int FF_scan_number;
	int FF_noscan_number;
	int logic_gate_number;
	int cell_number;

	map<string,generic_gate*> NameToPointer;
#ifdef __TIMING_SIMULATION__
	map<string,generic_gate*>::iterator iter;
	int gate_number;
	vector <string> list_gate;
#endif
	//PB
	int const_number;
	int memory_number;
	  //_PB
	int no_fault_list;
	// GDN: faults_number bisogna eliminarlo. Ci sara' solo piu' faultsites_number
	int faults_number;
	int faultsites_number;


	generic_cell (const char *pname, int no_fl);

	virtual ~generic_cell () {};

	void init() ;

	// *******
	//
	// methods
	
	void add_input (pi* primary_input) ;
	void set_input (int pi_index, int value) ;	
	void add_output (po* primary_output) ;

	void add_FF_scan (generic_ff* FF) ;
	void add_FF_noscan (generic_ff* FF) ;
	void add_generic_gate (generic_gate* gate) ;
	void add_generic_cell (generic_cell* cell) ;

	generic_gate* getGenericGate_fromName (char* name);
	//generic_ff* getGenericFF_fromName (char* name);

	//PB
	void add_const (generic_gate* gate) ;
	void add_memory (generic_memory* mem) ;
	//_PB

	int read_output (int po_index) ;
	int read_input (int pi_index) ;

	void pulse_clock () ;
	void propagate_output_FF() ;
        void print (ostream &os) ;

	void getFF_status(char* s);
	void setFF_status(char* s);

#ifdef __PROBA_CALCULATION__	
	void calculate_probabilities ();
	void calculate_support_sets ();
	int input_support_set_overall_done;
#endif
#ifdef __SLACK_CALCULATION__
	void calculate_asap (generic_gate* gate);
	void calculate_alap (generic_gate* gate);
	void calculate_slack_time (generic_gate* gate);
#endif
#ifdef __LOGIC_LEVELS__
	// It calculates the level of this gate, it propagates to next gates, and it returns the maximum found level
	int calculate_logic_level();
	int max_levels;
	vector<vector<generic_gate*> > gates_basedon_logic_levels;
#endif

};


#endif

