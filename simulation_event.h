#ifndef __SIMULATION_EVENT_H__
#define __SIMULATION_EVENT_H__

#ifdef __TIMING_SIMULATION__

#include "generic_gate.h"

using namespace std;


class simulation_event
{
public:
/*
Event type can be one of the following:
- 0 = propagation of a value to a port of the circuit
- 1 = primary input value
- 2 = inject input stuck at
- 3 = release input stuck at
- 4 = inject output stuck at
- 5 = release output stuck at
- ...
	int	event_type;
*/
	
	int 	 event_type; //0 inject stuck at; 1 release stuck at
	int 	 flag_fault;
	int		 flag_clk,flag_input;
	int		 fault_index; //index of fault for stuckat_faultlist[]
	int      port, propagate_output_flag;  
	int      time,temp_input_port;
	int 	 isPI, PIvalue,CLKvalue;
	generic_gate* g;
 	 simulation_event() {
	event_type = 0; 
	flag_fault = 0;
	flag_clk =0;
	fault_index =0; //index of fault for stuckat_faultlist[]
	port = 0;
	propagate_output_flag = 0;  
	time =0;
	isPI =0;
	PIvalue = 2;
	CLKvalue =2;
	generic_gate* g = NULL;
	};
	
	friend bool operator < (const simulation_event& event1, const simulation_event& event2) {
		if (event1.time == event2.time)
		{	if(event1.flag_fault == event2.flag_fault) {
				if (event1.flag_clk == event2.flag_clk) {
					if(event1.propagate_output_flag == event2.propagate_output_flag){
						return event1.isPI < event2.isPI;
					}
					else {
						return event1.propagate_output_flag > event2.propagate_output_flag;
					}
				} else {
					return event1.flag_clk < event2.flag_clk;
				}
			}else {
				return event1.flag_fault > event2.flag_fault;
			}	
		} else {
			return event1.time > event2.time;
		}
	};
/*	friend bool operator < (const simulation_event& event1, const simulation_event& event2) {
		if (event1.time == event2.time)
		{ 
			if (event1.flag_clk == event2.flag_clk) {
			return event1.propagate_output_flag > event2.propagate_output_flag;
			} else {
			return event1.flag_clk < event2.flag_clk;
			}
		} else {
			return event1.time > event2.time;
		}
	};*/
	/*friend bool operator < (const simulation_event& event1, const simulation_event& event2) {
		if (event1.time == event2.time)
		{
			return event1.propagate_output_flag > event2.propagate_output_flag;
		} else {
			return event1.time > event2.time;
		}
	};*/
};

	

#endif

#endif

