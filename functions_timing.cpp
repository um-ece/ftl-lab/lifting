#ifdef __TIMING_SIMULATION__
#include <queue>
#include "functions.h"
#include "simulation_event.h"
#include "generic_gate.h"

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <iomanip> //---------25/10/2011------lu
using namespace std;

void timing_simulation(int printFlag, int executeInit)
{
	int i,j,k,l,m,n,pre_rising=0;
	int flag_fallow_fault=0;
	int flag_is_output=0;
	
	simulation_event event , event_temp;
	char* prename;
	ofstream l_fout; //
	ofstream wf_fout;
	generic_gate* test_g;//test_g for print Stimulus for each gate; temp_g for output Waveform
	generic_gate* temp_g;//temp_g for output Waveform
	generic_gate* temp_wf_g;
	l_fout.open(fault_output_filename);	//----25/10/2011----lu
	wf_fout.open(waveform_output_filename);
	output_patterns[0][0]= 0;

	if (executeInit==1) {
		top_level->init();
	}

	if (printFlag==1) {
		cout << endl << "Running simulation of " << input_patterns_number  << " patterns.. \n";
		cout << "fault_free_simulation_result: ";
		fflush(stdout);
	}

	for (i=0; i<input_patterns_number; i++) {
		for (j=0; j<top_level->input_number; j++) {
			event.time = input_patterns[i][1];
			event.g = top_level->list_PI[j];
			event.propagate_output_flag=1;
			event.flag_fault = 0;
			event.flag_clk = 0;
			event.port=0;
//			event.isPI=1;
			event.isPI=1+j;
			event.PIvalue=input_patterns[i][j+2];
			timing_queue.push (event);
		}
	}
// for output Waveform	
	wf_fout<<"t`s ";
	for (j=0; j<top_level->input_number; j++) {			
			temp_g = top_level->list_PI[j];
			wf_fout<<"in("<<temp_g->name<<")`digital`logic_4 ";			
		}
	for (j=0; j<top_level->output_number; j++) {			
			temp_g = top_level->list_PO[j];
			wf_fout<<"out("<<temp_g->name<<")`digital`logic_4 ";			
		}
	if(flag_wf_all==1) {
		for(l=0;l<top_level->gate_number;l++) {
					test_g = top_level->NameToPointer[top_level->list_gate[l]];
					for(m=0;m < test_g->outputs_number;m++) {
						flag_is_output=0;
						for(n=0;n<top_level->output_number;n++) {
							temp_wf_g = top_level->list_PO[n];
							if(strcmp(temp_wf_g->name,test_g->output_line_name[m].c_str())==0) flag_is_output=1;
						}
						if(flag_is_output !=1 && test_g->output_line_name[m]!="")
							//wf_fout<<"out("<<test_g->output_line_name[m]<<")`digital`logic_4 "<<"["<<test_g->name<<"->"<<"output("<<m<<")]"<<"\n" ;
							wf_fout<<"out("<<test_g->output_line_name[m]<<")`digital`logic_4 ";				
					}	
		}
	}
	wf_fout<<"\n";
	
	current_time=0;
	// We use i as index for output_patterns
	// i is updated when new primary inputs arrive
	i=0;
	

	while (!timing_queue.empty()) {

#ifdef __DEBUG__
//		printTimingQueue();
#endif
		
		prename = (event.g)->name;
		event = timing_queue.top();
		event_temp = event;
		/*if (13500<=event.time && event.time<=14000) {
		cout << current_time;
		printTimingQueue();
		}*/
		timing_queue.pop();

//-----------------------------clk_event------------------------------
		if (current_time!=event.time || event.flag_fault==1||flag_fallow_fault == 1) {
			flag_fallow_fault = 0;
			top_level->propagate_output_FF();
		//	cout << "TIME (" <<setw(5)<< current_time << ") :";
					//---------------25/10/2011-------------lu
			l_fout << "TIME ("<<setw(5)<< current_time << ") :";
			l_fout << "\t PI  ";
			//wf_fout<<current_time<<"E-12 "<< "event_time="<<event.time<<" ";
			wf_fout<<current_time<<"E-12 ";
		//	cout <<"inputs are: ";
			for	(k=0; k<top_level->input_number;k++) {
		//		cout << top_level->read_input(k);
				l_fout << top_level->read_input(k);
				if(top_level->read_input(k)>1) wf_fout <<"X ";
				else wf_fout << top_level->read_input(k)<<" ";
				}
		//	cout << " --> ";	
			l_fout << " -->  PO  ";
			//-----------------end of 25/10/2011-------------lu	
		//		printf("output is ");	
			
			// Read primary output
			for (j=0; j<top_level->output_number; j++) {
#ifdef __DEBUG__

				cout << top_level->read_output(j);
#endif				
				l_fout << top_level->read_output(j);
				if(top_level->read_output(j)>1) wf_fout <<"X ";
				else wf_fout << top_level->read_output(j)<<" ";


				if (event.isPI>0 && current_time>=0 ) {
					output_patterns[i][0]=current_time; //--25/10/2011--------lu
					output_patterns[i][j+1]=top_level->read_output(j);
					
				//printf("output[%d][0]= %d ; value = %d\n",i,current_time,output_patterns[i][j+1]);
				
				}
			}
//***********************print waveform *************************		
			if(flag_wf==1&&flag_wf_all==1) {
				for(l=0;l<top_level->gate_number;l++) {
					test_g = top_level->NameToPointer[top_level->list_gate[l]];
					for(m=0;m < test_g->outputs_number;m++) {
						flag_is_output=0;
						for(n=0;n<top_level->output_number;n++) {
							temp_wf_g = top_level->list_PO[n];
							if(strcmp(temp_wf_g->name,test_g->output_line_name[m].c_str())==0) flag_is_output=1;
						}
						//if(event.flag_fault != 1 )
						test_g->wf_output_value[m]=test_g->read_output(m);
						if(flag_is_output !=1 && test_g->output_line_name[m]!=""){
								if(test_g->wf_output_value[m]>1) wf_fout <<"X ";
								else wf_fout << test_g->wf_output_value[m]<<" ";	
#ifdef __DEBUG__
//							if(test_g->wf_output_value[m]>1) wf_fout <<"X("<<test_g->output_value[m]<<") ";
//							else wf_fout << test_g->wf_output_value[m]<<"("<<test_g->output_value[m]<<") ";
//							printf("\n*********************** output %d of %s at %d ps is %d   current_time= %d wf_output_value= %d\n",m,test_g -> name,event.time,test_g->read_output(m),current_time,test_g->wf_output_value[m]); 
#endif					
						}
					
					}	
				}
			}
			wf_fout<<"\n";
//			wf_fout<<"fault "<<event.flag_fault<<" "<<event.time<<"\n";
//******************* end print waveform**********************
		//	cout << "\t(" <<(event.g)->name <<") " ;
			l_fout << "\t(" <<prename <<") \n" ;
		 
		//	cout << endl;
			current_time=event.time;	
			if (event.isPI>0 && current_time>=0 ) {
//				cout << current_time<<" ---> i = "<<i<<endl;
				i++;
				
			}
			
			if(event.flag_fault==1) {
				flag_fallow_fault = 1;
				printFlag=1;
				current_time=event.time;
				if(event.event_type == 1) {
				//	cout << "TIME (" <<setw(5)<< current_time << ") :";
					l_fout << current_time<<"------- No."<<event.fault_index<<" fault released -----------"<<event.time<<"\n";
				//	cout << current_time<<"------- No."<<event.fault_index<<" fault released -----------"<<event.time<<"\n";
					stuckat_faultlist[event.fault_index] -> releaseFault();
				} 
				if(event.event_type == 0) {
				//	cout << "TIME (" <<setw(5)<< current_time << ") :";
					l_fout << current_time<<"------- No."<<event.fault_index<<" fault injected -----------"<<event.time<<"\n";
				//	cout << current_time<<"------- No."<<event.fault_index<<" fault injected -----------"<<event.time<<"\n";
					stuckat_faultlist[event.fault_index] -> injectFault();
				}
				
			}

		} 
		
		
		
		if (event_temp.isPI>0) {
			(event_temp.g)->set_input(0, event_temp.PIvalue);
		} else {
			(event_temp.g)->calculate_propagate_output (event_temp.port);
		}
		
	}
	// Read primary output for the last input
	for (j=0; j<top_level->output_number; j++) {
		output_patterns[i][0]=current_time; //--25/10/2011--------lu
		output_patterns[i][j+1]=top_level->read_output(j);
	}
	
	if (printFlag==1) {
//----------------18/10/2011-------------LU------
		top_level->propagate_output_FF();
	//	cout << "TIME (" <<setw(5)<< current_time << ") :";
		l_fout << "TIME ("<<setw(5)<< current_time << ") :";
		wf_fout<<current_time<<"E-12 ";
		l_fout << "\t PI  ";
	//	cout << "inputs are: ";
		// Read primary output
		for	(k=0; k<top_level->input_number;k++) {
	//		cout << top_level->read_input(k);
			l_fout << top_level->read_input(k);
			if(top_level->read_input(k)>1) wf_fout <<"X ";
			else wf_fout << top_level->read_input(k)<<" ";
		}
	//	cout << " --> ";	
		l_fout << " -->  PO  ";
	//	printf("output is ");
		for (j=0; j<top_level->output_number; j++) {

			cout << top_level->read_output(j);
			l_fout << top_level->read_output(j);
			if(top_level->read_output(j)>1) wf_fout <<"X ";
			else wf_fout << top_level->read_output(j)<<" ";

			//output_patterns[i][j]=top_level->read_output(j);
		}
		if(flag_wf==1&&flag_wf_all==1) {
			for(l=0;l<top_level->gate_number;l++) {
				test_g = top_level->NameToPointer[top_level->list_gate[l]];
				for(m=0;m < test_g->outputs_number;m++) {
					flag_is_output=0;
					for(n=0;n<top_level->output_number;n++) {
						temp_wf_g = top_level->list_PO[n];
						if(strcmp(temp_wf_g->name,test_g->output_line_name[m].c_str())==0) flag_is_output=1;
					}
					test_g->wf_output_value[m]=test_g->read_output(m);
					if(flag_is_output !=1 && test_g->output_line_name[m]!=""){
						if(test_g->wf_output_value[m]>1) wf_fout <<"X ";
						else wf_fout << test_g->wf_output_value[m]<<" ";
//						else wf_fout << test_g->wf_output_value[m]<<"+"<<test_g->output_value[m]<<" ";
#ifdef __DEBUG__						
//						printf("\n*********************** output %d of %s at %d ps is %d   current_time= %d wf_output_value= %d\n",m,test_g -> name,event.time,test_g->read_output(m),current_time,test_g->wf_output_value[m]); 
#endif					
					}
					
				}	
			}
		}
		l_fout << "\t("<<prename <<") \n" ;
		wf_fout<<"\n";
	//	cout << "\t("<<prename <<") \n" ;
//----------------18/10/2011-------------LU------
		
			cout << endl;
			printf ("done! \n");
	}
//**************write hspice file****************************	

if(flag_hspice==0 && flag_laser_fault==1&&flag_hspice_output==1){	
	
	output_hspice_file=fopen(hspice_output_filename,"w");	
	list_input_netname.clear();
fprintf(output_hspice_file,"** subcircuit with faults \n");
// calculate Hspice simulation duration  2013/03/06
	if (clock_flag == 1) {
		for(i=1; i<input_patterns_number;i++) {
			if (input_patterns[i][clock_no+2]==1 && input_patterns[i-1][clock_no+2]==0) {
				if (pre_rising <= fault_start && fault_start <= input_patterns[i][1]) {
					simulation_start = pre_rising;
					printf("simulation_start = %d\n",pre_rising);
				}
				if (pre_rising <= fault_end && fault_end <= input_patterns[i][1]) {
					simulation_end = input_patterns[i][1];
					printf("simulation_end = %d\n",input_patterns[i][1]);
				}
				pre_rising = input_patterns[i][1];
			}
//			printf(" AT time = %d; clock = %d\n",input_patterns[i][1],input_patterns[i][clock_no+2]);
		}
	} else{
		simulation_start = fault_start;
		simulation_end = 2 * fault_end - fault_start;
	}	
	if (flag_force_sim_time==1){
	//	simulation_start = fault_start;
		simulation_end = fault_end+2000 ;
	}
if (Hspice_wholetime_sim_flag==1) {
	fprintf(output_hspice_file,".TRAN 1ps %dps START=0ps \n",end_time);
} else {
	fprintf(output_hspice_file,".TRAN 1ps %dps START=%dps \n",simulation_end, simulation_start);
}
#ifdef __DEBUG__
printf("list_input_netname.size = %d\n",list_input_netname.size());
#endif	
for(i=0;i<list_subgate.size();i++) {	
		test_g = top_level->NameToPointer[list_subgate[i]];
		//test_g->print_inputs_sp(output_hspice_file);
		//test_g->print_netlist_sp(output_hspice_file);
		test_g->print_output_sp(output_hspice_file);

	}
fprintf(output_hspice_file,"\n VDD vdd! 0 3.3v \n");
for(i=0;i<list_subgate.size();i++) {	
		test_g = top_level->NameToPointer[list_subgate[i]];
		test_g->print_inputs_sp(output_hspice_file);
		//test_g->print_output_sp(output_hspice_file);

	}
fprintf(output_hspice_file,"\n");
for(i=0;i<list_subgate.size();i++) {	
		test_g = top_level->NameToPointer[list_subgate[i]];
		//test_g->print_inputs_sp(output_hspice_file);
		test_g->print_netlist_sp(output_hspice_file);
		//test_g->print_output_sp(output_hspice_file);

	}
// print capacitance netlist	
for(j=0;j<netlist_cap_name.size();j++){
	flag_is_output=0;
	for(i=0;i<top_level->output_number;i++){
		if(strcmp((top_level->list_PO[i])->name,netlist_cap_name[j].c_str())==0) flag_is_output=1;
	}	
		if(flag_is_output!=1&&netlist_cap_value[j]!=0) 
		fprintf(output_hspice_file,"c%s %s 0 %.3ff\n",netlist_cap_name[j].c_str(),netlist_cap_netname[j].c_str(),netlist_cap_value[j]);
}	
//fprintf(output_hspice_file,"\n .END \n");	

fclose (output_hspice_file);
flag_hspice=1;
if(flag_hspice_lib ==1){
		sprintf(tcl_command,"./gen_hspice_c35.tcl -l %s -t %s -o %s",hspice_libname,laser_fault_filename,hspice_output_filename);
		system(tcl_command);
	}

}

//********end of write hspice file*****************

	l_fout<<flush;l_fout.close();
	wf_fout<<flush;wf_fout.close();
	
	for(i=0;i<top_level->gate_number;i++) {
	test_g = top_level->NameToPointer[top_level->list_gate[i]];
#ifdef __DEBUG__
	test_g->print_gate_input_debug();
#endif
	free(test_g->gate_input_table);
	}
	
}

void run_fs_stuckat_timing()
{
	int i, gi, index_of_the_fault, c;
	float f;
	FILE *fout;
	int timeline=-1, lastLen=0, t;
	char s[128];
	 simulation_event fault_event;
	
	detected_faults=0;
	printf ("Allocating memory for fault list (%d Bytes)...", faultCombinations_number); fflush(stdout);
	faultlist_coverage=(char *) malloc (sizeof(char) * faultCombinations_number);
	for (gi=0; gi<faultCombinations_number; gi++) {
		faultlist_coverage[gi]=0;
	}
	printf ("done.");
	printf ("\nStarting stuck-at fault simulation for sequential circuits...\n"); 
	printf ("fault_simulation_result: ");fflush(stdout);
	// It opens the statistic file
	fout=open_statistic_file();
	// For each fault it runs the simulation
	for (gi=0; gi<faultCombinations_number; gi++) {
		// Initialize the circuit
		top_level->init();
		for (c=0; c<faultCombinations[gi].size(); c++) {
			index_of_the_fault = faultCombinations[gi].at(c);
			fault_event.flag_fault = 1;
			fault_event.fault_index = index_of_the_fault;
			fault_event.g = stuckat_faultlist[index_of_the_fault]-> g;
			fault_event.port = stuckat_faultlist[index_of_the_fault]-> port;
			fault_event.isPI = 0;
			fault_event.flag_clk = 0;
			fault_event.PIvalue =0;
			//Begin of fault injection 	
			fault_event.time = faultBegin[gi].at(c);
			fault_event.event_type = 0; //inject fault
			timing_queue.push(fault_event);
			//End of fault injection	
			fault_event.time = faultEnd[gi].at(c);
			fault_event.event_type = 1; //release fault
			timing_queue.push(fault_event);
		}

		// Simulation of the circuit (with faults enabled)
		simulation(0,0);
		if (strlen(param_output_filename)>0) {
			print_output(); // In functions.c
		}
		// Comparison of the output (during the time) to check whether the fault has been covered
		compare_fs_results(gi, fout);
		
		
		// Update the progress bar
		t=100 * gi / faultCombinations_number;
		for (i=0; i<lastLen; i++) { printf ("%c",8); }
		timeline=t;
		sprintf (s, "%d%% (%d faults)",timeline, gi);
		lastLen=strlen(s);
		printf ("%s", s);
		fflush(stdout);

	}
	// It closes the statistic file
	if (fout!=NULL) {
		fclose (fout);
	}
	f=(float) 100 * detected_faults / faultCombinations_number;
	for (i=0; i<lastLen; i++) { printf ("%c",8); }
	printf ("...done.                            \nFault coverage=%d on %d (%.4f)\n",detected_faults,faultCombinations_number, f);
	if (param_write_fault_statistic==1) {
		printFaultsStatistic(); // In functions.c
	}
	free (faultlist_coverage);
}

int read_laser_fault_table()
{
	FILE *f;
	int  i,flag_laser_fault=0;
	char *key_word, *subgate_name, *subgate_type, *fault_s,*fault_e;  
	char line[255], c_temp[32];
	string s_temp;
	subgate_number = 0;
	// Get number of input patterns
	if ((f=fopen(laser_fault_filename,"r"))==NULL) return -1;
	
	while(!feof(f))
	{	fgets(line,255,f);
		//fscanf(f,"%s",line);
		//printf("%s",line);
		strcpy (c_temp, line);
		key_word=strtok (c_temp," ");
	
		if (strcmp(key_word,"time")==0) {
		fault_s=strtok(NULL," ");
		fault_e=strtok(NULL," ");
		fault_start= atoi(fault_s);
		fault_end= atoi(fault_e);
#ifdef __DEBUG__
		printf("fault_start = %d,	fault_end= %d \n",fault_start,	fault_end);
#endif
		printf("simulation_time  %d %d \n",fault_start,	fault_end);
		}
	
		if (strcmp(key_word,"gate")==0) {
		flag_laser_fault=1;
		subgate_name=strtok(NULL," ");
		subgate_type=strtok(NULL," ");
		s_temp = subgate_name;
		list_subgate.push_back(s_temp);
#ifdef __DEBUG__
		printf("subgate_name = %s,	sub_gate_number= %d subgate_type=%s\n",list_subgate[subgate_number].c_str(),	subgate_number, subgate_type );
#endif
		subgate_number++;
		
		}
	}

	fclose (f);
	if (flag_laser_fault==0) return 1;	
#ifdef __DEBUG__	
		printf("sub_gate_list size : %d   ",list_subgate.size() );
		printf("sub_gate_list : ");
		for(i=0;i<list_subgate.size();i++) printf(" %s", list_subgate[i].c_str());
		printf("\n");
#endif
}


//****************************************************************


int read_input_patterns(char* filename)
{
	FILE *f;
	int rowSize=50+(top_level->FF_scan_number>top_level->input_number?top_level->FF_scan_number:top_level->input_number);
	int set_input_number=top_level->input_number; 
	int set_input_table[set_input_number]; 
	char s[20], p[rowSize];
	char set_input_name[rowSize];
	int i=0,j,k,qt,type,qto, time, flag_set_input, row_number,set_input_exist,i_row,i_set_input,temp_no;

	flag_set_input = 0;
	// Get number of input patterns
	if ((f=fopen(filename,"r"))==NULL) return -1;
	input_patterns_number=0;
	row_number=0;
	for (i=0;i<set_input_number;i++)set_input_table[i]=i ; 
	i=0;
	//clock_cycle=0;
	//duty_cycle=0;
	while (fgets(p, rowSize, f)) {
		if (strlen(p)>2 ) {
			row_number++;
			if (p[0] != 's' && p[1] != 'e' && p[2] != 't') {
			input_patterns_number++;
			}
		}
	}
#ifdef __DEBUG__
	printf("input_patterns_number = %d,	row_number= %d\n",input_patterns_number,	row_number );
#endif
	fclose (f);


	input_patterns = (int **) malloc (input_patterns_number * sizeof(int*));
	output_patterns = (int **) malloc (input_patterns_number * sizeof(int*));
	golden_output_patterns = (int **) malloc (input_patterns_number * sizeof(int*));

	if ((f=fopen(filename,"r"))==NULL) return -1;
	for (i_row=0; i_row<row_number; i_row++) {
		fscanf (f, "%s %d %s", s, &time, p);
		//printf ("%s %d %s ; i= %d\n", s, time, p,i);
//-----------   12/10/2011	-------------------	
		if (strcmp(s,"PI")==0) {
			// It's a primary input
			flag_set_input = 0;
			qt=top_level->input_number;
			qto=top_level->output_number;
			type=APPLY_PRIMARY_INPUT;
			
		} else if (strcmp(s,"set_input")==0 || strcmp(s,"set_clock")==0){
			if (strcmp(s,"set_clock")==0){
				set_clock_no = time;
			}
			set_input_no = time;
#ifdef __DEBUG__
			printf("\n set_input_name = %s",p);
			printf("\n set_input_number is %d",set_input_number);
#endif
			for (k=0; k<set_input_number; k++) {
				if (strcmp(p,top_level->list_PI[k]->name)==0) 
					{set_input_table[set_input_no] = k;
#ifdef __DEBUG__
					printf ("\n top_level->list_PI[%d]->name is %s", k,top_level->list_PI[k]->name);
#endif
					}				
			}
#ifdef __DEBUG__
			printf("\n set_input_table[%d] = %d", set_input_no,set_input_table[set_input_no]);
			printf("\n set No. %d for port \"%s\"(%d)", set_input_no,p,set_input_table[set_input_no]);
#endif			
			flag_set_input = 1;
			//set_input_exist=1;
		
		} else {
			// It's a scan chain
			flag_set_input = 0;
			qt=top_level->FF_scan_number;
			qto=top_level->FF_scan_number;
			type=LOAD_SCAN_CHAIN;
			
		}
		
/*		if (flag_set_input == 1) {
		i = i-1;
		row_number= row_number-1;
		}
		flag_set_input = 0; */
//------------------end 12/10/2011-------------------		
		
		//qt=top_level->input_number;
		//qto=top_level->output_number;
		if (flag_set_input == 0){
			input_patterns[i]=(int*) malloc ((2+qt) * sizeof(int));
			output_patterns[i]=(int*) malloc ((1+qto) * sizeof(int));
			golden_output_patterns[i]=(int*) malloc (qto * sizeof(int));
			input_patterns[i][1]=time;
			input_patterns[i][0]=type; //------------12/10/2011----------------
			for (j=0; j<qt; j++) {

			//findTokenInString (set_input_table[j], inputs_name);
				if (p[j]=='0') {
					input_patterns[i][set_input_table[j]+2]=0;
				} else if (p[j]=='1') {
					input_patterns[i][set_input_table[j]+2]=1;
				} else {
					input_patterns[i][set_input_table[j]+2]=2;
				}
			}
			i++;
		}
		
	}
	end_time = time;
	clock_no = set_input_table[set_clock_no];
#ifdef __DEBUG__
	printf ("end_time = %d\n", end_time);
#endif
//-------------------2013/2/6-----------
fclose (f);
}

void printTimingQueue ()
{
	priority_queue <simulation_event> temp_queue = timing_queue;
	simulation_event event;

	cout << "== timing_queue:" << endl;
	while (!temp_queue.empty())
	{
		event = temp_queue.top();
		if (event.isPI>0) {
			
			
			cout << event.time << " - PI " << (event.g)->name << " set to " << event.PIvalue << endl;
			
		} else {
			if(event.flag_fault==1) {
				if(event.event_type) {
				cout << event.time << " - Release stuck at " << stuckat_faultlist[event.fault_index]->stuckat << " of " <<stuckat_faultlist[event.fault_index]->portName <<" in "<<(event.g)->name << endl;
				} else {
				cout << event.time << " - Inject stuck at " << stuckat_faultlist[event.fault_index]->stuckat << " of " <<stuckat_faultlist[event.fault_index]->portName <<" in " <<(event.g)->name << endl;
				}
			}
			else if(event.flag_clk==1) {
			cout << event.time << " - Clock set to " << (event.g)->input_value[0] <<" for "<<(event.g)->name<<" Port "<<event.port<< endl;
			}
			
			else{
			
			cout << event.time << " - " << (event.g)->name << " (port " << event.port << " will be calculated)" << endl;
			}
		}
		temp_queue.pop();
	}
}
void print_output()
{
	int i,j,qt1,qt2;
	char c;
	FILE *f;

	f=fopen (param_output_filename, "w");
	if (f==NULL) {
		printf ("\nERROR: cannot open output files for writing. Output aborted.\n");
		return;
	}

	for (i=0; i<input_patterns_number; i++) {
		if (input_patterns[i][0]==APPLY_PRIMARY_INPUT) {
			qt1=top_level->input_number;
			qt2=top_level->output_number;
			fprintf (f, "PO %5d \t", input_patterns[i][1]);
		} else {
			qt1=top_level->FF_scan_number;
			qt2=top_level->FF_scan_number;
			fprintf (f, "SC ");
		}
		for (j=0; j<qt1; j++) {
			switch (input_patterns[i][j+2]) {
				case 0: c='0'; break;
				case 1: c='1'; break;
				case 2: c='x'; break;
				case 3: c='Z'; break;
			}
			fprintf (f, "%c",c);
		}
		fprintf (f, " -> ");
		fprintf (f, " %5d \t",output_patterns[i][0]);
		for (j=0; j<qt2; j++) {
			switch (output_patterns[i][j+1]) {
				case 0: c='0'; break;
				case 1: c='1'; break;
				case 2: c='x'; break;
				case 3: c='Z'; break;
				//case 4: c='s'; break;
				//case 5: c='h'; break;	
			}
			fprintf (f, "%c",c);
		}
		
		fprintf (f, "\n");
	}
	fclose (f);
}

#endif

