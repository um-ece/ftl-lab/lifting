// verilog_drive.h: interface for the verilog parser
//
//
//
// Author : Alberto BOSIO	
//
// Last Update : 08-09-2007
/////////////////////////////////////////////////////////////////////


#ifndef VERILOG_DRIVER_HH
#define VERILOG_DRIVER_HH

#include <string>
#include <iostream>
#include <map>

#include "verilog_parser.hpp"


#include "./techlib/common/library.h"

#include "./techlib/common/pi.h"
#include "./techlib/common/po.h"
#include "./techlib/common/one.h"
#include "./techlib/common/zero.h"

#include "./generic_cell.h"

using namespace std;

     class verilog_driver;
     // Announce to Flex the prototype we want for lexing function, ...
     # define YY_DECL					\
       yy::verilog_parser::token_type                         \
       yylex (yy::verilog_parser::semantic_type* yylval,      \
              yy::verilog_parser::location_type* yylloc,      \
              verilog_driver& driver)
     // ... and declare it for the parser's sake.
     YY_DECL;

    // Conducting the whole scanning and parsing of Verilog.
    //
     
     typedef struct {
		string line;
		generic_gate* ref;
		int pos;	
     } _incomplete;

     typedef struct {
		string line;
		generic_gate* ref;
		int pos;	
     } _unconnect;

	typedef struct {
		string line;
		int v_pos;
	} _hash_mnemonic_key;

	typedef struct {
		vector <_hash_mnemonic_key> mylist;
	} _hash_mnemonic_key_vector;


     // able to manage only one module
     class verilog_driver
     {
     public:
       
       int k ;

       library *lib;
	
       // debug information	     
       std::string module_name; 	// deal only one verilog module

       
       
	int module_number;

       int node_number; // gate numbers
       int PI_number;
       int PO_number;
       
       int u_val;   // number of unconnected gate
       int i_val;   // number of incomplete gate

       vector <string> terminal;		// wire name
       vector <string> terminal_name;		// connection name  ex: .I()
       vector <_incomplete> incomplete_gate;	//  these two vectors are used during the parsing of the verilog graph
       vector <_unconnect> unconnected_gate;	//        
       //vector < vector <_hash_mnemonic_key> > incomp_hash;
       _hash_mnemonic_key_vector incomp_hash[270];
       map<string,int>  unconn_index;

       generic_cell *circuit; // module instance

       verilog_driver (int l);

       virtual ~verilog_driver ();
     

	// build the circtui
	
       generic_gate*    get_unconnected_gate (const char *name, int *pos);
       generic_gate*	check_incomplete_gate (const char *name, generic_gate *p_gate, int out_pos);
	

       void print (ostream &os);

       void check_po (generic_gate *    p_po, int out_pos); // set po

       // Handling the scanner.
       void scan_begin ();
       void scan_end ();
       bool trace_scanning;
       // Handling the parser.
       void parse (const std::string& f);
       std::string file;
       bool trace_parsing;
       // Error handling.
       void error (const yy::location& l, const std::string& m);
       void error (const std::string& m);

     };




#endif // ! VERILOG_DRIVER_HH

