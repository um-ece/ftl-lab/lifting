%{
#include <stdio.h>
#include <string.h>
#include "generic_cell.cpp"
#include "generic_gate.cpp"
//#include "generic_ff.h"
#include "def_parser.h"

char *gate_name,*gate_type,*temp;
char *direction;
int orientation;
/* orientation = 0 -> N
				 1 -> FN
				 2 -> S
				 3 -> FS
				 4 -> W
				 5 -> FW
				 6 -> E
				 7 -> FE
*/
int i,j,m;

		
//int flag_component = 0;		
int flag_EOF = 0;
int flag = 0;
// flag 1 -> DIEAREA
//      2 -> COMPONENTS
//		3 -> GATE & LOCATION
//		4 -> ORIENTATION

long int location[2], diearea[4];
int component_number;

%}

%%
"DIEAREA" {flag=1;}
"COMPONENTS" {
//			flag_component = 1;
			flag = 2;
			}
"END COMPONENTS" {flag = 0;}



[\-():/|"+#*! ] {}
[\n] {}
[;] {
		if (flag>1) {flag=3; j=0;}
	}
[0-9.]+ { 	
			switch (flag) {
			case 1 : {diearea[m]= atoi(yytext);  m++;}
						if(m>=4)
						{
						 m=0;
						 printf("Diearea are (%d , %d) to (%d , %d) \n", diearea[0],diearea[1],diearea[2],diearea[3]);
						 flag=0;
						 return 1;} 
						 break; 
			case 2 : {component_number = atoi(yytext); printf("Number of components is %d \n", component_number);flag=3;}
					 break;
		
			case 3 : {location[i]= atoi(yytext);  i++;}
						if(i>=2)
						{
						 i=0;
						 printf(" location are %d & %d \n", location[0],location[1]);
						 flag=4;
						 //return 1;
						 } 
						 break;
			
			default : break;
			}
	}
	
[a-zA-Z0-9_\[\]$]+ {
				switch (flag) {
	
				case 3: {
							if(j==0) {
								gate_name =strdup(yytext); 
								j=1;
								printf("\nName of component is %s; ",gate_name);
							} else if(j==1) {
								gate_type=strdup(yytext); 
								j=2; 
								printf("gate type is %s\n",gate_type);
							}
						} break;	
				case 4: {	
							direction = strdup(yytext);
							
							if (strcmp(direction, "N")==0) {orientation = 0;}
							else if (strcmp(direction, "FN")==0) {orientation = 1;}
							else if (strcmp(direction, "S")==0) {orientation = 2;}
							else if (strcmp(direction, "FS")==0) {orientation = 3;}
							else if (strcmp(direction, "W")==0) {orientation = 4;}
							else if (strcmp(direction, "FW")==0) {orientation = 5;}
							else if (strcmp(direction, "E")==0) {orientation = 6;}
							else if (strcmp(direction, "FE")==0) {orientation = 7;}
							
							printf(" direction is %s (%d)\n",direction, orientation);
							flag=3; j=0;
							return 1;	
						} break;
							
				default : break;
				}				
	}
	

%%

void def_parser(char *def_filename, generic_cell *cell){
int c;
generic_gate *g;
//yyin=fopen("test_def.def","r");
yyin=fopen(def_filename,"r");

 do{
	c=0;
	c=yylex();
	
	if (flag>=3) {
		printf("Name of component is %s (%s)-> (%d, %d) %d\n",gate_name, gate_type,location[0],location[1],orientation);
		g = cell -> getGenericGate_fromName(gate_name);
		g -> location[0] = location[0]; g -> location[1] = location[1];
		g -> orientation = orientation;
	}
}while(c == 1);

}

int yywrap(void)
{
    return 1;                          
}