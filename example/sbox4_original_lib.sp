** Generated for: hspiceD
** Generated on: Oct 28 15:06:51 2013
** Design library name: my_lib
** Design cell name: rsmr
** Design view name: schematic
.GLOBAL vdd!


.TEMP 25.0
.OPTION
+    ABSMOS=1e-9
+    ABSVDC=1e-6
+    ABSV=1e-6
+    ARTIST=2
+    CHGTOL=1e-14
+    DEFAD=5.29e-12
+    DEFAS=5.29e-12
+    DEFNRD=0.1
+    DEFNRS=0.1
+    DEFPD=9.2e-06
+    DEFPS=9.2e-06
+    INGOLD=2
+    LVLTIM=2
+    METHOD=GEAR
+    NOMOD
+    OPTS
+    PARHIER=LOCAL
+    PSF=2
+    RELI=1e-3
+    RELMOS=1e-3
+    TNOM=27
.option probe post=2 measdgt=3 ingold=2 
.option numdgt=6
.LIB "~/Libraries/hspiceS/c35/cmos49.lib" cmostm
.LIB "~/Libraries/hspiceS/c35/res.lib" restm
.LIB "~/Libraries/hspiceS/c35/cap.lib" captm
.LIB "~/Libraries/hspiceS/c35/bip.lib" biptm

** Library name: GATES
** Cell name: nand3_core
** View name: schematic
.subckt nand3_core a b c out inh_gnd inh_vdd
mn1 out a net6 inh_gnd modn L=gt_mn1l W=gt_mn1w AD='sx*gt_mn1w' AS='sx*gt_mn1w' PD='2*sx+gt_mn1w' PS='2*sx+gt_mn1w' NRD='lc/gt_mn1w' NRS='lc/gt_mn1w' M=1
mn3 net10 c inh_gnd inh_gnd modn L=gt_mn3l W=gt_mn3w AD='sx*gt_mn3w' AS='sx*gt_mn3w' PD='2*sx+gt_mn3w' PS='2*sx+gt_mn3w' NRD='lc/gt_mn3w' NRS='lc/gt_mn3w' M=1
mn2 net6 b net10 inh_gnd modn L=gt_mn2l W=gt_mn2w AD='sx*gt_mn2w' AS='sx*gt_mn2w' PD='2*sx+gt_mn2w' PS='2*sx+gt_mn2w' NRD='lc/gt_mn2w' NRS='lc/gt_mn2w' M=1
mp1 out a inh_vdd inh_vdd modp L=gt_mp1l W=gt_mp1w AD='sx*gt_mp1w' AS='sx*gt_mp1w' PD='2*sx+gt_mp1w' PS='2*sx+gt_mp1w' NRD='lc/gt_mp1w' NRS='lc/gt_mp1w' M=1
mp3 out c inh_vdd inh_vdd modp L=gt_mp3l W=gt_mp3w AD='sx*gt_mp3w' AS='sx*gt_mp3w' PD='2*sx+gt_mp3w' PS='2*sx+gt_mp3w' NRD='lc/gt_mp3w' NRS='lc/gt_mp3w' M=1
mp2 out b inh_vdd inh_vdd modp L=gt_mp2l W=gt_mp2w AD='sx*gt_mp2w' AS='sx*gt_mp2w' PD='2*sx+gt_mp2w' PS='2*sx+gt_mp2w' NRD='lc/gt_mp2w' NRS='lc/gt_mp2w' M=1
.ends nand3_core
** End of subcircuit definition.

** Library name: CORELIB
** Cell name: NAND33
** View name: cmos_sch
.subckt NAND33 a b c q inh_gnd inh_vdd
xi1 a b c q inh_gnd inh_vdd nand3_core gt_mn1l=350e-9 gt_mn1w=9e-6 sx=850e-9 lc=500e-9 gt_mn3l=350e-9 gt_mn3w=9e-6 gt_mn2l=350e-9 gt_mn2w=9e-6 gt_mp1l=350e-9 gt_mp1w=4.8e-6 gt_mp3l=350e-9 gt_mp3w=4.8e-6 gt_mp2l=350e-9 gt_mp2w=4.8e-6
.ends NAND33
** End of subcircuit definition.

** Library name: GATES
** Cell name: nand2_core
** View name: schematic
.subckt nand2_core a b out inh_gnd inh_vdd
mn1 out a net13 inh_gnd modn L=gt_mn1l W=gt_mn1w AD='sx*gt_mn1w' AS='sx*gt_mn1w' PD='2*sx+gt_mn1w' PS='2*sx+gt_mn1w' NRD='lc/gt_mn1w' NRS='lc/gt_mn1w' M=1
mn2 net13 b inh_gnd inh_gnd modn L=gt_mn2l W=gt_mn2w AD='sx*gt_mn2w' AS='sx*gt_mn2w' PD='2*sx+gt_mn2w' PS='2*sx+gt_mn2w' NRD='lc/gt_mn2w' NRS='lc/gt_mn2w' M=1
mp1 out a inh_vdd inh_vdd modp L=gt_mp1l W=gt_mp1w AD='sx*gt_mp1w' AS='sx*gt_mp1w' PD='2*sx+gt_mp1w' PS='2*sx+gt_mp1w' NRD='lc/gt_mp1w' NRS='lc/gt_mp1w' M=1
mp2 out b inh_vdd inh_vdd modp L=gt_mp2l W=gt_mp2w AD='sx*gt_mp2w' AS='sx*gt_mp2w' PD='2*sx+gt_mp2w' PS='2*sx+gt_mp2w' NRD='lc/gt_mp2w' NRS='lc/gt_mp2w' M=1
.ends nand2_core
** End of subcircuit definition.

** Library name: CORELIB
** Cell name: NAND22
** View name: cmos_sch
.subckt NAND22 a b q inh_gnd inh_vdd
xi1 a b q inh_gnd inh_vdd nand2_core gt_mn1l=350e-9 gt_mn1w=4e-6 sx=850e-9 lc=500e-9 gt_mn2l=350e-9 gt_mn2w=4e-6 gt_mp1l=350e-9 gt_mp1w=3.2e-6 gt_mp2l=350e-9 gt_mp2w=3.2e-6
.ends NAND22
** End of subcircuit definition.

** Library name: GATES
** Cell name: invb_core
** View name: schematic
.subckt invb_core a q inh_gnd inh_vdd
mp2 q a inh_vdd inh_vdd modp L=gt_mp2l W=gt_mp2w AD='sx*gt_mp2w' AS='sx*gt_mp2w' PD='2*sx+gt_mp2w' PS='2*sx+gt_mp2w' NRD='lc/gt_mp2w' NRS='lc/gt_mp2w' M=1
mn2 q a inh_gnd inh_gnd modn L=gt_mn2l W=gt_mn2w AD='sx*gt_mn2w' AS='sx*gt_mn2w' PD='2*sx+gt_mn2w' PS='2*sx+gt_mn2w' NRD='lc/gt_mn2w' NRS='lc/gt_mn2w' M=1
.ends invb_core
** End of subcircuit definition.

** Library name: CORELIB
** Cell name: CLKIN2
** View name: cmos_sch
.subckt CLKIN2 a q inh_gnd inh_vdd
xi3 a q inh_gnd inh_vdd invb_core gt_mp2l=350e-9 gt_mp2w=3.2e-6 sx=850e-9 lc=500e-9 gt_mn2l=350e-9 gt_mn2w=1.6e-6
.ends CLKIN2
** End of subcircuit definition.

** Library name: GATES
** Cell name: clinva_core
** View name: schematic
.subckt clinva_core a c cn q inh_gnd inh_vdd
mn1 net18 c inh_gnd inh_gnd modn L=gt_mn1l W=gt_mn1w AD='sx*gt_mn1w' AS='sx*gt_mn1w' PD='2*sx+gt_mn1w' PS='2*sx+gt_mn1w' NRD='lc/gt_mn1w' NRS='lc/gt_mn1w' M=1
mn0 q a net18 inh_gnd modn L=gt_mn0l W=gt_mn0w AD='sx*gt_mn0w' AS='sx*gt_mn0w' PD='2*sx+gt_mn0w' PS='2*sx+gt_mn0w' NRD='lc/gt_mn0w' NRS='lc/gt_mn0w' M=1
mp1 net10 cn inh_vdd inh_vdd modp L=gt_mp1l W=gt_mp1w AD='sx*gt_mp1w' AS='sx*gt_mp1w' PD='2*sx+gt_mp1w' PS='2*sx+gt_mp1w' NRD='lc/gt_mp1w' NRS='lc/gt_mp1w' M=1
mp0 q a net10 inh_vdd modp L=gt_mp0l W=gt_mp0w AD='sx*gt_mp0w' AS='sx*gt_mp0w' PD='2*sx+gt_mp0w' PS='2*sx+gt_mp0w' NRD='lc/gt_mp0w' NRS='lc/gt_mp0w' M=1
.ends clinva_core
** End of subcircuit definition.

** Library name: GATES
** Cell name: inv_core
** View name: schematic
.subckt inv_core in out inh_gnd inh_vdd
mn1 out in inh_gnd inh_gnd modn L=gt_mn1l W=gt_mn1w AD='sx*gt_mn1w' AS='sx*gt_mn1w' PD='2*sx+gt_mn1w' PS='2*sx+gt_mn1w' NRD='lc/gt_mn1w' NRS='lc/gt_mn1w' M=1
mp1 out in inh_vdd inh_vdd modp L=gt_mp1l W=gt_mp1w AD='sx*gt_mp1w' AS='sx*gt_mp1w' PD='2*sx+gt_mp1w' PS='2*sx+gt_mp1w' NRD='lc/gt_mp1w' NRS='lc/gt_mp1w' M=1
.ends inv_core
** End of subcircuit definition.

** Library name: GATES
** Cell name: tgate_core
** View name: schematic
.subckt tgate_core en ep in out inh_gnd inh_vdd
mp1 out ep in inh_vdd modp L=gt_mp1l W=gt_mp1w AD='sx*gt_mp1w' AS='sx*gt_mp1w' PD='2*sx+gt_mp1w' PS='2*sx+gt_mp1w' NRD='lc/gt_mp1w' NRS='lc/gt_mp1w' M=1
mn1 out en in inh_gnd modn L=gt_mn1l W=gt_mn1w AD='sx*gt_mn1w' AS='sx*gt_mn1w' PD='2*sx+gt_mn1w' PS='2*sx+gt_mn1w' NRD='lc/gt_mn1w' NRS='lc/gt_mn1w' M=1
.ends tgate_core
** End of subcircuit definition.

** Library name: CORELIB
** Cell name: DF3
** View name: cmos_sch
.subckt DF3 c d q qn inh_gnd inh_vdd
xi52 d cn ci net48 inh_gnd inh_vdd clinva_core gt_mn1l=350e-9 gt_mn1w=1e-6 sx=850e-9 lc=500e-9 gt_mn0l=350e-9 gt_mn0w=1e-6 gt_mp1l=350e-9 gt_mp1w=1.6e-6 gt_mp0l=350e-9 gt_mp0w=1.6e-6
xi58 net48 net63 inh_gnd inh_vdd inv_core gt_mn1l=350e-9 gt_mn1w=1e-6 sx=850e-9 lc=500e-9 gt_mp1l=350e-9 gt_mp1w=1.6e-6
xi59 net57 q inh_gnd inh_vdd inv_core gt_mn1l=350e-9 gt_mn1w=3e-6 sx=850e-9 lc=500e-9 gt_mp1l=350e-9 gt_mp1w=4.8e-6
xi60 net55 qn inh_gnd inh_vdd inv_core gt_mn1l=350e-9 gt_mn1w=3e-6 sx=850e-9 lc=500e-9 gt_mp1l=350e-9 gt_mp1w=4.8e-6
xi61 net63 net47 inh_gnd inh_vdd inv_core gt_mn1l=350e-9 gt_mn1w=1e-6 sx=850e-9 lc=500e-9 gt_mp1l=350e-9 gt_mp1w=1.6e-6
xi62 net57 net55 inh_gnd inh_vdd inv_core gt_mn1l=350e-9 gt_mn1w=1e-6 sx=850e-9 lc=500e-9 gt_mp1l=350e-9 gt_mp1w=1.6e-6
xi63 x net57 inh_gnd inh_vdd inv_core gt_mn1l=350e-9 gt_mn1w=1e-6 sx=850e-9 lc=500e-9 gt_mp1l=350e-9 gt_mp1w=1.6e-6
xi55 cn ci net55 x inh_gnd inh_vdd tgate_core gt_mp1l=350e-9 gt_mp1w=1e-6 sx=850e-9 lc=500e-9 gt_mn1l=350e-9 gt_mn1w=1e-6
xi56 ci cn net47 net48 inh_gnd inh_vdd tgate_core gt_mp1l=350e-9 gt_mp1w=1e-6 sx=850e-9 lc=500e-9 gt_mn1l=350e-9 gt_mn1w=1e-6
xi57 ci cn net63 x inh_gnd inh_vdd tgate_core gt_mp1l=350e-9 gt_mp1w=1e-6 sx=850e-9 lc=500e-9 gt_mn1l=350e-9 gt_mn1w=1e-6
xi54 c cn inh_gnd inh_vdd invb_core gt_mp2l=350e-9 gt_mp2w=1e-6 sx=850e-9 lc=500e-9 gt_mn2l=350e-9 gt_mn2w=500e-9
xi53 cn ci inh_gnd inh_vdd invb_core gt_mp2l=350e-9 gt_mp2w=800e-9 sx=850e-9 lc=500e-9 gt_mn2l=350e-9 gt_mn2w=400e-9
.ends DF3
** End of subcircuit definition.

** Library name: GATES
** Cell name: nor2_core
** View name: schematic
.subckt nor2_core a b out inh_gnd inh_vdd
mn1 out b inh_gnd inh_gnd modn L=gt_mn1l W=gt_mn1w AD='sx*gt_mn1w' AS='sx*gt_mn1w' PD='2*sx+gt_mn1w' PS='2*sx+gt_mn1w' NRD='lc/gt_mn1w' NRS='lc/gt_mn1w' M=1
mn2 out a inh_gnd inh_gnd modn L=gt_mn2l W=gt_mn2w AD='sx*gt_mn2w' AS='sx*gt_mn2w' PD='2*sx+gt_mn2w' PS='2*sx+gt_mn2w' NRD='lc/gt_mn2w' NRS='lc/gt_mn2w' M=1
mp1 out a net17 inh_vdd modp L=gt_mp1l W=gt_mp1w AD='sx*gt_mp1w' AS='sx*gt_mp1w' PD='2*sx+gt_mp1w' PS='2*sx+gt_mp1w' NRD='lc/gt_mp1w' NRS='lc/gt_mp1w' M=1
mp2 net17 b inh_vdd inh_vdd modp L=gt_mp2l W=gt_mp2w AD='sx*gt_mp2w' AS='sx*gt_mp2w' PD='2*sx+gt_mp2w' PS='2*sx+gt_mp2w' NRD='lc/gt_mp2w' NRS='lc/gt_mp2w' M=1
.ends nor2_core
** End of subcircuit definition.

** Library name: CORELIB
** Cell name: NOR22
** View name: cmos_sch
.subckt NOR22 a b q inh_gnd inh_vdd
xi1 b a q inh_gnd inh_vdd nor2_core gt_mn1l=350e-9 gt_mn1w=2e-6 sx=850e-9 lc=500e-9 gt_mn2l=350e-9 gt_mn2w=2e-6 gt_mp1l=350e-9 gt_mp1w=6.4e-6 gt_mp2l=350e-9 gt_mp2w=6.4e-6
.ends NOR22
** End of subcircuit definition.

** Library name: CORELIB
** Cell name: INV2
** View name: cmos_sch
.subckt INV2 a q inh_gnd inh_vdd
xi3 a q inh_gnd inh_vdd inv_core gt_mn1l=350e-9 gt_mn1w=2e-6 sx=850e-9 lc=500e-9 gt_mp1l=350e-9 gt_mp1w=3.2e-6
.ends INV2
** End of subcircuit definition.
