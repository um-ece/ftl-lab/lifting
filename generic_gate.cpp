#include <queue>
#include <math.h>
#include "generic_gate.h"
#include "simulation_event.h"
#include "functions.h"
/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = 2
 * Z = 3
*/


generic_gate::generic_gate (const char *pname) {
	int i,j;
	for (i=0; i<MAX_INPUTS; i++) {
		input_value[i]=2;
		ref_inputs[i].p_gg=NULL;
		ref_inputs[i].port=-1;
		stuck_at_input[i]=0;
		//input_capa[i]=0;
	}
	for (i=0; i<MAX_OUTPUTS; i++) {
		fan_out[i]=0;
		output_value[i]=2;
		wf_output_value[i]=2;
		stuck_at_output[i]=0;
		ref_outputs[i].p_gg.clear();
		ref_outputs[i].port.clear();
		output_is_changed[i]=0;
		fan_out_capa[i]=0;
#ifdef __SLACK_CALCULATION__
		for(j=0; j<MAX_OUTPUTS; j++) {
			slack[i][j]=NULL;
			slack_done[i][j]=NULL;
		}
#endif
	}
	name=(char *)malloc((1+strlen(pname))*sizeof(char));
	strcpy (name, pname);
	type=0;
#ifdef __TIMING_SIMULATION__	
	flag_hspice_sim_list=0;
	number_pnj = 0;

#endif	
	outputs_number = 0;
	inputs_number = 0;
	
	propagate_output_during_init = 0 ; // default during init, the output is neither calculated and propagated
#ifdef __TIMING_SIMULATION__
	flag_fault_subgate=0;
	// TODO: just for debug, delay of each port is constant and equal to 1
	for (i=0; i<MAX_INPUTS; i++) {
		for (j=0; j<MAX_OUTPUTS; j++) {
			output_delay [i][j][0]=0;
			output_delay [i][j][1]=0;
		}
	}
	for(i=0; i<MAX_PN_Junction; i++) {
		location_pnj[i][0]=0;
		location_pnj[i][1]=0;
	}
#endif
#ifdef __LOGIC_LEVELS__
	logic_level=0;
	logic_level_input_defined=0;
	isFF=0;
#endif
#ifdef __PROBA_CALCULATION__
	input_support_set_done=false;
	noReconvergency=true;
#endif
};

void generic_gate::InitAfterConstr () {
	
	output_line_name = new string[outputs_number];
	output_sp_name = new string[outputs_number];
	input_sp_name= new string[inputs_number];
	input_line_name = new string[inputs_number];
	if (output_line_name == NULL || input_line_name == NULL) {	
		cerr << " memory error during creation of gate ( " << name << " ) " << endl;
		exit (-1);
	}
};

#ifdef __TIMING_SIMULATION__
void generic_gate::InitAfterInput (){
	int i,j;

/*	gate_input_table = (int (**)[2])malloc(inputs_number * sizeof(int **));
	for(i=0;i<inputs_number;i++) 
		gate_input_table[i] = (int (*)[2])malloc((input_patterns_number) * sizeof(int *));
//	for(i=0;i<inputs_number;i++)
//		for(j=0;j<input_patterns_number;j++) 
//			gate_input_table[i][j] = (int*)malloc(2 * sizeof(int));
*/
	flag_fault_subgate=0;
#ifdef __TIMING_SIMULATION__
	flag_hspice_sim_list=0;
#endif	
	gate_input_table = (int***)malloc(inputs_number * sizeof(int**));
	for(i=0;i<inputs_number;i++) {
		gate_input_table[i] = (int**)malloc((input_patterns_number *10) * sizeof(int*));
		for(j=0;j<(input_patterns_number*10);j++) {
			gate_input_table[i][j] = (int*)malloc(2 * sizeof(int));
		}
	}

/*	gate_input_value_table = new int* [inputs_number];
	for(i=0;i<inputs_number;i++) {
		gate_input_value_table[i] = new int[input_patterns_number];
//		for(j=0;j<input_patterns_number;j++) {
//			gate_input_table[i][j]= new int[2];}
		}
	gate_input_time_table = new int* [inputs_number];
	for(i=0;i<inputs_number;i++) {
		 gate_input_time_table[i] = new int[input_patterns_number];}	
*/	

//	gate_output_table = (int***)malloc(outputs_number * sizeof(int**));
//	for(i=0;i<outputs_number;i++) {
//		gate_output_table[i] = (int**)malloc((input_patterns_number) * sizeof(int*));
//		for(j=0;j<input_patterns_number;j++) {
//			gate_output_table[i][j] = (int*)malloc(2 * sizeof(int));
//		}
//	}
	count_change = (int*)malloc(inputs_number * sizeof(int));

	for(i=0;i<inputs_number;i++) {
		count_change[i]=0;
		
		for(j=0;j<input_patterns_number*10;j++) {
/*			gate_input_time_table[i][j] = -1;
			gate_input_value_table[i][j] = -1;  */
			gate_input_table[i][j][0] = -1;  gate_input_table[i][j][1] = -1;
//			printf("gate_input_table is init!\n");
		}
	}
};
#endif

void generic_gate::init() {
	int i;
	for (i=0; i<inputs_number; i++) {
		if (stuck_at_output[i]==0) {
			input_value[i]=2;
		}
#ifdef __TIMING_SIMULATION__
		if (type == 100) {
			last_input_change[i] = 0;
		//	printf("FF is initialed!!!!!!!!!!!!!!!");
		}
#endif
	}	
	for (i=0; i<outputs_number; i++) {
		if (stuck_at_output[i]==0) {
			output_value[i]=2;
			wf_output_value[i]=2;	
			temp_output_value[i]=2;
#ifdef __DEBUG__
			cout << " INIT for gate " << name << ". Setting output (port " << i << ")=2." << endl;
#endif
			if (propagate_output_during_init == 1) {
				calculate_output (i);
				propagate_output (i);	
			}
		}
	}
};
int generic_gate::is_clock (int port){

 if (type==100 || type== 101) { // _C35_DFF_GATE=100  _C35_TFF_GATE=101
	if (port==0) return 1;
		return 0;
 }else {return 0;}
}
void generic_gate::set_input (int port, int value) {
	set_input (port, value, 0);
}

void generic_gate::set_input (int port, int value, int dont_touch_old_value) {
	int i;

#ifdef __TIMING_SIMULATION__
	simulation_event event;
	extern int current_time;
	extern priority_queue <simulation_event> timing_queue;
#endif

	// First step of simulation: it saves the input value
	if (stuck_at_input[port]==0) {
#ifdef __DEBUG__
		cout << " Input changed for gate " << name << "(inPort " << port << "). Old value=" << input_value[port];
		cout << ". New value=" << value << endl;
#endif

//		old_input_value[port] =input_value[port];
		input_value[port] = value;
//		temp_input_value[port] = value;
#ifdef __TIMING_SIMULATION__
	} else {
		if (dont_touch_old_value==0) {
			input_value_without_fault[port] = value;
		}
#endif
	}
	
#ifdef __TIMING_SIMULATION__

	if(type >0 ){
	//printf("count_change[%d] = %d,input_patterns_number= %d\n",port,count_change[port],input_patterns_number);
	gate_input_table[port][count_change[port]][0]=current_time;
	gate_input_table[port][count_change[port]][1]=value;
	
		if(count_change[port]< input_patterns_number*10 -1){
		count_change[port]=count_change[port]+1;
		}
		else count_change[port]=0;
#ifdef __DEBUG__
	cout << "****** AT " <<current_time<< " gate : "<<name<<"  Port "<<port<<" Value "<<value<<" count_change = "<<count_change[port]<<"  ******\n";
#endif
		}
#endif	
	
	// Second step: for each output:
	for (i=0; i<outputs_number; i++) {
	
#ifdef __TIMING_SIMULATION__
		// ==============================================
		// TIMING SIMULATION 
		// ==============================================
		// Timing simulation follows this flow:
		// 1) it calculates the output value (with this new input)
		// 2) new output value is saved in temp_output_value
		// 3) if the new output value differs from the current one, it calculates the dealy from this input to the output (for the new output value)
		// 2) It adds to timing_queue this event (by sending the gate and the information whether this input signal is a clock or not)
		propagate_output_flag=1;
		if (stuck_at_output[i]==0) {
			calculate_output(i);
			
		}
		if ((output_value[i] != temp_output_value[i])) {
#ifdef __DEBUG__
		cout << "  Output possibly changed for gate " << name << " (outPort " << i << "). Old value=" << output_value[i]<< "temp value=" << temp_output_value[i] ;
		cout << ". New value=" << temp_output_value[i] << endl;
#endif	
			
			if ((temp_output_value[i]==0 || temp_output_value[i]==1)) {
				event.time = current_time + output_delay[port][i][temp_output_value[i]];
//				cout <<event.time<<" = "<<current_time<<" + "<<output_delay[port][i][temp_output_value[i]]<<endl;
//				event.flag_input=1;
//				event.temp_input_port=port;
//				flag_input_return=1;
				
			} else {
				// No delay for X and Z
				event.time = current_time;
			}
			event.propagate_output_flag=propagate_output_flag;
			
			// Output port:
			event.port=i;
			event.isPI=0;
			event.g=this;
			
			if (this->is_clock(i)) {
			event.flag_clk=1;
			clock_flag = 1;
			} else {
			event.flag_clk=0;
			}
			event.flag_fault=0;
#ifdef __DEBUG__			
			cout << "Push event ----------------------Time is "<<event.time<<" gate is "<< (event.g)->name << " port "<< i <<" flag_clock is "<< event.flag_clk << endl;
#endif		
			timing_queue.push(event);
		}
#else
		// ==============================================
		// LOGIC SIMULATION (no timing)
		// ==============================================
		// Logic simulation follows this flow:
		// 1) it calculates the output value (with this new input)
		// 2) new output value is saved in temp_output_value
		// 3) if the new output value differs from the current one, it propagates the value
		// 4) except for the clock: in this case, the calculate_output function modifies the flag propagate_output_flag
		//    The output will be propagated after all FFs have saved the data value (it's handled in simlation() function)
		calculate_propagate_output (i);
#endif
	}
//	if (flag_input_return==1){
//		flag_input_return=0;
//		input_value[port]=old_input_value[port];
//	}
	
};

// 1) it calculates the output value (with current inputs)
// 2) new output value is saved in temp_output_value
// 3) if the new output value differs from the current one, it propagates the value
// 4) except for the clock: in this case, the calculate_output function modifies the flag propagate_output_flag
//    The output will be propagated after all FFs have saved the data value (it's handled in simlation() function)
void generic_gate::calculate_propagate_output (int output_port) {
	propagate_output_flag=1;
	if (stuck_at_output[output_port]==0) {
		calculate_output(output_port);
	}
	if (output_value[output_port] != temp_output_value[output_port]) {
#ifdef __DEBUG__
		cout << "  Output changed for gate " << name << " (outPort " << output_port << "). Old value=" << output_value[output_port] ;
		cout << ". New value=" << temp_output_value[output_port] << endl;
#endif				
		output_value[output_port] = temp_output_value[output_port];
		output_is_changed[output_port]=1;
//		cout<<"test calculate_propagate_output"<<endl;

		if (propagate_output_flag==1) {
			propagate_output (output_port);
		}
	}
};

int generic_gate::read_output (int o) { return output_value[o]; };

int generic_gate::read_input (int port) { return input_value[port]; };

void generic_gate::add_output (int this_output_port, generic_gate* g, int g_input_port) {
	ref_outputs[this_output_port].p_gg.push_back(g);
	ref_outputs[this_output_port].port.push_back(g_input_port);
	fan_out[this_output_port]++;
	g->ref_inputs[g_input_port].p_gg=this;
	g->ref_inputs[g_input_port].port=this_output_port;
//	g->ref_inputs[g_input_port].port=g_input_port;
};

void generic_gate::propagate_output (int o) {
	int i;

	if (output_is_changed[o]==1) {
		// This line must be executed first, otherwise it generates loops
		output_is_changed[o]=0;
#ifdef __PROBA_CALCULATION__
		if (this->logic_level <= max_logic_level_toSimulate) {
#endif				
			for (i=0; i<fan_out[o]; i++) {
#ifdef __DEBUG__
				cout<< " Propagate to "<< ref_outputs[o].p_gg[i]->name <<", port="<< ref_outputs[o].port[i] <<", value="<< output_value[o] << endl;
#endif				
				ref_outputs[o].p_gg[i]->set_input (ref_outputs[o].port[i], output_value[o]);
			}
#ifdef __PROBA_CALCULATION__
		}
#endif				
	}
};

void generic_gate::print_gates (ostream &os) {
	int i;
	os << type << " " << name << " Location ("<<location[0]<<", "<<location[1]<<") Size ("<<gate_size[0]<<", "<<gate_size[1]<<") Orientation "<<orientation<<"\n (" ;  
	os.flush ();
	
	os << "inputs " << inputs_name << " : " ;
	
	for (i =0; i < inputs_number; i++)  {
		
		os <<  input_line_name[i] << ",";
	}

	os <<". outpus ";

	for (i =0; i < outputs_number-1; i++)  {
		os <<  output_line_name[i] << ",";
	}

	os <<  output_line_name[i] ;
	
	for (i =0; i < inputs_number; i++)  {
		os <<  "," << ref_inputs[i].p_gg->name << " (port " << ref_inputs[i].port << ")";
	}
	os << ");" << endl;
	os.flush ();

};

// Stuck-at
void generic_gate::force_input (int port, int value) {
#ifdef __DEBUG__
	cout << "******************** Force Input changed for gate " << name << "(inPort " << port << "). Old value=" << input_value[port];
	cout << ". New value=" << value << endl;
#endif
	input_value_without_fault[port]=input_value[port];
	input_value[port]=value;
	stuck_at_input[port]=1;
	this->set_input (port,value,1);
};

void generic_gate::release_input (int port) {
	stuck_at_input[port]=0;
	this->set_input (port, input_value_without_fault[port]);
	//cout << "======Return to old value =" << input_value_without_fault[port] << endl;
};

void generic_gate::force_output (int port, int value) {
	output_value_without_fault[port]=output_value[port];
	output_value[port]=value;
	temp_output_value[port]=value;
	stuck_at_output[port]=1;
	output_is_changed[port]=1;
	propagate_output (port);
};

void generic_gate::release_output (int port) {
	stuck_at_output[port]=0;
	output_value[port]=output_value_without_fault[port];
	temp_output_value[port]=output_value_without_fault[port];
	output_is_changed[port]=1;
	propagate_output (port);
#ifdef __TIMING_SIMULATION__
//	calculate_propagate_output(port);
#else
	propagate_output (port);
#endif
};

int generic_gate::getInputPort (char* s) {
	return findTokenInString (s, inputs_name);
};
char *generic_gate::getInputName (int num) 
{
	char s[100], *p;
	int i=0;

	strcpy (s, inputs_name.c_str());
	p=strtok (s,",");
	for (i=0;i<num;i++)
	{
		p=strtok(NULL,",");
	}
	return p;
}

int generic_gate::getOutputPort (char* s) {
	return findTokenInString (s, outputs_name);
};

#ifdef __TIMING_SIMULATION__
/*void generic_gate::print_gate_input() {
	int i,j;
	for(i=0;i<inputs_number;i++) {
		
		for(j=0;j<input_patterns_number;j++) {
			if(gate_input_table[i][j][0]!=-1 && gate_input_table[i][j][1] != -1)
			printf("Gate %s -> inupt value of port %d : time -> %5d  value -> %d \n",name,i, gate_input_table[i][j][0], gate_input_table[i][j][1]);
		}
		printf("\n");
	}
}*/

void generic_gate::print_laser_fault_list(FILE* f) {
	int i, j,m;
	int flag_subcell_in_list=0;
	vector <string> list_subcell;
	vector <string> list_type_subcell;
	vector <int> list_coms[MAX_SUBCELLS];
	vector <float> list_power[MAX_SUBCELLS];
	int temp_pn_location[2];
	double distance, dis_x, dis_y, temp_power;
	
	for (i=0;i<number_pnj; i++) {
	//******calculate the absolute coordinates of each pn junction*******
		switch (orientation){
			case 0: // orientation N
					temp_pn_location[0]=location_pnj[i][0]+location[0];  //***x***
					temp_pn_location[1]=location_pnj[i][1]+location[1];  //***y***
					break;
			case 1:	// orientation FN
					temp_pn_location[0]=location[0]+ (int)(gate_size[0]*1000) - location_pnj[i][0];  //***x***
					temp_pn_location[1]=location_pnj[i][1]+location[1];  //***y***
					break;
			case 2:	// orientation S
					temp_pn_location[0]=location[0]+ (int)(gate_size[0]*1000) - location_pnj[i][0];  //***x***
					temp_pn_location[1]=location[1]+ (int)(gate_size[1]*1000) - location_pnj[i][1];  //***y***
					break;
			case 3:	// orientation FS
					temp_pn_location[0]=location_pnj[i][0]+location[0];  //***x***
					temp_pn_location[1]=location[1]+ (int)(gate_size[1]*1000) - location_pnj[i][1];  //***y***
					break;		
			default : break;		
		}
		dis_x= (double)(temp_pn_location[0]- laser_center[0]);
		dis_y= (double)(temp_pn_location[1]- laser_center[1]);
		distance = sqrt(dis_x*dis_x + dis_y*dis_y);
		//printf("dis_x= %d - %d = %f; dis_y= %d - %d =%f; distance = %f\n",temp_pn_location[0],laser_center[0],dis_x, temp_pn_location[1],laser_center[1],dis_y, distance); 
		if (distance < laser_radius) {
	//	printf("temp_pn_location[0] = %d\n", temp_pn_location[0]);
			//temp_power = ((laser_radius - distance)/laser_radius)*laser_power; //Linear distribution
			temp_power = laser_power; //Uniform distribution
			flag_subcell_in_list=0;
			for(m=0;m<list_subcell.size();m++) {
		
				if (strcmp(netlilst_pnj[i].name_subcell.c_str(),list_subcell[m].c_str())==0) {
					flag_subcell_in_list=1;
					list_coms[m].push_back(i);
					list_power[m].push_back(temp_power);
				}
			}
			if (flag_subcell_in_list==0){
				list_subcell.push_back(netlilst_pnj[i].name_subcell);
				list_type_subcell.push_back(netlilst_pnj[i].type_subcell);
			//	list_coms[list_subcell.size()].push_back(i);
				list_coms[m].push_back(i);
				list_power[m].push_back(temp_power);
			}
			
		}	
	}
	if (list_subcell.size()>0) {
		fprintf(f,"gate %s %s {\n", name, gate_type.c_str());
		for(m=0;m<list_subcell.size();m++) {
			fprintf(f,"subcell %s %s {\n", list_subcell[m].c_str(),list_type_subcell[m].c_str());
			for(i=0;i<list_coms[m].size();i++) {
				j=list_coms[m][i];
				fprintf(f,"cmos %s %d %1.3f\n", netlilst_pnj[j].cmos_name.c_str(),netlilst_pnj[j].type_pn, list_power[m][i]);
			}
			fprintf(f,"}\n");
		}
		fprintf(f,"}\n");
	}
	
}

void generic_gate::print_netlist_sp(FILE* f) {
	int i,j;
#ifdef __DEBUG__	
	printf("x%s ",name);
#endif	
	fprintf(f,"x%s ",name);
	for(i=0;i<inputs_number;i++) {
		transform(input_sp_name[i].begin(),input_sp_name[i].end(),input_sp_name[i].begin(), ::tolower);
#ifdef __DEBUG__
		printf("%s ", input_sp_name[i].c_str());
#endif		
		if(input_sp_name[i] != "") 
			fprintf(f,"%s ", input_sp_name[i].c_str());
		else {
			fprintf(f,"nett_%d ", no_nett);
			no_nett++;
		}	
	}
	for(i=0;i<outputs_number;i++) {
		transform(output_sp_name[i].begin(),output_sp_name[i].end(),output_sp_name[i].begin(), ::tolower);
#ifdef __DEBUG__		
		printf("%s ", output_sp_name[i].c_str()); 
#endif	
		if(output_sp_name[i] != "") 
			fprintf(f,"%s ", output_sp_name[i].c_str());
		else {
			fprintf(f,"nett_%d ", no_nett);
			no_nett++;
		}	
		
	}
#ifdef __DEBUG__	
	printf("0 vdd! %s \n",gate_type.c_str());
#endif	
	if (flag_fault_subgate==1) fprintf(f,"0 vdd! %s_%s \n",gate_type.c_str(),name);
	else fprintf(f,"0 vdd! %s \n",gate_type.c_str());	
}

void generic_gate::print_inputs_sp(FILE* f) {
	int i,j,flag_input_sp=0;
	for(i=0;i<inputs_number;i++) {
		flag_input_sp=0;
		transform(input_line_name[i].begin(),input_line_name[i].end(),input_line_name[i].begin(), ::tolower);
		input_sp_name[i]=input_line_name[i];
#ifdef __DEBUG__		
		printf("%s 's input%d connected with %s 's output %d!\n",name,i,ref_inputs[i].p_gg->name,ref_inputs[i].port);
#endif		
		for(j=0;j<list_subgate.size();j++) {
			if (strcmp(ref_inputs[i].p_gg->name,list_subgate[j].c_str())==0) {
				flag_input_sp=1;
//				printf("ref_inputs[%d].p_gg->name = %s,list_subgate[%d].c_str() = %s\n",i,ref_inputs[i].p_gg->name,j,list_subgate[j].c_str());
				input_sp_name[i]=ref_inputs[i].p_gg->output_sp_name[ref_inputs[i].port];
//				printf("input_line_name[%d] = %s\n",i,ref_inputs[i].p_gg->output_sp_name[ref_inputs[i].port].c_str());
			}
		}
		for(j=0;j<list_input_netname.size();j++) {
			if (strcmp(input_sp_name[i].c_str(),list_input_netname[j].c_str())==0) {flag_input_sp=1;
	//		printf("input_line_name[%d].c_str() = %s ,list_input_netname[%d].c_str() =%s\n",i,input_line_name[i].c_str(),j,list_input_netname[j].c_str());
			}
	//		printf("input_line_name[%d].c_str() = %s ,list_input_netname[%d].c_str() =%s\n",i,input_line_name[i].c_str(),j,list_input_netname[j].c_str());
			
		}
		if (flag_input_sp!=1) {
			print_gate_input(i,f);			
		}
	}
}

void generic_gate::print_output_sp(FILE* f) {
	int i,j,k,sa_number,sa,flag=0,flag_output_sp=0;
	char sa_name[255];
	string str_temp;
	for(i=0;i<outputs_number;i++) {
		flag_output_sp=0;
		transform(output_line_name[i].begin(),output_line_name[i].end(),output_line_name[i].begin(), ::tolower);		
		for (k=0; k<fan_out[i]; k++) {
			flag=0;
			fan_out_capa[i] = fan_out_capa[i] + ref_outputs[i].p_gg[k]->input_capa[ref_outputs[i].port[k]];
//			printf("gate %s input_capa[%d] = %f",ref_outputs[i].p_gg[k]->name,ref_outputs[i].port[k],input_capa[ref_outputs[i].port[k]]);
			for(j=0;j<list_subgate.size();j++) {
				if (strcmp(ref_outputs[i].p_gg[k]->name,list_subgate[j].c_str())==0) {
					flag=1;
					fan_out_capa[i] = fan_out_capa[i] - ref_outputs[i].p_gg[k]->input_capa[ref_outputs[i].port[k]];
				}
			}
			
			if (flag==0) {
				flag_output_sp=1;
			}	
		}	
		if (flag_output_sp==1) {
			sa=0;
			for(sa_number=0;sa_number<faultlist_number;sa_number++) {
				if(strcmp((stuckat_faultlist[sa_number]-> g)->name,name)==0 && 
				   (stuckat_faultlist[sa_number]-> port == i) && stuckat_faultlist[sa_number]->direction!=DIR_INPUT) {
#ifdef __DEBUG__	   
				printf("SA_number : %d of %s %d\n",sa_number,name,i);
#endif
				sa=	sa_number;
				}
			}	
		sprintf(sa_name,"stuckat_%d_",sa-1);
		str_temp=output_line_name[i];
		output_sp_name[i]=strcat(sa_name,str_temp.c_str());
		fprintf(f,".print tran V(%s) \n",output_sp_name[i].c_str());
//		fprintf(f,".probe tran V(%s) \n",output_sp_name[i].c_str());
//		fprintf(f,"c%s %s 0 %ff\n",output_line_name[i].c_str(),output_sp_name[i].c_str(),fan_out_capa[i]);
		netlist_cap_name.push_back(output_line_name[i]);
		netlist_cap_value.push_back(fan_out_capa[i]);
		netlist_cap_netname.push_back(output_sp_name[i]);	
		} else {
		output_sp_name[i]=output_line_name[i];
		}
#ifdef __DEBUG__		
		printf("sa_name = %s \n",output_sp_name[i].c_str());
#endif		
	}
	
}

void generic_gate::print_gate_input(FILE* f) {
	int i;
	for(i=0;i<inputs_number;i++) {
		
		print_gate_input(i,f);
	}
}

void generic_gate::print_gate_input(int port,FILE* f) {
	int i,j, pre_value,lg;
	j = 0;
	transform(input_sp_name[port].begin(),input_sp_name[port].end(),input_sp_name[port].begin(), ::tolower);
	list_input_netname.push_back(input_sp_name[port]);	
	//if(strcmp(input_sp_name[port].c_str(),"vcc")==0) input_sp_name[port]="vdd!";
#ifdef __DEBUG__	
	printf("VS%s_%d %s 0 PWL (",name,port, input_sp_name[port].c_str());
#endif	
	fprintf(f,"VS%s_%d %s 0 PWL (",name,port, input_sp_name[port].c_str());
		for(j=0;j<input_patterns_number*10;j++) {
			if(gate_input_table[port][j][0]!=-1 && gate_input_table[port][j][1] != -1)
			//printf("Gate %s -> inupt value of port %d : time -> %5d  value -> %d \n",name,port, gate_input_table[port][j][0], gate_input_table[port][j][1]);
			if (j == 0) {
#ifdef __DEBUG__
				printf("%dp %0.1fv ",gate_input_table[port][j][0],(float)gate_input_table[port][j][1]*3.3);
#endif			
				fprintf(f,"%dp %0.1fv ",gate_input_table[port][j][0],(float)gate_input_table[port][j][1]*3.3);
				pre_value= gate_input_table[port][j][1];
			}
			else {
#ifdef __DEBUG__			
				printf("%dp %0.1fv %dp %0.1fv ",gate_input_table[port][j][0]-10,(float)pre_value*3.3, gate_input_table[port][j][0]+10, (float)gate_input_table[port][j][1]*3.3);
#endif			
				
				fprintf(f,"%dp %0.1fv %dp %0.1fv ",gate_input_table[port][j][0]-200,(float)pre_value*3.3, gate_input_table[port][j][0]+200, (float)gate_input_table[port][j][1]*3.3);
				pre_value=gate_input_table[port][j][1];}
			}
#ifdef __DEBUG__			
		printf(")\n");
#endif
		fprintf(f,")\n");
		
	
}
void generic_gate::print_gate_input_debug() {
	int port,j, pre_value,lg;
	j = 0;
	for(port=0;port<inputs_number;port++) {
		for(j=0;j<input_patterns_number*10;j++) {
			if(gate_input_table[port][j][0]!=-1 && gate_input_table[port][j][1] != -1)
			printf("Gate %s -> inupt value of port %d (%s): time -> %5d  value -> %d \n",name,port,input_line_name[port].c_str(), gate_input_table[port][j][0], gate_input_table[port][j][1]);
			
		}
		printf("\n");
		
	}	
	
}
#endif
#ifdef __LOGIC_LEVELS__
int generic_gate::calculate_logic_level(int previous_level)
{
	int current, max;

	if (isFF && previous_level!=-1) return previous_level;

	current=previous_level+1;
	if (current>logic_level) {
		logic_level=current;
	}
	max=logic_level;
	if (isFF && previous_level==-1) {
		propagate_current_level (&max);
	} else {
		logic_level_input_defined++;
		if (logic_level_input_defined == inputs_number) {
			// It stores in "gates_basedon_logic_levels" (in generic_cell) this gate (at the right index)
			// First it verifies that the size of gates_basedon_logic_levels is good enough, otherwise it creates the index
			if ( ((int)parent_cell->gates_basedon_logic_levels.size()-1) < logic_level ) {
				parent_cell->gates_basedon_logic_levels.push_back ( vector<generic_gate*>() );
			}
			parent_cell->gates_basedon_logic_levels[logic_level].push_back(this);

			// It propagates the current level to the next gates
			propagate_current_level (&max);
#ifdef __DEBUG3__
			cout << "Level of gate " << name << ": " << logic_level << endl;
#endif
		}
	}
	return max;
}

void generic_gate::propagate_current_level (int *max) 
{
	int i,o,n;

	// It propagates the current level to the next gates
	for(o=0; o<outputs_number; o++) {
		for (i=0; i<fan_out[o]; i++) {
			n=ref_outputs[o].p_gg[i]->calculate_logic_level(logic_level);
			if (n>*max) *max=n;
		}
	}
}

#endif

