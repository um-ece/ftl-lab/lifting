///////////////////////////////////////////////////////////////////////
// generic_fault.cpp: implementation interface for the generic_fault class.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 03-11-2009
//////////////////////////////////////////////////////////////////////

#include "generic_fault.h"
#include "functions.h"

using namespace std;

/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = -1
 * Z = -2
 * ...
*/


generic_fault::generic_fault (generic_gate *g, int port, int direction) {
	this->g = g;
	this->port = port;
	this->direction = direction;
	this->detected = 0;

	getPortName (this->g, port, direction, this->portName);
	sprintf (this->name, "%s-%s", g->name, this->portName);
};



void generic_fault::print (ostream &os) {
	os << name << endl;
}
