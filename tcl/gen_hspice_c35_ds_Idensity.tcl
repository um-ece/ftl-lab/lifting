#!/usr/bin/tclsh8.4

set i 1
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}
puts $list_arg
#set file_name $arg_1
#set file_name [string trim $file_name {. / ".v"}]

#set flag_sim 0
#set flag_truthtable 0

set flag_subcell 0
set flag_gate 0
set flag_dbug 0
set n 0

if {([regexp -- "-t" $list_arg] == 1)} {
#puts $list_arg
	set table_name [lindex $list_arg [expr [lsearch $list_arg "-t"] + 1]]
#	puts $table_name
	set Filetable [open $table_name "r"]
}

if {([regexp -- "-l" $list_arg] == 1)} {
	set lib_name [lindex $list_arg [expr [lsearch $list_arg "-l"] + 1]]
#	puts $lib_name
	set Filelib [open $lib_name "r"]
}

if {([regexp -- "-o" $list_arg] == 1)} {
	set out_file [lindex $list_arg [expr [lsearch $list_arg "-o"] + 1]]
	set gh_Fileout [open $out_file "a+"]
	puts $gh_Fileout "** subcircuit with faults"
	puts $gh_Fileout ".include '$lib_name'"
}
if {([regexp -- "-dbug" $list_arg] == 1)} {
	set flag_dbug 1
}


gets $Filetable gh_Line;

while {([eof $Filetable] != 1)} {
	regsub -all \t $gh_Line "" gh_Line
#	puts $gh_Line
	if {([regexp  "time" $gh_Line] == 1)} {
		set T_start [lindex [split $gh_Line] 1]
		set T_stop [lindex [split $gh_Line] 2]
		set temp_time_start $T_start
		append T_start "p"
		append T_stop "p"
	
		
	}

	if {([regexp  "gate" $gh_Line] == 1)} {
		set gate_name [lindex [split $gh_Line] 1 ]
		set gate_type($gate_name) [lindex [split $gh_Line] 2]
		lappend gate_list $gate_name  
		set flag_empty_gate($gate_name) 1	
#		puts $gate_list 
#		puts $gate_type($gate_name)
	}
	
	if {([regexp  "subcell" $gh_Line] == 1)} {
		set subcell_name [lindex [split $gh_Line] 1 ]
		set subcell_type($subcell_name,$gate_name) [lindex [split $gh_Line] 2]
		set flag_empty_gate($gate_name) 0
		lappend subcell_list($gate_name) $subcell_name   
		set temp_tran_name ""
	}
	
	if {([regexp  "cmos" $gh_Line] == 1)} {
		set tran_name [lindex [split $gh_Line] 1 ]
		set temp_net_name [lindex [split $gh_Line] 2 ]
		
		if {([regexp $tran_name $temp_tran_name] != 1)} { 
			lappend transistor($subcell_name,$gate_name) [lindex [split $gh_Line] 1 ]
		}
		set temp_tran_name $transistor($subcell_name,$gate_name)
		lappend net_name($tran_name,$subcell_name,$gate_name) [lindex [split $gh_Line] 2 ]
		set courant($temp_net_name,$tran_name,$subcell_name,$gate_name) [lindex [split $gh_Line] 3]
#		puts "tran_name = $tran_name"
	}
	
	


gets $Filetable gh_Line

}
close $Filetable



# ############# debug ###############
if {($flag_dbug == 1)} {
	puts "time_start = $T_start, time_stop = $T_stop"
    foreach gate_name $gate_list {
		puts "gate_name: $gate_name; gate_type($gate_name): $gate_type($gate_name)"
		#puts "$transistor($gate_name)"
		#set tran_temp $transistor($gate_name)
		foreach subcell_name $subcell_list($gate_name) {
		puts "subcell_name: $subcell_name; subcell_type($subcell_name,$gate_name):$subcell_type($subcell_name,$gate_name)"
			foreach tran_name $transistor($subcell_name,$gate_name) {
				foreach net_No $net_name($tran_name,$subcell_name,$gate_name) {
					puts "tran_name: $tran_name; courant($net_No,$tran_name,$subcell_name,$gate_name): $courant($net_No,$tran_name,$subcell_name,$gate_name)"
				}
			}
		}	
	}
}	
# ##################################

foreach gate_name $gate_list {
#   puts ".......... gate_name = $gate_name .............."
   
   if {($flag_empty_gate($gate_name)==1)} {
#		puts "ATTENTION : gate $gate_name is not injected fault!"
		set Filelib [open $lib_name "r"]
		gets $Filelib gh_Line;
		while {([eof $Filelib] != 1)} {
			if {([regexp  ".subckt $gate_type($gate_name)" $gh_Line] == 1)} {
				set flag_gate 1
				set fault_gate_type $gate_type($gate_name)
				append fault_gate_type "_$gate_name"
				regsub $gate_type($gate_name) $gh_Line $fault_gate_type gh_Line
				
#					puts $gh_Line
					
			}
			if {([regexp  ".ends $gate_type($gate_name)" $gh_Line] == 1)} {
				set flag_gate 0
				regsub $gate_type($gate_name) $gh_Line $fault_gate_type gh_Line
				puts $gh_Fileout $gh_Line
				puts $gh_Fileout "
				"
				
#					puts $gh_Line	
				
			}
			if {($flag_gate == 1)} {
				puts $gh_Fileout $gh_Line
			}		
			gets $Filelib gh_Line
		}

		close $Filelib
   }
   if {($flag_empty_gate($gate_name)==0)} {
	foreach subcell_name $subcell_list($gate_name) {
		set n 0
		set Filelib [open $lib_name "r"]
		gets $Filelib gh_Line;

		while {([eof $Filelib] != 1)} {
			if {([regexp  ".subckt $subcell_type($subcell_name,$gate_name)" $gh_Line] == 1)} {
				set flag_subcell 1
				set fault_cell_type $subcell_type($subcell_name,$gate_name)
				append fault_cell_type "_$gate_name"
				append fault_cell_type "_$subcell_name"
				regsub $subcell_type($subcell_name,$gate_name) $gh_Line $fault_cell_type gh_Line
			#	puts $gh_Line	
			}
			if {([regexp  ".ends $subcell_type($subcell_name,$gate_name)" $gh_Line] == 1)} {
				set flag_subcell 0
				regsub $subcell_type($subcell_name,$gate_name) $gh_Line $fault_cell_type gh_Line
				puts $gh_Fileout $gh_Line
				puts $gh_Fileout "
				"
			if {($flag_dbug == 1)} {
				puts $gh_Line	
			}
			}
			if {($flag_subcell == 1)} {
				puts $gh_Fileout $gh_Line	
				if {($flag_dbug == 1)} {
					puts $gh_Line	
				}	
				set tran_name [lindex [split $gh_Line] 0 ]
				#puts " transisitor_name = $tran_name" 
				set temp_tran_name $transistor($subcell_name,$gate_name)
				if {(([regexp "mn" $tran_name] == 1)||([regexp "mp" $tran_name] == 1))&&([regexp $tran_name $temp_tran_name] == 1)} {
					foreach net_No $net_name($tran_name,$subcell_name,$gate_name) {
						if { ($net_No == 1) } {
							set drain_name($tran_name,$subcell_name,$gate_name) [lindex [split $gh_Line] 1 ]
							set drain_area [lindex [split $gh_Line] 8 ]
						} 
						if { ($net_No != 1) } {
							set drain_name($tran_name,$subcell_name,$gate_name) [lindex [split $gh_Line] 3 ]
							set drain_area [lindex [split $gh_Line] 9 ]
						}						
						regsub -all "AD='" $drain_area "" drain_area
						regsub -all "AS='" $drain_area "" drain_area
						regsub -all "'" $drain_area "" drain_area
						if {([regexp "mn" $tran_name] == 1)} {
#							puts $gh_Fileout "Ilaser$n $drain_name($tran_name,$subcell_name,$gate_name) inh_gnd EXP(0 '1e9*sx*gt_mn1w*$courant($tran_name,$subcell_name,$gate_name)' $T_start '0.5*($T_stop-$T_start)' '$T_start + 0.5*($T_stop-$T_start)' '0.5*($T_stop-$T_start)')"
							puts $gh_Fileout "Ilaser$n $drain_name($tran_name,$subcell_name,$gate_name) inh_gnd EXP(0 '1e9*$drain_area*$courant($net_No,$tran_name,$subcell_name,$gate_name)' $T_start '0.4*($T_stop-$T_start)' '$T_start + 0.5*($T_stop-$T_start)' '0.5*($T_stop-$T_start)')"
							if {($flag_dbug == 1)} {
								puts  "Ilaser$n $drain_name($tran_name,$subcell_name,$gate_name) inh_gnd EXP(0 '1e9*sx*gt_mn1w*$courant($net_No,$tran_name,$subcell_name,$gate_name)' $T_start '0.5*($T_stop-$T_start)' $T_stop '0.4*($T_stop-$T_start)')"
							}
							incr n
						} 
						if {([regexp "mp" $tran_name] == 1)} {
#							puts $gh_Fileout "Ilaser$n inh_vdd $drain_name($tran_name,$subcell_name,$gate_name) EXP(0 '1e9*sx*gt_mp1w*$courant($tran_name,$subcell_name,$gate_name)' $T_start '0.5*($T_stop-$T_start)' '$T_start + 0.5*($T_stop-$T_start)' '0.5*($T_stop-$T_start)')"
							puts $gh_Fileout "Ilaser$n inh_vdd $drain_name($tran_name,$subcell_name,$gate_name) EXP(0 '1e9*$drain_area*$courant($net_No,$tran_name,$subcell_name,$gate_name)' $T_start '0.4*($T_stop-$T_start)' $T_stop '0.4*($T_stop-$T_start)')"
							if {($flag_dbug == 1)} {
								puts "Ilaser$n inh_vdd $drain_name($tran_name,$subcell_name,$gate_name) EXP(0 '1e9*sx*gt_mp1w*$courant($net_No,$tran_name,$subcell_name,$gate_name)' $T_start '0.5*($T_stop-$T_start)' '$T_start + 0.5*($T_stop-$T_start)' '0.5*($T_stop-$T_start)')"
							}
							incr n
						} 	
					}
				}		
			}	
		gets $Filelib gh_Line
		}
	close $Filelib
	}
	
	set Filelib [open $lib_name "r"]
	gets $Filelib gh_Line;
		while {([eof $Filelib] != 1)} {
			if {([regexp  ".subckt $gate_type($gate_name)" $gh_Line] == 1)} {
				set flag_gate 1
				set fault_gate_type $gate_type($gate_name)
				append fault_gate_type "_$gate_name"
				regsub $gate_type($gate_name) $gh_Line $fault_gate_type gh_Line
				if {($flag_dbug == 1)} {
					puts $gh_Line
				}	
			}
			if {([regexp  ".ends $gate_type($gate_name)" $gh_Line] == 1)} {
				set flag_gate 0
				regsub $gate_type($gate_name) $gh_Line $fault_gate_type gh_Line
				puts $gh_Fileout $gh_Line
				puts $gh_Fileout "
				"
				if {($flag_dbug == 1)} {
					puts $gh_Line	
				}
			}
			if {($flag_gate == 1)} {
						
				set subcell_name [lindex [split $gh_Line] 0 ]
				set temp_subcell_name $subcell_list($gate_name)
				if {([regexp $subcell_name $temp_subcell_name] == 1)} {
					set fault_cell_type $subcell_type($subcell_name,$gate_name)
					append fault_cell_type "_$gate_name"
					append fault_cell_type "_$subcell_name"
					regsub $subcell_type($subcell_name,$gate_name) $gh_Line $fault_cell_type gh_Line
					if {($flag_dbug == 1)} {
						puts $gh_Line 		 
					}	
				}
				puts $gh_Fileout $gh_Line
			}		
			gets $Filelib gh_Line
		}

		close $Filelib
	puts "done!"	
	}
}
puts $gh_Fileout ".END"
 close $gh_Fileout
 
foreach gate_name $gate_list {
	if {($flag_empty_gate($gate_name)==0)} { 
		foreach subcell_name $subcell_list($gate_name) {	
			foreach tran_name $transistor($subcell_name,$gate_name) {
				foreach net_No $net_name($tran_name,$subcell_name,$gate_name) {
					unset courant($net_No,$tran_name,$subcell_name,$gate_name)
				}	
				unset drain_name($tran_name,$subcell_name,$gate_name) 
				
			}
		}	
	}
}
foreach gate_name $gate_list {
	if {($flag_empty_gate($gate_name)==0)} { 
		foreach subcell_name $subcell_list($gate_name) {	
			unset subcell_type($subcell_name,$gate_name)
			foreach tran_name $transistor($subcell_name,$gate_name) {
				unset net_name($tran_name,$subcell_name,$gate_name)
			}
			unset transistor($subcell_name,$gate_name)
		}	
		unset subcell_list($gate_name)
	}
}
foreach gate_name $gate_list {
	unset gate_type($gate_name)
#	if {($flag_empty_gate($gate_name)==0)} {
#		unset n
#	}
	unset flag_empty_gate($gate_name)
	
}
if {($flag_gate == 1)} {
	unset temp_subcell_name
	unset fault_cell_type
	unset subcell_name
}
if {($flag_subcell == 1)} {
unset temp_tran_name
unset tran_name
}


unset fault_gate_type
unset gate_list
unset gate_name
unset T_start
unset T_stop
unset gh_Line
unset flag_subcell
unset flag_gate
unset n


exit	
