#!/usr/bin/tclsh8.4

set i 1
set flag_netlist_start 0
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}

if {([regexp -- "-i" $list_arg] == 1)} {
	set hspice_wholetime_lib_name [lindex $list_arg [expr [lsearch $list_arg "-i"] + 1]]
	set hspice_wholetime_lib [open $hspice_wholetime_lib_name "r"]
}
if {([regexp -- "-c" $list_arg] == 1)} {
	set circuit_name [lindex $list_arg [expr [lsearch $list_arg "-c"] + 1]]
	
}

	set Fault_table_nf_name "table_fault_"
	append Fault_table_nf_name $circuit_name
	append Fault_table_nf_name "_all_nf.txt"
	set Fault_table_nf [open $Fault_table_nf_name "w"]



gets $hspice_wholetime_lib hwl_Line;
while {([eof $hspice_wholetime_lib] != 1)} {
	if {([regexp  " Cell name: $circuit_name" $hwl_Line] == 1)} {
		gets $hspice_wholetime_lib hwl_Line;
		if {([regexp  " View name: schematic" $hwl_Line] == 1)} {
			set flag_netlist_start 1	
			gets $hspice_wholetime_lib hwl_Line;
		}
	}
	if {([regexp -nocase ".END" $hwl_Line] == 1)} {
		set flag_netlist_start 0
	}
	if {($flag_netlist_start == 1)} {
	set gate_name [lindex $hwl_Line 0]
	regsub x $gate_name "" gate_name
	regsub u $gate_name "U" gate_name
	set gate_type [lindex $hwl_Line end]
	puts $Fault_table_nf "gate $gate_name $gate_type \{ \n \}"
	}
	gets $hspice_wholetime_lib hwl_Line;
}
close $hspice_wholetime_lib
close $Fault_table_nf
exit