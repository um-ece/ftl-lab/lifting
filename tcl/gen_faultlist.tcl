#!/usr/bin/tclsh8.4

set i 1
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}
set data_start_flag 0
set n 0
set vth 1.65
if {([regexp -- "-sp" $list_arg] == 1)} {
	set result_file_name [lindex $list_arg [expr [lsearch $list_arg "-sp"] + 1]]

	set Fileresult [open $result_file_name "r"]
}



if {([regexp -- "-o" $list_arg] == 1)} {
	set out_file [lindex $list_arg [expr [lsearch $list_arg "-o"] + 1]]
	set Fileout [open $out_file "w"]
}
if {([regexp -- "-vth" $list_arg] == 1)} {
	set vth [lindex $list_arg [expr [lsearch $list_arg "-vth"] + 1]]
}
if {([regexp -- "-dbug" $list_arg] == 1)} {
	set flag_dbug 1
}


gets $Fileresult Line;

while {([eof $Fileresult] != 1)} {
	
	if {([regexp  "transient analysis" $Line] == 1)} {
		set data_start_flag 1	
		
	}
    if {($data_start_flag ==1)} {
		regsub -all "_" $Line " " Line
	}
	if {([regexp "stuckat" $Line] == 1)&&($data_start_flag ==1)} {
		set fault_index [lindex $Line 1]
		set n 0
		lappend list_index $fault_index
		
		puts $Line 
		puts "Fault_Index is $fault_index"
	
	}
	if {([regexp  {\-?([1-9]\d*\.\d*e(\-\+)?\d*)} $Line] == 1)&&($data_start_flag ==1)} {
		
		lappend time($fault_index) [format "%.0f" [expr [lindex $Line 0]*1000000000000]]
		lappend value($fault_index) [lindex $Line 1]
		
#		puts "$Line : [lindex $time($fault_index) $n] $n"
		incr n 1
	}

gets $Fileresult Line

}
close $Fileresult

foreach fault_index $list_index {
	lappend fault_list "[lindex $time($fault_index) 0]"
	for {set n 0} {$n < [expr [llength $time($fault_index)] -1] } {incr n 1} {
		#puts "[llength $time($fault_index)] $n [lindex $time($fault_index) $n]"
		if {([lindex $value($fault_index) $n] < $vth)&&([lindex $value($fault_index) [expr $n+1]] >= $vth)} {
		lappend fault_list "[lindex $time($fault_index) $n] $fault_index;[lindex $time($fault_index) [expr $n+1]]"
		}
		if {([lindex $value($fault_index) $n] > $vth)&&([lindex $value($fault_index) [expr $n+1]] <= $vth)} {
		lappend fault_list "[lindex $time($fault_index) $n] [expr $fault_index +1];[lindex $time($fault_index) [expr $n+1]]"
		}
	}
	if {([lindex $value($fault_index) end] > $vth)} {
		lappend fault_list "[lindex $time($fault_index) end] [expr $fault_index +1];"
	} else {
		lappend fault_list "[lindex $time($fault_index) end] $fault_index;"
	}
}

regsub -all "{" $fault_list "" fault_list
regsub -all "}" $fault_list "" fault_list
regsub  {(;)$} $fault_list "" fault_list
puts $Fileout $fault_list
exit	
