#!/usr/bin/tclsh8.5

set i 1
set flag_dbug 0
set flag_output 0
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}

if {([regexp -- "-i" $list_arg] == 1)} {
	set simulation_table_name [lindex $list_arg [expr [lsearch $list_arg "-i"] + 1]]
	set simulation_list [open $simulation_table_name "r"]
	set source_dir_name "[file dirname $simulation_table_name]"
}
if {([regexp -- "-o" $list_arg] == 1)} {
	set simulation_result_name [lindex $list_arg [expr [lsearch $list_arg "-o"] + 1]]
	set simulation_result [open $simulation_result_name "w"]
	set flag_output 1
}
if {([regexp -- "-dbug" $list_arg] == 1)} {
	set flag_dbug 1
}
set simulation_no 0
file mkdir "$source_dir_name/table_fault"
file mkdir "$source_dir_name/Hspice_file"
file mkdir "$source_dir_name/fault_list"
file mkdir "$source_dir_name/waveform"
file mkdir "$source_dir_name/output_file"
file mkdir "$source_dir_name/Matlab_file"

gets $simulation_list Line;
gets $simulation_list Line;
while {([eof $simulation_list] != 1 && [regexp ".end" $Line] != 1)} {	
set circuit_name [lindex $Line 0]
set level_no [lindex $Line 1]
set level "level$level_no"
set from_x [lindex $Line 2]
set from_y [lindex $Line 3]
set to_x [lindex $Line 4]
set to_y [lindex $Line 5]
set radius [lindex $Line 6]
set laser_fault_start [lindex $Line 7]
set laser_fault_end [lindex $Line 8]
set power [lindex $Line 9]
#set power [format "%.3f" [lindex $Line 9]]
set vth [lindex $Line 10]
set sum_input [lindex $Line 11]
set random_flag [lindex $Line 12]
set row 0
set column 0
set sum_byte_2_fault 0
set sum_byte_3_fault 0
set sum_no_detc_fault 0

for {set simulation_no 0} {$simulation_no < $random_flag} {incr simulation_no 1} {
	
		

set in_no [expr round([expr $sum_input * [expr rand()]])]
set center_x [expr $from_x +[expr round([expr ($to_x - $from_x) * [expr rand()]])]]
set center_y [expr $from_y +[expr round([expr ($to_y - $from_y) * [expr rand()]])]]

# ####### files setup ############		
		set temp_sim_file_name $circuit_name
		append temp_sim_file_name "_$level"
		append temp_sim_file_name "_$center_x"
		append temp_sim_file_name "_$center_y"
		append temp_sim_file_name "_$radius"
		append temp_sim_file_name "_$laser_fault_start"
		append temp_sim_file_name "_$laser_fault_end"
		append temp_sim_file_name "_$power"
		append temp_sim_file_name "_$vth"
		if {($random_flag != 1)} {
			append temp_sim_file_name "_in$in_no"
		}
		if {($random_flag == 1)} {
			append temp_sim_file_name "_inr$in_no"
		}		
# ###### Fault table #######################
		set fault_table "$source_dir_name/table_fault/table_fault_$temp_sim_file_name"
		append fault_table ".txt"
# ######end of Fault table #######################
		set hspice_faulttime_lib $circuit_name
#append hspice_faulttime_lib "_FaultTime_lib.sp"
		append hspice_faulttime_lib "_lib.sp"
		set hspice_lib_name $hspice_faulttime_lib
		set hspice_faulttime_lib "$source_dir_name/$hspice_lib_name"
		set netlist_file $source_dir_name/$circuit_name
		append netlist_file ".v"
		if {($random_flag != 1)} {
			set input_file $source_dir_name/Input_file/$circuit_name
		}
		if {($random_flag == 1)} {
			set input_file $source_dir_name/Input_file_r/$circuit_name
		}
		append input_file "_input_$in_no.txt"
		set sdf_file $source_dir_name/$circuit_name
		append sdf_file ".sdf"
		set def_file $source_dir_name/$circuit_name
		append def_file ".def"
		set sp_result_file "$source_dir_name/Hspice_file/$temp_sim_file_name"
		set fault_list_file "$source_dir_name/fault_list/fault_list_"
		append fault_list_file $temp_sim_file_name
		set waveform_file "$source_dir_name/waveform/wf_"
		append waveform_file $temp_sim_file_name
		append waveform_file ".txt"
		set tlifting_output_file "$source_dir_name/output_file/output_fault_"
		append tlifting_output_file $temp_sim_file_name
		append tlifting_output_file ".txt"
		append fault_list_file ".txt"
		set subcircuit_sp_file "$source_dir_name/Hspice_file/$temp_sim_file_name"
		append subcircuit_sp_file ".sp"
		set temp_spice_file $sp_result_file
		append sp_result_file ".lis"
# ####### end of files setup ############
		puts "\n Simulation No.$simulation_no : $level laser_center($center_x, $center_y) r($radius) t($laser_fault_start, $laser_fault_end) input($in_no)"
		if {($flag_output != 0)} {
			puts $simulation_result "\n Simulation No.$simulation_no : $level laser_center($center_x, $center_y) r($radius) t($laser_fault_start, $laser_fault_end) input($in_no)"
		}
		

		if {($flag_dbug == 1)} {
			puts " DEF file 			 :	 $def_file"
			puts " SDF file 			 :	 $sdf_file"
			puts " Input stimuli			 : 	 $input_file"
			puts " Circuit netlist		 :	 $netlist_file"
			puts "\n Hspice lib			 : 	 $hspice_faulttime_lib"
			puts " Subcircuit hspice file	 	 :	 $subcircuit_sp_file"
			puts " Hspice result file	 	 :	 $sp_result_file"
			puts " Fault list for tLIFTING	 :	 $fault_list_file"
			puts " Waveform of tLIFTING		 : 	 $waveform_file"
			puts " Output file of tLIFTING	 :	 $tlifting_output_file"
			}


# ----------------step1----------------
		if {($flag_dbug == 1)} {
			puts "./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -rdef $def_file -lpara $center_x $center_y $radius $laser_fault_start $laser_fault_end $power -wlf $fault_table -osp -level $level_no $subcircuit_sp_file -pfl $source_dir_name/fault_dic.txt -fs multiplesa -fl $source_dir_name/fault_list.txt"
		}
		exec ./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -rdef $def_file -lpara $center_x $center_y $radius $laser_fault_start $laser_fault_end $power -wlf $fault_table -osp -level $level_no -force_sim_time $subcircuit_sp_file -pfl $source_dir_name/fault_dic.txt -fs multiplesa -fl $source_dir_name/fault_list.txt
# ----------------step2----------------
			set Filetable [open $fault_table "r"]
			set gh_Fileout [open $subcircuit_sp_file "a"]
			set flag_spice_empty [tell $gh_Fileout]
			if {($flag_spice_empty !=0)} {
				puts $gh_Fileout ".include '../$hspice_lib_name'"
				#source ./gen_hspice_c35_bis.tcl
				source ./tcl/gen_hspice_c35_ds_Idensity_bis.tcl
				# ## time_start & time_end : for comparation 
				set time_start $temp_time_start
				set time_end 555000
				# ----------------step3----------------
				if {($flag_dbug == 1)} {
					puts "hspice $subcircuit_sp_file -o $sp_result_file"
				}
				catch {exec hspice $subcircuit_sp_file -o $sp_result_file}
				source ./tcl/gen_faultlist_bis.tcl
				# ----------------step4----------------
				if {($flag_dbug == 1)} {
					puts "./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -fs multiplesa -fl $fault_list_file -owf $waveform_file -of $tlifting_output_file"
				}
			#	exec ./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -fs multiplesa -fl $fault_list_file -owf "$waveform_file" -of "$tlifting_output_file"
			set tlifting_line [exec ./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -fs multiplesa -fl $fault_list_file ] 
				if {([regexp "fault_free_simulation_result:" $tlifting_line] == 1)} {
					set fault_free_result [lindex $tlifting_line [expr [lsearch $tlifting_line "fault_free_simulation_result:"] + 1]]
					puts $fault_free_result
				}
				if {([regexp "fault_simulation_result:" $tlifting_line] == 1)} {
					set fault_result [lindex $tlifting_line [expr [lsearch $tlifting_line "fault_simulation_result:"] + 1]]
					puts $fault_result
				}
				
				set sp_tr_file $temp_spice_file
				set sp_ic_file $temp_spice_file
				set sp_st_file $temp_spice_file
				set sp_pa_file $temp_spice_file
				append sp_tr_file ".tr0"
				append sp_ic_file ".ic0"
				append sp_st_file ".st0"
				append sp_pa_file ".pa0"
				
			#	exec rm $sp_result_file
			#	exec rm $sp_tr_file
			#	exec rm $sp_pa_file
			#	exec rm $sp_st_file
			#	exec rm $sp_ic_file

			} 
			if {($flag_spice_empty ==0)} {
				close $gh_Fileout
				if {($flag_dbug == 1)} {
					puts "\n There is NOT a FAULT injected"
				}	
				if {($flag_output != 0)} {
				puts $simulation_result "\n faults(0) = ()"
				puts $simulation_result " Detection signal = 0"
				}
				puts "\n faults(0) = ()"
				puts " Detection signal = 0"
			}

			set byte_0_fault_list ""
			set byte_1_fault_list ""
			set byte_2_fault_list ""
			set byte_3_fault_list ""
			set fault_bit_no 0
			set flag_byte_0_fault 0
			set flag_byte_1_fault 0
			set flag_byte_2_fault 0
			set flag_byte_3_fault 0
			if {($flag_spice_empty !=0)} {
				for {set bit 31} {$bit>=0} {incr bit -1} {
					set golden_bit [string index $fault_free_result $bit]
					set fault_bit [string index $fault_result $bit]
					if {($golden_bit != $fault_bit)} {
						if {($bit <= 31)&&($bit > 23)} {
							lappend byte_0_fault_list $bit
							incr fault_bit_no 1
							set flag_byte_0_fault 1
						}
						if {($bit <= 23)&&($bit > 15)} {
							lappend byte_1_fault_list $bit
							incr fault_bit_no 1
							set flag_byte_1_fault 1
						}
						if {($bit <= 15)&&($bit > 7)} {
							lappend byte_2_fault_list $bit
							incr fault_bit_no 1
							set flag_byte_2_fault 1
						}	
						if {($bit <= 7)} {
							lappend byte_3_fault_list $bit
							incr fault_bit_no 1
							set flag_byte_3_fault 1
						}		
					}
				}
			if {($flag_output != 0)} {
				puts $simulation_result "faults([llength $byte_3_fault_list]) = ($byte_3_fault_list)"
				puts $simulation_result "Detection signal = $flag_byte_2_fault"
				}	
			puts "faults([llength $byte_3_fault_list]) = ($byte_3_fault_list),  Detection signal = $flag_byte_2_fault"
			}
			
			incr sum_byte_2_fault $flag_byte_2_fault
			incr sum_byte_3_fault $flag_byte_3_fault
			if {(($flag_byte_3_fault == 1) && ($flag_byte_2_fault != 1))} {
				incr sum_no_detc_fault 1
			}
			puts "Error NO. : $sum_byte_3_fault ; Detected NO. : $sum_byte_2_fault ; No-detected Error No. : $sum_no_detc_fault"
			if {($flag_output != 0)} {
				puts $simulation_result "Error NO. : $sum_byte_3_fault ; Detected NO. : $sum_byte_2_fault ; No-detected Error No. : $sum_no_detc_fault"
				
				}

}

gets $simulation_list Line;
if {($flag_output != 0)} {
	puts $simulation_result "\n Error NO. : $sum_byte_3_fault  "
	puts $simulation_result " Detected NO. : $sum_byte_2_fault "
	puts $simulation_result " No-detected Error No. : $sum_no_detc_fault"
	}	
puts "\n Fault NO. : $sum_byte_3_fault  "
puts " Detected NO. : $sum_byte_2_fault "
puts " No-detected Error No. : $sum_no_detc_fault"
}


close $simulation_list 
if {($flag_output != 0)} {
	close $simulation_result
}
exit
