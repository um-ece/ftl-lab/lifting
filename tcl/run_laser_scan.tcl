#!/usr/bin/tclsh8.5

set i 1
set flag_dbug 0
set flag_output 0
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}

if {([regexp -- "-i" $list_arg] == 1)} {
	set simulation_table_name [lindex $list_arg [expr [lsearch $list_arg "-i"] + 1]]
	set simulation_list [open $simulation_table_name "r"]
	set source_dir_name "[file dirname $simulation_table_name]"
}
if {([regexp -- "-o" $list_arg] == 1)} {
	set simulation_result_name [lindex $list_arg [expr [lsearch $list_arg "-o"] + 1]]
	set simulation_result [open $simulation_result_name "w"]
	set flag_output 1
}
if {([regexp -- "-dbug" $list_arg] == 1)} {
	set flag_dbug 1
}
set simulation_no 0
file mkdir "$source_dir_name/table_fault"
file mkdir "$source_dir_name/Hspice_file"
file mkdir "$source_dir_name/fault_list"
file mkdir "$source_dir_name/waveform"
file mkdir "$source_dir_name/output_file"
file mkdir "$source_dir_name/Matlab_file"
set matlab_byte_distribution [open "$source_dir_name/Matlab_file/byte_distrbution.m" "w"]
set matlab_nomo_byte [open "$source_dir_name/Matlab_file/nomobyte_distrbution.m" "w"]
gets $simulation_list Line;
gets $simulation_list Line;
while {([eof $simulation_list] != 1 && [regexp ".end" $Line] != 1)} {	
set circuit_name [lindex $Line 0]
set level_no [lindex $Line 1]
set level "level$level_no"
set from_x [lindex $Line 2]
set from_y [lindex $Line 3]
set to_x [lindex $Line 4]
set to_y [lindex $Line 5]
set radius [lindex $Line 6]
set laser_fault_start [lindex $Line 7]
set laser_fault_end [lindex $Line 8]
set power [lindex $Line 9]
#set power [format "%.3f" [lindex $Line 9]]
set vth [lindex $Line 10]
set sum_input [lindex $Line 11]
set random_flag [lindex $Line 12]
set row 0
set column 0


for {set center_y $from_y} {$center_y <= $to_y} {incr center_y [expr 2*$radius]} {
	for {set center_x $from_x} {$center_x <= $to_x} {incr center_x [expr 2*$radius]} {
		set sum_byte_0_fault 0
		set sum_byte_1_fault 0
		set sum_byte_2_fault 0
		set sum_byte_3_fault 0
		set sum_nomobyte_fault 0
		set sum_multibyte_fault 0
		for {set in_no 0} {$in_no < $sum_input} {incr in_no 1} {
# ####### files setup ############		
		set temp_sim_file_name $circuit_name
		append temp_sim_file_name "_$level"
		append temp_sim_file_name "_$center_x"
		append temp_sim_file_name "_$center_y"
		append temp_sim_file_name "_$radius"
		append temp_sim_file_name "_$laser_fault_start"
		append temp_sim_file_name "_$laser_fault_end"
		append temp_sim_file_name "_$power"
		append temp_sim_file_name "_$vth"
		if {($random_flag != 1)} {
			append temp_sim_file_name "_in$in_no"
		}
		if {($random_flag == 1)} {
			append temp_sim_file_name "_inr$in_no"
		}		
# ###### Fault table #######################
		set fault_table "$source_dir_name/table_fault/table_fault_$temp_sim_file_name"
		append fault_table ".txt"
# ######end of Fault table #######################
		set hspice_faulttime_lib $circuit_name
#append hspice_faulttime_lib "_FaultTime_lib.sp"
		append hspice_faulttime_lib "_lib.sp"
		set hspice_lib_name $hspice_faulttime_lib
		set hspice_faulttime_lib "$source_dir_name/$hspice_lib_name"
		set netlist_file $source_dir_name/$circuit_name
		append netlist_file ".v"
		if {($random_flag != 1)} {
			set input_file $source_dir_name/Input_file/$circuit_name
		}
		if {($random_flag == 1)} {
			set input_file $source_dir_name/Input_file_r/$circuit_name
		}
		append input_file "_input_$in_no.txt"
		set sdf_file $source_dir_name/$circuit_name
		append sdf_file ".sdf"
		set def_file $source_dir_name/$circuit_name
		append def_file ".def"
		set sp_result_file "$source_dir_name/Hspice_file/$temp_sim_file_name"
		set fault_list_file "$source_dir_name/fault_list/fault_list_"
		append fault_list_file $temp_sim_file_name
		set waveform_file "$source_dir_name/waveform/wf_"
		append waveform_file $temp_sim_file_name
		append waveform_file ".txt"
		set tlifting_output_file "$source_dir_name/output_file/output_fault_"
		append tlifting_output_file $temp_sim_file_name
		append tlifting_output_file ".txt"
		append fault_list_file ".txt"
		set subcircuit_sp_file "$source_dir_name/Hspice_file/$temp_sim_file_name"
		append subcircuit_sp_file ".sp"
		set temp_spice_file $sp_result_file
		append sp_result_file ".lis"
# ####### end of files setup ############
		puts "\n Simulation No.$simulation_no : $level laser_center($center_x, $center_y) r($radius) t($laser_fault_start, $laser_fault_end) input($in_no)"
		if {($flag_output != 0)} {
			puts $simulation_result "\n Simulation No.$simulation_no : $level laser_center($center_x, $center_y) r($radius) t($laser_fault_start, $laser_fault_end) input($in_no)"
		}
		incr simulation_no 1

		if {($flag_dbug == 1)} {
			puts " DEF file 			 :	 $def_file"
			puts " SDF file 			 :	 $sdf_file"
			puts " Input stimuli			 : 	 $input_file"
			puts " Circuit netlist		 :	 $netlist_file"
			puts "\n Hspice lib			 : 	 $hspice_faulttime_lib"
			puts " Subcircuit hspice file	 	 :	 $subcircuit_sp_file"
			puts " Hspice result file	 	 :	 $sp_result_file"
			puts " Fault list for tLIFTING	 :	 $fault_list_file"
			puts " Waveform of tLIFTING		 : 	 $waveform_file"
			puts " Output file of tLIFTING	 :	 $tlifting_output_file"
			}


# ----------------step1----------------
		if {($flag_dbug == 1)} {
			puts "./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -rdef $def_file -lpara $center_x $center_y $radius $laser_fault_start $laser_fault_end $power -wlf $fault_table -osp -level $level_no $subcircuit_sp_file -pfl $source_dir_name/fault_dic.txt -fs multiplesa -fl $source_dir_name/fault_list.txt"
		}
		exec ./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -rdef $def_file -lpara $center_x $center_y $radius $laser_fault_start $laser_fault_end $power -wlf $fault_table -osp -level $level_no -force_sim_time $subcircuit_sp_file -pfl $source_dir_name/fault_dic.txt -fs multiplesa -fl $source_dir_name/fault_list.txt
# ----------------step2----------------
			set Filetable [open $fault_table "r"]
			set gh_Fileout [open $subcircuit_sp_file "a"]
			set flag_spice_empty [tell $gh_Fileout]
			if {($flag_spice_empty !=0)} {
				puts $gh_Fileout ".include '../$hspice_lib_name'"
				#source ./gen_hspice_c35_bis.tcl
				source ./tcl/gen_hspice_c35_ds_Idensity_bis.tcl
				# ## time_start & time_end : for comparation 
				set time_start $temp_time_start
				set time_end 555000
				# ----------------step3----------------
				if {($flag_dbug == 1)} {
					puts "hspice $subcircuit_sp_file -o $sp_result_file"
				}
				catch {exec hspice $subcircuit_sp_file -o $sp_result_file}
				source ./tcl/gen_faultlist_bis.tcl
				# ----------------step4----------------
				if {($flag_dbug == 1)} {
					puts "./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -fs multiplesa -fl $fault_list_file -owf $waveform_file -of $tlifting_output_file"
				}
			#	exec ./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -fs multiplesa -fl $fault_list_file -owf "$waveform_file" -of "$tlifting_output_file"
			set tlifting_line [exec ./tlifting -l c35 -n $netlist_file -i $input_file -rsdf $sdf_file 1 1000 -fs multiplesa -fl $fault_list_file ] 
				if {([regexp "fault_free_simulation_result:" $tlifting_line] == 1)} {
					set fault_free_result [lindex $tlifting_line [expr [lsearch $tlifting_line "fault_free_simulation_result:"] + 1]]
					puts $fault_free_result
				}
				if {([regexp "fault_simulation_result:" $tlifting_line] == 1)} {
					set fault_result [lindex $tlifting_line [expr [lsearch $tlifting_line "fault_simulation_result:"] + 1]]
					puts $fault_result
				}
				
				set sp_tr_file $temp_spice_file
				set sp_ic_file $temp_spice_file
				set sp_st_file $temp_spice_file
				set sp_pa_file $temp_spice_file
				append sp_tr_file ".tr0"
				append sp_ic_file ".ic0"
				append sp_st_file ".st0"
				append sp_pa_file ".pa0"
				
			#	exec rm $sp_result_file
			#	exec rm $sp_tr_file
			#	exec rm $sp_pa_file
			#	exec rm $sp_st_file
			#	exec rm $sp_ic_file

			} 
			if {($flag_spice_empty ==0)} {
				close $gh_Fileout
				if {($flag_dbug == 1)} {
					puts "\n There is NOT a FAULT injected"
				}	
				if {($flag_output != 0)} {
				puts $simulation_result "0 bit(s) error in 0 bytes : (0, 0, 0, 0)"
				}
				puts "0 bit(s) error in 0 bytes : (0, 0, 0, 0)"
			}

			set byte_0_fault_list ""
			set byte_1_fault_list ""
			set byte_2_fault_list ""
			set byte_3_fault_list ""
			set fault_bit_no 0
			set flag_byte_0_fault 0
			set flag_byte_1_fault 0
			set flag_byte_2_fault 0
			set flag_byte_3_fault 0
			if {($flag_spice_empty !=0)} {
				for {set bit 31} {$bit>=0} {incr bit -1} {
					set golden_bit [string index $fault_free_result $bit]
					set fault_bit [string index $fault_result $bit]
					if {($golden_bit != $fault_bit)} {
						if {($bit <= 31)&&($bit > 23)} {
							lappend byte_0_fault_list $bit
							incr fault_bit_no 1
							set flag_byte_0_fault 1
						}
						if {($bit <= 23)&&($bit > 15)} {
							lappend byte_1_fault_list $bit
							incr fault_bit_no 1
							set flag_byte_1_fault 1
						}
						if {($bit <= 15)&&($bit > 7)} {
							lappend byte_2_fault_list $bit
							incr fault_bit_no 1
							set flag_byte_2_fault 1
						}	
						if {($bit <= 7)} {
							lappend byte_3_fault_list $bit
							incr fault_bit_no 1
							set flag_byte_3_fault 1
						}		
					}
				}
			if {($flag_output != 0)} {
				puts $simulation_result "$fault_bit_no bit(s) error in [expr $flag_byte_0_fault+$flag_byte_1_fault+$flag_byte_2_fault+$flag_byte_3_fault] bytes : ([llength $byte_3_fault_list], [llength $byte_2_fault_list], [llength $byte_1_fault_list], [llength $byte_0_fault_list])"
			}	
			puts "$fault_bit_no bit(s) error in [expr $flag_byte_0_fault+$flag_byte_1_fault+$flag_byte_2_fault+$flag_byte_3_fault] bytes : ([llength $byte_3_fault_list], [llength $byte_2_fault_list], [llength $byte_1_fault_list], [llength $byte_0_fault_list])"
			puts "byte_3([llength $byte_3_fault_list]) = ($byte_3_fault_list)"
			puts "byte_2([llength $byte_2_fault_list]) = ($byte_2_fault_list)"
			puts "byte_1([llength $byte_1_fault_list]) = ($byte_1_fault_list)"
			puts "byte_0([llength $byte_0_fault_list]) = ($byte_0_fault_list)"
			}
			incr sum_byte_0_fault $flag_byte_0_fault
			incr sum_byte_1_fault $flag_byte_1_fault
			incr sum_byte_2_fault $flag_byte_2_fault
			incr sum_byte_3_fault $flag_byte_3_fault
		
			set byte_fault_no [expr $flag_byte_0_fault+$flag_byte_1_fault+$flag_byte_2_fault+$flag_byte_3_fault]
			if {($byte_fault_no ==1)} {
				incr sum_nomobyte_fault 1
			}	
			if {($byte_fault_no >1)} {
				incr sum_multibyte_fault 1	
			}
		}
		
	puts " row= $row, column= $column, ($sum_byte_0_fault, $sum_byte_1_fault, $sum_byte_2_fault, $sum_byte_3_fault) ($sum_nomobyte_fault, $sum_multibyte_fault)"
	if {($flag_output != 0)} {
		puts $simulation_result " row= $row, column= $column, ($sum_byte_0_fault, $sum_byte_1_fault, $sum_byte_2_fault, $sum_byte_3_fault) ($sum_nomobyte_fault, $sum_multibyte_fault)"
	}
	# ---------------- colorier -----------------
	set byte_0_color_perc [format "%.2f" [expr [format "%.2f" $sum_byte_0_fault]/$sum_input]] 	
	set byte_1_color_perc [format "%.2f" [expr [format "%.2f" $sum_byte_1_fault]/$sum_input]] 
	set byte_2_color_perc [format "%.2f" [expr [format "%.2f" $sum_byte_2_fault]/$sum_input]] 
	set byte_3_color_perc [format "%.2f" [expr [format "%.2f" $sum_byte_3_fault]/$sum_input]] 
	set nomobyte_perc [format "%.2f" [expr [format "%.2f" $sum_nomobyte_fault]/$sum_input]]
	set multibyte_perc [format "%.2f" [expr [format "%.2f" $sum_multibyte_fault]/$sum_input]]
	set nousefault_perc [format "%.2f" [expr 1-([format "%.2f" $sum_nomobyte_fault]/$sum_input)]]
#	puts "byte_0_color_perc = $byte_0_color_perc"
#	puts "byte_1_color_perc = $byte_1_color_perc"
#	puts "byte_2_color_perc = $byte_2_color_perc"
#	puts "byte_3_color_perc = $byte_3_color_perc"
	
	set byte_nomo_r [expr 1-$nomobyte_perc] 
	set byte_nomo_g [expr 1-$nomobyte_perc]
	set byte_nomo_b [expr 1-$nomobyte_perc]
#	set byte_multi_r 1 
#	set byte_multi_g [expr 1-$multibyte_perc]
#	set byte_multi_b [expr 1-$multibyte_perc]
	
	set byte_0_color_r [expr 1-$byte_0_color_perc] 
	set byte_0_color_g 1
	set byte_0_color_b [expr 1-$byte_0_color_perc]
	
	set byte_1_color_r 1 
	set byte_1_color_g [expr 1-$byte_1_color_perc]
	set byte_1_color_b [expr 1-$byte_1_color_perc]
	
	set byte_2_color_r 1
	set byte_2_color_g 1 
	set byte_2_color_b [expr 1-$byte_2_color_perc]
	
	set byte_3_color_r [expr 1-$byte_3_color_perc] 
	set byte_3_color_g [expr 1-$byte_3_color_perc]
	set byte_3_color_b 1
	
	set temp_color_r [expr  $byte_0_color_r * $byte_1_color_r * $byte_2_color_r * $byte_3_color_r]
	set temp_color_g [expr  $byte_0_color_g * $byte_1_color_g * $byte_2_color_g * $byte_3_color_g]
	set temp_color_b [expr  $byte_0_color_b * $byte_1_color_b * $byte_2_color_b * $byte_3_color_b]
	
#	set nomo_multi_color_r [expr  $byte_multi_r * $byte_nomo_r ]
#	set nomo_multi_color_g [expr  $byte_multi_g * $byte_nomo_g ]
#	set nomo_multi_color_b [expr  $byte_multi_b * $byte_nomo_b ]
	set nomo_multi_color_r $byte_nomo_r 
	set nomo_multi_color_g $byte_nomo_g 
	set nomo_multi_color_b $byte_nomo_b 
	
	puts $matlab_byte_distribution "rectangle ( 'Position', \[$column, $row, 1, 1\], 'Curvature', \[0, 0\], 'FaceColor', \[$temp_color_r, $temp_color_g, $temp_color_b\]);"
	puts $matlab_byte_distribution "% row= $row, column= $column, ($sum_byte_0_fault, $sum_byte_1_fault, $sum_byte_2_fault, $sum_byte_3_fault) "
	puts $matlab_byte_distribution "text($column+0.1,$row+0.5, '($sum_byte_0_fault,$sum_byte_1_fault,$sum_byte_2_fault,$sum_byte_3_fault)', 'FontSize',14);"
	#	puts  "rectangle ( 'Position', \[$column, $row, 1, 1\], 'Curvature', \[0, 0\], 'FaceColor', \[$temp_color_r, $temp_color_g, $temp_color_b\]);"
	puts $matlab_nomo_byte "rectangle ( 'Position', \[$column, $row, 1, 1\], 'Curvature', \[0, 0\], 'FaceColor', \[$nomo_multi_color_r, $nomo_multi_color_g, $nomo_multi_color_b\]);"
	puts $matlab_nomo_byte "% row= $row, column= $column, ($nomobyte_perc, $multibyte_perc, $nousefault_perc)"
	puts $matlab_nomo_byte "text($column+0.05,$row+0.5, '($nomobyte_perc, $multibyte_perc, $nousefault_perc)', 'Color', \[0.5, 1, 0.5\], 'FontSize',18);"
	incr column 1
	}

	incr row 1
	set column 0
}

gets $simulation_list Line;
}

close $matlab_byte_distribution 
close $matlab_nomo_byte 
close $simulation_list 
if {($flag_output != 0)} {
	close $simulation_result
}
exit
