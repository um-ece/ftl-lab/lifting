
set data_start_flag 0
set n 0

set Fileresult [open $sp_result_file "r"]
set gf_Fileout [open $fault_list_file "w"]

gets $Fileresult gf_Line;

while {([eof $Fileresult] != 1)} {
	
	if {([regexp  "transient analysis" $gf_Line] == 1)} {
		set data_start_flag 1	
		
	}
	if {([regexp  "job concluded" $gf_Line] == 1)} {
		set data_start_flag 0	
		
	}
	
    if {($data_start_flag ==1)} {
		regsub -all "_" $gf_Line " " gf_Line
	}
	if {([regexp "stuckat" $gf_Line] == 1)&&($data_start_flag ==1)} {
		set fault_index [lindex $gf_Line 1]
		set n 0
		lappend list_index $fault_index
		
#		puts $gf_Line 
		if {($flag_dbug == 1)} {
			puts "Fault_Index is $fault_index"
		}	
	
	}
	if {([regexp  {\-?([1-9]\d*\.\d*e(\-\+)?\d*)} $gf_Line] == 1)&&($data_start_flag ==1)} {
		
		lappend time($fault_index) [format "%.0f" [expr [lindex $gf_Line 0]*1000000000000]]
		lappend value($fault_index) [lindex $gf_Line 1]
		
#		puts "$gf_Line : [lindex $time($fault_index) $n] $n"
		incr n 1
	}

gets $Fileresult gf_Line

}
close $Fileresult

foreach fault_index $list_index {
	lappend fault_list "[lindex $time($fault_index) 0]"
	for {set n 0} {$n < [expr [llength $time($fault_index)] -1] } {incr n 1} {
		#puts "[llength $time($fault_index)] $n [lindex $time($fault_index) $n]"
		if {([lindex $value($fault_index) $n] < $vth)&&([lindex $value($fault_index) [expr $n+1]] >= $vth)} {
		lappend fault_list "[lindex $time($fault_index) $n] $fault_index;[lindex $time($fault_index) [expr $n+1]]"
		}
		if {([lindex $value($fault_index) $n] > $vth)&&([lindex $value($fault_index) [expr $n+1]] <= $vth)} {
		lappend fault_list "[lindex $time($fault_index) $n] [expr $fault_index +1];[lindex $time($fault_index) [expr $n+1]]"
		}
	}
	if {([lindex $value($fault_index) end] > $vth)} {
		lappend fault_list "[lindex $time($fault_index) end] [expr $fault_index +1];"
	} else {
		lappend fault_list "[lindex $time($fault_index) end] $fault_index;"
	}
}

regsub -all "{" $fault_list "" fault_list
regsub -all "}" $fault_list "" fault_list
regsub  {(;)$} $fault_list "" fault_list
puts $gf_Fileout $fault_list
	
close $gf_Fileout
foreach fault_index $list_index {
	unset time($fault_index) 
	unset value($fault_index)
}
unset fault_index
unset fault_list
unset data_start_flag
unset gf_Line
unset n
unset list_index
