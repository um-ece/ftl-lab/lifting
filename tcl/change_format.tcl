#!/usr/bin/tclsh8.4

set i 1
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}

if {([regexp -- "-i" $list_arg] == 1)} {
	set simulation_table_name [lindex $list_arg [expr [lsearch $list_arg "-i"] + 1]]
	set simulation_table [open $simulation_table_name "r"]
}

if {([regexp -- "-o" $list_arg] == 1)} {
	set excel_table_name [lindex $list_arg [expr [lsearch $list_arg "-o"] + 1]]
	set excel_table [open $excel_table_name "w"]
}

set temp_vth "	"
for {set threshold 165} {$threshold <= 165} {incr threshold 1} {
	append temp_vth "[expr [format "%.2f" $threshold] / 100]	"
}
#puts $excel_table $temp_vth

gets $simulation_table Line;
while {([eof $simulation_table] != 1)} {

	for {set level 1} {$level <= 3} {incr level 1} {
		puts $excel_table "level $level "
		puts $excel_table $temp_vth
		for {set current 430} {$current <= 730} {incr current 5} {
			set temp_current  $current
			append temp_current " "
			set output_Line "$temp_current	"
			for {set threshold 165} {$threshold <= 165} {incr threshold 1} {
				set Terr [format "%.5f" [lindex $Line 4]]
				set Tfault [format "%.5f" [lindex $Line 5]]
				set Ttotal [format "%.5f" [lindex $Line 6]]
				set Tdiff [format "%.5f" [lindex $Line 7]]
				set out_data [expr $Terr/$Tfault]
				if {$out_data== -1} {set out_data 0}
				append output_Line "$out_data	"
				gets $simulation_table Line;	
			}
			puts $excel_table $output_Line
		}
		
	}
}

close $excel_table
close $simulation_table
exit