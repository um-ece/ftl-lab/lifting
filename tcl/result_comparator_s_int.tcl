#!/usr/bin/tclsh8.4

set i 1
foreach arg $argv {
	set arg_$i $arg
	lappend list_arg $arg
	incr i
}
set data_start_flag 0
#set flag_time 0
set k 0
#set vth 1.65
#set time_start 0
#set time_end 0
set flag_dbug 0
set flag_fault_exist 0



# ######### read tlifting with fault result ########################
set k 0
gets $tlifting_f_result rc_Line;
regsub -all {in\(} $rc_Line "" rc_Line
regsub -all {out\(} $rc_Line "" rc_Line
regsub -all {\)`digital`logic_4} $rc_Line "" rc_Line
#puts $rc_Line
set net_index [lsearch $rc_Line $net_name]
#puts "net index $net_index" 
if {($net_index == -1)} {
	puts "Can't find net $net_name"
	exit
}
gets $tlifting_f_result rc_Line;	

while {([eof $tlifting_f_result] != 1)} {	
regsub -all {E-12} $rc_Line "" rc_Line
	if {([regexp {([0-9]+)} $rc_Line] == 1)} {		
		lappend time_tf [lindex $rc_Line 0]
		lappend value_tf [lindex $rc_Line $net_index]		
#		puts "$rc_Line : [lindex $time($fault_index) $k] $k"
		incr k 1
	}
gets $tlifting_f_result rc_Line
}
close $tlifting_f_result

#puts "$k  [lindex $time_tf end]"
lappend time_tlifting_f [lindex $time_tf 0]
lappend value_tlifting_f [lindex $value_tf 0]
set temp_value [lindex $value_tf 0]
set k 1
for {set i 1} {$i <= [lindex $time_tf end]  } {incr i 1} {
	if {([lindex $time_tf $k] == [lindex $time_tf [expr $k - 1]])} {
	incr k 1
	}
		if {([lindex $time_tf $k] != $i)} {
			lappend time_tlifting_f $i
			lappend value_tlifting_f $temp_value
		} 
		if {([lindex $time_tf $k] == $i )} {
			lappend time_tlifting_f $i
			lappend value_tlifting_f [lindex $value_tf $k]
			set temp_value [lindex $value_tf $k]
			#puts "$i [lindex $value_tf $k] $k [lindex $time_tf $k]"
			incr k 1
		
		}
		
}
# ######### end tlifting with fault result ########################


# ######### read hspice with fault result ########################
set data_start_flag 0
gets $hspice_f_result rc_Line;
while {([eof $hspice_f_result] != 1)} {
	
	if {([regexp  "transient analysis" $rc_Line] == 1)} {
		set data_start_flag 1	
		
	}
	if {([lindex $rc_Line 0] == "y")} {
		set data_start_flag 0
	}
    if {($data_start_flag ==1)} {
		regsub  "_" $rc_Line " " rc_Line
		regsub  "_" $rc_Line " " rc_Line
	}
	if {([regexp "stuckat" $rc_Line] == 1)&&($data_start_flag ==1)} {
		set flag_net_start 0
	}
	if {([regexp -nocase $net_name $rc_Line] == 1)&&($data_start_flag ==1)} {
		set flag_net_start 1	
	}
	if {([regexp  {\-?([1-9]\d*\.\d*e(\-\+)?\d*)} $rc_Line] == 1)&&($data_start_flag ==1)&&($flag_net_start ==1)} {
		
#		puts [lindex $rc_Line 0]
		lappend time_hspice_f [format "%.0f" [expr [lindex $rc_Line 0]*1000000000000]]
		if {([lindex $rc_Line 1] < $vth)} {
			lappend value_hspice_f 0
		}
		if {([lindex $rc_Line 1] >= $vth)} {
			lappend value_hspice_f 1
		}

	}
gets $hspice_f_result rc_Line
}
close $hspice_f_result
# ######### end hspice with fault result ########################
# ######### compare  results ########################

set k $time_end
#puts "[lindex $value_hspice_f $k]  [lindex $time_hspice_f $k]   [lindex $value_tlifting_f $k]  [lindex $time_tlifting_f $k]"
	if {([lindex $value_tlifting_f $k] != [lindex $value_hspice_f $k])} {
			set Serr 1
			if {([lindex $value_tlifting_f $k] == "X")} {
				set Serr 0.5
			}
		}
	if {([lindex $value_tlifting_f $k] == [lindex $value_hspice_f $k])} {
			set Serr 0
		}
set detect_time [lindex $time_tlifting_f $k]
set detect_value_tlifting [lindex $value_tlifting_f $k]
set detect_value_hspice [lindex $value_hspice_f $k]

#	for {set i 1} {$i <= [llength $value_tlifting_f]  } {incr i 1} {
#	puts $simulation_result "[lindex $value_tlifting_f $i]  [lindex $time_tlifting_f $i]"
#	}
	
#	for {set i 1} {$i <= [llength $time_tf]  } {incr i 1} {
#	puts "$i [lindex $value_tf $i]   [lindex $time_tf $i]"
#	}

unset time_tf
unset value_tf
unset time_tlifting_f
unset value_tlifting_f
unset time_hspice_f
unset value_hspice_f
