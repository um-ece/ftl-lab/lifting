#include "manageFaults.h"

// fault_to_exclude is added for double stuck-at simulation. In this case it is necessary to remove one fault from the faultlist.
// Then the fault simulation is executed and the algorithm has to look really in the fault list. 
// If "use_external_faultlist" was not set, it wouldn't work.
int in_faultlist (generic_gate* g, int port, int direction, int stuck)
{
	string s="";

	s=generateName_forFaultlist (g, port, direction, stuck);
	if (use_external_faultlist==0) {
	       if (s!=fault_to_exclude) {
		       return 1;
	       }
	       return 0;
	}
	return external_faultlist[s];
}


void add_stuckat (generic_gate* g, int *flIndex, int runInput, int runOutput)
{
	int port, stuck;
	int t,i;

	if (runInput==1) {
		for (port=0; port<g->inputs_number; port++) {
			for (stuck=0; stuck<2; stuck++) {
				if (in_faultlist(g, port, DIR_INPUT, stuck)) {
					stuckat_faultlist[*flIndex] = new stuckat_fault(g, port, DIR_INPUT, stuck);
				}
				(*flIndex)++;
			}
		}
	}
	if (runOutput==1) {
		for (port=0; port<g->outputs_number; port++) {
			for (stuck=0; stuck<2; stuck++) {
				if (in_faultlist(g, port, DIR_OUTPUT, stuck)) {
					stuckat_faultlist[*flIndex] = new stuckat_fault(g, port, DIR_OUTPUT, stuck);
				}
				(*flIndex)++;
			}
		}
	}
}


// faultlist line is in the form:  index_of_fault1 ; index_of_fault2 ; ...
// It creates the entry in faultCombinations at the index fl and it updates the max_faultCombinations according to the number of fault
// in the line temp
void parse_faultlist_line (int fl, char *temp)
{
	char* tok;
	int v,t_begin,t_end, max;

	if (strlen(temp)==0) {
		printf ("Fatal error: faultlist is not correct.\n");
		exit(-1);
	}
	max=0;
	tok=strtok(temp, ";");
	while (tok != NULL) {
		sscanf (tok, "%d %d %d", &t_begin, &t_end, &v);
		max++;
		faultCombinations[fl].push_back(v);
		faultBegin[fl].push_back(t_begin);
		faultEnd[fl].push_back(t_end);
		tok = strtok(NULL,";");
	}
	if (max>max_faultCombinations) {
		max_faultCombinations=max;
	}
}


// Fault list is managed with 2 main structures. The first contains the list of fault sites/effects.
// For instance for the stuck-at fault, the stuckat_faultlist vector contains the list of all fault sites (i.e., all the input and output
// node of each gate) for each stuck-at (0 and 1)
// A second vector contains, for each item, the list of faultsites that have to be forced (at the same time)
// For single stuck at fault, this vector contains the indexes of the first vector. For double stuckat fault, each item contains a couple a index
// of the first vector

void create_faultcombinations(char *filename)
{
	string s;
	char temp[1024];
	FILE *f;
	char sa[5], t[10], faultname[1000];
	int fl;
	int i,j;

	if (strlen(filename)==0) {
		use_external_faultlist=0;
		switch (param_fault_simulation) {
			case FS_STUCKAT: 
				faultCombinations_number = faultlist_number;
				faultCombinations = new vector<int> [faultCombinations_number];
				max_faultCombinations = 1;
				for (i=0; i<faultCombinations_number; i++) {
					faultCombinations[i].push_back(i);
				}
				break;
			case FS_DOUBLESTUCKAT:
				faultCombinations_number = (faultlist_number * (faultlist_number-1)) / 2;
				faultCombinations = new vector<int> [faultCombinations_number];
				max_faultCombinations = 2;
				fl=0;
				for (i=0; i<faultlist_number-1; i++) {
					for (j=i+1; j<faultlist_number; j++) {
						faultCombinations[fl].push_back(i);
						faultCombinations[fl].push_back(j);
						fl++;
					}
				}
				break;
			case FS_MULTIPLESTUCKAT:
				printf ("Fatal error: multiple stuck-at fault simulation cannot be performed without external faultlist.\n");
				printf ("Aborting...\n");
				exit(-1);
				break;
			case FS_BITFLIP: 
				printf ("Not yet implemented\n");
				printf ("Aborting...\n");
				exit(-1);
				break;
		}
	} else {
		use_external_faultlist=1;

		// First it reads the number fo fault combinations in the file
		if ((f=fopen(filename,"r"))==NULL) {
			printf ("ERROR: Cannot read faultlist file. Aborting.\n");
			exit(-1);
		}
		faultCombinations_number = 0;
		while (fgets(temp, 1024, f)) {
			faultCombinations_number++;
		}
		fclose (f);

		//printf ("Numero di guasti smarmellati come se piovesse: %d\n", faultCombinations_number);
		faultCombinations = new vector<int> [faultCombinations_number];
		faultBegin = new vector<int> [faultCombinations_number];
		faultEnd = new vector<int> [faultCombinations_number];
		max_faultCombinations = 0;
		f=fopen(filename,"r");
		fl=0;
		while (fgets(temp, 1024, f)) {
			parse_faultlist_line (fl, temp);
			fl++;
		}
		fclose (f);
	}
}

void create_faultlist(char* filename)
{
	int fl;
	int i;

	switch (param_fault_simulation) {
		case FS_STUCKAT: 
		case FS_DOUBLESTUCKAT:
		case FS_MULTIPLESTUCKAT:
			faultlist_number = 2 * top_level->faultsites_number; 
			stuckat_faultlist = new stuckat_fault* [faultlist_number];
			break;
		case FS_BITFLIP: 
			faultlist_number = top_level->faultsites_number; 
			break;
	}

	fl=0;
	for (i=0; i<top_level->input_number; i++) {
		if (param_fault_simulation==FS_STUCKAT || param_fault_simulation==FS_DOUBLESTUCKAT || param_fault_simulation==FS_MULTIPLESTUCKAT) {
			add_stuckat (top_level->list_PI[i], &fl, 0, 1);
		} 
	}
	for (i=0; i<top_level->output_number; i++) {
		if (param_fault_simulation==FS_STUCKAT || param_fault_simulation==FS_DOUBLESTUCKAT || param_fault_simulation==FS_MULTIPLESTUCKAT) {
			add_stuckat (top_level->list_PO[i], &fl, 1, 0);
		}
	}
	for (i=0; i<top_level->logic_gate_number; i++) {
		if (param_fault_simulation==FS_STUCKAT || param_fault_simulation==FS_DOUBLESTUCKAT || param_fault_simulation==FS_MULTIPLESTUCKAT) {
			add_stuckat (top_level->list_logic_gate[i], &fl, 1, 1);
		}
	}
	for (i=0; i<top_level->FF_noscan_number; i++) {
		if (param_fault_simulation==FS_STUCKAT || param_fault_simulation==FS_DOUBLESTUCKAT || param_fault_simulation==FS_MULTIPLESTUCKAT) {
			add_stuckat (top_level->list_FF_noscan[i], &fl, 1, 1);
		}
	}
	for (i=0; i<top_level->FF_scan_number; i++) {
		if (param_fault_simulation==FS_STUCKAT || param_fault_simulation==FS_DOUBLESTUCKAT || param_fault_simulation==FS_MULTIPLESTUCKAT) {
			add_stuckat (top_level->list_FF_scan[i], &fl, 1, 1);
		}
	}
	for (i=0; i<top_level->memory_number; i++) {
		if (param_fault_simulation==FS_STUCKAT || param_fault_simulation==FS_DOUBLESTUCKAT || param_fault_simulation==FS_MULTIPLESTUCKAT) {
			add_stuckat (top_level->list_memory[i], &fl, 1, 1);
		}
	}

	create_faultcombinations (filename);
}

// AB 23-02-09  
// added function to print the fault list

// to do 

// _AB

string generateName_forFaultlist (generic_gate* g, int port, int direction, int stuck)
{
	string s;
	char t[1024], t2[1024];
	getPortName (g, port, direction, t);
	sprintf (t2, "%d-%s/%s", stuck, g->name, t);
	s=t2;
	return s;
}

int removeFrom_faultlist (generic_gate* g, int port, int direction, int stuck)
{
	string s;
	s=generateName_forFaultlist (g, port, direction, stuck);
	fault_to_exclude=s;
	faultlist_number--;
	if (use_external_faultlist==1) {
		external_faultlist[s]=0;
	}
}

int addTo_faultlist (generic_gate* g, int port, int direction, int stuck)
{
	string s;
	s=generateName_forFaultlist (g, port, direction, stuck);
	fault_to_exclude="";
	faultlist_number++;
	if (use_external_faultlist==1) {
		external_faultlist[s]=1;
	}
}

void print_fault_list ()
{
	int i;
	FILE *f;

	f=fopen (param_fault_list_filename, "w");
	if (f==NULL) {
		printf ("\nERROR: cannot open faultlist file for writing.\n");
		return;
	}
	printf ("Writing fault list..."); fflush(stdout);
	for (i=0; i<faultlist_number; i++) {
		fprintf (f, "%d;%s\n", i, stuckat_faultlist[i]->name);
	}
	fclose(f);
	printf ("done.\n");
}
