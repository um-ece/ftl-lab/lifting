#include <stdio.h>
#include <stdlib.h>
#include "generic_gate.h"
#include "generic_ff.h"
#include "generic_cell.h"
#include "constants.h"
#include "manageFaults.h"
#include "time.h" 
#include "input_pattern.h"
#include "functions.h"

#include "techlib/common/pi.h"
#include "techlib/common/po.h"

#include "verilog-parser/verilog_driver.h"

#include "globalVariables.h"

#include "read_timing.h"
#include "def_parser.h"
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <iomanip> //---------25/10/2011------lu
using namespace std;


//Added comment from Feng


void run_simulation()
{
	int i,j,qt;

	simulation(1,1);
	// Store theresult in the golden_output_patterns vector
	for (i=0; i<input_patterns_number; i++) {
		if (input_patterns[i][0]==APPLY_PRIMARY_INPUT) {
			qt=top_level->output_number;
		} else {
			qt=top_level->FF_scan_number;
		}
		for (j=0; j<qt; j++) {
			golden_output_patterns[i][j]=output_patterns[i][j+1];
		}
	}
	// print the result
	if (strlen(param_output_filename)>0) {
		print_output(); // In functions.c
	}
	if (param_write_truth_fault_table==1) {
		print_truth_table("Fault free circuit"); // In functions.c
	}
}




void printFaultStatistics (int pattern_detected_faults, int i, FILE* f)
{
	float f1,f2;
	char s[1024];

	f1=(float)100*pattern_detected_faults/faultCombinations_number;
	f2=(float)100*detected_faults/faultCombinations_number;
	sprintf (s, "Input pattern %d...covered %d faults, total=%d (FC for this input: %.2f, total FC: %.2f)\n", i, pattern_detected_faults, 
		    detected_faults, f1, f2);
	printf ("%s", s);
	if (param_write_fault_statistic==1) {
		fprintf (f, "%s", s);
	}
}

void openStatisticFile (FILE** f)
{
	char s[1024];
	strcpy (s, param_fault_statistic_filename);
	strcat (s, ".report.txt");
	if (param_write_fault_statistic==1) {
		*f=fopen (s, "w");
		if (*f==NULL) {
			printf ("\nERROR: cannot open fault report files for writing. Fault report aborted.\n");
			param_write_fault_statistic=0;
		}
	} else {
		*f=NULL;
	}
}

// This function elaborates, for each input, all the faults that are still uncovered. It forces an input, than it checks whether with a fault
// the output changes. In this case it marks the fault as covered and the fault will be never simulated again (wiht another input vector)
// After each input, it stores the state of the whole circuit and it restore it after
void run_fs_stuckat_sequential()
{
	int i,j,gi,c;
	int pattern_detected_faults;
	generic_gate* g;
	FILE *f;
	int flIndex, index_of_the_fault;
	char **internal_states;

	openStatisticFile (&f);
	top_level->init();
	internal_states=(char**)malloc(faultCombinations_number*sizeof(char*));
	for (gi=0; gi<faultCombinations_number; gi++) {
		internal_states[i]=(char*)malloc((top_level->FF_scan_number+top_level->FF_noscan_number)*sizeof(char));
	}
	for (i=0; i<input_patterns_number; i++) {
		pattern_detected_faults=0;
		flIndex=0;
		// Set primary input values
		for (j=0; j<top_level->input_number; j++) {
			top_level->set_input (j, input_patterns[i][j+2]);
		}
		// For each fault
		for (gi=0; gi<faultCombinations_number; gi++) {
			// For each fault that is not yet detected
			if (faultlist_coverage[gi]==0) {
				// Restore the value of all flip flops
				if (i!=0) {
					top_level->setFF_status(internal_states[gi]);
					top_level->propagate_output_FF();
				}

				// Set all the stuck-at faults of this combination of fault
				for (c=0; c<faultCombinations[gi].size(); c++) {
					index_of_the_fault = faultCombinations[gi].at(c);
					stuckat_faultlist[faultCombinations[gi][c]]->injectFault();
				}

				// Propagate the output of the flip flops
				top_level->propagate_output_FF();

				// Check the outupt
				for (j=0; j<top_level->output_number; j++) {
					if (top_level->read_output(j) != golden_output_patterns[i][j]) {
						pattern_detected_faults++;
						faultlist_coverage[gi]=1;
						detected_faults++;
						break;
					}
				}
				// Remove all the stuck-at faults of this combination of fault
				for (c=0; c<faultCombinations[gi].size(); c++) {
					stuckat_faultlist[faultCombinations[gi][c]]->releaseFault();
				}

				// Save the value of all flip flops
				top_level->getFF_status(internal_states[gi]);
			}
		}

		// Statistics
		printFaultStatistics (pattern_detected_faults, i, f);
	}
	if (f!=NULL) {
		fclose (f);
	}
}

// This function elaborates, for each input, all the faults that are still uncovered. It forces an input, than it checks whether with a fault
// the output changes. In this case it marks the fault as covered and the fault will be never simulated again (wiht another input vector)
void run_fs_stuckat_combinational()
{
	int i,j,gi,c;
	int pattern_detected_faults;
	generic_gate* g;
	FILE *f;
	int flIndex, index_of_the_fault;

	openStatisticFile (&f);
	top_level->init();
	for (i=0; i<input_patterns_number; i++) {
		pattern_detected_faults=0;
		flIndex=0;
		// Set primary input values
		for (j=0; j<top_level->input_number; j++) {
			top_level->set_input (j, input_patterns[i][j+2]);
		}
		// For each fault
		for (gi=0; gi<faultCombinations_number; gi++) {
			// For each fault that is not yet detected
			if (faultlist_coverage[gi]==0) {
				// Set all the stuck-at faults of this combination of fault
				for (c=0; c<faultCombinations[gi].size(); c++) {
					index_of_the_fault = faultCombinations[gi].at(c);
				//	stuckat_faultlist[index_of_the_fault]->injectFault();
					stuckat_faultlist[faultCombinations[gi][c]]->injectFault();
				}
				// Check the outupt
				for (j=0; j<top_level->output_number; j++) {
					if (top_level->read_output(j) != golden_output_patterns[i][j]) {
						pattern_detected_faults++;
						faultlist_coverage[gi]=1;
						detected_faults++;
						break;
					}
				}
				// Remove all the stuck-at faults of this combination of fault
				for (c=0; c<faultCombinations[gi].size(); c++) {
					stuckat_faultlist[faultCombinations[gi][c]]->releaseFault();
				}
			}
		}

		// Statistics
		printFaultStatistics (pattern_detected_faults, i, f);
	}
	if (f!=NULL) {
		fclose (f);
	}
}
#ifndef __TIMING_SIMULATION__
void run_fs_stuckat()
{
	int i, gi, index_of_the_fault, c;
	float f;
	FILE *fout;
	int timeline=-1, lastLen=0, t;
	char s[128];
	
	detected_faults=0;
	printf ("Allocating memory for fault list (%d Bytes)...", faultCombinations_number); fflush(stdout);
	faultlist_coverage=(char *) malloc (sizeof(char) * faultCombinations_number);
	for (gi=0; gi<faultCombinations_number; gi++) {
		faultlist_coverage[gi]=0;
	}
	printf ("done.\n");
	if (top_level->FF_noscan_number==0 && top_level->FF_scan_number==0 && param_generate_full_fault_dictionary==0) {
		printf ("\nStarting stuck-at fault simulation for combinational circuit (no full dictionary)...\n");
		run_fs_stuckat_combinational();
		printf ("    ");
	} else if (top_level->FF_noscan_number==0 && top_level->FF_scan_number==0 && param_generate_full_fault_dictionary==0) {
		printf ("\nStarting stuck-at fault simulation for sequential circuit (no full dictionary)...\n");
		run_fs_stuckat_sequential();
		printf ("    ");
	} else {
		printf ("\nStarting stuck-at fault simulation (with full dictionary)..."); fflush(stdout);
		// It opens the statistic file
		fout=open_statistic_file();
		// For each fault it runs the simulation
		for (gi=0; gi<faultCombinations_number; gi++) {
			// Initialize the circuit
			top_level->init();
			
			// Set all the stuck-at faults of this combination of fault
			for (c=0; c<faultCombinations[gi].size(); c++) {
				index_of_the_fault = faultCombinations[gi].at(c);
				stuckat_faultlist[faultCombinations[gi][c]]->injectFault();
			}
			
			// Simulation of the circuit (with faults enabled)
			simulation(0,0);
			
			// Comparison of the output (during the time) to check whether the fault has been covered
			compare_fs_results(gi, fout);
			
			
			// Remove all the stuck-at faults of this combination of fault
			for (c=0; c<faultCombinations[gi].size(); c++) {
				stuckat_faultlist[faultCombinations[gi][c]]->releaseFault();
			}
			
			// Update the progress bar
			t=100 * gi / faultCombinations_number;
			for (i=0; i<lastLen; i++) { printf ("%c",8); }
			timeline=t;
			sprintf (s, "%d%% (%d faults)",timeline, gi);
			lastLen=strlen(s);
			printf ("%s", s);
			fflush(stdout);

		}
		// It closes the statistic file
		if (fout!=NULL) {
			fclose (fout);
		}
	}
	f=(float) 100 * detected_faults / faultCombinations_number;
	for (i=0; i<lastLen; i++) { printf ("%c",8); }
	printf ("...done.                            \nFault coverage=%d on %d (%.4f)\n",detected_faults,faultCombinations_number, f);
	if (param_write_fault_statistic==1) {
		printFaultsStatistic(); // In functions.c
	}
	free (faultlist_coverage);
}
#endif



void run_fs_bitflip (int tot_faults, int time_min, int time_max)
{
	int i;

	bitflip_enable=1;
	detected_faults=0;

	for (i=0; i<tot_faults; i++) {
		//bitflip_time = ...
		//bitflip_location = ...
		simulation(0,0);
		//compare_fs_results(g, FS_BITFLIP, 0, 0);
	}
	bitflip_enable=0;
}

#ifdef __SLACK_CALCULATION__
void calculate_slack_time ()
{
	int i;
#ifdef __DEBUG_SLACK__
	cout << endl << "Calculating asap signals of cell " << top_level->name << endl;
#endif
	for (i=0; i<top_level->output_number; top_level->calculate_asap(top_level->list_PO[i++]));	
#ifdef __DEBUG_SLACK__
	cout << endl << "Calculating alap signals of cell " << top_level->name << endl;
#endif
	for (i=0; i<top_level->output_number; top_level->calculate_alap(top_level->list_PO[i++]));	
	cout << endl << "Calculating slack times of signals of cell " << top_level->name << endl;
	for (i=0; i<top_level->output_number; top_level->calculate_slack_time(top_level->list_PO[i++]));	
}
#endif


int main(int argc, char* argv[])
{
	int i,j,pre_rising=0;
	int l,m,n,o,flag_list_subgate=0;
#ifdef __SLACK_CALCULATION__
	bool timing_done = false;
#endif
#ifdef __TIMING_SIMULATION__
	generic_gate* test_g;
	generic_gate* temp_sim_list_g;
	clock_t t_start, t_finish;
	double simulation_duration, faultsim_duration;
#endif
	char* lib_name ="AMS C35";

	parse_arguments(argc, argv); // This function is described in functions.c
	print_tool_head();
	
	printf ("SUMMARY:\n");
	switch (param_library) {
		case 1: lib_name="AMS C35"; break;
		case 2: lib_name="ST CORE9GP"; break;
		case 3: lib_name="ST CORE65LPHVT"; break;
	}
	printf ("  Library: %s\n", lib_name );
	printf ("  Netlist filename: %s\n", param_netlist_filename);
	printf ("  Input patterns filename: %s\n", param_input_filename);
	printf ("  Simulation output filename: %s\n", strlen(param_output_filename)>0 ? param_output_filename : "no output");
	printf ("  Fault model: %s\n", param_fault_simulation==0 ? "no fault simulation" : (param_fault_simulation==1 ? "Stuck-At" : "Bit-Flip"));
	printf ("  Write fault statistics: %s\n", param_write_fault_statistic==0 ? "no" : "yes");
	if (param_write_fault_statistic==1) {
		printf ("  Fault statistics filename: %s\n", param_fault_statistic_filename);
	}
	
	printf ("\nReading netlist....");
	fflush(stdout);
	verilog_driver driver(param_library); // parser class
     	driver.parse (param_netlist_filename);
	top_level= driver.circuit;
	printf ("done.\n");

#ifdef __LOGIC_LEVELS__
	max_levels = top_level->calculate_logic_level();
	cout << "Max levels: " << max_levels << endl;
	//return 0;
#endif
#ifdef __TIMING_SIMULATION__
	if (strlen(param_def_filename)>0) {
		printf ("\nReading DEF....\n");
		fflush(stdout);
		def_parser (param_def_filename, top_level);
		printf ("done.\n");
	}
#endif	
#ifdef __TIMING_SIMULATION__
	if (strlen(param_timing_filename)>0) {
		printf ("\nReading SDF....");
		fflush(stdout);
		read_timing (param_timing_filename, timing_type, timing_factor, top_level);
		printf ("done.\n");
#ifdef __SLACK_CALCULATION__
		timing_done = true;
#endif

	}
#endif

	if (param_fault_simulation>0 || param_write_fault_list==1) {
		create_faultlist (param_faultlist_filename);
		if (param_write_fault_list==1) {
			print_fault_list();
		}
	}


	if (param_write_fault_statistic==1) {
		printf ("Preparing statistic files...."); fflush(stdout);
		clearFile ("report"); // in functions.c
		clearFile ("detected");
		clearFile ("undetected");
		printf ("done.\n");
	}

#ifdef __PROBA_CALCULATION__
	if (param_proba_calculation==1) {
		top_level->calculate_probabilities();
		return 0;
	}
#endif	
#ifdef __SLACK_CALCULATION__
	if (param_slack_time_calculation==1) {
		if(!timing_done) {
		 	cout << "Error: Timing calculation must be performed before slack time." << endl;
			return -1;
		}
		calculate_slack_time();
		return 0;
	}
#endif	

	// Run simulation
	printf ("Reading input patterns....");
	fflush(stdout);
	read_input_patterns (param_input_filename); // In functions.c
	printf ("done.\n\n");
	printf ("summary..\n");
	printf (" - # PI\t\t\t\t : %d\n", top_level->input_number);
	printf (" - # PO\t\t\t\t : %d\n", top_level->output_number);
	printf (" - # combinational gates\t : %d\n", top_level->list_logic_gate.size());
	printf (" - # non_scan FFs\t\t : %d\n", top_level->list_FF_noscan.size());
	printf (" - # scan FFs\t\t\t : %d\n", top_level->list_FF_scan.size());
	printf (" - # memories\t\t\t : %d\n", top_level->list_memory.size());
	printf (" - # faults\t\t\t : %d\n", faultlist_number);

#ifdef __TIMING_SIMULATION__
#ifdef __DEBUG__
	cout << "Debug " << endl;

	for(i=0;i<top_level->gate_number;i++) {
		top_level->list_logic_gate[i]->print_gates (cout);		
	}
#endif
	printf (" - # End_time\t\t\t : %d\n", end_time);
	if(flag_write_laser_fault==1) fprintf(output_laser_list, "time %d %d\n", laser_start, laser_end);
	for(i=0;i<top_level->gate_number;i++) {
		test_g = top_level->NameToPointer[top_level->list_gate[i]];
		test_g->InitAfterInput ();
		if(flag_read_def==1&&flag_write_laser_fault==1) {
#ifdef __DEBUG__			
			printf (" %s is %s (%d, %d) %d Size (%f, %f)\n",  test_g->name,test_g->gate_type.c_str(), test_g->location[0], test_g->location[1], test_g->orientation, test_g->gate_size[0], test_g->gate_size[1]);
#endif		
			
			test_g->print_laser_fault_list(output_laser_list);
			
		}
	}
	if(flag_write_laser_fault==1) {
		fclose(output_laser_list);
		strcpy(laser_fault_filename,w_laser_fault_filename);
	}
	
	if(flag_laser_fault==1) {
		if (read_laser_fault_table() != 1) {
		for(i=0;i<list_subgate.size();i++) {
			test_g = top_level->NameToPointer[list_subgate[i]];
			test_g->flag_fault_subgate=1;
			test_g->flag_hspice_sim_list=1;
		}
//--------------12/04/2013---- add fault-free subgate for transistor-level simulation (level 2)--------lu	
		//if (Hspice_wholetime_sim_flag!=1) {
		//printf("Hspice level %d simulation", hspice_level);
		for(i=1;i<hspice_level;i++){
			temp_list_subgate = list_subgate;
			for(i=0;i<temp_list_subgate.size();i++) {	
				test_g = top_level->NameToPointer[temp_list_subgate[i]];
				//test_g->flag_fault_subgate=1;
				for(o=0; o<test_g->outputs_number; o++) {
					for (l=0; l<test_g->fan_out[o]; l++) {
						if (test_g->ref_outputs[o].p_gg[l]->type != -2) { // type = -2 is PO
							temp_sim_list_g = top_level->NameToPointer[test_g->ref_outputs[o].p_gg[l]->name];
							if (temp_sim_list_g->flag_hspice_sim_list==0 && temp_sim_list_g->type<100) {   //type =100 -> flipflop
								list_subgate.push_back(test_g->ref_outputs[o].p_gg[l]->name);
								temp_sim_list_g->flag_hspice_sim_list=1;
							}	
							
							/*flag_list_subgate=0;
							for(m=0;m<temp_list_subgate.size();m++) {
								if (strcmp(test_g->ref_outputs[o].p_gg[l]->name,temp_list_subgate[m].c_str())==0) {
									flag_list_subgate=1;
								}	
							}	
							if (flag_list_subgate==0)list_subgate.push_back(test_g->ref_outputs[o].p_gg[l]->name);
							*/
						}
					}
				}		
			}
		}
		//}
	} else {flag_laser_fault=0;}
	}
	
//--------------12/04/2013----end------------------	
	// calculate Hspice simulation duration  2013/03/06
/*	if (clock_flag == 1) {
		for(i=1; i<input_patterns_number;i++) {
			if (input_patterns[i][clock_no+2]==1 && input_patterns[i-1][clock_no+2]==0) {
				if (pre_rising <= fault_start && fault_start <= input_patterns[i][1]) {
					simulation_start = pre_rising;
					printf("simulation_start = %d\n",pre_rising);
				}
				if (pre_rising <= fault_end && fault_end <= input_patterns[i][1]) {
					simulation_end = input_patterns[i][1];
					printf("simulation_end = %d\n",input_patterns[i][1]);
				}
				pre_rising = input_patterns[i][1];
			}
			printf(" AT time = %d; clock = %d\n",input_patterns[i][1],input_patterns[i][clock_no+2]);
		}
	}*/
#endif

	for(i = 0 ; i< memory_number ; i++) load_memory(memory_name[i], memory_file[i]); // In functions.c
#ifdef __TIMING_SIMULATION__
	t_start= clock();
#endif
	run_simulation();
#ifdef __TIMING_SIMULATION__
	t_finish= clock();
	simulation_duration = (double)(t_finish - t_start) / CLOCKS_PER_SEC;
#endif

	if (param_fault_simulation==FS_STUCKAT || param_fault_simulation==FS_DOUBLESTUCKAT || param_fault_simulation==FS_MULTIPLESTUCKAT) {
#ifdef __TIMING_SIMULATION__
		for(i=0;i<top_level->gate_number;i++) {
			test_g = top_level->NameToPointer[top_level->list_gate[i]];
			test_g->InitAfterInput ();
		}
		t_start= clock();
		run_fs_stuckat_timing();
		t_finish=clock();
		faultsim_duration = (double)(t_finish - t_start) / CLOCKS_PER_SEC;
#else
		run_fs_stuckat();
#endif
	}
	
	// top_level->print (cout); // print the circuit netlist
#ifdef __TIMING_SIMULATION__
	printf("simulation duration is %f s; fault simulation duration is %f s.\n",simulation_duration, faultsim_duration);
#endif
	
	return 0;
}
