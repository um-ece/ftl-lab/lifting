///////////////////////////////////////////////////////////////////////
// generic_cell.cpp: implementation interface for the generic_cell class.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 10-07-2007
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "generic_cell.h"
#include "globalVariables.h"

using namespace std;

/*
 * LOGIC VALUES:
 * 0 = 0
 * 1 = 1
 * X = -1
 * Z = -2
 * ...
*/


	generic_cell::generic_cell (const char *pname = NULL, int no_fl=0) {

		list_PI.clear ();
		list_PO.clear ();

		if (pname == NULL)
			name[0] = '\0';
		else
			strcpy (name,pname);


		no_fault_list=no_fl;
		faults_number=0;
		faultsites_number=0;

		input_number=0;
		output_number=0;
		FF_scan_number=0;
		FF_noscan_number=0;
		const_number=0;
		memory_number=0;
		logic_gate_number=0;
#ifdef __TIMING_SIMULATION__
		int gate_number=0;
#endif
		cell_number=0;
		celldefine = 0;

	};

	void generic_cell::init() {
	  int i, j;

		for (i=0; i<input_number; list_PI[i++]->init());
		for (i=0; i<output_number; list_PO[i++]->init());
		for (i=0; i<logic_gate_number; list_logic_gate[i++]->init());
		for (i=0; i<FF_scan_number; list_FF_scan[i++]->init());
		for (i=0; i<FF_noscan_number; list_FF_noscan[i++]->init());
		for (i=0; i<cell_number; list_cell[i++]->init());
		for (i=0; i<memory_number; list_memory[i++]->init());
		for (i=0; i<const_number; list_const[i++]->init());
		

		int m = 0 ;
		for (i=0; i < FF_noscan_number; i++) {
		  for (j=0; j<list_FF_noscan[i]->inputs_number; j++) {
		    if (list_FF_noscan[i]->ref_inputs[j].p_gg == NULL ){
		      cout  << " **WARNING : unconnected input_port for non-scan FF " << list_FF_noscan[i]->name << " - current input value : " << list_FF_noscan[i]->input_value[j] << endl;
		      m++;
		    }
		  }		  
		}
		
		int l = 0;
		for (i=0; i < logic_gate_number; i++) {
		  for (j=0; j<list_logic_gate[i]->inputs_number; j++) {
		    if (list_logic_gate[i]->ref_inputs[j].p_gg == NULL  ){
		      cout  << " **WARNING : unconnected input_port for logic gate " << list_logic_gate[i]->name << " of type " << list_logic_gate[i]->type << " - current input value : " << list_logic_gate[i]->input_value[j] << endl;
		      l++;
		    }
		  }		  
		}
		
		
		if (m!=0)
		  cout << " **" << m << "  non-scan FFs with unconnected inputs " << endl ; 
		if (l!=0)
		  cout << " **" << l << "  logic gatess with unconnected inputs " << endl ; 				
		
	};

 	
	void generic_cell::add_input (pi* primary_input) {
		string tmp;

		primary_input->parent_cell=this;
		list_PI.push_back (primary_input);
		input_number++;

		tmp=primary_input->name;
		NameToPointer[tmp]=primary_input;

		faults_number += 2;
		faultsites_number += 1;
	};

	void generic_cell::set_input (int pi_index, int value) {
		list_PI[pi_index]->set_input (value);
	}	
		
	void generic_cell::add_output (po* primary_output) {
		string tmp;

		primary_output->parent_cell=this;
		list_PO.push_back (primary_output);
		output_number++;

		tmp=primary_output->name;
		NameToPointer[tmp]=primary_output;

		faults_number += 2;
		faultsites_number += 1;
	};

	void generic_cell::add_FF_scan (generic_ff* FF) {
		string tmp;

		FF->parent_cell=this;
		list_FF_scan.push_back (FF);
		FF_scan_number++;

		tmp=FF->name;
		NameToPointer[tmp]=FF;
#ifdef __TIMING_SIMULATION__
		list_gate.push_back (tmp);
		gate_number++;
#endif

		faults_number += 2 * (FF->inputs_number + FF->outputs_number);
		faultsites_number += (FF->inputs_number + FF->outputs_number);
	}
	void generic_cell::add_FF_noscan (generic_ff* FF) {
		string tmp;

		FF->parent_cell=this;
		list_FF_noscan.push_back (FF);
		FF_noscan_number++;
	
		tmp=FF->name;
		NameToPointer[tmp]=FF;
#ifdef __TIMING_SIMULATION__
		list_gate.push_back (tmp);
		gate_number++;
#endif

		faults_number += 2 * (FF->inputs_number + FF->outputs_number);
		faultsites_number += (FF->inputs_number + FF->outputs_number);
	}
	void generic_cell::add_generic_gate (generic_gate* gate) {
		string tmp;

		gate->parent_cell=this;
		list_logic_gate.push_back (gate);
		logic_gate_number++;

		tmp=gate->name;
		NameToPointer[tmp]=gate;
#ifdef __TIMING_SIMULATION__
		list_gate.push_back (tmp);
		gate_number++;
#endif

		faults_number += 2 * (gate->inputs_number + gate->outputs_number);
		faultsites_number += (gate->inputs_number + gate->outputs_number);
	}
	void generic_cell::add_generic_cell (generic_cell* cell) {
		list_cell.push_back (cell);
		cell_number++;
	}
        
	void generic_cell::add_const (generic_gate* gate) {
		gate->parent_cell=this;
		list_const.push_back (gate);
		const_number++;
	}

	void generic_cell::add_memory (generic_memory* cell) {
		list_memory.push_back (cell);
		memory_number++;
		
		faults_number += 2 * (cell->inputs_number + cell->outputs_number);
		faultsites_number += (cell->inputs_number + cell->outputs_number);

		

	}
#ifdef __TIMING_SIMULATION__	
	generic_gate* generic_cell::getGenericGate_fromName (char* name) {
		string tmp;
		tmp=name;
		iter = NameToPointer.find(tmp);
		if(iter != NameToPointer.end())
		{return NameToPointer[tmp];}
		else {return NULL;}
	}
#else	
	generic_gate* generic_cell::getGenericGate_fromName (char* name) {
		string tmp;
		tmp=name;
		return NameToPointer[tmp];
	}
#endif
	
	/*generic_ff* generic_cell::getGenericFF_fromName (char* name) {
		string tmp;
		tmp=name;
		return NameToPointer[tmp];
	}*/
        
	int generic_cell::read_output (int po_index) { return list_PO[po_index]->read_output(); };

	int generic_cell::read_input (int pi_index) { return list_PI[pi_index]->read_input(); };

	void generic_cell::propagate_output_FF() {
		int i,j;

#ifdef __DEBUG2__		
	        int m = 0;
		cout << "Inizio ricerca bastardi : " << endl ;
		for (i=0; i<FF_scan_number; i++) {
		  for (j=0; j<list_FF_scan[i]->outputs_number; j++) {
		    if (list_FF_scan[i]->output_value[j] == -1  ){
		      cout << "il FFs bastardo � " << list_FF_scan[i]->name << " tipo " << list_FF_scan[i]->type  << endl;
		      m++;
		      int pippo = 0;
		      cin >> pippo;
		    }
		  }		  
		}
		cout << "num FFs bastardi : " << m << endl ; 
		
		int pippo;
		cin >> pippo;
		m=0;
		for (i=0; i<FF_noscan_number; i++) {
		  for (j=0; j < list_FF_noscan[i]->outputs_number; j++) {
		    if (list_FF_noscan[i]->output_value[j] == -1  ){
		      cout << "il FFns bastardo " << m << " è " << list_FF_noscan[i]->name << " tipo " << list_FF_noscan[i]->type << " input_val " << list_FF_noscan[i]->input_value[0]  << endl;
		      m++;		      
		    }		    
		  }		    
		}
		cout << "num FFns bastardi : " << m << endl ; 
		
		cin >> pippo;
		
		
		m=0;
		for (i=0; i<logic_gate_number ; i++) {
		  for (j=0; j<list_logic_gate[i]->outputs_number; j++) {
		    if (list_logic_gate[i]->output_value[j] == -1  ){
		      cout << "il gate bastardo " << m << " e " << list_logic_gate[i]->name << " ha output " << j << " = " << list_logic_gate[i]->output_value[j] << " >>> " ;
		      
	              cout << " Backward connected to " << endl;
		      for (int k=0 ; k < list_logic_gate[i]->inputs_number ; k++)
			if ( list_logic_gate[i]->ref_inputs[k].p_gg != NULL ) {
			cout << "  value input port " << k << " = "<< list_logic_gate[i]->input_value[k] << ";" ; 	  			 
			cout << "    + port " << k << " --> " << list_logic_gate[i]->ref_inputs[k].p_gg->name <<", port="<< list_logic_gate[i]->ref_inputs[k].port <<", value="<< list_logic_gate[i]->input_value[k] << endl;
			  
			}
			else {
			  
			  cout << "    + port " << k << " is floating, value="<< list_logic_gate[i]->input_value[k] << endl; 
			}
		      
		      cout << endl;
		      m++;
		      
		    }
		  }
		}
		cout << "num gate bastardi : " << m << endl ; 
		
		cin >> pippo;	       
		cout << "Fine ricerca bastardi : " << endl ;		
#endif		

		for (i=0; i<FF_scan_number; i++) {
			for (j=0; j<list_FF_scan[i]->outputs_number; j++) {
				list_FF_scan[i]->propagate_output(j);
			}
		}
		for (i=0; i<FF_noscan_number; i++) {
			for (j=0; j<list_FF_noscan[i]->outputs_number; j++) {
				list_FF_noscan[i]->propagate_output(j);
			}
		}		
		for (i=0; i<memory_number; i++) {
#ifdef __DEBUG__
		  cout << "    -  forwarding memory ("<< i << "-" << memory_number << ")" ;
		  cout <<  " : output on " << list_memory[i]->outputs_number << "bits (" ; 
#endif
		  for (j = 0; j<list_memory[i]->outputs_number; j++) {
#ifdef __DEBUG__
		    cout << list_memory[i]->temp_output_value[j] ;
#endif
		    list_memory[i]->propagate_output(j);		    
		  }
#ifdef __DEBUG__
		  cout << ")" << endl;  
#endif
		}

		for (i=0; i<cell_number; i++) {
			list_cell[i]->propagate_output_FF();
		}
		
		
		
	}




void generic_cell::print (ostream &os) {

	int i;

	os << " module " <<  name << endl;
	os << " input  " << endl;

	for (i = 0; i < list_PI.size () ; i++) 
		list_PI[i]->print_gates (os);
	os << endl;

	os << " output  " << endl;
	for (i = 0; i < list_PO.size () ; i++) 
		list_PO[i]->print_gates (os);
	
	os << endl;

	os << " logic_gate  " << endl;
	for (i = 0; i < list_logic_gate.size () ; i++) 
		list_logic_gate[i]->print_gates (os);

	os << endl;
}


void generic_cell::getFF_status(char* s) {
	int i, j=0;
	for (i=0; i<FF_scan_number; i++) {
		s[j++]=(char)list_FF_scan[i]->read_output(0);
	}
	for (i=0; i<FF_noscan_number; i++) {
		s[j++]=(char)list_FF_noscan[i]->read_output(0);
	}
	s[j]='\0';
}

void generic_cell::setFF_status(char* s) {
	int i, j=0;
	for (i=0; i<FF_scan_number; i++) {
		list_FF_scan[i]->load_FF((int)s[j++]);
	}
	for (i=0; i<FF_noscan_number; i++) {
		list_FF_noscan[i]->load_FF((int)s[j++]);
	}
	s[j]='\0';
}

#ifdef __PROBA_CALCULATION__
void generic_cell::calculate_probabilities () {
	int i, g, maxSS=0, maxSSdepth=0;

	// Calculates the support set
	input_support_set_overall_done=0;
	calculate_support_sets();

	// Initialize the input probabilities
	for (i=0; i<input_number; i++) {
		list_PI[i]->set_input_probability(0, 0.5);
		list_PI[i]->set_input_probability(1, 0.5);
	}
	for (i=0; i<list_FF_scan.size(); i++) {
		list_FF_scan[i]->set_FF_probability(0, 0.5);
		list_FF_scan[i]->set_FF_probability(1, 0.5);
	}
	for (i=0; i<list_FF_noscan.size(); i++) {
		list_FF_noscan[i]->set_FF_probability(0, 0.5);
		list_FF_noscan[i]->set_FF_probability(1, 0.5);
	}

	// Calculate output probabilities
	for (i=1; i<=max_levels; i++) {
		for (g=0; g<gates_basedon_logic_levels[i].size(); g++) {
			(gates_basedon_logic_levels[i][g])->calculate_output_probabilities();
		}
	}

#ifdef __DEBUG_PROBA_1__
	for (i=1; i<=max_levels; i++) {
		for (g=0; g<gates_basedon_logic_levels[i].size(); g++) {
			if ((gates_basedon_logic_levels[i][g])->actual_support_set.size() > maxSS) {
				maxSS = (gates_basedon_logic_levels[i][g])->actual_support_set.size();
			}
			if ((gates_basedon_logic_levels[i][g])->actual_support_set_depth > maxSSdepth) {
				maxSSdepth = (gates_basedon_logic_levels[i][g])->actual_support_set_depth;
			}
		}
	}
	cout << endl << endl << "MAX SUPPORT SET SIZE = "  << maxSS;
	cout << endl << "MAX SUPPORT SET DEPTH = " << maxSSdepth;
	cout << endl << endl << "STATISTICS:" << endl;
	cout <<   "0 to 0.1; " << proba[0] << endl;
	cout << "0.1 to 0.2; " << proba[1] << endl;
	cout << "0.2 to 0.3; " << proba[2] << endl;
	cout << "0.3 to 0.4; " << proba[3] << endl;
	cout << "0.4 to 0.5; " << proba[4] << endl;
	cout << "0.5 to 0.6; " << proba[5] << endl;
	cout << "0.6 to 0.7; " << proba[6] << endl;
	cout << "0.7 to 0.8; " << proba[7] << endl;
	cout << "0.8 to 0.9; " << proba[8] << endl;
	cout << "0.9 to 1; "   << proba[9] << endl;
	cout << "less than 0.01;"   << proba_moins1   << endl;
	cout << "less than 0.001;"  << proba_moins01  << endl;
	cout << "less than 0.0001;" << proba_moins001 << endl;
#endif
}

void generic_cell::calculate_support_sets () {
	int i, g;
	for (i=1; i<=max_levels; i++) {
		for (g=0; g<gates_basedon_logic_levels[i].size(); g++) {
			(gates_basedon_logic_levels[i][g])->input_support_set_calculation();
		}
	}
	for (i=1; i<=max_levels; i++) {
		for (g=0; g<gates_basedon_logic_levels[i].size(); g++) {
			cout << "Gate " << (gates_basedon_logic_levels[i][g])->name << " (level " << i << "): ";
			(gates_basedon_logic_levels[i][g])->actual_support_set_calculation();
		}
	}
}
#endif

#ifdef __SLACK_CALCULATION__
void generic_cell::calculate_asap (generic_gate* gate) {
	int i;
#ifdef __DEBUG_SLACK__
	cout << " Asap of gate " << gate->name << endl;
#endif
	for(i=0; i<gate->outputs_number; gate->calculate_asap(i++));
}
void generic_cell::calculate_alap (generic_gate* gate) {
	int i;
#ifdef __DEBUG_SLACK__
	cout << " Alap of gate " << gate->name << endl;
#endif
	for(i=0; i<gate->inputs_number;  gate->calculate_alap(i++));
}
void generic_cell::calculate_slack_time (generic_gate* gate) {
	int i;
#ifdef __DEBUG_SLACK__
	cout << " Slack time of gate " << gate->name << endl;
#endif
	for(i=0; i<gate->outputs_number; gate->calculate_slack_time(i++));
}
#endif

#ifdef __LOGIC_LEVELS__
int generic_cell::calculate_logic_level ()
{
	int g;
	int i, n, j;
	max_levels=0;

	for (i=0; i<list_PI.size () ; i++) {
		n=list_PI[i]->calculate_logic_level (-1);
		if (n>max_levels) max_levels=n;
	}
	for (i=0; i<list_FF_scan.size () ; i++) {
		n=list_FF_scan[i]->calculate_logic_level (-1);
		if (n>max_levels) max_levels=n;
	}
	for (i=0; i<list_FF_noscan.size () ; i++) {
		n=list_FF_noscan[i]->calculate_logic_level (-1);
		if (n>max_levels) max_levels=n;
	}
#ifdef __DEBUG_LOGICLEVEL_1__
	for (i=1; i<=max_levels; i++) {
		cout << "Level " << i << ": ";
		for (g=0; g<gates_basedon_logic_levels[i].size(); g++) {
			cout << (gates_basedon_logic_levels[i][g])->name << ", ";
		}
		cout << endl;
	}
#endif
	return max_levels;
}
#endif


