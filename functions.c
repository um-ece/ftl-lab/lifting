#include <queue>

#include "functions.h"
#include "simulation_event.h"
#include "generic_gate.h"


void getPortName (generic_gate *g, int port, int direction, char *name)
{
	char s[30];
	char *p;
	int i;

	if (direction==1) {
		strcpy (s, g->inputs_name.c_str());
	} else {
		strcpy (s, g->outputs_name.c_str());
	}
	p=strtok (s,",");
	i=0;
	strcpy (name, "IO_Port");
	while (p!=NULL) {
		if (i==port) {
			strcpy (name, p);
		}
		i++;
		p=strtok(NULL,",");
	}
}

void print_tool_head()
{
	printf ("\n\n=========================================================================================\n");
	printf ("                                       tLIFTING\n");
	printf ("                      Delay Annotated LIrmm FaulT SimulatING v2.1\n\n");
	printf ("                             version for AMS 3.70 and ST90nm libraries\n");
	printf ("by Giorgio Di Natale (giorgio.dinatale@lirmm.fr)\n"); 
        printf ("   Alberto Bosio     (alberto.bosio@lirmm.fr) \n");
	printf ("Contributors:\n");
	printf ("   Paolo Bernardi    (paolo.bernardi@polito.it) -- Memories\n");
	printf ("   Feng Lu           (feng.lu@lirmm.fr) -- Timing\n");
	printf ("   Sophie Dupuis     (sophie.dupuis@lirmm.fr) -- Net probabilities\n");
	printf ("=========================================================================================\n\n");
}

void print_help(char* toolname)
{
/* TO MANAGE:
param_timing_filename;
timing_type;
timing_factor;
*/
	print_tool_head();

	printf ("\nUsage: %s [options]\n", toolname);
	printf ("\t-l [or --library] { c35 core9gp core6lp }    Tech library: c35=AMS_3.70   core9gp=ST_CORE9GP   core6lp=ST_CORE65LP Default=C35\n");
	printf ("\t-n [or --netlist] <filename>         File name of the verilog netlist. MANDATORY\n");
	printf ("\t-i [or --input] <filename>           File name of the input patterns. MANDATORY\n");
	printf ("\t-o [or --write_output] <filename>     Name of the file where the simulator writes the output of the simulation.\n");
#ifdef __TIMING_SIMULATION__
	printf ("\t-of [or --write_output_fault] <filename>     Name of the file where the simulator writes the output of the simulation with faults.\n");
	printf ("\t                                     If not specified, the output of the simulation is not written.\n");
	printf ("\t-owf [or --output_waveform] <filename>     Output Waveform file for Cscope in format *.txt\n");
	printf ("\t-rlf [or --read_laser_fault] <filename>     read laser fault table file for laser-fault simulation \n");
	printf ("\t-osp [or --output_hspice] <filename>     output Hspice file for laser-fault simulation \n");
#endif	
	printf ("\t-fs [or --fault_simulation] {stuckat|bitflip|doublesa|multiplesa}\n");
	printf ("\t                                     Type of the fault simulation. If not specified, the fault simulation is not executed.\n");
	printf ("\t                                     Default=no fault simulation.\n");
	printf ("\t-pfl [or --print_fault_list] <filename>\n");
	printf ("\t                                     It stores the faultlist in <filename>.\n");
	printf ("\t-pfs [or --print_fault_simulation]   Default=not asserted. If asserted, it prints the output of the simulation for each fault\n");
	printf ("\t                                     defined in the faultlist.\n");
	printf ("\t-pfsd [or --print_fault_simulation_dir] <dirname>\n");
	printf ("\t                                     Base directory for \"print fault simulation\" output. Default=./\n");
	printf ("\t-gffd [or --generate_full_fault_dictionary]\n");
	printf ("\t                                     If set, the fault simulator generates the uses the algorithm for sequential circuit.\n");
	printf ("\t-saf [or --saf_observe_last_input_only]\n");
	printf ("\t                                     If set, it observes the output of the circuit only after the last input pattern.\n");
	printf ("\t-ued [or --use_error_detection] N\n");
	printf ("\t                                     If N>0, it assumes that the last N output bits are error detection bits.\n");
	printf ("\t-wfs [or --write_fault_statistic] <filename>\n");
	printf ("\t                                     If set, it writes a file(s) with the statistics of covered and not covered faults.\n");
	printf ("\t                                     <filename> is the name of the output file for the statistics (no extension).\n");

	printf ("\t-fd [or --full_dictionary] \n");
	printf ("\t                                     If set, it stores the full dictionary. By default only the pass/fail dictionary is created .\n");

	printf ("\t-wtft [or --write_truth_fault_table] <filename>\n");
	printf ("\t                                     If set it writes a file(s) with the truth table of the good machine and all the faulty machines.\n");
	printf ("\t                                     <filename> is the name of the output file (it can be big!!).\n");
	printf ("\t                                     WARNING: Sequential algorithm must be used.\n");
	printf ("\t-fl [or --faultlist_filename] <filename>\n");
	printf ("\t                                     Name of the external filelist (tetramax format).\n");
	printf ("\t-bitflip_experiments <e>             Total number of bitflips.\n");
	printf ("\t-bitflip_mintime <m>                 For bit_flip fault model, the fault simulation is performed by injecting a bit flip\n");	
	printf ("\t-load_memory <memory_name><filename> Name of the memory instance and file storing the memory content\n");	
    printf ("\t                                     starting from a particular time. This parameter specifies the lower bound.\n");

	printf ("\t-rsdf [or --read_sdf] <filename><time_type><time_factor>\n");
	printf ("\t                                     If set, a file of standrad dealy format(.sdf) will be read. There are 3 Parameters:	\n");
	printf ("\t                                     <filename> is the name of the file of standrad dealy format(.sdf)\n");
	printf ("\t                                     <time_type> must be 0, 1 or 2, (0 = minimum, 1 = typical, 2= maximum,)\n");
	printf ("\t                                     <time_factor> is the factor to rounding value of delay. For example : 1000 \n");
	printf ("\nNOTE: Combinational algorithm uses \"cumulative\" fault model. It means that when a fault has been detected\n");
	printf ("      by a pattern, it won't be simulated using the following input patterns.\n");
	printf ("      On the other hand, the sequential algorithm simulates each fault using each pattern.\n");
	printf ("\t-pc [or --proba_calculation]\n");
	printf ("\t                                     If set, calculations of the nodes probabilities are done.\n");
	printf ("\t-st [or --slack_time]\n");
	printf ("\t                                     If set, calculations of the nodes slack times are done (rsdf option is needed).\n");
	printf ("\n");
	exit(-1);
}

void parse_arguments (int argc, char* argv[]) 
{
	int i,j=0;
	int netlist_flag=0, input_flag=0;
	char param_truth_fault_table_filename[1024];
	
	// Default values
	strcpy (param_output_filename, "");
#ifdef __TIMING_SIMULATION__	
	strcpy (fault_output_filename, "");
	strcpy (waveform_output_filename, "");
	strcpy (laser_fault_filename, "");
	strcpy (hspice_libname, "");
	strcpy (hspice_output_filename, "");
#endif	
	param_fault_simulation=FS_NOFAULTS;
	//param_force_sequential_algorithm=0;
	param_generate_full_fault_dictionary=0;
	param_write_fault_statistic=0;
	strcpy (param_fault_statistic_filename, "");
	param_bitflip_mintime=0;
	param_bitflip_experiments=0;
	param_saf_observe_last_input_only=0;
	param_faultlist_filename[0]=0;
	strcpy (param_print_fs_result_dir, "");
	param_print_fs_result=0;
	param_library=1;
	param_passfail_dictionary = 1; //default 1, the created dictionary is passfail. if 0 then the full dictionary is created
	param_use_errorDetection=0;
	strcpy(constant_fault_statistic, "");
#ifdef __PROBA_CALCULATION__
	param_proba_calculation=0;
#endif
#ifdef __SLACK_CALCULATION__
	param_slack_time_calculation=0;
#endif
/* TO MANAGE:
param_timing_filename;
timing_type;
timing_factor;
*/
	

	// Parsing
	for (i=1; i<argc; i++) {	  
		if (strcmp(argv[i],"--netlist")==0 || strcmp(argv[i],"-n")==0) {
			strcpy (param_netlist_filename, argv[i+1]);
			i++;
			netlist_flag=1;
		} else if (strcmp(argv[i],"--library")==0 || strcmp(argv[i],"-l")==0) {
			if (strcmp (argv[i+1], "core9gp")==0) {
				param_library=2;
			}else if (strcmp (argv[i+1], "core6lp")==0) {
				param_library=3;
			}
			i++;
		} else if (strcmp(argv[i],"--input")==0 || strcmp(argv[i],"-i")==0) {
			strcpy (param_input_filename, argv[i+1]);
			i++;
			input_flag=1;
		} else if (strcmp(argv[i],"--write_output")==0 || strcmp(argv[i],"-o")==0) {
			strcpy (param_output_filename, argv[i+1]);
			i++;
#ifdef __TIMING_SIMULATION__
		} else if (strcmp(argv[i],"--write_output_fault")==0 || strcmp(argv[i],"-of")==0) {
			strcpy (fault_output_filename, argv[i+1]);
			i++;
		} else if (strcmp(argv[i],"--output_waveform")==0 || strcmp(argv[i],"-owf")==0) {
			if (strcmp(argv[i+1],"-all")==0 ) {
				i++;
				flag_wf_all =1;
			}
			strcpy (waveform_output_filename, argv[i+1]);
			flag_wf=1;		
			i++;		
		} else if (strcmp(argv[i],"--read_laser_fault")==0 || strcmp(argv[i],"-rlf")==0) {
			strcpy (laser_fault_filename, argv[i+1]);
			flag_laser_fault=1;
			i++;
		} else if (strcmp(argv[i],"--hspice_cell_lib")==0 || strcmp(argv[i],"-spl")==0) {
			strcpy (hspice_libname, argv[i+1]);
			flag_hspice_lib =1;
			i++;
		} else if (strcmp(argv[i],"--output_hspice")==0 || strcmp(argv[i],"-osp")==0) {
			if (strcmp(argv[i+1],"-whole_time")==0 ) {
				i++;
				Hspice_wholetime_sim_flag =1;
//				printf("Hspice whole time simulation"); 
			} 
			if (strcmp(argv[i+1],"-level")==0 ) {
				sscanf(argv[i+2], "%d", &hspice_level);
				i=i+2;
			 
			}
			if (strcmp(argv[i+1],"-force_sim_time")==0 ) {
				flag_force_sim_time=1;
				i++;
			}
			strcpy (hspice_output_filename, argv[i+1]);
			flag_hspice_output=1;
			i++;	
		} else if (strcmp(argv[i],"-read_sdf")==0 || strcmp(argv[i],"-rsdf")==0) {
			strcpy (param_timing_filename, argv[i+1]);
			sscanf(argv[i+2], "%d", &timing_type);
			sscanf(argv[i+3], "%d", &timing_factor);
			i=i+3;
		} else if (strcmp(argv[i],"-read_def")==0 || strcmp(argv[i],"-rdef")==0) {
			strcpy (param_def_filename, argv[i+1]);
			flag_read_def = 1;
			i++;
		} else if (strcmp(argv[i],"-laser_para")==0 || strcmp(argv[i],"-lpara")==0) {
			sscanf(argv[i+1], "%d", &laser_center[0]);
			sscanf(argv[i+2], "%d", &laser_center[1]);
			sscanf(argv[i+3], "%d", &laser_radius);
			sscanf(argv[i+4], "%d", &laser_start);
			sscanf(argv[i+5], "%d", &laser_end);
			sscanf(argv[i+6], "%f", &laser_power);
			i=i+6;
		} else if (strcmp(argv[i],"--write_laser_fault")==0 || strcmp(argv[i],"-wlf")==0) {
			strcpy (w_laser_fault_filename, argv[i+1]);
			flag_laser_fault=1;
			flag_write_laser_fault=1;
			output_laser_list=fopen(w_laser_fault_filename,"w");
			i++;	
			
#endif
		} else if (strcmp(argv[i],"--faultlist_filename")==0 || strcmp(argv[i],"-fl")==0) {
			strcpy (param_faultlist_filename, argv[i+1]);
			i++;
		} else if (strcmp(argv[i],"--print_fault_simulation")==0 || strcmp(argv[i],"-pfs")==0) {
			param_print_fs_result=1;
		} else if (strcmp(argv[i],"--print_fault_simulation_dir")==0 || strcmp(argv[i],"-pfsd")==0) {
			strcpy (param_print_fs_result_dir, argv[i+1]);
			i++;
		} else if (strcmp(argv[i],"--fault_simulation")==0 || strcmp(argv[i],"-fs")==0) {
			if (strcmp(argv[i+1],"stuckat")==0) {
				param_fault_simulation=FS_STUCKAT;
			} else if (strcmp(argv[i+1],"bitflip")==0) {
				param_fault_simulation=FS_BITFLIP;
			} else if (strcmp(argv[i+1],"doublesa")==0) {
				param_fault_simulation=FS_DOUBLESTUCKAT;
			} else if (strcmp(argv[i+1],"multiplesa")==0) {
				param_fault_simulation=FS_MULTIPLESTUCKAT;
			} else {
				print_help(argv[0]);
			}
			i++;
		} else if (strcmp(argv[i],"--saf_observe_last_input_only")==0 || strcmp(argv[i],"-saf")==0) {
			param_saf_observe_last_input_only=1;
		//} else if (strcmp(argv[i],"--force_sequential_algorithm")==0 || strcmp(argv[i],"-fsa")==0) {
			//param_force_sequential_algorithm=1;
		} else if (strcmp(argv[i],"--generate_full_fault_dictionary")==0 || strcmp(argv[i],"-gffd")==0) {
			param_generate_full_fault_dictionary=1;
		} else if (strcmp(argv[i],"--print_fault_list")==0 || strcmp(argv[i],"-pfl")==0) {
			param_write_fault_list=1;
			strcpy (param_fault_list_filename, argv[i+1]);
			i++;
		} else if (strcmp(argv[i],"--write_fault_statistic")==0 || strcmp(argv[i],"-wfs")==0) {
			param_write_fault_statistic=1;
			strcpy (param_fault_statistic_filename, argv[i+1]);
			i++;
		} else if (strcmp(argv[i],"--full_dictionary")==0 || strcmp(argv[i],"-fd")==0) {
			param_passfail_dictionary=0;

		} else if (strcmp(argv[i],"--write_truth_fault_table")==0 || strcmp(argv[i],"-wtft")==0) {
			param_write_truth_fault_table=1;
			strcpy (param_truth_fault_table_filename, argv[i+1]);
			i++;
			param_truth_fault_table_file=fopen (param_truth_fault_table_filename, "w");
			if (param_truth_fault_table_file==NULL) {
				printf ("\nERROR: cannot open truth table file for writing. Truth table report aborted.\n");
				param_write_truth_fault_table=0;
			}
		} else if (strcmp(argv[i],"-bitflip_experiments")==0) {
			sscanf(argv[i+1], "%d", &param_bitflip_experiments);
			i++;
		} else if (strcmp(argv[i],"-use_error_detection")==0  || strcmp(argv[i],"-ued")==0) {
			sscanf(argv[i+1], "%d", &param_use_errorDetection);
			i++;			
		} else if (strcmp(argv[i],"-bitflip_mintime")==0) {
			sscanf(argv[i+1], "%d", &param_bitflip_mintime);
			i++;			
		} else if  (strcmp(argv[i],"-load_memory")==0) {
			strcpy (memory_name[j], argv[++i]);
			strcpy (memory_file[j++], argv[++i]);
			i++;
					
#ifdef __PROBA_CALCULATION__
		} else if (strcmp(argv[i],"--proba_calculation")==0 || strcmp(argv[i],"-pc")==0) {
			param_proba_calculation=1;
#endif
#ifdef __SLACK_CALCULATION__
		} else if (strcmp(argv[i],"--slack_time")==0 || strcmp(argv[i],"-st")==0) {
			param_slack_time_calculation=1;
#endif
		} else {
			printf("%s; type = %d; factor = %d", param_timing_filename, timing_type, timing_factor);
			print_help(argv[0]);
			
		}
	}
	memory_number = j;

#ifdef __PROBA_CALCULATION__
	if (netlist_flag==0) {
		printf("test mode proba");
		print_help(argv[0]);
}
#elif __SLACK_CALCULATION__
	if (netlist_flag==0 ) {
		printf("test mode slack");
		print_help(argv[0]);
}
#else
	if (netlist_flag==0 || input_flag==0 || (param_write_fault_statistic==1 && strlen(param_fault_statistic_filename)==0) ||
			(param_fault_simulation==FS_BITFLIP && param_bitflip_experiments<=0) ) {
		printf("test mode");
		print_help(argv[0]);
	}
#endif
}

void clearFile (char *r)
{
	char s[1000];
	FILE *f;

	sprintf (s, "%s.%s.txt", param_fault_statistic_filename, r);
	f=fopen (s, "w");
	if (f==NULL) {
		printf ("\nERROR: cannot open statistic files for writing. Aborting\n");
		exit (-1);
	}
	fclose(f);
}

int load_memory(char *memory_instance, char *memory_file) // return 0 if errors
{
	int i=0; 

	while( i<top_level->list_memory.size() && strcmp(top_level->list_memory[i]->name,memory_instance)!=0) i++ ;  
	if (i<top_level->list_memory.size()) {
		if(!(top_level->list_memory[i]->load_memory(memory_file))) cout << "error in file opening" ;
	} else {
		cout << "memory not found... sorry!" << endl;
		return 0;
	}

#ifdef __DEBUG__
	top_level->list_memory[i]->write_content_2_video();
#endif
  
	return 1;
}


// This functions prints out the truth table of the circuit (fault-free and for each fault).
// The output is a c-like vector, called mycircuit[#faults+1][#input_patterns][#output_width]
void print_truth_table(char *label)
{
	static int index=0;
	int i,j,qt2;
	char c;

	if (index==0) {
		fprintf (param_truth_fault_table_file,"char mycircuit[%d][%d][%d] = {\n",1+faultlist_number,input_patterns_number,top_level->output_number);
	}
	fprintf (param_truth_fault_table_file,"\t{ /* [%d] %s */\n", index, label);
	for (i=0; i<input_patterns_number; i++) {
		fprintf (param_truth_fault_table_file,"\t\t{");
		if (input_patterns[i][0]==APPLY_PRIMARY_INPUT) {
			qt2=top_level->output_number;
		} else {
			qt2=top_level->FF_scan_number;
		}
		for (j=0; j<qt2; j++) {
			switch (output_patterns[i][j]) {
				case 0: c='0'; break;
				case 1: c='1'; break;
				case 2: c='X'; break;
				case 3: c='Z'; break;
			}
			fprintf (param_truth_fault_table_file, "'%c'",c);
			if (j<qt2-1) {
				fprintf (param_truth_fault_table_file, ",");
			}
		}
		fprintf (param_truth_fault_table_file, "}");
		if (i<input_patterns_number-1) {
			fprintf (param_truth_fault_table_file, ",");
		}
		fprintf (param_truth_fault_table_file, "\n");
	}
	fprintf (param_truth_fault_table_file, "\t}");
	if (index<faultlist_number) {
		fprintf (param_truth_fault_table_file, ",\n");
	} else {
		fprintf (param_truth_fault_table_file, "\n};\n");
	}
	index++;
}

FILE* open_statistic_file()
{
	FILE *fout;
	char s[1024];

	fout=NULL;
	if (param_write_fault_statistic==1) {
		strcpy (s, param_fault_statistic_filename);
		strcat (s, ".report.csv");
		fout=fopen (s, "a");
		if (fout==NULL) {
			printf ("\nERROR: cannot open fault report files for writing. Fault report aborted.\n");
			param_write_fault_statistic=0;
		}
	}
	return fout;
}

void generate_faultCombination_name (int flIndex, char *s) 
{
	int i, index_of_the_fault;

	strcpy (s, "");
	for (i=0; i<faultCombinations[flIndex].size(); i++) {
		index_of_the_fault = faultCombinations[flIndex].at(i);
		strcat (s, stuckat_faultlist[index_of_the_fault]->name);
		if (i<faultCombinations[flIndex].size() - 1) {
			strcat (s, " + ");
		}
	}
}

void generate_report_header (int flIndex, char *s) 
{
	int i, index_of_the_fault;

	strcpy (s, "");
	for (i=0; i<faultCombinations[flIndex].size(); i++) {
		index_of_the_fault = faultCombinations[flIndex].at(i);
		strcat (s, stuckat_faultlist[index_of_the_fault]->name_forReport_File);
		strcat (s, ";");
	}
	for (; i<max_faultCombinations; i++) {
		strcat (s, ";;;");
	}
}

void generate_html_filename (int flIndex, char* filename)
{
	int i, index_of_the_fault;

	strcpy (filename, "");
	for (i=0; i<faultCombinations[flIndex].size(); i++) {
		index_of_the_fault = faultCombinations[flIndex].at(i);
		strcat (filename, stuckat_faultlist[index_of_the_fault]->name_forHTML_File);
		if (i<faultCombinations[flIndex].size() - 1) {
			strcat (filename, "-");
		}
	}
}

void printFaultsStatistic () {
	FILE *fdet, *fundet;
	char s[1024];
	int flIndex;

	strcpy (s, param_fault_statistic_filename);
	strcat (s, ".detected.txt");
	fdet=fopen (s, "a");

	strcpy (s, param_fault_statistic_filename);
	strcat (s, ".undetected.txt");
	fundet=fopen (s, "a");

	if (fdet==NULL || fundet==NULL) {
		printf ("\nERROR: cannot open statistic files for writing. Statistic aborted.\n");
		return;
	}

	for (flIndex=0; flIndex<faultCombinations_number; flIndex++) {
		generate_faultCombination_name (flIndex, s);
		if (faultlist_coverage[flIndex]==0) {
			fprintf (fundet,"%s\n", s);
		} else {
			fprintf (fdet,"%s\n", s);
		}
	}
	fclose (fdet);
	fclose (fundet);
}




int findTokenInString (char* token, string searchin) 
{
	char s[100], *p;
	int r=0;

	strcpy (s, searchin.c_str());
	p=strtok (s,",");
	while (p!=NULL)
	{
		if (strcmp(p, token)==0) return r;
		r++;
		p=strtok(NULL,",");
	}
	return -1;
}


void dec2bit (uint32_t value, int size, int* res)
{
	int i;
	size--;
	for(i=size; i >= 0; i--)        {
		if( (1 << i) & value)
			res[size - i] = 1;
		else
			res[size - i] = 0;                   
	}
}
 
uint32_t LFSR_32()
{
	static uint32_t lfsr = 1;
	/* taps: 32 31 29 1; feedback polynomial: x^32 + x^31 + x^29 + x + 1 */
	lfsr = (lfsr >> 1) ^ (-(lfsr & 1u) & 0xD0000001u); 
	return lfsr;
}
 
void simulation(int printFlag, int executeInit)
{
#ifdef __TIMING_SIMULATION__
	timing_simulation(printFlag, executeInit);
#else
	logic_simulation(printFlag, executeInit);
#endif
}

void compare_fs_results (int flIndex, FILE *f)
{
	// Negative values of "port" means that it is an output
	// Dir= 1 for Input, 0 for Output
	
	int i,j,qt, detected, partially_detected, start_inputs;
	char faultName[1024];
	FILE *fs_file;
	char c;
	char *signature; // AB
	int *codeDetections;
	int ed_noEffect, ed_Anomalie, ed_notDetected, ed_code_detected;
	char s[1024];
	char fullname[100], filename[100];


	detected=0;

	// File .report.txt
	if (param_write_fault_statistic==1) {
		generate_report_header (flIndex, s);
		fprintf (f, s);
	}
	
	// HTML files
	if (param_print_fs_result==1) {
		generate_html_filename (flIndex, filename);
		sprintf (fullname, "%s/%s", param_print_fs_result_dir, filename);
		fs_file=fopen(fullname, "w");
		if (fs_file==NULL) {
			printf ("Error while opening fault simulation output file (%s)...Aborting\n", fullname);
			exit (-1);
		}
		fprintf (fs_file, "<html><body><code>\n");
	}

	// AB
	// cout << " AB : " << top_level->output_number << endl;
	// signature w.r.t the POs and current test vector
	signature = new char [top_level->output_number]; //  number of PO
	if (signature == NULL) {
		cerr << "error when allocating signature !" << endl;
		exit (-1);
	}
	// _AB
	
	// GDN
	if (param_use_errorDetection>0) {
		codeDetections = new int [param_use_errorDetection]; //  number of error detection bits
		if (codeDetections == NULL) {
			cerr << "error when allocating codeDetections !" << endl;
			exit (-1);
		}
		for (i=0; i<param_use_errorDetection; i++) {
			codeDetections[i]=0;
		}
		ed_noEffect=0;
		ed_Anomalie=0;
		ed_notDetected=0;
	}
	// _GDN

	if (param_saf_observe_last_input_only==0) {
		start_inputs=0;
	} else {
		start_inputs = input_patterns_number-1;
	}
	for (i=start_inputs; i<input_patterns_number; i++) {
		if (input_patterns[i][0]==APPLY_PRIMARY_INPUT) {
			qt=top_level->output_number;
		} else {
			qt=top_level->FF_scan_number;
		}
		partially_detected=0;
		ed_code_detected=0;
		//cout << "test vet = " << i << endl; 
		if (param_use_errorDetection>0) {
			for (j=0; j<qt-param_use_errorDetection; j++) {
				if (output_patterns[i][j+1]>=0 && golden_output_patterns[i][j]>=0) {
					if (output_patterns[i][j+1]!=golden_output_patterns[i][j]) {
						partially_detected=1;
					}
				}
			}
			for (j=qt-param_use_errorDetection; j<qt; j++) {
				if (output_patterns[i][j+1]>=0 && golden_output_patterns[i][j]>=0) {
					if (output_patterns[i][j+1]!=golden_output_patterns[i][j]) {
						ed_code_detected++;
					}
				}
			}
			if (partially_detected==0 && ed_code_detected==0) ed_noEffect++;
			if (partially_detected==0 && ed_code_detected>0) ed_Anomalie++;
			if (partially_detected==1 && ed_code_detected==0) ed_notDetected++;
			if (partially_detected==1 && ed_code_detected>0) codeDetections[ed_code_detected-1]++;
		} else {
			for (j=0; j<qt; j++) {
				//cout << "PO [" << j << "] = " << output_patterns[i][j] << endl; 
				//cout << "golden PO [" << j << "] = " << golden_output_patterns[i][j] << endl; 
				if (output_patterns[i][j+1]>=0 && golden_output_patterns[i][j]>=0) {
					if (output_patterns[i][j+1]!=golden_output_patterns[i][j]) {
						partially_detected=1;
						signature[j] = 1; // AB
	/*
						if (param_print_fs_result==0) {
							break;
						}
	*/
					} else signature[j] = 0; // AB
				}
				if (param_print_fs_result==1) {
					switch (output_patterns[i][j+1]) {
						case 3: c='Z'; break;
						case 2: c='X'; break;
						case 0: c='0'; break;
						case 1: c='1'; break;
					}
					if (partially_detected==1) {
						fprintf (fs_file, "<font color=#880000>%c</font>", c);
					} else {
						fprintf (fs_file, "%c", c);
					}
				}
			}
		}

		if (partially_detected==1) {
			detected=1;
		}
		if (param_print_fs_result==1) {
			fprintf (fs_file, "<br>\n");
		}
		if (param_write_fault_statistic==1 && param_use_errorDetection==0) {

			// AB
			if (!param_passfail_dictionary) { //default 1, the created dictionary is passfail. if 0 then the full dictionary is created
				
				// full dictionary 
				for (j=0; j<qt; j++) {
					fprintf (f, "%d", signature[j]);
		
				}
				fprintf (f, ";");
			} else { // pass/fail dictionary
				if (partially_detected==1) {
					fprintf (f, "1;");
				} else {
					fprintf (f, "0;");
				}
			}
		}
	}
	if (param_print_fs_result==1) {
		fprintf (fs_file, "</code></body></html>\n");
		fclose(fs_file);
	}
	if (param_write_fault_statistic==1) {
		if (param_use_errorDetection>0) {
			fprintf (f, "%d;%d;%d", ed_noEffect, ed_Anomalie, ed_notDetected);
			for (j=0; j<param_use_errorDetection; j++) {
				fprintf (f, ";%d", codeDetections[j]);
			}
		}
		fprintf (f, "\n");
	}
	if (param_write_truth_fault_table==1) {
		generate_faultCombination_name (flIndex, faultName);
		print_truth_table(faultName); // In functions.c
	}
	if (detected==1) {
		detected_faults++;
		faultlist_coverage[flIndex]=1;
	}

}
