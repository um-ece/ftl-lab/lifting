#ifndef __CONSTANTS_H
#define __CONSTANTS_H

#define APPLY_PRIMARY_INPUT	1
#define LOAD_SCAN_CHAIN		2

#define FS_NOFAULTS		0
#define FS_STUCKAT		1
#define FS_BITFLIP		2
#define FS_DOUBLESTUCKAT	3
#define FS_MULTIPLESTUCKAT	4

#define FS_CUMULATIVE	1
#define FS_COMPLETE	2

#define FORCE_INPUT	1
#define FORCE_OUTPUT	2

#define ANALYZE_INPUT	1
#define ANALYZE_OUTPUT	2

#define DIR_INPUT	1
#define DIR_OUTPUT	2

#endif

