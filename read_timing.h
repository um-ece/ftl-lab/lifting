//////////////////////////////////////////////////////////////////////
// read_timing.h: header for the "read_timing" module
//
// Author : Feng LU
//
// Last Update : 29-09-2011
//////////////////////////////////////////////////////////////////////
#ifndef __read_timing
#define __read_timing


void read_timing(char *sdf_filename,int time_type, int factor, generic_cell *cell);


#endif

