//////////////////////////////////////////////////////////////////////
// generic_gate.h: interface for the generic_gate class.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 11-06-2007
//////////////////////////////////////////////////////////////////////
#ifndef __generic_gate
#define __generic_gate

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

#define MAX_INPUTS	10
#define MAX_OUTPUTS	10
#define MAX_PN_Junction	50
#define MAX_SUBCELLS	50
class generic_gate;
class generic_cell;

struct conn_matrix
{
	vector <generic_gate*> p_gg;
        vector<int> port;
};

struct input_conn_matrix
{
	generic_gate* p_gg;
	int port;
};

class generic_gate {

public:
	// INFORMATION
	char* name;
	// To define the type of gate. In the library, each element will define a specific type (e.g., 0=AND, 1=OR, ...)
	// type>0 --> user defined
	// type=-1 --> primary input
	// type=-2 --> primary output
	int type;
	string gate_type; 
	generic_cell* parent_cell;
	int propagate_output_during_init;
	string inputs_name, outputs_name;
	
#ifdef __TIMING_SIMULATION__
	int output_delay[MAX_OUTPUTS][MAX_INPUTS][2];
	int clk_width[MAX_INPUTS][2]; //edge_width[clock]
	 int hold_time[MAX_INPUTS][MAX_INPUTS][2][2]; //hold_time[data_input][clock_input][data: 1->posedge;0->negedge][clock: 1->posedge;0->negedge]
	 int setup_time[MAX_INPUTS][MAX_INPUTS][2][2]; //setup_time[data_input][clock_input][data: 1->posedge;0->negedge][clock: 1->posedge;0->negedge
	int FF_data_input_flag[MAX_INPUTS]; //
	// ******for layout analysis******
	int number_pnj;
	int location_pnj[MAX_PN_Junction][2];
	int bottom_left_pnj[MAX_PN_Junction][2];
	struct pnj_info {
		string node_name;
		string cmos_name;
		int type_pn; // 1-> drain, 2-> gate, 3-> source
		string name_subcell;
		string type_subcell;
	} netlilst_pnj[MAX_PN_Junction];
	// ******** end of layout analysis*******
//	int(**gate_input_table)[2];
	int*** gate_input_table;
/*	int** gate_input_value_table; 
	int** gate_input_time_table; */
	int*** gate_output_table;
	int* count_change;
	int last_input_change[MAX_INPUTS];
	int flag_fault_subgate;
	int flag_hspice_sim_list;
#endif

	// OUTPUTS
	int outputs_number;
	int output_value[MAX_OUTPUTS]; // current output value
	int temp_output_value[MAX_OUTPUTS]; // temporary output value, used to calculate the output
	int pre_output_value[MAX_OUTPUTS]; // temporary output value, used to calculate the output
	int wf_output_value[MAX_OUTPUTS];
	string*  output_line_name; // name of the output wire, the order is specified in each gate
	string*  output_sp_name;
	struct conn_matrix ref_outputs[MAX_OUTPUTS]; // the reference of each gate connected to each output
	int fan_out[MAX_OUTPUTS]; // Number of outputs connected to this port
	int stuck_at_output[MAX_OUTPUTS];
	int output_value_without_fault[MAX_OUTPUTS]; 
	float fan_out_capa[MAX_OUTPUTS];

	// INPUTS
	int inputs_number;
	int input_value[MAX_INPUTS];
	int old_input_value[MAX_INPUTS];
	int temp_input_value[MAX_INPUTS];
	float input_capa[MAX_INPUTS];
	int flag_input_return;
	string*  input_line_name; // name of the input wire, the order is specified in each gate
	struct input_conn_matrix ref_inputs[MAX_INPUTS];
	int stuck_at_input[MAX_INPUTS];
	int input_value_without_fault[MAX_INPUTS];
	string*  input_sp_name;
	// FFs have not to propagate output when there is a clock cycle.
	// This variable allows to block the output propagation
	int propagate_output_flag;
	int output_is_changed[MAX_OUTPUTS];

	//LAYOUT
	char *direction;
	int orientation;
					   /* orientation = 0 -> N
										1 -> FN
										2 -> S
										3 -> FS
										4 -> W
										5 -> FW
										6 -> E
										7 -> FE
						*/
	float gate_size[2]; // gate_size[0] : x ; gate_size[1] : y
	long int location[2];
	
	
	// METHODS
	generic_gate (const char *pname) ;
	void InitAfterConstr () ;
	virtual ~generic_gate () {};
	void init() ;
	virtual void set_input (int port, int value) ;
	virtual void set_input (int port, int value, int dont_touch_old_value) ;
	int is_clock (int port) ;
	void calculate_propagate_output (int output_port) ;
	int read_output (int o) ;
	int read_input (int port) ;
	void add_output (int this_output_port, generic_gate* g, int g_input_port) ;
	void propagate_output (int o) ;
	virtual void print_gates (ostream &os) ;
	virtual void calculate_output(int o)=0;
	int getInputPort(char* s);
	int getOutputPort(char* s);
	char *getInputName (int num);

	// Stuck-at
	void force_input (int port, int value) ;
	void release_input (int port) ;
	void force_output (int port, int value) ;
	void release_output (int port) ;

#ifdef __PROBA_CALCULATION__
	long double output_probabilities[MAX_OUTPUTS][4];
	void calculate_output_probabilities();

	generic_gate** input_support_set;
	bool input_support_set_done;
	void input_support_set_calculation(void);
	void add_to_input_support_set(generic_gate *g);

	vector<struct input_conn_matrix> actual_support_set;
	vector<struct input_conn_matrix> temp_support_set;
	vector<struct input_conn_matrix> toExpand_support_set;

	vector<int> toEraseTemp;
	vector<int> toEraseActual;

	bool actual_support_set_done;
	void actual_support_set_calculation(void);
	bool noReconvergency;
	void set_propagate_output (int port, int value) ;
	void generate_combinations_4_output_probabilities(int*);
	bool problem_gate_gate (generic_gate* g1, generic_gate* g2);
	bool problem_gate_pi (generic_gate* g, generic_gate* p);
	void add_to_support_set(struct input_conn_matrix icm, vector<struct input_conn_matrix> &support_set);
	void expand_support_set (int conflit_gate_1, int conflit_gate_2, int phase);
#endif
#ifdef __DEBUG_PROBA_1__
	int actual_support_set_depth;
#endif

#ifdef __SLACK_CALCULATION__
	int asap[MAX_OUTPUTS];
	bool asap_done[MAX_OUTPUTS];
	int alap[MAX_INPUTS];
	bool alap_done[MAX_INPUTS];
	int slack[MAX_OUTPUTS][MAX_OUTPUTS];
	bool slack_done[MAX_OUTPUTS][MAX_OUTPUTS];
	void calculate_asap(int port);
	void calculate_alap(int port);
	void calculate_slack_time(int port);
#endif
#ifdef __LOGIC_LEVELS__
	int logic_level, logic_level_input_defined;
	// It calculates the level of this gate, it propagates to next gates, and it returns the maximum found level
	int calculate_logic_level(int previous_level);
	void propagate_current_level (int *max);
	int isFF;
#endif
#ifdef __TIMING_SIMULATION__
	void print_laser_fault_list(FILE* f);
	void print_gate_input(FILE* f);
	void print_gate_input(int port,FILE* f);
	void InitAfterInput ();
	void print_netlist_sp(FILE* f);
	void print_inputs_sp(FILE* f);
	void print_output_sp(FILE* f);
	void print_gate_input_debug();
#endif
};


#endif

