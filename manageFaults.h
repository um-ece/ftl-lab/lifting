#ifndef __MANAGEFAULTS_H
#define __MANAGEFAULTS_H

#include <stdio.h>
#include <stdlib.h>
#include "generic_gate.h"
#include "generic_ff.h"
#include "generic_cell.h"
#include "constants.h"

#include "input_pattern.h"
#include "functions.h"

#include "techlib/common/pi.h"
#include "techlib/common/po.h"

#include "verilog-parser/verilog_driver.h"

#include "globalVariables.h"
#include "manageFaults.h"

#include <iostream>
#include <vector>
#include <map>
#include <string>
using namespace std;

int in_faultlist (generic_gate* g, int port, int direction, int stuck);
void add_stuckat (generic_gate* g, int *flIndex, int runInput, int runOutput);
void create_faultlist(char* filename);
string generateName_forFaultlist (generic_gate* g, int port, int direction, int stuck);
int removeFrom_faultlist (generic_gate* g, int port, int direction, int stuck);
int addTo_faultlist (generic_gate* g, int port, int direction, int stuck);
void print_fault_list ();

#endif
