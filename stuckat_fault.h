//////////////////////////////////////////////////////////////////////
// stuckat_fault.h: interface for the stuckat_fault class.
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 03-11-2009
//////////////////////////////////////////////////////////////////////
#ifndef __stuckat_fault
#define __stuckat_fault

#include <iostream>
#include <vector>
#include "constants.h"
#include "generic_gate.h"
#include "generic_ff.h"
#include "generic_fault.h"
#include "techlib/common/pi.h"
#include "techlib/common/po.h"
#include <stdio.h>

using namespace std;

class stuckat_fault : public generic_fault {

public:
	int stuckat;
	stuckat_fault (generic_gate *g, int port, int direction, int stuckat) : generic_fault (g, port, direction) {
		int i;

		this->stuckat = stuckat;
		strcpy (this->faultType, "STUCKAT");
		sprintf (this->name, "SA%d;%s;%s", stuckat, g->name, this->portName);
		sprintf (this->name_forReport_File, "SA%d;%s;%s", stuckat, g->name, this->portName);

		sprintf (this->name_forHTML_File, "SA%d_%s_%s", stuckat, g->name, this->portName);
		for (i=0; i<strlen(this->name_forHTML_File); i++) {
			if (this->name_forHTML_File[i]=='\\' || this->name_forHTML_File[i]=='/') {
				this->name_forHTML_File[i]='_';
			}
		}

	}

	// *******
	//
	// methods
	
	void injectFault() {
		if (this->direction==DIR_INPUT) {
			this->g->force_input (port, stuckat);
		} else {
			this->g->force_output (port, stuckat);
		}
	}
	void releaseFault() {
		if (this->direction==DIR_INPUT) {
			this->g->release_input (port);
		} else {
			this->g->release_output (port);
		}
	}
};


#endif

