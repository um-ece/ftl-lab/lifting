// verilog_parser.yy: verilog parser 
//
//
//
// Author : Alberto BOSIO	
//
// Last Update : 19-11-2007 - Paolo BERNARDI 
//
//   sintassi mancante : assign , vedi esempio ST
//		 	  reg	,   idem come sopra
//////////////////////////////////////////////////////////////////////


     %skeleton "lalr1.cc"                          /*  -*- C++ -*- */
     %require "2.1a"  
     %defines
     %define parser_class_name "verilog_parser"
     %code requires {
     #include <string>	 
     #include "./generic_cell.h"

     #include "./techlib/common/library.h"
     #include "./techlib/common/pi.h"
     #include "./techlib/common/po.h"


     #include "./verilog-parser/tokenizer.h"
     #include "./verilog-parser/verilog_driver.h"


     #include <cstring>
     #include <cstdio>
     //#define __PBDEBUG__
     //#define __DEBUG__

    	
     class verilog_driver;

          

     }
    // The parsing context.
     %parse-param { verilog_driver& driver }
     %lex-param   { verilog_driver& driver }
   
     %locations
     %initial-action

     {
       // Initialize the initial location.  
       @$.begin.filename = @$.end.filename = &driver.file;
     };


     %debug
     %error-verbose

   // Symbols.
     %union
     {
       int          ival;
       std::string *sval;
     };


     %code {
     # include "./verilog-parser/verilog_driver.h"
     }

     %token <sval> BIT   "bit"   /* constant 0 or 1 */
     %token        INOUT   "inputoutp"
     %token        REG   "register"
     %token        CELLDEF   "celldefine"
     %token        END_CELLDEF    "endcelldefine"
     %token        MODULE    "module"
     %token        END_MODULE    "end module"
     %token        INPUT    "input"
     %token        WIRE    "wire"
     %token        OUTPUT    "output"
     %token        END      0 "end of file"
     %token <sval> IDENTIFIER "identifier" // can be signal name,  gate name or module name
     %token <ival> NUMBER     "number"     // used for the delay

     %printer    { debug_stream () << *$$; } "identifier"
     %destructor { delete $$; } "identifier"
     
     %printer    { debug_stream () << $$; } "number" 

     %%


     %start sentences;

     
     sentences: sentences statement  
		|  ; 
          
     statement: modules | cells ;
     

     cells: CELLDEF  modules  END_CELLDEF 
		{
			
		
			// driver.add_lib_cell (); 
#ifdef __DEBUG__
				cout << " cell library module, not yet managed " << endl;
#endif

			
		}


     modules: begin_module input_lists output_lists inout_lists wire_lists reg_lists gates END_MODULE;

     inout_lists: inout_lists inout_list
	 | ;

     inout_list: INOUT inout_list_id ';'  
		| INOUT '[' NUMBER ':' NUMBER ']' IDENTIFIER ';'
		;

     inout_list_id: inout_list_id inout_id
		| ;

     inout_id: IDENTIFIER
		| ',' IDENTIFIER ;



     reg_lists: reg_lists reg_list
	 | ;

     reg_list: REG reg_list_id ';'  
		| REG '[' NUMBER ':' NUMBER ']' IDENTIFIER ';'
		;

     reg_list_id: reg_list_id reg_id
		| ;

     reg_id: IDENTIFIER
		| ',' IDENTIFIER ;




     wire_lists: wire_lists wire_list
	 | ;

     wire_list: WIRE wire_list_id ';'  
		| WIRE '[' NUMBER ':' NUMBER ']' IDENTIFIER ';'
		;

     wire_list_id: wire_list_id wire_id
		| ;

     wire_id: IDENTIFIER
		| ',' IDENTIFIER ;


     output_lists: output_lists output_list
		| ;

     input_lists: input_lists input_list
		| ;
 
     gates: gates gate
	   | ;

     begin_module: MODULE IDENTIFIER '(' parameter_list ')' ';' 
		{ 


			driver.circuit = NULL;
			//cout << "pp" ;
			driver.u_val = 0;
			driver.i_val = 0;	
			driver.module_name = *$2;  // used only for check
			driver.module_number++;	

			driver.circuit = new generic_cell ( (*$2).c_str() ,0);  // cretes a new circuite
		
			//driver.incomp_hash.reserve(270);	

			if (driver.circuit == NULL)  {


				cerr << "Memory Error,  current_module ( " << *$2 << " ) cannot be created ... " << endl;
				exit (-1);
		
			}


			// PB - using one & zero classes


		_unconnect unc_gate;


		one* my_one;

		my_one = new one (); // create new POWER SUPPLY connection

		if (my_one == NULL)  {


			cerr << "Memory Error,  current POWER_SUPPLY cannot be created ... " << endl;
			exit (-1);

		}

		unc_gate.line = "VCC";
		unc_gate.pos = 0;
		unc_gate.ref = my_one;				

		driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
		//PB
		//cout << "pp" ;
		
		driver.u_val++;
		driver.unconn_index[unc_gate.line] = driver.u_val ; 			

		//_PB


		// driver.circuit->add_generic_gate (my_one); // insert the new VCC in the circuit

		driver.circuit->add_const (my_one); // insert the new VCC in the circuit as a const




		
		zero* my_zero;

		my_zero = new zero (); // create new GND connection

		if (my_zero == NULL)  {


			cerr << "Memory Error,  current GROUND cannot be created ... " << endl;
			exit (-1);

		}

		unc_gate.line = "GND";
		unc_gate.pos = 0;
		unc_gate.ref = my_zero;				

		driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
		//PB
		//cout << "pp" ;
		driver.u_val++;
		driver.unconn_index[unc_gate.line] = driver.u_val ; 			
		//_PB



		// driver.circuit->add_generic_gate (my_zero); // insert the new VCC in the circuit
		driver.circuit->add_const (my_zero); // insert the new VCC in the circuit as a const

			
	 	// _PB - end using one & zero classe

/* */

			// driver.add_modules (); 
			
	
			

		} 

	
     parameter_list: parameter_list param {}
		   | /* Nothing.  */        {};

     param: IDENTIFIER ',' 
	  | IDENTIFIER; 

     input_list: INPUT pi_list ';' {}
		| INPUT '[' NUMBER ':' NUMBER ']' IDENTIFIER ';'
		{

			
			pi* my_pi;


			_unconnect unc_gate;
			char  current_name[255];
			string name;
			int i;


			if ($3 > $5) {
				for (i = $3 ; i >= $5 ; i-- )	{ 
					
					sprintf (current_name,"%s[%d]",$7->c_str(),  i) ;

#ifdef __DEBUG__
						cout << "new input : " << current_name << endl;
#endif
					
					name = current_name;

					my_pi = new pi (name.c_str()); // create new primary input

					if (my_pi == NULL)  {


						cerr << "Memory Error,  current Primary Input  ( " << name << " ) cannot be created ... " << endl;
						exit (-1);
		
					}

					unc_gate.line = name;
					unc_gate.pos = 0;
					unc_gate.ref = my_pi;				

					driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
					//PB
					driver.u_val++;
					//cout << "pp" ;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 

					//_PB
					driver.circuit->add_input (my_pi); // set the new PI as PI of the previous in the circuit

					

				
				}
			}
			else {

				for (i = $3 ; i <= $5 ; i++ )	{

					sprintf (current_name,"%s[%d]",$7->c_str(),  i) ;

#ifdef __DEBUG__
						cout << "new input : " << current_name << endl;
#endif

					name = current_name;

					my_pi = new pi (name.c_str()); // create new primary input

					if (my_pi == NULL)  {


						cerr << "Memory Error,  current Primary Input  ( " << name << " ) cannot be created ... " << endl;
						exit (-1);
		
					}


					unc_gate.line = name;
					unc_gate.pos = 0;
					unc_gate.ref = my_pi;				
					
					driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array									
					//PB
					//cout << "pp" ;	
					driver.u_val++;	
					driver.unconn_index.insert( make_pair( unc_gate.line , driver.u_val  ) ); 					

					//_PB
					driver.circuit->add_input (my_pi); // set the new PI as PI of the previous in the circuit
				
				}
			}
			
		}
		;
     
     pi_list: pi_list pi 
	    | ;

     pi:  IDENTIFIER 
	   {
		

		_unconnect unc_gate;

		pi* my_pi;

		my_pi = new pi ((*$1).c_str()); // create new primary input

		if (my_pi == NULL)  {


			cerr << "Memory Error,  current Primary Input  ( " << *$1 << " ) cannot be created ... " << endl;
			exit (-1);

		}

		unc_gate.line = *$1;
		unc_gate.pos = 0;
		unc_gate.ref = my_pi;				

		driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
					//PB
					//cout << "pp" ;
					driver.u_val++;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 			

					//_PB
		driver.circuit->add_input (my_pi); // set the new PI as PI of the previous in the circuit
		


	   }
	  | ',' IDENTIFIER 
	   {   

		_unconnect unc_gate;
		pi* my_pi;

		my_pi = new pi ( (*$2).c_str()); // create new primary input

		if (my_pi == NULL)  {


			cerr << "Memory Error,  current Primary Input  ( " << *$2 << " ) cannot be created ... " << endl;
			exit (-1);

		}

		unc_gate.line = *$2;
		unc_gate.pos = 0;
		unc_gate.ref = my_pi;				

		driver.unconnected_gate.push_back (unc_gate); // push it into unconnected gate array
					//PB
					//cout << "pp" ;
					driver.u_val++;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 			
					//_PB
		driver.circuit->add_input (my_pi); // set the new PI as PI of the previous in the circuit






	   };

     output_list: OUTPUT po_list ';' 
		| OUTPUT '[' NUMBER ':' NUMBER ']' IDENTIFIER ';'

		{
			

			po* my_po;
			char  current_name[255];
			string name ;
			int i;
			
			// split the po array 

			if ($3 > $5) {
				for (i = $3 ; i >= $5 ; i-- )	{
					
					sprintf (current_name,"%s[%d]",$7->c_str(),  i) ;

#ifdef __DEBUG__
						cout << "new input : " << current_name << endl;
#endif

					name = current_name;


					my_po = new po (name.c_str()); // create new primary input

					if (my_po == NULL)  {


						cerr << "Memory Error,  current Primary Output  ( " << name << " ) cannot be created ... " << endl;
						exit (-1);
		
					}


					// driver.unconnected_gate.push_back (my_pi); // push it into unconnected gate array      // DA VERIFICARE SE DEVO METTERLI O MENO IN QUESTO VETTORE
					driver.circuit->add_output (my_po); // set the new PO as PO of the previous in the circuit



				}
			}
			else {

				for (i = $3 ; i <= $5 ; i++ )	{

					sprintf (current_name,"%s[%d]",$7->c_str(),  i) ;

#ifdef __DEBUG__
						cout << "new input : " << current_name << endl;
#endif


					name = current_name;

					my_po = new po (name.c_str()); // create new primary input

					if (my_po == NULL)  {


						cerr << "Memory Error,  current Primary Output  ( " << name << " ) cannot be created ... " << endl;
						exit (-1);
		
					}


					// driver.unconnected_gate.push_back (my_pi); // push it into unconnected gate array      // DA VERIFICARE SE DEVO METTERLI O MENO IN QUESTO VETTORE
					driver.circuit->add_output (my_po); // set the new PO as PO of the previous in the circuit
				

				}
			}
			


		}


		; 
     po_list: po_list po
	   | ;

     po: IDENTIFIER  {


			po* my_po;

			my_po = new po ( (*$1).c_str()); // create new primary output

			if (my_po == NULL)  {


				cerr << "Memory Error,  current Primary Output  ( " << *$1 << " ) cannot be created ... " << endl;
				exit (-1);

			}


			// driver.unconnected_gate.push_back (my_pi); // push it into unconnected gate array      // DA VERIFICARE SE DEVO METTERLI O MENO IN QUESTO VETTORE
			driver.circuit->add_output (my_po); // set the new PO as PO of the previous in the circuit
				




			

		     }
	    | ',' IDENTIFIER  
		{

	
			po* my_po;


			my_po = new po ( (*$2).c_str()); // create new primary output

			if (my_po == NULL)  {


				cerr << "Memory Error,  current Primary Output  ( " << *$2 << " ) cannot be created ... " << endl;
				exit (-1);

			}


			// driver.unconnected_gate.push_back (my_pi); // push it into unconnected gate array      // DA VERIFICARE SE DEVO METTERLI O MENO IN QUESTO VETTORE
			driver.circuit->add_output (my_po); // set the new PO as PO of the previous in the circuit
				


		};

	
     gate: IDENTIFIER IDENTIFIER '(' terminal_list ')' ';' 
	   {

		// aggiungere il codice di creazione dei gate vero e proprio 
#ifdef __DEBUG__
			cout << " parsing gate " << endl;
#endif
		

		int my_gate_id, pos, terminal_index, input_pos;
		const char* gate_name= $2->c_str () ;

#ifdef __DEBUG__
			cout << " parsing gate " << gate_name  << endl;
#endif


		_incomplete inc_gate;
		_unconnect unc_gate;

		//library lib;   // instantiate the current library   currentlib/librrary.h

		generic_gate *p_gate = NULL;
		generic_ff* s_gate = NULL;
		//PB
		generic_memory* m_gate = NULL;
		//_PB		
		generic_gate *input_gate;

		generic_cell *lib_cell, *mod_cell;


		my_gate_id = driver.lib->getGateType (*$1);

		tokenizer t_input(',');
		tokenizer t_output(',');

#ifdef __DEBUG__
			cout << " my_gate_id   " <<  my_gate_id   << endl;
#endif
	
		

		switch (my_gate_id) {
			case 1: // Gate combinatiorio

				p_gate=driver.lib->createCombGate((*$1), gate_name);   // instatiate the current gate 
				
				
				driver.circuit->add_generic_gate (p_gate);

				break;

			case 2:  { // FF
					s_gate=driver.lib->createSeqGate((*$1), gate_name);  // 
					
					
					p_gate = s_gate;
					
					driver.circuit->add_FF_noscan (s_gate);


					break;
				}
			case 3: { // FF scan


					s_gate=driver.lib->createSeqGate((*$1), gate_name);  // 

					p_gate = s_gate;
					
					driver.circuit->add_FF_scan (s_gate);


					break;
				}
			//PB
			case 4: { // memory
					m_gate=driver.lib->createMemory((*$1), gate_name);  // 
					
					
					p_gate = m_gate;
					
					driver.circuit->add_memory (m_gate);

					break;
				}	
			//_PB
			default: {
				cerr << " Unkonwn gate ( " << *$1 << " ) " << endl;
				exit (-1);
				break; 

			}		
				
		}


#ifdef __DEBUG__
			cout << "gate name : " << *$1 << " gate number : " << driver.lib->getGateType (*$1) << " number of terminal : " << driver.terminal.size () << endl;
#endif



	//if (my_gate_id == 1)	{  // comb
		
#ifdef __DEBUG__
			cout << " processing current gate :  " << p_gate->name << endl;
#endif
		// for each input 
		
		if (!driver.terminal_name.empty ()) {   // case of notation .I()

			
			t_input.tokeinze (p_gate->inputs_name);
			t_output.tokeinze (p_gate->outputs_name);

			string t_name;
			for (terminal_index = 0; terminal_index < driver.terminal_name.size (); terminal_index++) {
			
				// t_name = driver.terminal_name[terminal_index].substr (1, driver.terminal_name[terminal_index].length () );  // remove '.'
				t_name = driver.terminal_name[terminal_index];
					
#ifdef __DEBUG__
				cout << " term = " << driver.terminal_name[terminal_index] << endl;
				cout << " signal name = " << driver.terminal[terminal_index] << endl;
				cout << " t_name  = " << t_name << endl;
#endif
				pos = t_input.result[t_name];
				if (pos >  0)  { // input terminal
					

					//PB
					/* if(strcmp(driver.terminal[terminal_index].c_str(), "VCC" ) == 0){
						//p_gate->input_value[pos-1] = 1;
						for (int p = 0 ; p < p_gate->outputs_number ; p++ )
							p_gate->output_value[p] = 0;	
						p_gate->ref_inputs[pos-1].p_gg = NULL;
						p_gate->ref_inputs[pos-1].port = pos-1;
						//RISKIO
						p_gate->set_input(pos-1, 1 );
						//p_gate->propagate_output_during_init = 1;
						
					}
					else
						if(strcmp(driver.terminal[terminal_index].c_str(), "GND" ) == 0 ){
							//p_gate->input_value[pos-1] = 0;
							for (int p = 0 ; p < p_gate->outputs_number ; p++ )
								p_gate->output_value[p] = 0;	
							p_gate->ref_inputs[pos-1].p_gg = NULL;
							p_gate->ref_inputs[pos-1].port = pos-1;
							//RISKIO					
							p_gate->set_input(pos-1, 0 );		
							//p_gate->propagate_output_during_init = 1;
						} */
					//_PB	
					
					input_gate = driver.get_unconnected_gate (driver.terminal[terminal_index].c_str(), &input_pos); 										
					char tmp;

					
					
					if (input_gate != NULL)	 {

#ifdef __DEBUG__
						cout << " input  gate :  " << input_gate->name << " ( " << input_pos << " )  si connette a  " << p_gate->name  << " in pos  " <<  pos-1 << endl;
#endif

						input_gate->add_output (input_pos, p_gate, pos-1);

#ifdef __DEBUG__
						cout << " qui arriva " << input_gate->name << endl;						
#endif
						
	
					}
					else { // one input has yet defined

						
#ifdef __DEBUG__
						cerr << "Not ordered netlist" << endl ;
#endif

						inc_gate.line = driver.terminal[terminal_index]; // put not complete gate in the list 
						inc_gate.ref = p_gate;	
						inc_gate.pos = pos-1;
						driver.incomplete_gate.push_back (inc_gate);
					//PB	
					
					//cout << "terra " << inc_gate.line << " luna " << driver.i_val << " sole " << (inc_gate.line[0] + inc_gate.line[1] + inc_gate.line[inc_gate.line.size()-1] + inc_gate.line[inc_gate.line.size()-2] ) % 263 << endl;
					// driver.incomp_index[inc_gate.line] = driver.i_val ; 			
					_hash_mnemonic_key app = { inc_gate.line, driver.i_val };
					// cout << " # " << app.line << " @ " << app.v_pos << endl;
					driver.incomp_hash[ (inc_gate.line[0] + inc_gate.line[1] + inc_gate.line[inc_gate.line.size()-1] + inc_gate.line[inc_gate.line.size()-2] ) % 263].mylist.push_back(app);
					driver.i_val++;	
					//_PB
					}



				}
				else if ( (pos = t_output.result[t_name]) > 0 )  { // output terminal

#ifdef __DEBUG__
					cout << " output pos = " << pos << endl;
#endif
					
					if (p_gate->output_line_name == NULL) {
						cerr << " Parser, error when processing  " << p_gate->name << " . output_line_name has no element " << endl;
						exit (-1);
					}
					p_gate->output_line_name[pos-1] = driver.terminal[terminal_index]; // copy the output line name
					// OKKIO	
											
#ifdef __DEBUG__
					cout << " output pos = " << pos << endl;
#endif

					unc_gate.line = driver.terminal[terminal_index]; // put not complete gate in the list 
					unc_gate.ref = p_gate;	
					unc_gate.pos = pos-1;
					driver.unconnected_gate.push_back (unc_gate);
					//PB
					//cout << "pp" ;
					driver.u_val++;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 			

					//_PB

					driver.check_po (p_gate, pos-1); 
					
#ifdef __DEBUG__

					cout << " check inc gate ( " << p_gate->name << " ) terminal " << 	driver.terminal[terminal_index].c_str() << endl;
#endif					
					
					driver.check_incomplete_gate (driver.terminal[terminal_index].c_str(), p_gate, pos-1); // checl if there some incomplete nodes

				}
				else {
					// error or the case of constatn 1 or 0 - the last case is not trated yet


					cerr << "Unknown terminal ( " << t_name << " ) when processing gate ( " << p_gate->name << " ) " <<  endl;
					exit (-1);

				}
				
			}



			
		}
		else {  // whitout notation .T

	/*		// assumin only ine output line in position 0 of the terminal
		
			p_gate->output_line_name[pos-1] = driver.terminal[0]; // copy the output line name
				
			#ifdef __DEBUG__
				cout << " out line  name : " << p_gate->output_line_name[0] << endl;
				cout << " gate  name : " << p_gate->name << endl;
			#endif

			driver.check_po (p_gate,0);
	
			driver.check_incomplete_gate (driver.terminal[0].c_str(), p_gate,0); // checl if there some incomplete nodes




			for (int i = 1; i < driver.terminal.size (); i++) {
				input_gate = driver.get_unconnected_gate (driver.terminal[i].c_str(),i-1);
				
				if (input_gate != NULL)	 {

					// cout << " input  gate :  " << input_gate->name << endl;
					input_gate->add_output (0,p_gate, i-1);


				}
				else { // one input has yet defined
					// cerr << "Not ordered netlist" << endl ; exit (-1);
					// cerr << "Not ordered netlist" << endl ;

					inc_gate.line = driver.terminal[i]; // put not complete gate in the list 
					inc_gate.ref = p_gate;	
					inc_gate.pos = i-1;
					driver.incomplete_gate.push_back (inc_gate);
					//PB
					//cout << "terra " <<  inc_gate.line << " luna " << driver.i_val++ << endl;
					//driver.incomp_index[inc_gate.line] = driver.i_val ; 			
					_hash_menmonic_key app = { inc_gate.line, driver.i_val };
					driver.incomp_hash[ (inc_gate.line[0] + inc_gate.line[1] + inc_gate.line[inc_gate.line.size()-1] + inc_gate.line[inc_gate.line.size()-2]) % 263].push_back(app);
					driver.i_val++;
					//_PB
				}
			}
		
			unc_gate.line = driver.terminal[terminal_index]; // put not complete gate in the list 
			unc_gate.ref = p_gate;	
			unc_gate.pos = pos-1;
			driver.unconnected_gate.push_back (unc_gate);
					//PB
					//cout << "pp" ;
					driver.u_val++;
					driver.unconn_index[unc_gate.line] = driver.u_val ; 			

					//_PB
			driver.circuit->add_generic_gate (p_gate);

			
*/
		}

		
	//}

	  driver.terminal.clear ();	
	  driver.terminal_name.clear ();	

};


     terminal_list: terminal_list terminal
		 | ;


    



     terminal: IDENTIFIER  // output line name . in this case, the first identifier is considered as input 
	   {

		
	/*	driver.terminal.clear ();	
		driver.terminal_name.clear ();	*/
		

		driver.terminal.push_back (*$1);
		
	   }

	   | ',' IDENTIFIER // input lines
           {
			
		
		driver.terminal.push_back (*$2);

	   }   


	  | '.' IDENTIFIER '(' IDENTIFIER ')'   // example .A(signal_name)
	  {

/* -----\/----- EXCLUDED -----\/-----

		driver.terminal.clear ();	
		driver.terminal_name.clear ();	
 -----/\----- EXCLUDED -----/\----- */
		driver.terminal.push_back (*$4);    // signal_name
		driver.terminal_name.push_back (*$2);  // .A
		

	  }

	  | ',' '.' IDENTIFIER '(' IDENTIFIER ')' 
	  {
		
		driver.terminal.push_back (*$5);    // signal_name
		driver.terminal_name.push_back (*$3);  // .A
	  }

 

	  | '.' IDENTIFIER '('  ')'   // exqmple .A()
	  {


/* -----\/----- EXCLUDED -----\/-----
		driver.terminal.clear ();	
		driver.terminal_name.clear ();	
 -----/\----- EXCLUDED -----/\----- */
		driver.terminal.push_back ("  ");    // signal_name
		driver.terminal_name.push_back (*$2);  // .A
		

	  }
	  | ',' '.' IDENTIFIER '('  ')' 

	  {
		
		driver.terminal.push_back (" ");    // signal_name
		driver.terminal_name.push_back (*$3);  // .A
	  }



	  |  '.' IDENTIFIER '(' BIT ')'    
	   
    	  {

		// example  1'b1 1'b0    
		// no control on the parallesim is made		

		driver.terminal.push_back (*$4);    // signal_name
		driver.terminal_name.push_back (*$2); // .A

	  }

	  |  ','  '.' IDENTIFIER '(' BIT ')'
	   
    	  {

		// example  1'b1   or 1'b0    
		// no control on the parallesim is made

		driver.terminal.push_back (*$5);    // signal_name
		driver.terminal_name.push_back (*$3);
	  }
// PB
           | '.' IDENTIFIER '(' '{' bus_list '}' ')'   // example .A({    })
	   {	
		char str[50];


		#ifdef __PBDEBUG__
		cout << "Ecco un bus della memoria " << *$2 << endl ;
		#endif
		for (int j = 0; j < driver.k ; j++){
			sprintf(str,"%s%d", (*$2).c_str(),j);			
	    		driver.terminal_name.push_back (str);  // .A
			#ifdef __PBDEBUG__
			cout << "pushpush " << *$2<<j << endl ;
			#endif
		}
	   }

	   | ',' '.' IDENTIFIER '(' '{' bus_list '}' ')'   // example .A({    })
	   {
		char str[50];
		#ifdef __PBDEBUG__
		cout << "Ecco un bus della memoria" << *$3 << endl  ;
		#endif
		for (int j = 0; j < driver.k ; j++){
			sprintf(str,"%s%d", (*$3).c_str(),j);		
		    	driver.terminal_name.push_back (str);  // .A
			
			#ifdef __PBDEBUG__
			cout << "pushpush " << str << endl ;
			#endif
		}
	   }
// _PB 

	  ;

// PB

     bus_list: bus_list bus
	    | ;

     bus : IDENTIFIER 
	  {

	  driver.k=0;
	  #ifdef __PBDEBUG__
		cout << "{"  << *$1   ;
	  #endif
	  driver.terminal.push_back (*$1);
	  driver.k++;
	  }	

	  | ',' IDENTIFIER
	  {
	  #ifdef __PBDEBUG__
		cout << " , "  << *$2 << endl  ;
	  #endif
	  driver.terminal.push_back (*$2);
	  driver.k++;


	  }
	;

// PB


;


     %%

     void
     yy::verilog_parser::error (const yy::verilog_parser::location_type& l,
                               const std::string& m)
     {
       driver.error (l, m);
       exit (-1);
     }


