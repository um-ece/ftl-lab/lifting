// verilog_drive.cpp: implementation for the verilog parser
//
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 12-06-2007
/////////////////////////////////////////////////////////////////////


     #include "verilog_driver.h"
     #include "../verilog_parser.hpp"
     #include "../techlib/c35/c35_library.h"
     #include "../techlib/st9GPhs/CORE90GPSVT_library.h"
     //#include "../st9GPll/st9GPll_library.h"
     
     verilog_driver::verilog_driver (int library_code)
       : trace_scanning (false), trace_parsing (false)
      // : trace_scanning (true), trace_parsing (true) // debug 
      // Library codes:
      // 	- 1 = AMS C35
      // 	- 2 = ST CORE 9GP HS
      // 	- 3 = ST CORE 9GP LL
     {
	node_number = 0;
	PI_number = 0;
	PO_number = 0;
	circuit = NULL;

	switch (library_code) {
		case 1: lib = new c35_library(); break;
		case 2: lib = new CORE90GPSVT_library(); break;
	}

	module_name = "";
	module_number = 0;

	unconnected_gate.clear ();

	terminal.clear ();	
	terminal_name.clear ();

     }
     
     verilog_driver::~verilog_driver ()
     {
     }
     
     void
     verilog_driver::parse (const std::string &f)
     {
       file = f;
       scan_begin ();
       yy::verilog_parser parser (*this);
       parser.set_debug_level (trace_parsing);
       parser.parse ();
       scan_end ();
     }
     
     void
     verilog_driver::error (const yy::location& l, const std::string& m)
     {
       std::cerr << l << ": " << m << std::endl;
     }
     
     void
     verilog_driver::error (const std::string& m)
     {
       std::cerr << m << std::endl;
     }


      
void verilog_driver::check_po (generic_gate *    p_po, int out_pos) { // set po

	int i;
	// modified 8-9-2007 
	//p_gate->output_line_name[pos-1]

	if (!circuit->list_PO.empty ()) {
		for (i = 0; i  < circuit->list_PO.size (); i++) {
		  if (strcmp (circuit->list_PO[i]->name ,   p_po->output_line_name[out_pos].c_str ()) == 0) {
#ifdef __DEBUG__
					cout << " PO = " << p_po->output_line_name[out_pos] << endl;
#endif
				
					p_po->add_output (out_pos,  circuit->list_PO[i] , 0);
					// circuit->list_PO[i]->add_output  (p_po); old code
			}
		}
	}
}


generic_gate* verilog_driver::get_unconnected_gate (const char *name, int *pos ) {
  
  int i, tmp, j;


  if (!unconnected_gate.empty ()) {    
    i = unconn_index[name];	

    if (i) {
      *pos = unconnected_gate[i-1].pos;
    }
     else
      return NULL;
    return unconnected_gate[i-1].ref;
    }
}

generic_gate*	verilog_driver::check_incomplete_gate (const char *name, generic_gate *p_gate, int out_pos) {

	int i;

	//_incomplete tmp[1400];

	if (!incomplete_gate.empty ()) {    
		int key = (name[0] + name[1] + name[strlen(name)-1] + name[strlen(name)-2]) % 263;


		for ( i = 0 ; i < incomp_hash[key].mylist.size() ; i++) {
			if ( strcmp( incomp_hash[key].mylist[i].line.c_str() , name) == 0 ){
				p_gate->add_output (out_pos, incomplete_gate[incomp_hash[key].mylist[i].v_pos].ref, incomplete_gate[incomp_hash[key].mylist[i].v_pos].pos);
			}
		}  
	}  
	return NULL;  
}


void verilog_driver::print (ostream &os) {
	/*int i;

	os << " module " << 	
	for (i = 0; i < unconnected_gate.size (); i++)
		unconnected_gate[i]->print_gates (os);
*/	
  return ;
}
