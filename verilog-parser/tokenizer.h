// tokenizer.h: string tokenizer
//
//
//
// Author : Alberto BOSIO	
//
// Last Update : 08-09-2007
/////////////////////////////////////////////////////////////////////


#ifndef _TOKEINZER_HH
#define _TOKEINZER_HH

#include <string>
#include <iostream>
#include <map>

using namespace std;


class tokenizer {

public:
	
	char token;    
		
	map<string,int> result;


	// methods

	tokenizer (char tok) { token = tok; result.clear (); };
	~tokenizer () {};

	void reset () {

		result.clear ();
	};
	
	int tokeinze (string my_s) {
	
		int i, occourence, last_i;
		string element;
	
		reset ();	
		occourence = 1;

		last_i = 0;
		for (i = 0; i < my_s.length (); i++) {
			
			if (my_s[i] == token) {

				// cout << " get token lat_i = " << last_i << " i = " << i  << endl;
				element = "";

				element = my_s.substr (last_i, i - last_i); // get the i-th element
				
				/*
				cout << "str = " << my_s << endl;	
				cout << "substr = " << my_s.substr (last_i, i - last_i)  << endl;
				*/
				result[element] = occourence;
				occourence++;
				last_i = i+1;				

			}
		
		}
		
		element = "";
		element = my_s.substr (last_i, i - last_i); // get the i-th element
		
		result[element] = occourence;


		return 1;
	};

	void print () {

		map<string,int>::iterator w;

		for(w = result.begin(); w != result.end(); w++)
			cout << w->first << ": " << w->second << endl;

	};
};

#endif
