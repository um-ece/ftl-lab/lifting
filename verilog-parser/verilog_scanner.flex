%{                                            /* -*- C++ -*- */


/***********************************************
// verilog_scanner.lex: verilog lex scanner 
//
//
//
// Author : Alberto BOSIO	
//
// Last Update : 27-07-2007
//
*********************************************************/	


	/* gestiti i commenti stile C, ma non ancora i commenti stile C++ */



     #include <cstdlib>
     #include <errno.h>
     #include <limits.h>
     #include <string>


     #include "verilog-parser/verilog_driver.h"
     #include "verilog_parser.hpp"
     
     /* Work around an incompatibility in flex (at least versions
        2.5.31 through 2.5.33): it generates code that does
        not conform to C89.  See Debian bug 333231
        <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=333231>.  */

     #undef yywrap
     #define yywrap() 1
     
     /* By default yylex returns int, we use token_type.
        Unfortunately yyterminate by default returns 0, which is
        not of token_type.  */

     #define yyterminate() return token::END

%}


%option noyywrap nounput batch debug


id    [a-zA-Z_\/\\][a-zA-Z_0-9\/\\\[\]]*
int   [0-9]+
blank [ \t]


%{
  #define YY_USER_ACTION  yylloc->columns (yyleng);
%}

%x comment
%%

%{
  yylloc->step ();
%}

	/* {blank}+   yylloc->step (); */

[ \t]+ yylloc->step ();

[\n]+      yylloc->lines (yyleng); yylloc->step ();


%{
    typedef yy::verilog_parser::token token;
%}
                /* Convert ints to the actual type of tokens.  */

"/*"         BEGIN(comment);

<comment>[^*\n]*        /* eat anything that's not a '*' */
<comment>"*"+[^*/\n]*   /* eat up '*'s not followed by '/'s */
<comment>\n             ;
<comment>"*"+"/"        BEGIN(INITIAL);

	/* [\/\/].	 ;		*/

[\/\/].*	;


[,;)(#\[\]:.{}]     return yy::verilog_parser::token_type (yytext[0]);

"1'b0"		yylval->sval = new std::string ("GND"); return token::BIT; /* bit costant  */
"1'b1"		yylval->sval = new std::string ("VCC"); return token::BIT; /* bit costant  */

"inout"		return token::INOUT;
"reg"		return token::REG;
"`celldefine"    return token::CELLDEF;
"`endcelldefine" return token::END_CELLDEF;
"module"    return token::MODULE;
"endmodule" return token::END_MODULE;
"input"    return token::INPUT;
"wire"    return token::WIRE;
"output"    return token::OUTPUT;

[0-9]+  {
       errno = 0;
       long n = strtol (yytext, NULL, 10);
       if (! (INT_MIN <= n && n <= INT_MAX && errno != ERANGE))
         driver.error (*yylloc, "integer is out of range");
       yylval->ival = n;
       return token::NUMBER;
     }


	/* {id}       yylval->sval = new std::string (yytext); return token::IDENTIFIER; */


	/* [a-zA-Z][a-zA-Z_0-9]* yylval->sval = new std::string (yytext); return token::IDENTIFIER;  */


[a-z_!A-Z\\][a-zA-Z_0-9\/\\\[\]]* yylval->sval = new std::string (yytext); return token::IDENTIFIER; 


.          driver.error (*yylloc, "invalid character"); 
%%


void  verilog_driver::scan_begin ()
     {
       yy_flex_debug = trace_scanning;
       printf ("%s ",file.c_str());
       
       if (!(yyin = fopen (file.c_str (), "r")))
         error (std::string ("cannot open ") + file);
     }

void verilog_driver::scan_end ()
     {
       fclose (yyin);
     }


