//////////////////////////////////////////////////////////////////////
// functions.h: header for the "functions" module
//
// Author : Alberto BOSIO, Giorgio DI NATALE
//
// Last Update : 04-11-2009
//////////////////////////////////////////////////////////////////////
#ifndef __functions
#define __functions

#include "generic_gate.h"
#include "globalVariables.h"
#include <stdint.h>
#include   <fstream>
#include <iostream>


void getPortName (generic_gate *g, int port, int direction, char *name);
void print_help(char* toolname);
void parse_arguments (int argc, char* argv[]);
void clearFile (char *r);
int load_memory(char *memory_instance, char *memory_file);
void print_truth_table(char *label);
int read_input_patterns(char* filename);
int read_laser_fault_table();
void print_output();
FILE* open_statistic_file();
void generate_faultCombination_name (int flIndex, char *s);
void generate_report_header (int flIndex, char *s);
void generate_html_filename (int flIndex, char* filename);
void printFaultsStatistic ();
void printTimingQueue ();
int findTokenInString (char* token, string searchin);
void print_tool_head();
void dec2bit (uint32_t value, int size, int* res);
uint32_t LFSR_32();

void simulation(int printFlag, int executeInit);
void compare_fs_results (int flIndex, FILE *f);

#ifdef __TIMING_SIMULATION__
void timing_simulation(int printFlag, int executeInit);
void run_fs_stuckat_timing();
#else
void logic_simulation(int printFlag, int executeInit);
#endif

#endif

